<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * Displays a date and dateTime filtering input.  Allows for picking a type of filter and sometimes depending on
     * the filter, entering a specific date value.
     */
    abstract class MixedDateTypesElement extends Element
    {
        abstract protected function getValueTypeEditableInputId();

        abstract protected function getValueFirstDateEditableInputId();

        abstract protected function getValueSecondDateEditableInputId();

        abstract protected function getValueTypeEditableInputName();

        abstract protected function getValueFirstDateEditableInputName();

        abstract protected function getValueSecondDateEditableInputName();

        abstract protected function getValueFirstDate();

        abstract protected function getValueSecondDate();

        abstract protected function getValueType();

        protected function renderEditable()
        {
            $data = array();
            $data['label']     = $this->renderLabel();
            $data['content']   = $this->renderEditableValueTypeContent() . $this->renderControlEditable();
            $data['error']     = $this->renderError();
            $data['colspan']   = $this->getColumnSpan();
            return $this->resolveContentTemplate($this->editableTemplate, $data);
        }

        /**
         * Render a date JUI widget
         * @return The element's content as a string.
         */
        protected function renderControlEditable()
        {
            $valueTypeId                        = $this->getValueTypeEditableInputId();
            $firstDateSpanAreaSuffix            = '-first-date-area';
            $secondDateSpanAreaSuffix           = '-second-date-area';
            $thirdDateSpanAreaSuffix            = '-third-date-area';
            $firstDateSpanAreaId                = $valueTypeId . $firstDateSpanAreaSuffix;
            $secondDateSpanAreaId               = $valueTypeId . $secondDateSpanAreaSuffix;
            $thirdDateSpanAreaId                = $valueTypeId . $thirdDateSpanAreaSuffix;
            $valueTypesRequiringFirstDateInput  = MixedDateTypesSearchFormAttributeMappingRules::
                                                  getValueTypesRequiringFirstDateInput();
            $valueTypesRequiringSecondDateInput = MixedDateTypesSearchFormAttributeMappingRules::
                                                  getValueTypesRequiringSecondDateInput();
            $valueTypesRequiringThirdDateInput  = $this->getValueTypesRequiringThirdDateInput();
            Yii::app()->clientScript->registerScript('mixedDateTypes', "
                $('.dateValueType').change( function()
                    {
                        arr  = " . CJSON::encode($valueTypesRequiringFirstDateInput) . ";
                        arr2 = " . CJSON::encode($valueTypesRequiringSecondDateInput) . ";
                        arr3 = " . CJSON::encode($valueTypesRequiringThirdDateInput) . ";
                        firstDateSpanAreaQualifier = '#' + $(this).attr('id') + '" . $firstDateSpanAreaSuffix . "';
                        secondDateSpanAreaQualifier = '#' + $(this).attr('id') + '" . $secondDateSpanAreaSuffix . "';
                        thirdDateSpanAreaQualifier = '#' + $(this).attr('id') + '" . $thirdDateSpanAreaSuffix . "';
                        if ($.inArray($(this).val(), arr) != -1)
                        {
                            $(firstDateSpanAreaQualifier).show();
                            $(firstDateSpanAreaQualifier).find('.hasDatepicker').prop('disabled', false);
                        }
                        else
                        {
                            $(firstDateSpanAreaQualifier).hide();
                            $(firstDateSpanAreaQualifier).find('.hasDatepicker').prop('disabled', true);
                        }
                        if ($.inArray($(this).val(), arr2) != -1)
                        {
                            $(secondDateSpanAreaQualifier).show();
                            $(secondDateSpanAreaQualifier).find('.hasDatepicker').prop('disabled', false);
                        }
                        else
                        {
                            $(secondDateSpanAreaQualifier).hide();
                            $(secondDateSpanAreaQualifier).find('.hasDatepicker').prop('disabled', true);
                        }
                        if ($.inArray($(this).val(), arr3) != -1)
                        {
                            $(thirdDateSpanAreaQualifier).show();
                            $(thirdDateSpanAreaQualifier).find('.hasDatepicker').prop('disabled', false);
                        }
                        else
                        {
                            $(thirdDateSpanAreaQualifier).hide();
                            $(thirdDateSpanAreaQualifier).find('.hasDatepicker').prop('disabled', true);
                        }
                    }
                );
            ");
            $startingDivStyleFirstDate   = null;
            $startingDivStyleSecondDate  = null;
            $startingDivStyleThirdDate   = null;
            if (!in_array($this->getValueType(), $valueTypesRequiringFirstDateInput))
            {
                $startingDivStyleFirstDate = "display:none;";
                $firstDateDisabled         = 'disabled';
            }
            else
            {
                $firstDateDisabled         = null;
            }
            if (!in_array($this->getValueType(), $valueTypesRequiringSecondDateInput))
            {
                $startingDivStyleSecondDate = "display:none;";
                $secondDateDisabled         = 'disabled';
            }
            else
            {
                $secondDateDisabled = null;
            }
            if (!in_array($this->getValueType(), $valueTypesRequiringThirdDateInput))
            {
                $startingDivStyleThirdDate = "display:none;";
                $thirdDateDisabled         = 'disabled';
            }
            else
            {
                $thirdDateDisabled = null;
            }
            $content  = EzcodxHtml::tag('span', array('id'    => $firstDateSpanAreaId,
                                                     'class' => 'first-date-area',
                                                     'style' => $startingDivStyleFirstDate),
                                                     $this->renderEditableFirstDateContent($firstDateDisabled));
            $content .= EzcodxHtml::tag('span', array('id'    => $secondDateSpanAreaId,
                                                     'class' => 'second-date-area',
                                                     'style' => $startingDivStyleSecondDate),
                                                     EzcodxHtml::Tag('span', array('class' => 'dynamic-and-for-mixed'), Ezcodx::t('Core', 'and')) .
                                                     $this->renderEditableSecondDateContent($secondDateDisabled));
            $content .= EzcodxHtml::tag('span', array('id'    => $thirdDateSpanAreaId,
                                                     'class' => 'third-date-area',
                                                     'style' => $startingDivStyleThirdDate),
                                                     $this->renderEditableThirdDateContent($thirdDateDisabled));
            return $content;
        }

        protected function renderEditableValueTypeContent()
        {
            return       EzcodxHtml::dropDownList($this->getValueTypeEditableInputName(),
                                                 $this->getValueType(),
                                                 $this->getValueTypeDropDownArray(),
                                                 $this->getEditableValueTypeHtmlOptions());
        }

        protected function renderEditableFirstDateContent($disabled = null)
        {
            assert('$disabled === null || $disabled = "disabled"');
            $cClipWidget = new CClipWidget();
            $cClipWidget->beginClip("EditableDateElement");
            $cClipWidget->widget('application.core.widgets.EzcodxJuiDatePicker', array(
                'attribute'           => $this->attribute,
                'value'               => DateTimeUtil::resolveValueForDateLocaleFormattedDisplay(
                                         $this->getValueFirstDate(),
                                         DateTimeUtil::DISPLAY_FORMAT_FOR_INPUT),
                'htmlOptions'         => array(
                    'id'              => $this->getValueFirstDateEditableInputId(),
                    'name'            => $this->getValueFirstDateEditableInputName(),
                    'disabled'        => $disabled,
            )));
            $cClipWidget->endClip();
            $content =  $cClipWidget->getController()->clips['EditableDateElement'];
            return      EzcodxHtml::tag('div', array('class' => 'has-date-select'), $content);
        }

        protected function renderEditableSecondDateContent($disabled = null)
        {
            assert('$disabled === null || $disabled = "disabled"');
            $cClipWidget = new CClipWidget();
            $cClipWidget->beginClip("EditableDateElement");
            $cClipWidget->widget('application.core.widgets.EzcodxJuiDatePicker', array(
                'attribute'           => $this->attribute,
                'value'               => DateTimeUtil::resolveValueForDateLocaleFormattedDisplay(
                                         $this->getValueSecondDate(),
                                         DateTimeUtil::DISPLAY_FORMAT_FOR_INPUT),
                'htmlOptions'         => array(
                    'id'              => $this->getValueSecondDateEditableInputId(),
                    'name'            => $this->getValueSecondDateEditableInputName(),
                    'disabled'        => $disabled,
            )));
            $cClipWidget->endClip();
            $content = $cClipWidget->getController()->clips['EditableDateElement'];
            return EzcodxHtml::tag('div', array('class' => 'has-date-select'), $content);
        }

        protected function getEditableValueTypeHtmlOptions()
        {
            $htmlOptions = array(
                'id'    => $this->getValueTypeEditableInputId(),
                'class' => 'dateValueType',
            );
            $htmlOptions['empty']    = Ezcodx::t('Core', '(None)');
            $htmlOptions['disabled'] = $this->getDisabledValue();
            return $htmlOptions;
        }

        protected function getValueTypeDropDownArray()
        {
            return MixedDateTimeTypesSearchFormAttributeMappingRules::getValueTypesAndLabels();
        }

        /**
         * Renders the attribute from the model.
         * @return The element's content.
         */
        protected function renderControlNonEditable()
        {
            throw new NotSupportedException();
        }

        protected function renderLabel()
        {
            $label = $this->getFormattedAttributeLabel();
            if ($this->form === null)
            {
                return $label;
            }
            return EzcodxHtml::label($label, false);
        }

        /**
         * Render during the Editable render
         * (non-PHPdoc)
         * @see Element::renderError()
         */
        protected function renderError()
        {
        }

        protected function getValueTypesRequiringThirdDateInput()
        {
            return array();
        }

        protected function renderEditableThirdDateContent($disabled = null)
        {
        }
    }
?>