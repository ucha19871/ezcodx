<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * Rules for working with operators that can be used for triggers in workflows or filters in reporting.
     */
    class OperatorRules
    {
        const TYPE_EQUALS                         = 'equals';

        const TYPE_DOES_NOT_EQUAL                 = 'doesNotEqual';

        const TYPE_STARTS_WITH                    = 'startsWith';

        const TYPE_DOES_NOT_STARTS_WITH           = 'doesNotStartsWith';

        const TYPE_ENDS_WITH                      = 'endsWith';

        const TYPE_DOES_NOT_ENDS_WITH             = 'doesNotEndsWith';

        const TYPE_CONTAINS                       = 'contains';

        const TYPE_DOES_NOT_CONTAINS              = 'doesNotContains';

        const TYPE_GREATER_THAN_OR_EQUAL_TO       = 'greaterThanOrEqualTo';

        const TYPE_LESS_THAN_OR_EQUAL_TO          = 'lessThanOrEqualTo';

        const TYPE_GREATER_THAN                   = 'greaterThan';

        const TYPE_LESS_THAN                      = 'lessThan';

        const TYPE_ONE_OF                         = 'oneOf';

        const TYPE_BETWEEN                        = 'between';

        const TYPE_IS_NULL                        = 'isNull';

        const TYPE_IS_NOT_NULL                    = 'isNotNull';

        const TYPE_BECOMES                        = 'becomes';

        const TYPE_WAS                            = 'was';

        const TYPE_BECOMES_ONE_OF                 = 'becomesOneOf';

        const TYPE_WAS_ONE_OF                     = 'wasOneOf';

        const TYPE_CHANGES                        = 'changes';

        const TYPE_DOES_NOT_CHANGE                = 'doesNotChange';

        const TYPE_IS_EMPTY                       = 'isEmpty';

        const TYPE_IS_NOT_EMPTY                   = 'isNotEmpty';

        public static function getTranslatedTypeLabel($type)
        {
            assert('is_string($type)');
            $labels             = self::translatedTypeLabels();
            if (isset($labels[$type]))
            {
                return $labels[$type];
            }
            throw new NotSupportedException();
        }

        public static function translatedTypeLabels()
        {
            return array(OperatorRules::TYPE_EQUALS                      => Ezcodx::t('Core', 'Equals'),
                         OperatorRules::TYPE_DOES_NOT_EQUAL              => Ezcodx::t('Core', 'Does Not Equal'),
                         OperatorRules::TYPE_STARTS_WITH                 => Ezcodx::t('Core', 'Starts With'),
                         OperatorRules::TYPE_DOES_NOT_STARTS_WITH        => Ezcodx::t('Core', 'Does Not Start With'),
                         OperatorRules::TYPE_ENDS_WITH                   => Ezcodx::t('Core', 'Ends With'),
                         OperatorRules::TYPE_DOES_NOT_ENDS_WITH          => Ezcodx::t('Core', 'Does Not End With'),
                         OperatorRules::TYPE_CONTAINS                    => Ezcodx::t('Core', 'Contains'),
                         OperatorRules::TYPE_DOES_NOT_CONTAINS           => Ezcodx::t('Core', 'Does Not Contain'),
                         OperatorRules::TYPE_GREATER_THAN_OR_EQUAL_TO    => Ezcodx::t('Core', 'Greater Than Or Equal To'),
                         OperatorRules::TYPE_LESS_THAN_OR_EQUAL_TO       => Ezcodx::t('Core', 'Less Than Or Equal To'),
                         OperatorRules::TYPE_GREATER_THAN                => Ezcodx::t('Core', 'Greater Than'),
                         OperatorRules::TYPE_LESS_THAN                   => Ezcodx::t('Core', 'Less Than'),
                         OperatorRules::TYPE_ONE_OF                      => Ezcodx::t('Core', 'One Of'),
                         OperatorRules::TYPE_BETWEEN                     => Ezcodx::t('Core', 'Between'),
                         OperatorRules::TYPE_IS_NULL                     => Ezcodx::t('Core', 'Is Null'), // Not Coding Standard
                         OperatorRules::TYPE_IS_NOT_NULL                 => Ezcodx::t('Core', 'Is Not Null'), // Not Coding Standard
                         OperatorRules::TYPE_BECOMES                     => Ezcodx::t('Core', 'Becomes'),
                         OperatorRules::TYPE_WAS                         => Ezcodx::t('Core', 'Was'),
                         OperatorRules::TYPE_BECOMES_ONE_OF              => Ezcodx::t('Core', 'Becomes One Of'),
                         OperatorRules::TYPE_WAS_ONE_OF                  => Ezcodx::t('Core', 'Was One Of'),
                         OperatorRules::TYPE_CHANGES                     => Ezcodx::t('Core', 'Changes'),
                         OperatorRules::TYPE_DOES_NOT_CHANGE             => Ezcodx::t('Core', 'Does Not Change'),
                         OperatorRules::TYPE_IS_EMPTY                    => Ezcodx::t('Core', 'Is Empty'),
                         OperatorRules::TYPE_IS_NOT_EMPTY                => Ezcodx::t('Core', 'Is Not Empty'),
            );
        }

        public static function availableTypes()
        {
            return array(OperatorRules::TYPE_EQUALS,
                         OperatorRules::TYPE_DOES_NOT_EQUAL,
                         OperatorRules::TYPE_STARTS_WITH,
                         OperatorRules::TYPE_DOES_NOT_STARTS_WITH,
                         OperatorRules::TYPE_ENDS_WITH,
                         OperatorRules::TYPE_DOES_NOT_ENDS_WITH,
                         OperatorRules::TYPE_CONTAINS,
                         OperatorRules::TYPE_DOES_NOT_CONTAINS,
                         OperatorRules::TYPE_GREATER_THAN_OR_EQUAL_TO,
                         OperatorRules::TYPE_LESS_THAN_OR_EQUAL_TO,
                         OperatorRules::TYPE_GREATER_THAN,
                         OperatorRules::TYPE_LESS_THAN,
                         OperatorRules::TYPE_ONE_OF,
                         OperatorRules::TYPE_BETWEEN,
                         OperatorRules::TYPE_IS_NULL,
                         OperatorRules::TYPE_IS_NOT_NULL,
                         OperatorRules::TYPE_BECOMES,
                         OperatorRules::TYPE_WAS,
                         OperatorRules::TYPE_BECOMES_ONE_OF,
                         OperatorRules::TYPE_WAS_ONE_OF,
                         OperatorRules::TYPE_CHANGES,
                         OperatorRules::TYPE_DOES_NOT_CHANGE,
                         OperatorRules::TYPE_IS_EMPTY,
                         OperatorRules::TYPE_IS_NOT_EMPTY,
            );
        }

        public static function getOperatorsWhereValueIsRequired()
        {
            return array(   OperatorRules::TYPE_EQUALS,
                            OperatorRules::TYPE_DOES_NOT_EQUAL,
                            OperatorRules::TYPE_STARTS_WITH,
                            OperatorRules::TYPE_DOES_NOT_STARTS_WITH,
                            OperatorRules::TYPE_ENDS_WITH,
                            OperatorRules::TYPE_DOES_NOT_ENDS_WITH,
                            OperatorRules::TYPE_CONTAINS,
                            OperatorRules::TYPE_DOES_NOT_CONTAINS,
                            OperatorRules::TYPE_GREATER_THAN_OR_EQUAL_TO,
                            OperatorRules::TYPE_LESS_THAN_OR_EQUAL_TO,
                            OperatorRules::TYPE_GREATER_THAN,
                            OperatorRules::TYPE_LESS_THAN,
                            OperatorRules::TYPE_ONE_OF,
                            OperatorRules::TYPE_BETWEEN,
                            OperatorRules::TYPE_BECOMES,
                            OperatorRules::TYPE_WAS,
                            OperatorRules::TYPE_BECOMES_ONE_OF,
                            OperatorRules::TYPE_WAS_ONE_OF,
            );
        }

        public static function getOperatorsWhereSecondValueIsRequired()
        {
            return array(OperatorRules::TYPE_BETWEEN);
        }
    }
?>