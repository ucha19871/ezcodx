<?php

/* * *******************************************************************************
 * Ezcodx is a business management program by VALORAN, Ltd.
 * Copyright (C) 2014 VALORAN LTD.
 *
 * You can contact VALORAN, LTD with a mailing address at 422C Karvasla
 * Dadiani St No 7, Tbilisi, Georgia. or at email address info@valoran.ge
 * ****************************************************************************** */

/**
 *
 * Interacts with the L10N server ( http://translate.ezcodx.org )
 *
 */
class EzcodxTranslationServerUtil {

    /**
     * Domain of the l10n server
     */
    // private static $serverDomain = 'http://translate.ezcodx.com';
    private static $serverDomain = '';

    /**
     * Path to the info XML file
     */
    // private static $infoXmlPath = 'sites/default/files/l10n_packager/l10n_server.xml';
    private static $infoXmlPath = 'translate/l10n_packager/l10n_server.xml';

    /**
     * l10n Server info XML
     */
    private static $l10nInfo;

    /**
     * Holds an array with available languages
     */
    private static $availableLanguages;

    /**
     * Getter for the domain fo the l10n server
     *
     * @return string
     */
    public static function getServerDomain() {
        return self::$serverDomain = Yii::app()->getBaseUrl(true);
    }

    /**
     * @return string
     */
    private static function getReleaseVersion() {
        return join('.', array(MAJOR_VERSION, MINOR_VERSION));
    }

    /**
     * @return string
     */
    private static function getCoreVersion() {
        return '2.6';
    }

    /**
     * Downloads the l10n info XML file
     *
     * @return SimpleXMLElement
     */
    protected static function getServerInfo() {
        if (self::$l10nInfo &&
                isset(self::$l10nInfo) &&
                self::$l10nInfo->version == '1.1') {
            return self::$l10nInfo;
        }

        $cacheIdentifier = 'l10nServerInfo';
        try {
            // not using default value to save cpu cycles on requests that follow the first exception.
            self::$l10nInfo = GeneralCache::getEntry($cacheIdentifier);
        } catch (NotFoundException $e) {
            $infoFileUrl = self::getServerDomain() . '/' . self::$infoXmlPath;
            $xml = simplexml_load_file($infoFileUrl);
            self::$l10nInfo = json_decode(json_encode($xml));
            GeneralCache::cacheEntry($cacheIdentifier, self::$l10nInfo);
        }

        if (isset(self::$l10nInfo->version) &&
                self::$l10nInfo->version == '1.1') {
            return self::$l10nInfo;
        }

        throw new FailedServiceException();
    }

    /**
     * Retrives the list of all languages available on the l10n server
     */
    public static function getAvailableLanguages() {
        if (is_array(self::$availableLanguages) &&
                !empty(self::$availableLanguages)) {
            return self::$availableLanguages;
        }

        try {
            $l10nInfo = self::getServerInfo();
        } catch (FailedServiceException $e) {
            throw new FailedServiceException();
        }

        self::$availableLanguages = array();
        foreach ($l10nInfo->languages->language as $language) {
            self::$availableLanguages[$language->code] = array(
                'code' => $language->code,
                'name' => $language->name,
                'nativeName' => $language->native,
            );
        }

        if (is_array(self::$availableLanguages) &&
                !empty(self::$availableLanguages)) {
            return self::$availableLanguages;
        }

        throw new FailedServiceException();
    }

    protected static function getUrlPattern() {
        $l10nInfo = self::getServerInfo();
        return $l10nInfo->update_url;
    }

    /**
     * Formats the url to the po file for the specified language
     */
    public static function getPoFileUrl($languageCode) {
        $availableLanguages = self::getavailableLanguages();
        if (!empty($availableLanguages) && isset($availableLanguages[$languageCode])) {
            $urlPattern = self::getUrlPattern();
            $searchArray = array(
                '%project' => 'ezcodx',
                '%release' => self::getReleaseVersion(),
                '%language' => $languageCode,
                '%url' => Yii::app()->getBaseUrl(true),
            );
            return str_replace(array_keys($searchArray), $searchArray, $urlPattern);
        }

        throw new FailedServiceException();
    }

}

?>