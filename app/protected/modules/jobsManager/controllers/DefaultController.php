<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * Controller Class for managing languages.
     *
     */
    class JobsManagerDefaultController extends EzcodxModuleController
    {
        public function filters()
        {
            return array(
                array(
                    EzcodxBaseController::RIGHTS_FILTER_PATH,
                    'moduleClassName' => 'JobsManagerModule',
                    'rightName' => JobsManagerModule::RIGHT_ACCESS_JOBSMANAGER,
               ),
            );
        }

        public function actionIndex()
        {
            $this->actionList();
        }

        public function actionList()
        {
            $this->processListAction(null);
        }

        protected function processListAction($messageBoxContent = null)
        {
            $breadCrumbLinks = array(
                Ezcodx::t('JobsManagerModule',
                    'JobsManagerModuleSingularLabel',
                    LabelUtil::getTranslationParamsForAllModules()),
            );
            $view = new JobsManagerTitleBarAndListView(
                            $this->getId(),
                            $this->getModule()->getId(),
                            JobsToJobsCollectionViewUtil::getMonitorJobData(),
                            JobsToJobsCollectionViewUtil::getNonMonitorJobsData(),
                            $messageBoxContent);
            $view = new JobsManagerPageView(EzcodxDefaultAdminViewUtil::
                            makeViewWithBreadcrumbsForCurrentUser($this, $view, $breadCrumbLinks, 'SettingsBreadCrumbView'));
            echo $view->render();
        }

        /**
         * @param string $type
         */
        public function actionResetJob($type)
        {
            assert('is_string($type) && $type != ""');
            $jobClassName = $type . 'Job';
            try
            {
                $jobInProcess      = JobInProcess::getByType($type);
                $jobInProcess->delete();
                $messageBoxContent = HtmlNotifyUtil::renderHighlightBoxByMessage(
                                     Ezcodx::t('JobsManagerModule', 'The job {jobName} has been reset.',
                                         array('{jobName}' => $jobClassName::getDisplayName())));
                $this->processListAction($messageBoxContent);
            }
            catch (NotFoundException $e)
            {
                $messageBoxContent = HtmlNotifyUtil::renderHighlightBoxByMessage(
                                 Ezcodx::t('JobsManagerModule', 'The job {jobName} was not found to be stuck and therefore was not reset.',
                                         array('{jobName}' => $jobClassName::getDisplayName())));
                $this->processListAction($messageBoxContent);
            }
        }

        /**
         * @param string $type
         */
        public function actionJobLogsModalList($type)
        {
            assert('is_string($type) && $type != ""');
            $jobClassName = $type . 'Job';
            $searchAttributeData = array();
            $searchAttributeData['clauses'] = array(
                1 => array(
                    'attributeName'        => 'type',
                    'operatorType'         => 'equals',
                    'value'                => $type,
                ),
            );
            $searchAttributeData['structure'] = '1';
            $pageSize     = Yii::app()->pagination->resolveActiveForCurrentUserByType('subListPageSize');
            $dataProvider = new RedBeanModelDataProvider( 'JobLog', 'startDateTime', true,
                                                                $searchAttributeData, array(
                                                                    'pagination' => array(
                                                                        'pageSize' => $pageSize,
                                                                    )
                                                                ));
            Yii::app()->getClientScript()->setToAjaxMode();
            $jobLogsListView = new JobLogsModalListView(
                                    $this->getId(),
                                    $this->getModule()->getId(),
                                    'JobLog',
                                    $dataProvider,
                                    'modal');
            $view = new ModalView($this, $jobLogsListView);
            echo $view->render();
        }

        public function actionJobLogDetails($id)
        {
            $jobLog      = JobLog::getById(intval($id));
            $detailsView = new JobLogDetailsView($this->getId(), $this->getModule()->getId(), $jobLog, strval($jobLog));
            $view   = new JobsManagerPageView(EzcodxDefaultAdminViewUtil::makeStandardViewForCurrentUser($this, $detailsView));
            echo $view->render();
        }

        public function actionRunJob($type, $timeLimit = 500, $messageLoggerClassName = 'MessageLogger')
        {
            if (!Group::isUserASuperAdministrator(Yii::app()->user->userModel))
            {
                echo Ezcodx::t('JobsManagerModule', 'Only super administrators can run jobs from the browser');
                Yii::app()->end(0, false);
            }
            $breadCrumbLinks = array(
                Ezcodx::t('JobsManagerModule',
                         'JobsManagerModuleSingularLabel',
                         LabelUtil::getTranslationParamsForAllModules()) => array('/jobsManager/default'),
                Ezcodx::t('JobsManagerModule', 'Run Job'),
            );
            $runJobView = new RunJobView($this->getId(), $this->getModule()->getId(), $type, (int)$timeLimit);
            $view = new JobsManagerPageView(EzcodxDefaultAdminViewUtil::
                        makeViewWithBreadcrumbsForCurrentUser($this, $runJobView, $breadCrumbLinks, 'SettingsBreadCrumbView'));
            echo $view->render();
            $template = EzcodxHtml::script("$('#logging-table ol').append('<li>{message}</li>');");
            $isJobInProgress = false;
            JobsManagerUtil::runFromJobManagerCommandOrBrowser($type, (int)$timeLimit, $messageLoggerClassName,
                                                               $isJobInProgress, true, $template);
            echo EzcodxHtml::script('$("#progress-table").hide(); $("#complete-table").show();');
        }

        public function actionQueueJob($type, $delay = 0, $messageLoggerClassName = 'MessageLogger')
        {
            if (!Group::isUserASuperAdministrator(Yii::app()->user->userModel))
            {
                echo Ezcodx::t('JobsManagerModule', 'Only super administrators can run jobs from the browser');
                Yii::app()->end(0, false);
            }
            if (!Yii::app()->jobQueue->isEnabled())
            {
                echo Ezcodx::t('JobsManagerModule', 'Job queuing must be enabled in order to queue a job');
                Yii::app()->end(0, false);
            }
            $breadCrumbLinks = array(
                Ezcodx::t('JobsManagerModule',
                         'JobsManagerModuleSingularLabel',
                         LabelUtil::getTranslationParamsForAllModules()) => array('/jobsManager/default'),
                         Yii::app()->jobQueue->getQueueJobLabel(),
            );
            $messageLogger = new $messageLoggerClassName();
            $queueJobView = new QueueJobView($this->getId(), $this->getModule()->getId(), $type, (int)$delay, $messageLogger);
            $view = new JobsManagerPageView(EzcodxDefaultAdminViewUtil::
                        makeViewWithBreadcrumbsForCurrentUser($this, $queueJobView, $breadCrumbLinks, 'SettingsBreadCrumbView'));
            echo $view->render();
            $template = EzcodxHtml::script("$('#logging-table ol').append('<li>{message}</li>');");
        }
    }
?>