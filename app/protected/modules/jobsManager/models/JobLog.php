<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * A class to store historical information on past jobs that have run.
     */
    class JobLog extends Item
    {
        /**
         * Utilized by the status attribute to define the status as complete without an error.
         * @var integer
         */
        const STATUS_COMPLETE_WITHOUT_ERROR = 1;

        /**
         * Utilized by the status attribute to define the status as complet with an error.
         * @var integer
         */
        const STATUS_COMPLETE_WITH_ERROR    = 2;

        /**
         * Do not audit JobLog model
         * @var bool
         */
        protected $isAudited = false;

        public function __toString()
        {
            if ($this->type == null)
            {
                return null;
            }
            return JobsUtil::resolveStringContentByType($this->type);
        }

        /**
         * @param $type
         * @param null $pageSize
         * @param null $orderBy
         * @return An
         */
        public static function getByType($type, $pageSize = null, $orderBy = null)
        {
            assert('is_string($type)');
            assert('is_string($orderBy) || $orderBy == null');
            $searchAttributeData = array();
            $searchAttributeData['clauses'] = array(
                1 => array(
                    'attributeName'             => 'type',
                    'operatorType'              => 'equals',
                    'value'                     => $type,
                ),
            );
            $searchAttributeData['structure'] = '1';
            $joinTablesAdapter                = new RedBeanModelJoinTablesQueryAdapter(get_called_class());
            $where = RedBeanModelDataProvider::makeWhere(get_called_class(), $searchAttributeData, $joinTablesAdapter);
            return self::getSubset($joinTablesAdapter, null, $pageSize, $where, $orderBy);
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                    'endDateTime',
                    'isProcessed',
                    'message',
                    'startDateTime',
                    'status',
                    'type'
                ),
                'rules' => array(
                    array('endDateTime',    'required'),
                    array('endDateTime',    'type', 'type' => 'datetime'),
                    array('isProcessed',    'boolean'),
                    array('isProcessed',    'validateIsProcessedIsSet'),
                    array('message',        'type',   'type' => 'string'),
                    array('startDateTime',  'required'),
                    array('status',         'required'),
                    array('status',         'type',   'type' => 'integer'),
                    array('startDateTime',  'type', 'type' => 'datetime'),
                    array('type',           'required'),
                    array('type',           'type',   'type' => 'string'),
                    array('type',           'length', 'min'  => 1, 'max' => 64),
                ),
                'defaultSortAttribute' => 'type',
                'elements' => array(
                    'description'     => 'TextArea',
                    'endDateTime'     => 'DateTime',
                    'startDateTime'   => 'DateTime',
                ),
            );
            return $metadata;
        }

        /**
         * Because isProcessed is a boolean attribute, disallow if the value is not specified. We do
         * not want NULL values in the database for this attribute.
         */
        public function validateIsProcessedIsSet()
        {
            if ($this->isProcessed == null)
            {
                $this->addError('isProcessed', Ezcodx::t('JobsManagerModule', 'Is Processed must be set as true or false, not null.'));
            }
        }

        public static function isTypeDeletable()
        {
            return true;
        }

        /**
         * Returns the display name for the model class.
         * @param null | string $language
         * @return dynamic label name based on module.
         */
        protected static function getLabel($language = null)
        {
            return Ezcodx::t('JobsManagerModule', 'Job Log', array(), null, $language);
        }

        /**
         * Returns the display name for plural of the model class.
         * @param null | string $language
         * @return dynamic label name based on module.
         */
        protected static function getPluralLabel($language = null)
        {
            return Ezcodx::t('JobsManagerModule', 'Job Logs', array(), null, $language);
        }

        protected static function translatedAttributeLabels($language)
        {
            return array_merge(parent::translatedAttributeLabels($language),
                array(
                    'endDateTime'   => Ezcodx::t('EzcodxModule',       'End Date Time',  array(), null, $language),
                    'isProcessed'   => Ezcodx::t('JobsManagerModule', 'Is Processed',  array(), null, $language),
                    'message'       => Ezcodx::t('JobsManagerModule', 'Message',  array(), null, $language),
                    'startDateTIme' => Ezcodx::t('EzcodxModule',       'Start Date Time',  array(), null, $language),
                    'status'        => Ezcodx::t('EzcodxModule',       'Status',  array(), null, $language),
                    'type'          => Ezcodx::t('Core',              'Type',  array(), null, $language),
                )
            );
        }
    }
?>