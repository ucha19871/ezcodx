<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * A job for testing that the outbound connection to SMTP is still working correctly.
     */
    class TestOutboundEmailJob extends BaseJob
    {
        /**
         * @returns Translated label that describes this job type.
         */
        public static function getDisplayName()
        {
           return Ezcodx::t('EmailMessagesModule', 'Testing Outbound Email Connection Job');
        }

        /**
         * @return The type of the NotificationRules
         */
        public static function getType()
        {
            return 'TestOutboundEmail';
        }

        public static function getRecommendedRunFrequencyContent()
        {
            return Ezcodx::t('JobsManagerModule', 'Once a day, early in the morning.');
        }

        /**
         *
         * (non-PHPdoc)
         * @see BaseJob::run()
         */
        public function run()
        {
            $messageContent            = null;
            $userToSendMessagesFrom    = BaseControlUserConfigUtil::getUserToRunAs();
            $emailMessage              = new EmailMessage();
            $emailMessage->owner       = Yii::app()->user->userModel;
            $emailMessage->subject     = Ezcodx::t('EmailMessagesModule', 'A test email from Ezcodx',
                                         LabelUtil::getTranslationParamsForAllModules());
            $emailContent              = new EmailMessageContent();
            $emailContent->textContent = Ezcodx::t('EmailMessagesModule', 'A test text message from Ezcodx.',
                                         LabelUtil::getTranslationParamsForAllModules());
            $emailContent->htmlContent = Ezcodx::t('EmailMessagesModule', 'A test text message from Ezcodx.',
                                         LabelUtil::getTranslationParamsForAllModules());
            $emailMessage->content     = $emailContent;
            $sender                    = new EmailMessageSender();
            $sender->fromAddress       = Yii::app()->emailHelper->resolveFromAddressByUser($userToSendMessagesFrom);
            $sender->fromName          = strval($userToSendMessagesFrom);
            $emailMessage->sender      = $sender;
            $recipient                 = new EmailMessageRecipient();
            $recipient->toAddress      = Yii::app()->emailHelper->defaultTestToAddress;
            $recipient->toName         = 'Test Recipient';
            $recipient->type           = EmailMessageRecipient::TYPE_TO;
            $emailMessage->recipients->add($recipient);
            $box                       = EmailBox::resolveAndGetByName(EmailBox::NOTIFICATIONS_NAME);
            $emailMessage->folder      = EmailFolder::getByBoxAndType($box, EmailFolder::TYPE_DRAFT);
            $validated                 = $emailMessage->validate();
            if (!$validated)
            {
                throw new NotSupportedException();
            }
            Yii::app()->emailHelper->sendImmediately($emailMessage);
            if (!$emailMessage->hasSendError())
            {
                $messageContent .= Ezcodx::t('EmailMessagesModule', 'Message successfully sent') . "\n";
                return true;
            }
            else
            {
                $messageContent .= Ezcodx::t('EmailMessagesModule', 'Message failed to send') . "\n";
                $messageContent .= $emailMessage->error     . "\n";
                $this->errorMessage = $messageContent;
                return false;
            }
        }
    }
?>