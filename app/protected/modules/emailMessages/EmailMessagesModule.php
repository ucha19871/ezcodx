<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * Module used to manage email messages, folders, and boxes.
     */
    class EmailMessagesModule extends SecurableModule
    {
        const RIGHT_ACCESS_CONFIGURATION         = 'Access Email Configuration';
        const RIGHT_CREATE_EMAIL_MESSAGES        = 'Create Emails';
        const RIGHT_DELETE_EMAIL_MESSAGES        = 'Delete Emails';
        const RIGHT_ACCESS_EMAIL_MESSAGES        = 'Access Emails Tab';

        public function getDependencies()
        {
            return array(
                'configuration',
                'ezcodx',
            );
        }

        public function getRootModelNames()
        {
            return array('EmailBox',
                         'EmailFolder',
                         'EmailMessageContent',
                         'EmailMessage',
                         'EmailMessageSender',
                         'EmailMessageRecipient',
                         'EmailMessageSendError'
                         );
        }

        public static function getTranslatedRightsLabels()
        {
            $labels                                    = array();
            $labels[self::RIGHT_ACCESS_CONFIGURATION]  = Ezcodx::t('EmailMessagesModule', 'Access Email Configuration');
            $labels[self::RIGHT_CREATE_EMAIL_MESSAGES] = Ezcodx::t('EmailMessagesModule', 'Create Emails');
            $labels[self::RIGHT_DELETE_EMAIL_MESSAGES] = Ezcodx::t('EmailMessagesModule', 'Delete Emails');
            $labels[self::RIGHT_ACCESS_EMAIL_MESSAGES] = Ezcodx::t('EmailMessagesModule', 'Access Emails Tab');
            return $labels;
        }

        public static function getDefaultMetadata()
        {
            $metadata = array();
            $metadata['global'] = array(
                'configureMenuItems' => array(
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EmailMessagesModule', 'Email Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EmailMessagesModule', 'Manage Email Configuration')",
                        'route'            => '/emailMessages/default/configurationEdit',
                        'right'            => self::RIGHT_ACCESS_CONFIGURATION,
                    ),
                ),
                'userHeaderMenuItems' => array(
                    array(
                        'label' => "eval:Ezcodx::t('EmailMessagesModule', 'Data Cleanup')",
                        'url' => array('/emailMessages/default/matchingList'),
                        'right' => self::RIGHT_ACCESS_EMAIL_MESSAGES,
                        'order' => 3,
                    ),
                ),
                'configureSubMenuItems' => array(
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EmailMessagesModule', 'Email SMTP Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EmailMessagesModule', 'Manage Email SMTP Configuration')",
                        'route'            => '/emailMessages/default/configurationEditOutbound',
                        'right'            => self::RIGHT_ACCESS_CONFIGURATION,
                    ),
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EmailMessagesModule', 'Email Archiving Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EmailMessagesModule', 'Manage Email Archiving Configuration')",
                        'route'            => '/emailMessages/default/configurationEditImap',
                        'routeParams'      => array('type' => 1),
                        'right'            => self::RIGHT_ACCESS_CONFIGURATION,
                    ),
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EmailMessagesModule', 'Bounce Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EmailMessagesModule', 'Manage Bounce Configuration')",
                        'route'            => '/emailMessages/default/configurationEditImap',
                        'routeParams'      => array('type' => 2),
                        'right'            => self::RIGHT_ACCESS_CONFIGURATION,
                    ),
                )
            );
            return $metadata;
        }

        public static function getPrimaryModelName()
        {
            return 'EmailMessage';
        }

        public static function getAccessRight()
        {
            return self::RIGHT_ACCESS_EMAIL_MESSAGES;
        }

        public static function getCreateRight()
        {
            return self::RIGHT_CREATE_EMAIL_MESSAGES;
        }

        public static function getDeleteRight()
        {
            return self::RIGHT_DELETE_EMAIL_MESSAGES;
        }

        public static function getDefaultDataMakerClassName()
        {
            return 'EmailMessagesDefaultDataMaker';
        }

        public static function getDemoDataMakerClassNames()
        {
            return array('EmailMessageUrlsDemoDataMaker');
        }

        public static function hasPermissions()
        {
            return true;
        }

        public static function isReportable()
        {
            return true;
        }

        public static function getLastArchivingImapDropboxCheckTime()
        {
            return static::getLastImapDropboxCheckTimeByKey('lastImapDropboxCheckTime');
        }

        public static function setLastArchivingImapDropboxCheckTime($lastImapDropboxCheckTime)
        {
            static::setLastImapDropboxCheckTimeByKey('lastImapDropboxCheckTime', $lastImapDropboxCheckTime);
        }

        public static function getLastBounceImapDropboxCheckTime()
        {
            return static::getLastImapDropboxCheckTimeByKey('lastBounceImapDropboxCheckTime');
        }

        public static function setLastBounceImapDropboxCheckTime($lastImapDropboxCheckTime)
        {
            static::setLastImapDropboxCheckTimeByKey('lastBounceImapDropboxCheckTime', $lastImapDropboxCheckTime);
        }

        protected static function getLastImapDropboxCheckTimeByKey($key)
        {
            $lastImapDropboxCheckTime = EzcodxConfigurationUtil::getByModuleName('EmailMessagesModule', $key);
            return $lastImapDropboxCheckTime;
        }

        protected static function setLastImapDropboxCheckTimeByKey($key, $value)
        {
            assert('isset($key)');
            assert('$key != ""');
            EzcodxConfigurationUtil::setByModuleName('EmailMessagesModule', $key, $value);
        }

        protected static function getSingularModuleLabel($language)
        {
            return Ezcodx::t('EmailMessagesModule', 'Email Message', array(), null, $language);
        }

        protected static function getPluralModuleLabel($language)
        {
            return Ezcodx::t('EmailMessagesModule', 'Email Messages', array(), null, $language);
        }
    }
?>