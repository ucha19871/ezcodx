<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * Model for storing email boxes.
     */
    class EmailBox extends Item
    {
        const NOTIFICATIONS_NAME    = 'System Notifications';

        const USER_DEFAULT_NAME     = 'Default';

        const AUTORESPONDERS_NAME   = 'Autoresponders';

        const CAMPAIGNS_NAME        = 'Campaigns';

        protected $isSpecialBox     = false;

        public static function getByName($name)
        {
            assert('is_string($name)');
            assert('$name != ""');
            $bean = EzcodxRedBean::findOne(EmailBox::getTableName(), "name = :name ", array(':name' => $name));
            assert('$bean === false || $bean instanceof RedBean_OODBBean');
            if ($bean === false)
            {
                throw new NotFoundException();
            }
            else
            {
                $box = self::makeModel($bean);
            }
            $box->setSpecialBox();
            return $box;
        }

        public static function resolveAndGetByName($name)
        {
            assert('is_string($name)');
            assert('!empty($name)');
            try
            {
                $box = static::getByName($name);
            }
            catch (NotFoundException $e)
            {
                // we do this to avoid three "||" inside if below.
                $allowedBoxes = array(static::NOTIFICATIONS_NAME, static::AUTORESPONDERS_NAME, static::CAMPAIGNS_NAME);
                if (in_array($name, $allowedBoxes))
                {
                    $box = static::makeSystemMissingBox($name);
                }
                else
                {
                    throw new NotFoundException();
                }
            }
            $box->setSpecialBox();
            return $box;
        }

        protected static function makeSystemMissingBox($name)
        {
            $box                = new EmailBox();
            $box->name          = $name;
            $folder             = new EmailFolder();
            $folder->name       = EmailFolder::getDefaultDraftName();
            $folder->type       = EmailFolder::TYPE_DRAFT;
            $folder->emailBox   = $box;
            $box->folders->add($folder);
            $folder             = new EmailFolder();
            $folder->name       = EmailFolder::getDefaultSentName();
            $folder->type       = EmailFolder::TYPE_SENT;
            $folder->emailBox   = $box;
            $box->folders->add($folder);
            $folder             = new EmailFolder();
            $folder->name       = EmailFolder::getDefaultOutboxName();
            $folder->type       = EmailFolder::TYPE_OUTBOX;
            $folder->emailBox   = $box;
            $box->folders->add($folder);
            $folder             = new EmailFolder();
            $folder->name       = EmailFolder::getDefaultOutboxErrorName();
            $folder->type       = EmailFolder::TYPE_OUTBOX_ERROR;
            $folder->emailBox   = $box;
            $box->folders->add($folder);
            $folder             = new EmailFolder();
            $folder->name       = EmailFolder::getDefaultOutboxFailureName();
            $folder->type       = EmailFolder::TYPE_OUTBOX_FAILURE;
            $folder->emailBox   = $box;
            $box->folders->add($folder);
            $folder             = new EmailFolder();
            $folder->name       = EmailFolder::getDefaultInboxName();
            $folder->type       = EmailFolder::TYPE_INBOX;
            $folder->emailBox   = $box;
            $box->folders->add($folder);
            $folder             = new EmailFolder();
            $folder->name       = EmailFolder::getDefaultArchivedName();
            $folder->type       = EmailFolder::TYPE_ARCHIVED;
            $folder->emailBox   = $box;
            $box->folders->add($folder);
            $folder             = new EmailFolder();
            $folder->name       = EmailFolder::getDefaultArchivedUnmatchedName();
            $folder->type       = EmailFolder::TYPE_ARCHIVED_UNMATCHED;
            $folder->emailBox   = $box;
            $box->folders->add($folder);
            $saved              = $box->save();
            assert('$saved');
            return $box;
        }

        protected static function translatedAttributeLabels($language)
        {
            return array_merge(parent::translatedAttributeLabels($language),
                array(
                    'folders' => Ezcodx::t('EmailMessagesModule', 'Folders', array(), null, $language),
                    'name'    => Ezcodx::t('EzcodxModule',         'Name',    array(), null, $language),
                    'users'   => Ezcodx::t('UsersModule',         'Users',   array(), null, $language),
                )
            );
        }

        protected function setSpecialBox()
        {
            $this->isSpecialBox = true;
        }

        public function isSpecialBox()
        {
            return $this->isSpecialBox;
        }

        public function __toString()
        {
            if (trim($this->name) == '')
            {
                return Ezcodx::t('Core', '(Unnamed)');
            }
            return $this->name;
        }

        public static function getModuleClassName()
        {
            return 'EmailMessagesModule';
        }

        public static function canSaveMetadata()
        {
            return false;
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                    'name',
                ),
                'relations' => array(
                    'folders' => array(static::HAS_MANY, 'EmailFolder'),
                    'user'    => array(static::HAS_MANY_BELONGS_TO, 'User'),
                ),
                'rules' => array(
                    array('name',          'required'),
                    array('name',          'type',    'type' => 'string'),
                    array('name',          'length',  'min'  => 1, 'max' => 64),
                )
            );
            return $metadata;
        }

        public static function isTypeDeletable()
        {
            return true;
        }

        /**
         * Returns the display name for the model class.
         * @param null | string $language
         * @return dynamic label name based on module.
         */
        protected static function getLabel($language = null)
        {
            return Ezcodx::t('EmailMessagesModule', 'Email Box', array(), null, $language);
        }

        /**
         * Returns the display name for plural of the model class.
         * @param null | string $language
         * @return dynamic label name based on module.
         */
        protected static function getPluralLabel($language = null)
        {
            return Ezcodx::t('EmailMessagesModule', 'Email Boxes', array(), null, $language);
        }

        public function isDeletable()
        {
            return !$this->isSpecialBox;
        }
    }
?>
