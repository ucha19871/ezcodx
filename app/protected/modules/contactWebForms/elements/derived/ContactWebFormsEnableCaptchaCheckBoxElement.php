<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class ContactWebFormsEnableCaptchaCheckBoxElement extends CheckBoxElement
    {
        protected function getDisabledValue()
        {
            if ($this->isSetCaptchaKeys() === false)
            {
                return 'disabled';
            }
            return parent::getDisabledValue();
        }

        protected function renderControlEditable()
        {
            if ($this->isSetCaptchaKeys() === false)
            {
                $message = 'Please set ReCaptcha keys in Global Configuration to enable captcha for web forms.';
                $message = EzcodxHtml::tag('span', array('class' => 'row-description'), $message);
                return parent::renderControlEditable() . Ezcodx::t('ContactWebFormsModule', $message);
            }
            return parent::renderControlEditable();
        }

        protected function isSetCaptchaKeys()
        {
            $reCaptchaPrivateKey = EzcodxConfigurationUtil::getByModuleName('EzcodxModule', 'reCaptchaPrivateKey');
            $reCaptchaPublicKey  = EzcodxConfigurationUtil::getByModuleName('EzcodxModule', 'reCaptchaPublicKey');

            if ($reCaptchaPrivateKey === null || empty($reCaptchaPrivateKey) ||
                $reCaptchaPublicKey === null  || empty($reCaptchaPublicKey))
            {
                return false;
            }
            return true;
        }
    }
?>