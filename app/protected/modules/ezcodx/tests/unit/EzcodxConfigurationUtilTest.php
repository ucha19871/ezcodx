<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class EzcodxConfigurationUtilTest extends EzcodxBaseTest
    {
        public static function setUpBeforeClass()
        {
            parent::setUpBeforeClass();
            EzcodxDatabaseCompatibilityUtil::dropStoredFunctionsAndProcedures();
            SecurityTestHelper::createSuperAdmin();
            UserTestHelper::createBasicUser('billy');
            UserTestHelper::createBasicUser('sally');
        }

        public function testGetAndSetByCurrentUserByModuleName()
        {
            Yii::app()->user->userModel =  User::getByUsername('super');
            $this->assertNull(EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'aKey'));
            EzcodxConfigurationUtil::setForCurrentUserByModuleName('EzcodxModule', 'aKey', 'aValue');
            Yii::app()->user->userModel =  User::getByUsername('billy');
            $this->assertNull(EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'aKey'));
            EzcodxConfigurationUtil::setForCurrentUserByModuleName('EzcodxModule', 'aKey', 'bValue');
            Yii::app()->user->userModel =  User::getByUsername('sally');
            $this->assertNull(EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'aKey'));
            EzcodxConfigurationUtil::setForCurrentUserByModuleName('EzcodxModule', 'aKey', 'cValue');

            //now retrieve again.
            Yii::app()->user->userModel =  User::getByUsername('super');
            $this->assertEquals('aValue', EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'aKey'));
            Yii::app()->user->userModel =  User::getByUsername('billy');
            $this->assertEquals('bValue', EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'aKey'));
            Yii::app()->user->userModel =  User::getByUsername('sally');
            $this->assertEquals('cValue', EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'aKey'));

            //Test retrieving a generic value that is set globally on EzcodxModule. The value returned should be the
            //same for all users.
            $metadata = EzcodxModule::getMetadata();
            $this->assertTrue(!isset($metadata['global']['bKey']));
            $metadata['global']['bKey'] = 'GlobalValue';
            EzcodxModule::setMetadata($metadata);
            Yii::app()->user->userModel =  User::getByUsername('super');
            $this->assertEquals('GlobalValue',
                EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'bKey'));
            Yii::app()->user->userModel =  User::getByUsername('billy');
            $this->assertEquals('GlobalValue',
                EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'bKey'));
            Yii::app()->user->userModel =  User::getByUsername('sally');
            $this->assertEquals('GlobalValue',
                EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'bKey'));

            //Now change the bKey value, just for billy and retrieve again for all users. Only billy's bKey value
            //should be different.
            EzcodxConfigurationUtil::setByUserAndModuleName(
                User::getByUsername('billy'), 'EzcodxModule', 'bKey', 'BillyBKey');
            Yii::app()->user->userModel =  User::getByUsername('super');
            $this->assertEquals('GlobalValue',
                EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'bKey'));
            Yii::app()->user->userModel =  User::getByUsername('billy');
            $this->assertEquals('BillyBKey',
                EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'bKey'));
            Yii::app()->user->userModel =  User::getByUsername('sally');
            $this->assertEquals('GlobalValue',
                EzcodxConfigurationUtil::getForCurrentUserByModuleName('EzcodxModule', 'bKey'));
        }

        public function testSetGetByModuleName()
        {
            EzcodxConfigurationUtil::setByModuleName('EzcodxModule', 'testSetGetByModuleName', 'someValue');
            $this->assertEquals('someValue',
                EzcodxConfigurationUtil::getByModuleName('EzcodxModule', 'testSetGetByModuleName'));
        }
    }
?>