<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class DevelopmentMenuView extends ConfigureModulesMenuView
    {
        public function getTitle()
        {
            return '<h1>' . Ezcodx::t('EzcodxModule', 'Developer Tools') . '</h1>';
        }

        protected function getCategoryData()
        {
            $categories = array();
            $categories['misc'][] = array('titleLabel'          => Ezcodx::t('EzcodxModule', 'Clear Cache'),
                                          'descriptionLabel'    => Ezcodx::t('EzcodxModule', 'In the case where you have reloaded the database, some cached items ' .
                                                                        'might still exist. This is a way to clear that cache.'),
                                          'route'               => 'ezcodx/development?clearCache=1' // Not Coding Standard
            );
            $categories['misc'][] = array('titleLabel'          => Ezcodx::t('EzcodxModule', 'Update Custom Data'),
                                          'descriptionLabel'    => Ezcodx::t('EzcodxModule', 'If there is new metadata to load using CustomManagement, use this option.'),
                                          'route'               => 'ezcodx/development?resolveCustomData=1' // Not Coding Standard
            );
            $categories['misc'][] = array('titleLabel'          => Ezcodx::t('EzcodxModule', 'System Check'),
                                          'descriptionLabel'    => Ezcodx::t('EzcodxModule', 'Make sure the application is properly configured.'),
                                          'route'               => 'configuration/default/runDiagnostic' // Not Coding Standard
            );
            $categories['misc'][] = array('titleLabel'          => Ezcodx::t('EzcodxModule', 'Compile CSS'),
                                          'descriptionLabel'    => Ezcodx::t('EzcodxModule', 'Compile the .less files into CSS files'),
                                          'route'               => 'ezcodx/development/compileCss' // Not Coding Standard
            );
            $this->setLinkText(Ezcodx::t('EzcodxModule', 'Run'));
            return $categories;
        }
    }
?>