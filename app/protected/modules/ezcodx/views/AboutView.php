<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class AboutView extends View
    {
        protected function renderContent()
        {
            $ezcodxVersion    = self::getEzcodxVersionDisplayContent();
            $yiiVersion     = YiiBase::getVersion();
            if (method_exists('R', 'getVersion'))
            {
                $redBeanVersion =  EzcodxRedBean::getVersion();
            }
            else
            {
                $redBeanVersion = '&lt; 1.2.9.1';
            }
            // Begin Not Coding Standard
            $content  = '';
            // End Not Coding Standard
            return $content;
        }

        protected static function getEzcodxVersionDisplayContent()
        {
            if (defined('COMMERCIAL_VERSION'))
            {
                $ezcodxVersion = COMMERCIAL_VERSION;
                // Remove REPO_ID from Ezcodx version
                $ezcodxVersion =  'pro.' . substr($ezcodxVersion, 0, strpos($ezcodxVersion, '(') - 1);
            }
            else
            {
                $ezcodxVersion = VERSION;
                // Remove REPO_ID from Ezcodx version
                $ezcodxVersion =  substr($ezcodxVersion, 0, strpos($ezcodxVersion, '(') - 1);
            }
            return $ezcodxVersion;
        }

        public static function renderSocialLinksContent()
        {
            return '<ul class="social-links clearfix">
                            <li>
                                <a href="https://www.facebook.com/pages/Ezcodx/117701404997971" class="facebook" title="ezcodx on facebook" target="_blank">
                                    <span>Ezcodx CRM on Facebook</span>
                                </a>
                            </li>
                            <li>
                                <a href="http://twitter.com/EzcodxCRM" class="twitter" title="ezcodx on twitter" target="_blank">
                                    <span>Ezcodx CRM on Twitter</span>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.linkedin.com/company/ezcodx-open-source" class="linkedin" title="ezcodx on linkedin" target="_blank">
                                    <span>Ezcodx CRM on LinkedIn</span>
                                </a>
                            </li>
                            <li><a href="https://bitbucket.org/ezcodx/" class="bitbucket" title="ezcodx on bitbucket" target="_blank">
                                <span>Ezcodx CRM on BitBucket</span>
                            </a></li>
                            <li>
                                <a href="https://www.pivotaltracker.com/projects/380027" class="pivotal" title="ezcodx on pivotal tracker" target="_blank">
                                    <span>Ezcodx CRM on Pivotal Tracker</span>
                                </a>
                            </li>
                            <li>
                                <a href="http://ezcodx.org/feed" class="rss" title="ezcodx rss" target="_blank">
                                    <span>Ezcodx CRM RSS</span>
                                </a>
                            </li>
                        </ul>';
        }
    }
?>
