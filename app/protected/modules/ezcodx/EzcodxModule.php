<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class EzcodxModule extends SecurableModule
    {
        const ADMINISTRATION_CATEGORY_GENERAL = 1;

        const ADMINISTRATION_CATEGORY_AUTHENTICATION = 2;

        const ADMINISTRATION_CATEGORY_PLUGINS = 3;

        const RIGHT_ACCESS_ADMINISTRATION         = 'Access Administration Tab';
        const RIGHT_BULK_WRITE                    = 'Mass Update';
        const RIGHT_ACCESS_GLOBAL_CONFIGURATION   = 'Access Global Configuration';
        const RIGHT_ACCESS_CURRENCY_CONFIGURATION = 'Access Currency Configuration';
        const RIGHT_BULK_DELETE                   = 'Mass Delete';
        const RIGHT_BULK_MERGE                    = 'Mass Merge';

        const AUDIT_EVENT_ITEM_CREATED            = 'Item Created';
        const AUDIT_EVENT_ITEM_MODIFIED           = 'Item Modified';
        const AUDIT_EVENT_ITEM_DELETED            = 'Item Deleted';
        const AUDIT_EVENT_ITEM_VIEWED             = 'Item Viewed';

        public static function getTranslatedRightsLabels()
        {
            $labels                                             = array();
            $labels[self::RIGHT_ACCESS_ADMINISTRATION]          = Ezcodx::t('EzcodxModule', 'Access Administration Tab');
            $labels[self::RIGHT_BULK_WRITE]                     = Ezcodx::t('Core', 'Mass Update');
            $labels[self::RIGHT_ACCESS_GLOBAL_CONFIGURATION]    = Ezcodx::t('EzcodxModule', 'Access Global Configuration');
            $labels[self::RIGHT_ACCESS_CURRENCY_CONFIGURATION]  = Ezcodx::t('EzcodxModule', 'Access Currency Configuration');
            $labels[self::RIGHT_BULK_DELETE]                    = Ezcodx::t('Core', 'Mass Delete');
            $labels[self::RIGHT_BULK_MERGE]                     = Ezcodx::t('Core', 'Mass Merge');
            return $labels;
        }

        public function canDisable()
        {
            return false;
        }

        public function getDependencies()
        {
            return array();
        }

        public function getRootModelNames()
        {
            // EzcodxModule is a special case in that most of its models
            // are non-root models of things that root models in the other
            // modules, and because EzcodxModule is the root of the module
            // dependence hierarchy it needed concern itself, other than
            // with the models that are specific to itself.
            return array('ActiveLanguage', 'AuditEvent', 'NamedSecurableItem', 'GlobalMetadata', 'PerUserMetadata', 'Portlet',
                         'CustomFieldData', 'CalculatedDerivedAttributeMetadata', 'DropDownDependencyDerivedAttributeMetadata',
                         'SavedSearch', 'MessageSource', 'MessageTranslation', 'KanbanItem');
        }

        public static function getDefaultMetadata()
        {
            $metadata = array();
            $metadata['global'] = array(
                'configureMenuItems' => array(
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EzcodxModule', 'Global Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EzcodxModule', 'Manage Global Configuration')",
                        'route'            => '/ezcodx/default/configurationEdit',
                        'right'            => self::RIGHT_ACCESS_GLOBAL_CONFIGURATION,
                    ),
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EzcodxModule', 'Currency Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EzcodxModule', 'Manage Currency Configuration')",
                        'route'            => '/ezcodx/currency/configurationList',
                        'right'            => self::RIGHT_ACCESS_CURRENCY_CONFIGURATION,
                    ),
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('Core', 'Languages')",
                        'descriptionLabel' => "eval:Ezcodx::t('EzcodxModule', 'Manage Active Languages')",
                        'route'            => '/ezcodx/language/configurationList',
                        'right'            => self::RIGHT_ACCESS_GLOBAL_CONFIGURATION,
                    ),
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EzcodxModule', 'Developer Tools')",
                        'descriptionLabel' => "eval:Ezcodx::t('EzcodxModule', 'Access Developer Tools')",
                        'route'            => '/ezcodx/development/',
                        'right'            => self::RIGHT_ACCESS_GLOBAL_CONFIGURATION,
                    ),
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EzcodxModule', 'Authentication Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EzcodxModule', 'Manage Authentication Configuration')",
                        'route'            => '/ezcodx/authentication/configurationEdit',
                        'right'            => self::RIGHT_ACCESS_GLOBAL_CONFIGURATION,
                    ),
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EzcodxModule', 'Plugins')",
                        'descriptionLabel' => "eval:Ezcodx::t('EzcodxModule', 'Manage Plugins and Integrations')",
                        'route'            => '/ezcodx/plugins/configurationEdit',
                        'right'            => self::RIGHT_ACCESS_GLOBAL_CONFIGURATION,
                    ),
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('EzcodxModule', 'User Interface Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EzcodxModule', 'Manage User Interface Configuration')",
                        'route'            => '/ezcodx/default/userInterfaceConfigurationEdit',
                        'right'            => self::RIGHT_ACCESS_GLOBAL_CONFIGURATION,
                    ),
                ),
                'headerMenuItems' => array(
                    array(
                        'label'  => "eval:Ezcodx::t('EzcodxModule', 'Administration')",
                        'url'    => array('/configuration'),
                        'right'  => self::RIGHT_ACCESS_ADMINISTRATION,
                        'order'  => 1,
                        'mobile' => false,
                    ),
                    array(
                        'label'  => "eval:Ezcodx::t('EzcodxModule', 'Need Support?')",
                        'url'    => 'http://www.ezcodx.com/needSupport.php',
                        'order'  => 9,
                        'mobile' => true,
                    ),
                    array(
                        'label'  => "eval:Ezcodx::t('EzcodxModule', 'About Ezcodx')",
                        'url'    => array('/ezcodx/default/about'),
                        'order'  => 10,
                        'mobile' => true,
                    ),
                ),
                'configureSubMenuItems' => array(
                    array(
                        'category'         => self::ADMINISTRATION_CATEGORY_AUTHENTICATION,
                        'titleLabel'       => "eval:Ezcodx::t('EzcodxModule', 'LDAP Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('EzcodxModule', 'Manage LDAP Authentication')",
                        'route'            => '/ezcodx/ldap/configurationEditLdap',
                        'right'            => self::RIGHT_ACCESS_GLOBAL_CONFIGURATION,
                    ),
                ),
                'adminTabMenuItemsModuleOrdering' => array(
                    'home',
                    'configuration',
                    'designer',
                    'import',
                    'groups',
                    'users',
                    'roles',
                    'workflows',
                    'contactWebForms',
                ),
                'tabMenuItemsModuleOrdering' => array(
                    'home',
                    'mashableInbox',
                    'accounts',
                    'leads',
                    'contacts',
                    'opportunities',
                    'marketing',
                    'projects',
                    'products',
                    'reports',
                )
            );
            return $metadata;
        }

        public static function stringifyAuditEvent(AuditEvent $auditEvent, $format = 'long')
        {
            assert('$format == "long" || $format == "short"');
            $s = null;
            switch ($auditEvent->eventName)
            {
                case self::AUDIT_EVENT_ITEM_CREATED:
                case self::AUDIT_EVENT_ITEM_DELETED:
                    if ($format == 'short')
                    {
                        return Ezcodx::t('EzcodxModule', $auditEvent->eventName);
                    }
                    $s   .= strval($auditEvent);
                    $name = unserialize($auditEvent->serializedData);
                    $s   .= ", $name";
                    break;

                case self::AUDIT_EVENT_ITEM_MODIFIED:
                    list($name, $attributeNames, $oldValue, $newValue) = unserialize($auditEvent->serializedData);
                    $modelClassName = $auditEvent->modelClassName;
                    $model          = new $modelClassName();
                    if ($format == 'long')
                    {
                        $s             .= strval($auditEvent);
                        $s             .= ", $name";
                        $s             .= ', ' . Ezcodx::t('EzcodxModule', 'Changed') . ' ';
                    }
                    $attributeModel = $model;
                    $attributeLabels = array();
                    for ($i = 0; $i < count($attributeNames); $i++)
                    {
                        $attributeName = $attributeNames[$i];
                        if (!$attributeModel instanceof RedBeanModels)
                        {
                            $attributeLabels[] = $attributeModel->getAttributeLabel($attributeName);
                        }
                        else
                        {
                            // TODO - auditing of related collections
                            $attributeLabels[] = 'Collection';
                            break;
                        }
                        if ($i < count($attributeNames) - 1)
                        {
                            $attributeModel = $attributeModel->$attributeName;
                        }
                    }
                    $s .= join(' ', $attributeLabels);
                    $s .= ' ' . Ezcodx::t('Core', 'from') . ' ';
                    $s .= AuditUtil::stringifyValue($attributeModel, $attributeName, $oldValue, $format) . ' ';
                    $s .= Ezcodx::t('Core', 'to') . ' ';
                    $s .= AuditUtil::stringifyValue($attributeModel, $attributeName, $newValue, $format);
                    break;
            }
            return $s;
        }

        public static function getDemoDataMakerClassNames()
        {
            return array('EzcodxDemoDataMaker');
        }

        public static function getDefaultDataMakerClassName()
        {
            return 'EzcodxDefaultDataMaker';
        }

        /**
        * When updates info are pulled from ezcodx home.
        * @return $lastAttemptedInfoUpdateTimeStamp
        */
        public static function getLastAttemptedInfoUpdateTimeStamp()
        {
            $lastAttemptedInfoUpdateTimeStamp = EzcodxConfigurationUtil::getByModuleName('EzcodxModule', 'lastAttemptedInfoUpdateTimeStamp');
            return $lastAttemptedInfoUpdateTimeStamp;
        }

        /**
         * Set $lastAttemptedInfoUpdateTimeStamp global configuration.
         * This function is called during execution of EzcodxModule::checkAndUpdateEzcodxInfo()
         */
        public static function setLastAttemptedInfoUpdateTimeStamp()
        {
            EzcodxConfigurationUtil::setByModuleName('EzcodxModule', 'lastAttemptedInfoUpdateTimeStamp', time());
        }

        /**
         * Get last Ezcodx Stable version from global configuration property.
         */
        public static function getLastEzcodxStableVersion()
        {
            $lastEzcodxStableVersion = EzcodxConfigurationUtil::getByModuleName('EzcodxModule', 'lastEzcodxStableVersion');
            return $lastEzcodxStableVersion;
        }

        /**
         * Set lastEzcodxStableVersion global pconfiguration property.
         * @param string $ezcodxVersion
         */
        public static function setLastEzcodxStableVersion($ezcodxVersion)
        {
            assert('isset($ezcodxVersion)');
            assert('$ezcodxVersion != ""');
            EzcodxConfigurationUtil::setByModuleName('EzcodxModule', 'lastEzcodxStableVersion', $ezcodxVersion);
        }

        /**
         * Check if available ezcodx updates has been checked within the last 7 days. If not, then perform
         * update and update the lastAttemptedInfoUpdateTimeStamp and lastEzcodxStableVersion global configuration properties.
         * @param boolean $forceCheck - If true, it will ignore the last time the check was made
         */
        public static function checkAndUpdateEzcodxInfo($forceCheck = false)
        {
            $lastAttemptedInfoUpdateTimeStamp = self::getLastAttemptedInfoUpdateTimeStamp();
            if ( $forceCheck || $lastAttemptedInfoUpdateTimeStamp == null ||
            (time() - $lastAttemptedInfoUpdateTimeStamp) > (7 * 24 * 60 * 60))
            {
                $headers = array(
                            'Accept: application/json',
                            'EZCODX_API_REQUEST_TYPE: REST',
                );
                $data = array(
                            'ezcodxToken' => EZCODX_TOKEN,
                            'ezcodxVersion' => VERSION,
                            'serializedData' => ''
                );

                if (isset($_SERVER['SERVER_ADDR']))
                {
                    $data['serverIpAddress'] = $_SERVER['SERVER_ADDR'];
                }

                if (isset($_SERVER['SERVER_NAME']))
                {
                    $data['serverName'] = $_SERVER['SERVER_NAME'];
                }

                if (isset($_SERVER['SERVER_SOFTWARE']))
                {
                    $data['serverSoftware'] = $_SERVER['SERVER_SOFTWARE'];
                }

                $response = ApiRestHelper::createApiCall('http://updates.ezcodx.com/app/index.php/updatesManager/api/create', 'POST', $headers, array('data' => $data));
                $response = json_decode($response, true);
                if (ApiResponse::STATUS_SUCCESS == $response['status'])
                {
                    if (isset($response['data']['latestStableEzcodxVersion']) && $response['data']['latestStableEzcodxVersion'] != '')
                    {
                        self::setLastEzcodxStableVersion($response['data']['latestStableEzcodxVersion']);
                    }

                    $ezcodxServiceHelper = new EzcodxServiceHelper();
                    if (!$ezcodxServiceHelper->runCheckAndGetIfSuccessful())
                    {
                        $message                    = new NotificationMessage();
                        $message->textContent       = $ezcodxServiceHelper->getMessage();
                        $rules = new NewEzcodxVersionAvailableNotificationRules();
                        NotificationsUtil::submit($message, $rules);
                    }
                }
                self::setLastAttemptedInfoUpdateTimeStamp();
            }
        }

        /**
         * Ezcodx is a special case, where the module label is always the label of the application
         * @param string $language
         * @return string
         */
        protected static function getSingularModuleLabel($language)
        {
            return Yii::app()->label;
        }

        /**
         * Ezcodx is a special case, where the module label is always the label of the application and is always singular
         * @param string $language
         * @return string
         */
        protected static function getPluralModuleLabel($language)
        {
            return Yii::app()->label;
        }
    }
?>
