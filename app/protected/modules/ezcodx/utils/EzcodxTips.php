<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * Ezcodx Tips.
     * This file contains a library of tips that can be displayed to users.
     */
    $tips = array(
        Ezcodx::t('EzcodxModule', 'Are you number one{spaceAndFirstName}? Find out how you size up to your colleagues on the Leaderboard.'),
        Ezcodx::t('EzcodxModule', 'Need to update multiple records at once? Select them from the list and hit the Update button.'),
        Ezcodx::t('EzcodxModule', 'Add new fields, remove ones you don\'t need and change the layout using Designer.'),
        Ezcodx::t('EzcodxModule', 'Let Ezcodx crunch the numbers, add calculated fields to your records in Designer.'),
        Ezcodx::t('EzcodxModule', 'Parlez vous Français{spaceAndFirstName}? Choose your preferred language in your My Profile area.'),
        Ezcodx::t('EzcodxModule', 'Click on the Map link on records to see the customer location on Google Maps.'),
        Ezcodx::t('EzcodxModule', 'Save yourself time and click the pencil icon to jump right in and edit your record.'),
        Ezcodx::t('EzcodxModule', 'See more search results by clicking the down arrow at the bottom of the screen.'),
        Ezcodx::t('EzcodxModule', 'Remember{spaceAndFirstName}, the Audit Trail shows you what has changed on a record and who changed it.'),
        Ezcodx::t('EzcodxModule', 'Need to see the big picture? Click Roll-Up to see activities from related records.'),
        Ezcodx::t('EzcodxModule', 'Upload attachments to a record by clicking "More Options" when adding a note.'),
        Ezcodx::t('EzcodxModule', 'Want to export your data directly to Excel? Search for the data you need and hit the Export button.'),
        Ezcodx::t('EzcodxModule', 'Recently Viewed lets you jump back to any records you have looked at lately.'),
        Ezcodx::t('EzcodxModule', 'Find what you need quickly using the global search at the top of the screen.'),
        Ezcodx::t('EzcodxModule', 'The Import Wizard helps get your data into the system easily, find it under the config icon (that\'s the cog in the top right).'),
        Ezcodx::t('EzcodxModule', 'Got questions{spaceAndFirstName}? Find answers in the Ezcodx Wiki.'),
        Ezcodx::t('EzcodxModule', 'Tag Cloud fields allow you to tag records with values so that you can easily find them later.'),
        Ezcodx::t('EzcodxModule', 'Need help{spaceAndFirstName}, why not ask a question in the forum?'),
        Ezcodx::t('EzcodxModule', 'Decide who can view and edit a record, set Group module rights and permissions.'),
        Ezcodx::t('EzcodxModule', 'The Notifications speech bubble keeps you up to date on important events.'),
        Ezcodx::t('EzcodxModule', 'Set your preferences, password, and contact details in your My Profile area.'),
        Ezcodx::t('EzcodxModule', 'Edit charts on the dashboard by clicking the config icon in the top right of each portlet.'),
    );
    return $tips;
?>