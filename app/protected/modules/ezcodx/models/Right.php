<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class Right extends OwnedModel
    {
        const NONE  = 0x0;
        const ALLOW = 0x1;
        const DENY  = 0x2;

        /**
         * @param string $moduleName
         * @param string $rightName
         * @return An
         * @throws NotFoundException
         */
        public static function getByModuleNameAndRightName($moduleName, $rightName)
        {
            assert('is_string($moduleName)');
            assert('is_string($rightName)');
            assert('$moduleName != ""');
            assert('$rightName != ""');
            $bean = EzcodxRedBean::findOne('_right', "modulename = '$moduleName' and name = '$rightName'");
            assert('$bean === false || $bean instanceof RedBean_OODBBean');
            if ($bean === false)
            {
                throw new NotFoundException();
            }
            return self::makeModel($bean);
        }

        public static function removeAllForPermitable(Permitable $permitable)
        {
            EzcodxRedBean::exec("delete from _right where permitable_id = :id;",
                    array('id' => $permitable->getClassId('Permitable')));
        }

        public static function rightToString($right)
        {
            switch ($right)
            {
                case self::NONE:
                    return Ezcodx::t('Core', '(None)');

                case self::ALLOW:
                    return Ezcodx::t('EzcodxModule', 'Allow');

                case self::DENY:
                    return Ezcodx::t('EzcodxModule', 'Deny');

                default:
                    return '???';
            }
        }

        public function __toString()
        {
            $s  = self::rightToString($this->type);
            $s .= ':';
            $s .= Ezcodx::t('EzcodxModule', $this->name);
            return $s;
        }

        public static function mangleTableName()
        {
            return true;
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                    'moduleName',
                    'name',
                    'type',
                ),
                'relations'   => array(
                    'permitable' => array(static::HAS_MANY_BELONGS_TO, 'Permitable'),
                ),
                'rules' => array(
                    array('moduleName', 'required'),
                    array('moduleName', 'type',      'type' => 'string'),
                    array('moduleName', 'length',    'min'  => 1, 'max' => 64),
                    array('name',       'required'),
                    array('name',       'type',      'type' => 'string'),
                    array('name',       'length',    'min'  => 1, 'max' => 64),
                    array('type',       'required'),
                    array('type',       'type',      'type' => 'integer'),
                    array('type',       'numerical', 'min'  => 0, 'max' => 2),
                ),
            );
            return $metadata;
        }

        public static function isTypeDeletable()
        {
            return true;
        }
    }
?>