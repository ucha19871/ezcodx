<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class Opportunity extends OwnedSecurableItem implements StarredInterface
    {
        public static function getByName($name)
        {
            return self::getByNameOrEquivalent('name', $name);
        }

        /**
         * @return value of what is considered the 'closed won' stage. It could be in the future named something else
         * or changed by the user.  This api will be expanded to handle that.  By default it will return 'Closed Won'
         */
        public static function getStageClosedWonValue()
        {
            return 'Closed Won';
        }

        protected function beforeSave()
        {
            if (parent::beforeSave())
            {
                $automaticMappingDisabled = OpportunitiesModule::isAutomaticProbabilityMappingDisabled();
                if (!isset($this->originalAttributeValues['probability']) && $automaticMappingDisabled === false)
                {
                    $this->resolveStageToProbability();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public function __toString()
        {
            try
            {
                if (trim($this->name) == '')
                {
                    return Ezcodx::t('Core', '(Unnamed)');
                }
                return $this->name;
            }
            catch (AccessDeniedSecurityException $e)
            {
                return '';
            }
        }

        public static function getModuleClassName()
        {
            return 'OpportunitiesModule';
        }

        public static function translatedAttributeLabels($language)
        {
            $params = LabelUtil::getTranslationParamsForAllModules();
            return array_merge(parent::translatedAttributeLabels($language), array(
                'account'     => Ezcodx::t('AccountsModule',      'AccountsModuleSingularLabel', $params, null, $language),
                'amount'      => Ezcodx::t('OpportunitiesModule', 'Amount',  array(), null, $language),
                'closeDate'   => Ezcodx::t('OpportunitiesModule', 'Close Date',  array(), null, $language),
                'contacts'    => Ezcodx::t('ContactsModule',      'ContactsModulePluralLabel',   $params, null, $language),
                'description' => Ezcodx::t('EzcodxModule',         'Description',  array(), null, $language),
                'meetings'    => Ezcodx::t('MeetingsModule',      'MeetingsModulePluralLabel', $params, null, $language),
                'name'        => Ezcodx::t('EzcodxModule',         'Name',  array(), null, $language),
                'notes'       => Ezcodx::t('NotesModule',         'NotesModulePluralLabel', $params, null, $language),
                'probability' => Ezcodx::t('OpportunitiesModule', 'Probability',  array(), null, $language),
                'source'      => Ezcodx::t('ContactsModule',      'Source',   array(), null, $language),
                'stage'       => Ezcodx::t('EzcodxModule',         'Stage',  array(), null, $language),
                'tasks'       => Ezcodx::t('TasksModule',         'TasksModulePluralLabel', $params, null, $language)));
        }

        public static function canSaveMetadata()
        {
            return true;
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                    'closeDate',
                    'description',
                    'name',
                    'probability',
                ),
                'relations' => array(
                    'account'       => array(static::HAS_ONE,   'Account'),
                    'amount'        => array(static::HAS_ONE,   'CurrencyValue',    static::OWNED,
                                             static::LINK_TYPE_SPECIFIC, 'amount'),
                    'products'      => array(static::HAS_MANY, 'Product'),
                    'contacts'      => array(static::MANY_MANY, 'Contact'),
                    'stage'         => array(static::HAS_ONE,   'OwnedCustomField', static::OWNED,
                                             static::LINK_TYPE_SPECIFIC, 'stage'),
                    'source'        => array(static::HAS_ONE,   'OwnedCustomField', static::OWNED,
                                             static::LINK_TYPE_SPECIFIC, 'source'),
                    'projects'      => array(static::MANY_MANY, 'Project'),
                ),
                'derivedRelationsViaCastedUpModel' => array(
                    'meetings' => array(static::MANY_MANY, 'Meeting', 'activityItems'),
                    'notes'    => array(static::MANY_MANY, 'Note',    'activityItems'),
                    'tasks'    => array(static::MANY_MANY, 'Task',    'activityItems'),
                ),
                'rules' => array(
                    array('amount',        'required'),
                    array('closeDate',     'required'),
                    array('closeDate',     'type',      'type' => 'date'),
                    array('description',   'type',      'type' => 'string'),
                    array('name',          'required'),
                    array('name',          'type',      'type' => 'string'),
                    array('name',          'length',    'min'  => 1, 'max' => 64),
                    array('probability',   'type',      'type' => 'integer'),
                    array('probability',   'numerical', 'min' => 0, 'max' => 100),
                    array('probability',   'required'),
                    array('probability',   'default',   'value' => 0),
                    array('probability',   'probability'),
                    array('stage',         'required'),
                ),
                'elements' => array(
                    'amount'      => 'CurrencyValue',
                    'account'     => 'Account',
                    'closeDate'   => 'Date',
                    'description' => 'TextArea',
                ),
                'customFields' => array(
                    'stage'  => 'SalesStages',
                    'source' => 'LeadSources',
                ),
                'defaultSortAttribute' => 'name',
                'rollupRelations' => array(
                    'contacts',
                ),
                'noAudit' => array(
                    'description'
                ),
            );
            return $metadata;
        }

        public static function isTypeDeletable()
        {
            return true;
        }

        public static function getRollUpRulesType()
        {
            return 'Opportunity';
        }

        public static function hasReadPermissionsOptimization()
        {
            return true;
        }

        public static function getGamificationRulesType()
        {
            return 'OpportunityGamification';
        }

        private function resolveStageToProbability()
        {
            if ($this->stage === null)
            {
                throw new NotSupportedException();
            }
            else
            {
                $this->probability = OpportunitiesModule::getProbabilityByStageValue($this->stage->value);
            }
        }
    }
?>
