<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    // KEEP these in alphabetical order.
    // KEEP them indented correctly.
    // KEEP all the language files up-to-date with each other.
    // DON'T MAKE A MESS!
    return array(
        'Account Management'
            => 'Gestion des comptes',
        'Achievements'
            => 'Réussites',
        'Communication'
            => 'Communication', // Same Word Translated
        'Congratulations!'
            => 'Félicitations',
        'Continue'
            => 'Continuer',
        'Game on!'
             => 'Game on!', // Same Word Translated
        'I reached level {nextLevel}'
            => 'J\'ai atteint niveau {nextLevel}',
        'I received a new badge: {badgeContent}'
            => 'J\'ai reçu un nouveau badge: {badgeContent}',
        'Leaderboard'
            => 'Leaderboard', // Same Word Translated
        'Leaderboard Ranking'
            => 'Classement Leaderboard',
        'Level'
            => 'Niveau',
        'Monthly'
            => 'Mensuel',
        'New Badge'
            => 'Nouveau Badge',
        'New Business'
            => 'Nouvelles Affaires',
        'No achievements earned.'
            => 'Aucunes réussites obtenues.',
        'Overall'
            => 'Globale',
        'Overall Statistics'
            => 'Statistiques Globales',
        'Points'
            => 'Points', // Same Word Translated
        'Rank'
            => 'Rang',
        'Sales'
            => 'Affaires',
        'Skip'
            => 'Ignorer',
        'Time Management'
            => 'Gestion du temps',
        'Weekly'
            => 'Hebdomadaire',
        'You have reached level {nextLevel}'
            => 'Vous avez atteint le niveau {nextLevel}',
        '{n} Ezcodx early morning login|{n} Ezcodx early morning logins'
            => '{n} login Ezcodx lève tôt|{n} logins Ezcodx lèves tôts',
        '{n} Ezcodx login|{n} Ezcodx logins'
            => '{n} login Ezcodx|{n} logins Ezcodx',
        '{n} Ezcodx nighttime login|{n} Ezcodx nighttime logins'
            => '{n} login Ezcodx noctambule|{n} logins noctambules',
    );
?>
