<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * Class for displaying a user's game dashboard
     */
    class UserGameDashboardView extends View
    {
        const COLLECTION_CONTAINER_ID_PREFIX  = 'game-collection-container';

        const COMPLETE_COLLECTION_LINK_PREFIX = 'complete-collection-link';

        protected $controller;

        protected $user;

        protected $generalLevelData;

        protected $badgeData;

        protected $rankingData;

        protected $statisticsData;

        public static function renderCollectionContent(User $user, GameCollection $collection)
        {
            $gameCollectionRules  = GameCollectionRulesFactory::createByType($collection->type);

            $collectionImageUrl   = Yii::app()->themeManager->baseUrl . '/default/images/collections/' .
                                    $gameCollectionRules::getType() . '/' .
                                    $gameCollectionRules::makeLargeCollectionImageName();
            $collectionBadgeImage = static::resolveLazyLoadImage($collectionImageUrl);
            $content  = EzcodxHtml::tag('div', array('class' => 'collection-badge'), $collectionBadgeImage);
            $content .= EzcodxHtml::tag('h3', array(), $gameCollectionRules->getCollectionLabel() . ' ' .
                                       Ezcodx::t('GamificationModule', 'Collection'));
            $content .= static::renderCollectionItemsContent($user, $collection, $gameCollectionRules);
            $content  = EzcodxHtml::tag('div', array(), $content);
            if ($collection->canRedeem() && $user->id == Yii::app()->user->userModel->id)
            {
                $extraClass = ' redeemable';
            }
            else
            {
                $extraClass = null;
            }
            return EzcodxHtml::tag('div', array('id'    => static::getCollectionContainerId($collection->id),
                                               'class' => 'gd-collection-panel clearfix'. $extraClass), $content);
        }

        public static function renderCoinsContent($coinValue, User $user)
        {
            $url  = Yii::app()->createUrl('gameRewards/default/redeemList/');
            $content  = EzcodxHtml::tag('span', array('id' => 'gd-z-coin'), '');
            $content .= EzcodxHtml::tag('h3', array(), Ezcodx::t('GamificationModule', '{n} coin|{n} coins',
                array($coinValue)));
            if($user->id == Yii::app()->user->userModel->id)
            {
                $content .= EzcodxHtml::link(Ezcodx::t('EzcodxModule', 'Redeem'), $url);
            }
            return      EzcodxHtml::tag('div', array('id' => self::getGameCoinContainerId()), $content);
        }

        /**
         * @param CController $controller
         * @param User $user
         * @param array $generalLevelData
         * @param array $badgeData
         * @param array $rankingData
         * @param array $statisticsData
         * @param array $collectionData
         */
        public function __construct(CController $controller, User $user, array $generalLevelData, array $badgeData,
                                    array $rankingData, array $statisticsData, array $collectionData)
        {
            $this->controller       = $controller;
            $this->user             = $user;
            $this->generalLevelData = $generalLevelData;
            $this->badgeData        = $badgeData;
            $this->rankingData      = $rankingData;
            $this->statisticsData   = $statisticsData;
            $this->collectionData   = $collectionData;
        }

        public function isUniqueToAPage()
        {
            return true;
        }

        protected function renderContent()
        {
            $this->registerScripts();
            return $this->renderDashboardContent();
        }

        protected function registerScripts()
        {
            $this->registerCoreScriptAndPublishToAssets();
            $this->registerCloseButtonScript();
            $this->registerLazyLoadImagesScript();
        }

        protected function registerCoreScriptAndPublishToAssets()
        {
            Yii::app()->clientScript->registerCoreScript('gamification-dashboard');
            Yii::app()->clientScript->registerScriptFile(
                Yii::app()->getAssetManager()->publish(
                    Yii::getPathOfAlias('application.modules.gamification.views.assets')
                ) . '/gamification-dashboard.js',
                CClientScript::POS_END
            );
        }

        protected function registerCloseButtonScript()
        {
            // Begin Not Coding Standard
            $script = "$('.close-dashboard-button a').on('click', function(){
                           if($('#UserGameDashboardView').length){
                               closeGamificationDashboard();
                               return false;
                           }
                       });
                       $('#gd-overlay, #gd-container, #gd-centralizer').on('click', function(event){
                           if(this === event.target && $('#UserGameDashboardView').length){
                               closeGamificationDashboard();
                               return false;
                           }
                       });";
            // End Not Coding Standard
            Yii::app()->clientScript->registerScript('closeGameficationDashboardScript', $script);
        }

        protected function registerLazyLoadImagesScript()
        {
            $dummyImageUrl = static::getDummyImageUrl();
            $script = <<<SPT
// we only monitor nav-right because the ones on left would already be loaded.
$('a.nav-button#nav-right[href="#"]').unbind('click.lazyLoadImages').bind('click.lazyLoadImages', function(event)
    {
        updateGamificationImagesSrcForLazyLoading();
    });

    function updateGamificationImagesSrcForLazyLoading()
    {
        $('.visible-panel img[src*="{$dummyImageUrl}"]').each(function()
        {
            var dataSrc = $(this).data('src');
            if (typeof dataSrc !== 'undefined')
            {
                this.src = dataSrc;
            }
        });
    }
    updateGamificationImagesSrcForLazyLoading(); // called on page load to resolve src for the first 4 images.
SPT;
            Yii::app()->clientScript->registerScript('lazyLoadGameficationDashboardImagesScript', $script);
        }

        protected function renderDashboardContent()
        {
            $content  = $this->renderProfileContent();
            $content .= $this->renderBadgesContent();
            $content .= static::renderCoinsContent($this->getGameCoinForUser()->value, $this->user);
            $content .= $this->renderLeaderboardContent();
            $content .= $this->renderStatisticsContent();
            $content .= $this->renderCollectionsContent();
            $content  = EzcodxHtml::tag('div', array('id' => 'game-dashboard', 'class' => 'clearfix'), $content);
            $content  = $this->renderDashboardCloseButton() . $content;
            $content  = EzcodxHtml::tag('div', array('id' => 'gd-centralizer'), $content);
            $blackOut  = EzcodxHtml::tag('div', array('id' => 'gd-overlay'), '');
            $container = EzcodxHtml::tag('div', array('id' => 'gd-container'), $content);
            return $blackOut . $container;
        }

        protected function renderProfileContent()
        {
            $content  = $this->user->getAvatarImage(240);
            $content .= EzcodxHtml::tag('h3', array(), strval($this->user));
            $content .= $this->renderMiniStatisticsContent();
            return      EzcodxHtml::tag('div', array('id' => 'gd-profile-card'), $content);
        }

        protected function renderMiniStatisticsContent()
        {
            $percentageToNextLabel = Ezcodx::t('GamificationModule',
                                              '{percentage}% to Level {level}',
                                              array('{percentage}' => (int)$this->generalLevelData['nextLevelPercentageComplete'],
                                                    '{level}'      => (int)$this->generalLevelData['level'] + 1));

            $levelContent  = EzcodxHtml::tag('strong', array(), $this->generalLevelData['level']);
            $levelContent .= EzcodxHtml::tag('span', array(), Ezcodx::t('GamificationModule', 'Level'));
            $levelContent .= EzcodxHtml::tag('span', array(), $percentageToNextLabel);

            $content  = EzcodxHtml::tag('div', array('id'    => 'gd-mini-stats-chart-div'), $this->renderMiniStatisticsChart());
            $content .= EzcodxHtml::tag('div', array('class' => 'gd-level'), $levelContent);
            $badgeLabelContent = Ezcodx::t('GamificationModule', '<strong>{n}</strong> Badge|<strong>{n}</strong> Badges',
                                          array(count($this->badgeData)));
            $content .= EzcodxHtml::tag('div', array('class' => 'gd-num-badges'), $badgeLabelContent);
            $collectionLabelContent = Ezcodx::t('GamificationModule', '<strong>{n}</strong> Collection|<strong>{n}</strong> Collections',
                                               array($this->getCompletedCollectionCount()));
            $content .= EzcodxHtml::tag('div', array('class' => 'gd-num-collections'), $collectionLabelContent);
            return      EzcodxHtml::tag('div',  array('id'    => 'gd-mini-stats-card'), $content);
        }

        protected function renderMiniStatisticsChart()
        {
            $chartData = array(array('column' => 'level-undone',
                                     'value'  => 100 - (int)$this->generalLevelData['nextLevelPercentageComplete']),
                               array('column' => 'level-done',
                                     'value'  => (int)$this->generalLevelData['nextLevelPercentageComplete']));
            Yii::import('ext.amcharts.AmChartMaker');
            $amChart = new AmChartMaker();
            $amChart->data = $chartData;
            $amChart->id   =  'miniChart';
            $amChart->type = ChartRules::TYPE_DONUT_PROGRESSION;
            $amChart->addSerialGraph('value', 'column');
            $amChart->addSerialGraph('value', 'column');
            $javascript = $amChart->javascriptChart();
            Yii::app()->getClientScript()->registerScript(__CLASS__ . '-mini-chart', $javascript);
            $cClipWidget = new CClipWidget();
            $cClipWidget->beginClip("Chart");
            $cClipWidget->widget('application.core.widgets.AmChart', array(
                'id'        => 'miniChart',
                'height'    => '150px',
            ));
            $cClipWidget->endClip();
            return $cClipWidget->getController()->clips['Chart'];
        }

        protected function renderBadgesContent()
        {
            $content  = EzcodxHtml::tag('h2', array(), Ezcodx::t('GamificationModule', 'Badges Achieved'));
            if (empty($this->badgeData))
            {
                $content .= $this->renderEmptyBadgeContent();
            }
            else
            {
                $content .= $this->renderPopulatedBadgeContent();
            }
            return EzcodxHtml::tag('div', array('id' => 'gd-badges-list'), $content);
        }

        protected function renderEmptyBadgeContent()
        {
            $content  = EzcodxHtml::tag('span', array('class' => 'icon-empty'), '');
            $content .= Ezcodx::t('GamificationModule', 'No Achievements Found');
            return EzcodxHtml::tag('span', array('class' => 'empty type-achievements'), $content);
        }

        protected function renderPopulatedBadgeContent()
        {
            $content = '<ul>' . "\n";
            foreach ($this->badgeData as $badge)
            {
                $gameBadgeRulesClassName = $badge->type . 'GameBadgeRules';
                $value                   = $gameBadgeRulesClassName::getItemCountByGrade((int)$badge->grade);
                $badgeDisplayLabel       = $gameBadgeRulesClassName::getPassiveDisplayLabel($value);
                $badgeContent      = null;
                $badgeIconContent  = EzcodxHtml::tag('div',   array('class' => 'gloss'), '');
                $badgeIconContent .= EzcodxHtml::tag('strong',   array('class' => 'badge-icon',
                    'title' => $badgeDisplayLabel), '');
                $badgeIconContent .= EzcodxHtml::tag('span',   array('class' => 'badge-grade'), (int)$badge->grade);
                $badgeContent .= EzcodxHtml::tag('div',   array('class' => 'badge ' . $badge->type), $badgeIconContent);
                $badgeContent .= EzcodxHtml::tag('h3',   array(), $badgeDisplayLabel);
                $badgeContent .= EzcodxHtml::tag('span', array(),
                    DateTimeUtil::convertDbFormattedDateTimeToLocaleFormattedDisplay(
                        $badge->createdDateTime, 'long', null));
                $content      .= EzcodxHtml::tag('li',   array(), $badgeContent);
            }
            $content .= '</ul>' . "\n";
            return $content;
        }

        protected function renderLeaderboardContent()
        {
            $content  = EzcodxHtml::tag('h2', array(), Ezcodx::t('GamificationModule', 'Leaderboard Rankings'));
            foreach ($this->rankingData as $ranking)
            {
                $rankingContent  = EzcodxHtml::tag('strong', array(), $ranking['rank']);
                $rankingContent .= EzcodxHtml::tag('span', array(), $ranking['typeLabel']);
                $content .= EzcodxHtml::tag('div', array('class' => 'leaderboard-rank'), $rankingContent);
            }
            return      EzcodxHtml::tag('div', array('id' => 'gd-leaderboard', 'class' => 'clearfix'), $content);
        }

        protected function renderStatisticsContent()
        {
            $content = EzcodxHtml::tag('h2', array(), Ezcodx::t('GamificationModule', 'Overall Statistics'));
            $rows = '';
            foreach ($this->statisticsData as $statistics)
            {
                $statisticsContent  = EzcodxHtml::tag('h3', array(), $statistics['levelTypeLabel']);
                $statisticsContent .= EzcodxHtml::tag('span', array('class' => 'stat-level'), $statistics['level']);
                $pointsContent      = Ezcodx::t('GamificationModule', '{n}<em>Point</em>|{n}<em>Points</em>', array($statistics['points']));
                $statisticsContent .= EzcodxHtml::tag('span', array('class' => 'stat-points'), $pointsContent);
                $statisticsContent .= $this->renderPercentHolderContent((int)$statistics['nextLevelPercentageComplete']);
                $rows .= EzcodxHtml::tag('div', array('class' => 'stat-row'), $statisticsContent);
            }
            $content .= EzcodxHtml::tag('div', array('id' => 'gd-stats-wrapper'), $rows);
            return      EzcodxHtml::tag('div', array('id' => 'gd-statistics'), $content);
        }

        protected function renderCollectionsContent()
        {
            $content  = EzcodxHtml::link('&cedil;', '#', array('id' => 'nav-left', 'class' => 'nav-button'));
            $content .= $this->renderCollectionsCarouselWrapperAndContent();
            $content .= EzcodxHtml::link('&circ;', '#', array('id' => 'nav-right', 'class' => 'nav-button'));
            return      EzcodxHtml::tag('div', array('id' => 'gd-collections'), $content);
        }

        protected function renderCollectionsCarouselWrapperAndContent()
        {
            $collectionsListContent = null;
            foreach ($this->collectionData as $collection)
            {
                $collectionsListContent .= $this->renderCollectionContent($this->user, $collection);
            }
            $width = count($this->collectionData) * 285; //closed panel width.
            $content = EzcodxHtml::tag('div', array('id' => 'gd-carousel', 'style' => "width:" . $width . "px"), $collectionsListContent);
            return     EzcodxHtml::tag('div', array('id' => 'gd-carousel-wrapper'), $content);
        }

        protected static function renderCollectionItemsContent(User $user, GameCollection $collection,
                                                               GameCollectionRules $gameCollectionRules)
        {
            $itemTypesAndLabels = $gameCollectionRules->getItemTypesAndLabels();
            $content    = null;
            $canCollect = true;
            foreach ($collection->getItemsData() as $itemType => $quantityCollected)
            {
                $itemLabel               = $itemTypesAndLabels[$itemType];
                $collectionItemImagePath = $gameCollectionRules::makeMediumCOllectionItemImagePath($itemType);
                $itemContent = static::resolveLazyLoadImage($collectionItemImagePath, $itemLabel,
                                          array('class' => 'qtip-shadow', 'data-tooltip' => $itemLabel));
                $qtip = new EzcodxTip(array('options' => array('position' => array('my' => 'bottom center', 'at' => 'top center'),
                                                          'content'  => array('attr' => 'data-tooltip'))));
                $qtip->addQTip(".gd-collection-item img");
                $itemContent .= EzcodxHtml::tag('span', array('class' => 'num-collected'), 'x' . $quantityCollected);
                $classContent = 'gd-collection-item';
                if ($quantityCollected == 0)
                {
                    $classContent .= ' missing';
                    $canCollect = false;
                }
                $content .= EzcodxHtml::tag('div', array('class' => $classContent), $itemContent);
            }
            $itemRedeemContent = static::renderCompleteButton($collection->id, $user->id, $canCollect);
            $content           .= EzcodxHtml::tag('div', array('class' => 'gd-collection-item-redeemed'), $itemRedeemContent);
            return EzcodxHtml::tag('div', array('class' => 'gd-collection-items clearfix'), $content);
        }

        protected static function renderCompleteButton($collectionId, $userId, $canCollect = true)
        {
            assert('is_int($collectionId)');
            assert('is_int($userId)');
            assert('is_bool($canCollect)');
            $url           = Yii::app()->createUrl('gamification/default/redeemCollection/', array('id' => $collectionId));
            $htmlOptions   = array();
            $disabledClass = null;
            $disabled      = false;
            if (!$canCollect || $userId != Yii::app()->user->userModel->id)
            {
                $disabledClass = ' disabled';
                $disabled      = true;
            }
            $id                      = static::getCompleteCollectionLinkId($collectionId);
            $htmlOptions['id']       = $id;
            $htmlOptions['name']     = $id;
            $htmlOptions['class']    = 'attachLoading z-button coin-button' . $disabledClass;
            if ($disabled)
            {
                $htmlOptions['onclick']   = 'js:return false;';
            }
            else
            {
                $htmlOptions['onclick']   = 'js:$(this).addClass("loading").addClass("loading-ajax-submit");
                                                        $(this).makeOrRemoveLoadingSpinner(true, "#" + $(this).attr("id"), "#763d05");';
            }
            $aContent                = EzcodxHtml::wrapLink(Ezcodx::t('Core', 'Complete'));
            $containerId             = static::getCollectionContainerId($collectionId);
            return EzcodxHtml::ajaxLink($aContent, $url, array(
                'type'    => 'GET',
                'success' => 'js:function(data)
                    {
                        $("#' . $containerId . '").replaceWith(data);
                        $("#' . $containerId . '").addClass("visible-panel");
                        ' . self::renderGameCoinRefreshAjax($userId) . '
                        updateGamificationImagesSrcForLazyLoading();
                    }'
            ), $htmlOptions);
        }

        protected static function getCollectionContainerId($collectionId)
        {
            return self::COLLECTION_CONTAINER_ID_PREFIX . '-' . $collectionId;
        }

        protected static function getCompleteCollectionLinkId($collectionId)
        {
            return self::COMPLETE_COLLECTION_LINK_PREFIX . '-' . $collectionId;
        }

        protected function getCompletedCollectionCount()
        {
            $count = 0;
            foreach ($this->collectionData as $collection)
            {
                if ($collection->getRedemptionCount() > 0)
                {
                    $count++;
                }
            }
            return $count;
        }

        protected function getGameCoinForUser()
        {
            return GameCoin::resolveByPerson($this->user);
        }

        protected function renderPercentHolderContent($percentageComplete)
        {
            assert('is_int($percentageComplete)');
            $percentCompleteContent = EzcodxHtml::tag('span',
                array('class' => 'percentComplete z_' . $percentageComplete),
                      EzcodxHtml::tag('span', array('class' => 'percent'), $percentageComplete . '%'));
            return EzcodxHtml::tag('span', array('class' => 'percentHolder'), $percentCompleteContent);
        }

        protected static function renderGameCoinRefreshAjax($userId)
        {
            assert('is_int($userId)');
            return EzcodxHtml::ajax(array(
                'type' => 'GET',
                'url'  =>  static::getGameCoinRefreshUrl($userId),
                'success' => 'function(data){$("#' . self::getGameCoinContainerId() . '").replaceWith(data)}',
            ));
        }

        protected static function getGameCoinRefreshUrl($userId)
        {
            assert('is_int($userId)');
            return Yii::app()->createUrl('gamification/default/refreshGameDashboardCoinContainer', array('id' => $userId));
        }

        protected static function getGameCoinContainerId()
        {
            return 'gd-z-coins';
        }

        protected static function renderDashboardCloseButton()
        {
            return '<div class="close-dashboard-button"><a href="#"><span class="ui-icon ui-icon-closethick">close</span></a></div>';
        }

        protected static function getDummyImageUrl()
        {
            return PlaceholderImageUtil::resolveOneByOnePixelImageUrl(false);
        }

        protected static function resolveLazyLoadImage($source, $alt = '', $htmlOptions = array())
        {
            $dummyImageUrl = static::getDummyImageUrl();
            if ($source != $dummyImageUrl)
            {
                $htmlOptions['data-src']    = $source;
                $source                     = $dummyImageUrl;
            }
            return EzcodxHtml::image($source, $alt, $htmlOptions);
        }
    }
?>
