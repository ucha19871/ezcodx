<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * View when a user comes to redeem a reward.
     */
    class GameRewardsRedemptionIntroView extends IntroView
    {
        protected function renderIntroContent()
        {
            $content  = '<h1>' . Ezcodx::t('GameRewardsModule', 'How do Game Rewards work in Ezcodx?',
                         LabelUtil::getTranslationParamsForAllModules()). '</h1>';
            $content .= '<div id="game-rewards-redemption-intro-steps" class="module-intro-steps clearfix">';
            $content .= '<div class="third"><h3>' . Ezcodx::t('Core', 'Step') . '<strong>1<span>➜</span></strong></h3>';
            $content .= '<p><strong>' . Ezcodx::t('GameRewardsModule', 'Earn') . '</strong>';
            $content .= Ezcodx::t('GameRewardsModule', 'Collect coins as you level up, earn badges, ' .
                                 'and complete collections. You will also find coins as you use the application');
            $content .= '</p>';
            $content .= '</div>';
            $content .= '<div class="third"><h3>' . Ezcodx::t('Core', 'Step') . '<strong>2<span>➜</span></strong></h3>';
            $content .= '<p><strong>' . Ezcodx::t('EzcodxModule', 'Redeem') . '</strong>';
            $content .= Ezcodx::t('GameRewardsModule', 'Use coins to redeem rewards added by your manager and HR department');
            $content .= '</p>';
            $content .= '</div>';
            $content .= '<div class="third"><h3>' . Ezcodx::t('Core', 'Step') . '<strong>3<span>➜</span></strong></h3>';
            $content .= '<p><strong>' . Ezcodx::t('GameRewardsModule', 'Enjoy') . '</strong>';
            $content .= Ezcodx::t('GameRewardsModule', 'You work hard so you should enjoy your reward. ' .
                                 'Then keep using the application to redeem more rewards');
            $content .= '</p>';
            $content .= '</div>';
            $content .= '</div>';
            $this->registerScripts();
            return $content;
        }
    }
?>
