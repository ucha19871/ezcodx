<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    /**
     * View when a user first comes to the marketing dashboard. Provides an overview of how marketing works
     */
    class MarketingDashboardIntroView extends IntroView
    {
        protected function renderIntroContent()
        {
            $content  = '<h1>' . Ezcodx::t('MarketingModule', 'How does Email Marketing work in Ezcodx?', LabelUtil::getTranslationParamsForAllModules()). '</h1>';
            $content .= '<div id="marketing-intro-steps" class="module-intro-steps clearfix">';
            $content .= '<div class="third"><h3>' . Ezcodx::t('Core', 'Step') . '<strong>1<span>➜</span></strong></h3>';
            $content .= '<p><strong>' . Ezcodx::t('EzcodxModule', 'Group') . '</strong>';
            $content .= Ezcodx::t('MarketingModule', 'Group together the email recipients into a list, use different lists for different purposes');
            $content .= '</p>';
            $content .= '</div>';
            $content .= '<div class="third"><h3>' . Ezcodx::t('Core', 'Step') . '<strong>2<span>➜</span></strong></h3>';
            $content .= '<p><strong>' . Ezcodx::t('Core', 'Create') . '</strong>';
            $content .= Ezcodx::t('MarketingModule', 'Create the template for the email you are going to send, import and use either full, ' .
                        'rich HTML templates or plain text');
            $content .= '</p>';
            $content .= '</div>';
            $content .= '<div class="third"><h3>' . Ezcodx::t('Core', 'Step') . '<strong>3<span>➜</span></strong></h3>';
            $content .= '<p><strong>' . Ezcodx::t('MarketingModule', 'Launch') . '</strong>';
            $content .= Ezcodx::t('MarketingModule', 'Create a campaign where you can schedule your email to go out, pick the List(s) of recipients, ' .
                        'add and schedule autoresponders and track your overall campaign performance');
            $content .= '</p>';
            $content .= '</div>';
            $content .= '</div>';
            $this->registerScripts();
            return $content;
        }
    }
?>
