<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class MarketingModule extends SecurableModule
    {
        const RIGHT_ACCESS_CONFIGURATION    = 'Access Marketing Configuration';
        const RIGHT_ACCESS_MARKETING        = 'Access Marketing Tab';

        public function getDependencies()
        {
            return array(
                'configuration',
                'ezcodx',
            );
        }

        public static function getTranslatedRightsLabels()
        {
            $labels                                         = array();
            $labels[self::RIGHT_ACCESS_CONFIGURATION]       = Ezcodx::t('MarketingModule', 'Access Marketing Configuration');
            $labels[self::RIGHT_ACCESS_MARKETING]           = Ezcodx::t('MarketingModule', 'Access Marketing Tab');
            return $labels;
        }

        public static function getDefaultMetadata()
        {
            $metadata = array();
            $metadata['global'] = array(
                'tabMenuItems' => array(
                    array(
                        'label'  => "eval:Ezcodx::t('MarketingModule', 'Marketing')",
                        'url'    => array('/marketing/default/dashboardDetails'),
                        'mobile' => false,
                    ),
                ),
                'configureMenuItems' => array(
                    array(
                        'category'         => EzcodxModule::ADMINISTRATION_CATEGORY_GENERAL,
                        'titleLabel'       => "eval:Ezcodx::t('MarketingModule', 'Marketing Configuration')",
                        'descriptionLabel' => "eval:Ezcodx::t('MarketingModule', 'Manage Marketing Configuration')",
                        'route'            => '/marketing/default/configurationEdit',
                        'right'            => self::RIGHT_ACCESS_CONFIGURATION,
                    ),
                ),
            );
            return $metadata;
        }

        public static function getAccessRight()
        {
            return self::RIGHT_ACCESS_MARKETING;
        }

        protected static function getSingularModuleLabel($language)
        {
            return Ezcodx::t('MarketingModule', 'Marketing', array(), null, $language);
        }

        protected static function getPluralModuleLabel($language)
        {
            return static::getSingularModuleLabel($language);
        }
    }
?>