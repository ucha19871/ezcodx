<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class MarketingListDetailsView extends SecuredDetailsView
    {
        public static function assertModelIsValid($model)
        {
            assert('$model instanceof MarketingList');
        }

        public static function getDefaultMetadata()
        {
            $metadata = array(
                'global' => array(
                    'toolbar' => array(
                        'elements' => array(
                            array('type'        => 'MarketingListsDetailsMenu',
                                  'iconClass'   => 'icon-details',
                                  'id'          => 'ListViewDetailsActionMenu',
                                  'itemOptions' => array('class' => 'hasDetailsFlyout'),
                                  'model'       => 'eval:$this->model',
                            ),
                            array('type'        => 'MarketingListsOptionsMenu',
                                  'id'          => 'ListViewOptionsActionMenu',
                                  'iconClass'   => 'icon-edit',
                            ),
                        ),
                    ),
                ),
            );
            return $metadata;
        }

        public function getTitle()
        {
            return strval($this->model) . ' - ' . Ezcodx::t('Core', 'List');
        }

        protected function renderContent()
        {
            // TODO: @Shoaibi/@Jason: Low: Do security walkthrough
            $actionElementBarContent        = $this->renderActionElementBar(false);
            $content                        = $this->renderTitleContent();
            $content                       .= EzcodxHtml::tag('div', array('class' => 'view-toolbar-container clearfix'),
                                                EzcodxHtml::tag('nav', array('class' => 'pillbox clearfix'),
                                                                                    $actionElementBarContent));
            return $content;
        }
    }
?>