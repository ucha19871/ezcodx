<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    // KEEP these in alphabetical order.
    // KEEP them indented correctly.
    // KEEP all the language files up-to-date with each other.
    // DON'T MAKE A MESS!
    return array(
        '1 Column'
            => '1 Columna',
        '2 Columns'
            => '2 Columnas',
        '2 Columns Left Strong'
            => 'Izquierda',
        '2 Columns Right Strong'
            => 'Derecha',
        'Access Dashboards'
            => 'Acceso a Panel de Portlets',
        'Add Portlet'
            => 'Agregar Portlet',
        'Are you sure want to delete this dashboard?'
            => 'Está seguro que quiere eliminar está panel de portlets?',
        'Create Dashboard'
            => 'Crear un Panel de Portlets',
        'Create Dashboards'
            => 'Crear un Panel de Portlets',
        'Dashboard'
            => 'Panel de Portlets',
        'Delete Dashboard'
            => 'Eliminar Panel de Portlets',
        'Delete Dashboards'
            => 'Eliminar Panel de Portlets',
        'Don\'t show me this screen again'
            => 'No volver a mostrar esta ventana',
        'Don\'t worry you can turn it on again'
            => 'se puede regresar',
        'Edit Dashboard'
            => 'Editar el Panel de Portlets',
        'Go to the dashboard'
            => 'Continuar al dashboard',
        'Helpful Links'
            => 'Enlaces útiles',
        'Home'
            => 'Inicio',
        'Is Default'
            => 'Es predeterminado',
        'Join the forum'
            => 'Unirse al foro',
        'Layout Id'
            => 'Id de Diseño',
        'Layout Type'
            => 'Tipo de Diseño',
        'Next Tip'
            => 'Siguiente consejo',
        'Read the wiki'
            => 'Leer el wiki',
        'Return to Home'
            => 'Volver a Inicio',
        'Switch Dashboard'
            => 'Cambiar Dashboard',
        'Tip of the Day'
            => 'Sugerencia del día',
        'Using a CRM shouldn\'t be a chore. With Ezcodx, you can earn points, ' .
        'collect badges, and compete against co-workers while getting your job done.'
            => 'El uso de un CRM no debe ser una tarea. Con Ezcodx, usted puede ganar ' .
               'puntos, ganar insignias, y competir contra sus colegas al completar su trabajo.',
        'View a tutorial'
            => 'Ver un tutorial',
        'Watch a video'
            => 'Mirar un vídeo',
        'Welcome to Ezcodx'
            => 'Bienvenido a Ezcodx',
    );
?>
