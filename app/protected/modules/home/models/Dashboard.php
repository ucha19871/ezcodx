<?php
    /*********************************************************************************
     * Ezcodx is a customer relationship management program developed by
     * Ezcodx, Inc. Copyright (C) 2014 Ezcodx Inc.
     *
     * Ezcodx is free software; you can redistribute it and/or modify it under
     * the terms of the GNU Affero General Public License version 3 as published by the
     * Free Software Foundation with the addition of the following permission added
     * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
     * IN WHICH THE COPYRIGHT IS OWNED BY EZCODX, EZCODX DISCLAIMS THE WARRANTY
     * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
     *
     * Ezcodx is distributed in the hope that it will be useful, but WITHOUT
     * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
     * details.
     *
     * You should have received a copy of the GNU Affero General Public License along with
     * this program; if not, see http://www.gnu.org/licenses or write to the Free
     * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
     * 02110-1301 USA.
     *
     * You can contact Ezcodx, Inc. with a mailing address at 27 North Wacker Drive
     * Suite 370 Chicago, IL 60606. or at email address contact@ezcodx.com.
     *
     * The interactive user interfaces in original and modified versions
     * of this program must display Appropriate Legal Notices, as required under
     * Section 5 of the GNU Affero General Public License version 3.
     *
     * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
     * these Appropriate Legal Notices must retain the display of the Ezcodx
     * logo and Ezcodx copyright notice. If the display of the logo is not reasonably
     * feasible for technical reasons, the Appropriate Legal Notices must display the words
     * "Copyright Ezcodx Inc. 2014. All rights reserved".
     ********************************************************************************/

    class Dashboard extends OwnedSecurableItem
    {
        const DEFAULT_USER_LAYOUT_ID = 1;

        /**
         * @param int $layoutId
         * @param User $user
         * @throws NotFoundException
         */
        public static function getByLayoutIdAndUser($layoutId, $user)
        {
            assert('is_integer($layoutId) && $layoutId >= 1');
            assert('$user instanceof User && $user->id > 0');
            $sql = 'select dashboard.id id '            .
                   'from dashboard, ownedsecurableitem '                          .
                   'where ownedsecurableitem.owner__user_id = ' . $user->id       .
                   ' and dashboard.ownedsecurableitem_id = ownedsecurableitem.id '.
                   ' and layoutid = ' . $layoutId                                 .
                   ' order by layoutId;';
            $ids = EzcodxRedBean::getCol($sql);
            assert('count($ids) <= 1');
            if (count($ids) == 0)
            {
                if ($layoutId == Dashboard::DEFAULT_USER_LAYOUT_ID)
                {
                    return Dashboard::setDefaultDashboardForUser($user);
                }
                throw new NotFoundException();
            }
            $bean = EzcodxRedBean::load(Dashboard::getTableName(), $ids[0]);
            assert('$bean === false || $bean instanceof RedBean_OODBBean');
            if ($bean === false)
            {
                throw new NotFoundException();
            }
            return self::makeModel($bean);
        }

        /**
         * @param int $userId
         * @return array
         */
        public static function getRowsByUserId($userId)
        {
            assert('is_integer($userId) && $userId >= 1');
            $sql = 'select dashboard.id id, dashboard.name name, layoutid layoutId ' .
                   'from dashboard, ownedsecurableitem '                             .
                   'where ownedsecurableitem.owner__user_id = ' . $userId            .
                   ' and dashboard.ownedsecurableitem_id = ownedsecurableitem.id '   .
                   'order by layoutId;';
            return EzcodxRedBean::getAll($sql);
        }

        public static function getNextLayoutId()
        {
            return max(2, (int)EzcodxRedBean::getCell('select max(layoutId) + 1 from dashboard'));
        }

        protected static function translatedAttributeLabels($language)
        {
            return array_merge(parent::translatedAttributeLabels($language),
                array(
                    'layoutId'   => Ezcodx::t('HomeModule',  'Layout Id',   array(), null, $language),
                    'layoutType' => Ezcodx::t('HomeModule',  'Layout Type', array(), null, $language),
                    'isDefault'  => Ezcodx::t('HomeModule',  'Is Default',  array(), null, $language),
                    'name'       => Ezcodx::t('Core', 'Name',        array(), null, $language),
                )
            );
        }

        public function __toString()
        {
            try
            {
                if (trim($this->name) == '')
                {
                    return Ezcodx::t('Core', '(Unnamed)');
                }
                return $this->name;
            }
            catch (AccessDeniedSecurityException $e)
            {
                return '';
            }
        }

        public static function getDefaultMetadata()
        {
            $metadata = parent::getDefaultMetadata();
            $metadata[__CLASS__] = array(
                'members' => array(
                    'layoutId',
                    'layoutType',
                    'isDefault',
                    'name',
                ),
                'rules' => array(
                    array('isDefault',  'boolean'),
                    array('layoutId',   'required'),
                    array('layoutId',   'type',   'type' => 'integer'),
                    array('layoutType', 'required'),
                    array('layoutType', 'type',   'type' => 'string'),
                    array('layoutType', 'length', 'max' => 10),
                    array('name',       'required'),
                    array('name',       'type',   'type' => 'string'),
                    array('name',       'length', 'min' => 1, 'max' => 64),
                ),
                'defaultSortAttribute' => 'name'
            );
            return $metadata;
        }

        /**
         * Used to set the default dashboard information
         * for dashboard layoutId=1 for each user
         * @return Dashboard model.
         */
        private static function setDefaultDashboardForUser($user)
        {
            assert('$user instanceof User && $user->id > 0');
            $dashboard             = new Dashboard();
            $dashboard->name       = Ezcodx::t('EzcodxModule', 'Dashboard');
            $dashboard->layoutId   = Dashboard::DEFAULT_USER_LAYOUT_ID;
            $dashboard->owner      = $user;
            $dashboard->layoutType = '50,50'; // Not Coding Standard
            $dashboard->isDefault  = true;
            $saved                 = $dashboard->save();
            assert('$saved'); // TODO - deal with the properly.
            return $dashboard;
        }

        public static function isTypeDeletable()
        {
            return true;
        }

        public static function getModuleClassName()
        {
            return 'HomeModule';
        }

        /**
         * BEFORE ADDING TO THIS ARRAY - Remember to change the assertion in JuiPortlets:::init()
         */
        public static function getLayoutTypesData()
        {
            return array(
                '100'   => Ezcodx::t('HomeModule', '{number} Column', array('{number}' => 1)),
                '50,50' => Ezcodx::t('HomeModule', '{number} Columns', array('{number}' => 2)), // Not Coding Standard
                '75,25' => Ezcodx::t('HomeModule', '{number} Columns Left Strong', array('{number}' => 2)), // Not Coding Standard
            );
        }

        /**
         * Returns the display name for the model class.
         * @param null | string $language
         * @return dynamic label name based on module.
         */
        protected static function getLabel($language = null)
        {
            return Ezcodx::t('EzcodxModule', 'Dashboard', array(), null, $language);
        }

        /**
         * Returns the display name for plural of the model class.
         * @param null | string $language
         * @return dynamic label name based on module.
         */
        protected static function getPluralLabel($language = null)
        {
            return Ezcodx::t('EzcodxModule', 'Dashboards', array(), null, $language);
        }
    }
?>
