<?php
    $basePath = Yii::app()->getBasePath();
    require_once($basePath . '/core/adapters/EzcodxExtScriptToMinifyVendorAdapter.php');
    return EzcodxExtScriptToMinifyVendorAdapter::getConfig();
?>