-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2014 at 09:39 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ezcodx`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`admin`@`localhost` PROCEDURE `cache_securableitem_actual_permissions_for_permitable`(
                                in _securableitem_id  int(11),
                                in _permitable_id     int(11),
                                in _allow_permissions tinyint,
                                in _deny_permissions  tinyint
                              )
begin
                # Tables cannot be created inside stored routines
                # so this cannot automatically create the cache
                # table if it doesn't exist. So it is done when
                # the stored routines are created.
                insert into actual_permissions_cache
                values (_securableitem_id, _permitable_id, _allow_permissions, _deny_permissions);
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `clear_cache_all_actual_permissions`()
    READS SQL DATA
begin
                delete from actual_permissions_cache;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `clear_cache_securableitem_actual_permissions`(
                                in _securableitem_id int(11)
                              )
begin
                delete from actual_permissions_cache
                where securableitem_id = _securableitem_id;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `decrement_count`(
                                in munge_table_name  varchar(255),
                                in _securableitem_id int(11),
                                in item_id           int(11),
                                in _type             char
                             )
begin

                update munge_table_name
                set count = count - 1
                where securableitem_id = _securableitem_id and
                      munge_id         = concat(_type, item_id);
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `decrement_parent_roles_counts`(
                                in munge_table_name varchar(255),
                                in securableitem_id int(11),
                                in role_id          int(11)
                              )
begin
                declare parent_role_id int(11);

                select role_id
                into   parent_role_id
                from   role
                where  id = role_id;
                if parent_role_id is not null then
                    call decrement_count              (munge_table_name, securableitem_id, parent_role_id);
                    call decrement_parent_roles_counts(munge_table_name, securableitem_id, parent_role_id);
                end if;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `get_group_inherited_actual_right_ignoring_everyone`(
                                in  _group_id   int(11),
                                in  module_name varchar(255),
                                in  right_name  varchar(255),
                                out result      tinyint
                              )
begin
                declare parent_group_id int(11);

                set result = 0;
                select _group._group_id
                into   parent_group_id
                from   _group
                where  id = _group_id;
                if parent_group_id is not null then
                    call get_group_inherited_actual_right_ignoring_everyone(parent_group_id, module_name, right_name, result);
                    select result |
                           get_group_explicit_actual_right(parent_group_id, module_name, right_name)
                    into result;
                    if (result & 2) = 2 then
                        set result = 2;
                    end if;
                end if;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `get_securableitem_cached_actual_permissions_for_permitable`(
                                in  _securableitem_id  int(11),
                                in  _permitable_id     int(11),
                                out _allow_permissions tinyint,
                                out _deny_permissions  tinyint
                             )
begin
                select allow_permissions, deny_permissions
                into   _allow_permissions, _deny_permissions
                from   actual_permissions_cache
                where  securableitem_id = _securableitem_id and
                       permitable_id    = _permitable_id;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `get_securableitem_explicit_actual_permissions_for_permitable`(
                                in  _securableitem_id int(11),
                                in  _permitable_id    int(11),
                                out allow_permissions tinyint,
                                out deny_permissions  tinyint
                              )
begin
                select bit_or(permissions)
                into   allow_permissions
                from   permission
                where  type = 1                          and
                       permitable_id    = _permitable_id and
                       securableitem_id = _securableitem_id;

                select bit_or(permissions)
                into   deny_permissions
                from   permission
                where  type = 2                       and
                       permitable_id = _permitable_id and
                securableitem_id = _securableitem_id;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `get_securableitem_explicit_inherited_permissions_for_permitable`(
                                in  _securableitem_id int(11),
                                in  _permitable_id    int(11),
                                out allow_permissions tinyint,
                                out deny_permissions  tinyint
                              )
begin
                declare permissions_permitable_id int(11);
                declare _type, _permissions, permission_applies tinyint;
                declare no_more_records tinyint default 0;
                declare permitable_id_type_and_permissions cursor for
                    select permitable_id, type, bit_or(permissions)
                    from   permission
                    where  securableitem_id = _securableitem_id
                    group  by permitable_id, type;
                declare continue handler for not found
                    set no_more_records = 1;

                set allow_permissions = 0;
                set deny_permissions  = 0;
                open permitable_id_type_and_permissions;
                fetch permitable_id_type_and_permissions into
                            permissions_permitable_id, _type, _permissions;
                # The query will return at most one row with the allow bits and
                # one with the deny bits, so this loop will loop 0, 1, or 2 times.
                while no_more_records = 0 do
                    select permitable_contains_permitable(permissions_permitable_id, _permitable_id)
                    into   permission_applies;
                    if permission_applies then
                        if _type = 1 then
                            set allow_permissions = allow_permissions | _permissions;
                        else                                                    # Not Coding Standard
                            set deny_permissions  = deny_permissions  | _permissions;
                        end if;
                    end if;
                    fetch permitable_id_type_and_permissions into
                                permissions_permitable_id, _type, _permissions;
                end while;
                close permitable_id_type_and_permissions;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `get_securableitem_module_and_model_permissions_for_permitable`(
                                in  _securableitem_id int(11),
                                in  _permitable_id    int(11),
                                in  class_name        varchar(255),
                                in  module_name       varchar(255),
                                out allow_permissions tinyint,
                                out deny_permissions  tinyint
                               )
begin
                declare permissions_permitable_id int(11);
                declare _type, _permissions, permission_applies tinyint;
                declare no_more_records                         tinyint default 0;
                declare permitable_id_type_and_permissions_for_namedsecurableitem cursor for
                    select permitable_id, type, bit_or(permissions)
                    from   permission, namedsecurableitem
                    where  permission.securableitem_id = namedsecurableitem.securableitem_id and
                           (name = class_name or name = module_name)
                           group by permitable_id, type;
                declare continue handler for not found
                    set no_more_records = 1;

                set allow_permissions = 0;
                set deny_permissions  = 0;
                open permitable_id_type_and_permissions_for_namedsecurableitem;
                fetch permitable_id_type_and_permissions_for_namedsecurableitem into
                            permissions_permitable_id, _type, _permissions;
                # The query will return at most one row with the allow bits and
                # one with the deny bits, so this loop will loop 0, 1, or 2 times.
                while no_more_records = 0 do
                    select permitable_contains_permitable(permissions_permitable_id, _permitable_id)
                    into   permission_applies;
                    if permission_applies then
                        if _type = 1 then
                            set allow_permissions = allow_permissions | _permissions;
                        else                                                    # Not Coding Standard
                            set deny_permissions  = deny_permissions  | _permissions;
                        end if;
                    end if;
                    fetch permitable_id_type_and_permissions_for_namedsecurableitem into
                                permissions_permitable_id, _type, _permissions;
                end while;
                close permitable_id_type_and_permissions_for_namedsecurableitem;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `get_securableitem_propagated_allow_permissions_for_permitable`(
                                in  _securableitem_id int(11),
                                in  _permitable_id    int(11),
                                in  class_name        varchar(255),
                                in  module_name       varchar(255),
                                out allow_permissions tinyint
                              )
begin
                declare user_id int(11);
                declare user_role_id int(11);
                declare parent_role_id int(11);

                select role_id into user_role_id from _user where permitable_id = _permitable_id;
                set allow_permissions = 0;
                select get_permitable_user_id(_permitable_id)
                into   user_id;
                if user_id is not null then
                    call recursive_get_all_descendent_roles(_permitable_id, user_role_id);
                    call recursive_get_securableitem_propagated_allow_permissions_permit(_securableitem_id, _permitable_id, class_name, module_name, allow_permissions);
                end if;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `increment_count`(
                                in munge_table_name varchar(255),
                                in securableitem_id int(11),
                                in item_id          int(11),
                                in _type            char
                              )
begin
                # TODO: insert only if the row doesn't exist
                # in a way that doesn't ignore all errors.

                set @sql = concat("insert into ", munge_table_name,
                                  "(securableitem_id, munge_id, count) ",
                                  "values (", securableitem_id, ", '", concat(_type, item_id), "', 1) ",
                                  "on duplicate key ",
                                  "update count = count + 1");
                prepare statement from @sql;
                execute statement;
                deallocate prepare statement;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `increment_parent_roles_counts`(
                                in munge_table_name varchar(255),
                                in securableitem_id int(11),
                                in _role_id         int(11)
                              )
begin
                declare parent_role_id int(11);

                select role_id
                into   parent_role_id
                from   role
                where  id = _role_id;
                if parent_role_id is not null then
                    call increment_count              (munge_table_name, securableitem_id, parent_role_id, "R");
                    call increment_parent_roles_counts(munge_table_name, securableitem_id, parent_role_id);
                end if;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild`(
                                in model_table_name varchar(255),
                                in munge_table_name varchar(255)
                              )
begin
                call recreate_tables(munge_table_name);
                call rebuild_users  (model_table_name, munge_table_name);
                call rebuild_groups (model_table_name, munge_table_name);
                call rebuild_roles  (model_table_name, munge_table_name);
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_a_permitable`(
                                in munge_table_name varchar(255),
                                in securableitem_id int(11),
                                in actual_id        int(11),
                                in _permitable_id   int(11),
                                in _type            char
                              )
begin
                declare allow_permissions, deny_permissions, effective_explicit_permissions smallint default 0;

                call get_securableitem_explicit_actual_permissions_for_permitable(securableitem_id, _permitable_id, allow_permissions, deny_permissions);
                set effective_explicit_permissions = allow_permissions & ~deny_permissions;
                if (effective_explicit_permissions & 1) = 1 then # Permission::READ
                    call increment_count(munge_table_name, securableitem_id, actual_id, _type);
                    if _type = "G" then
                        call rebuild_roles_for_users_in_group(munge_table_name, securableitem_id, actual_id);
                        call rebuild_sub_groups              (munge_table_name, securableitem_id, actual_id);
                    end if;
                end if;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_groups`(
                                in model_table_name varchar(255),
                                in munge_table_name varchar(255)
                              )
begin
                set @select_statement  = concat("select permission.securableitem_id, _group.id, permission.permitable_id
                                         from ", model_table_name , ", ownedsecurableitem, permission, _group
                                         where
                                         ", model_table_name, ".ownedsecurableitem_id = ownedsecurableitem.id AND
                                         ownedsecurableitem.securableitem_id = permission.securableitem_id AND
                                         permission.permitable_id = _group.permitable_id");
                set @rebuild_groups_temp_table = CONCAT("create temporary table rebuild_temp_table as ", @select_statement);
                prepare statement FROM @rebuild_groups_temp_table;
                execute statement;
                deallocate prepare statement;
                begin
                    declare _securableitem_id, __group_id, _permitable_id int(11);
                    declare no_more_records tinyint default 0;
                    declare securableitem_group_and_permitable_ids cursor for
                        select * from rebuild_temp_table;
                    declare continue handler for not found
                        set no_more_records = 1;
                    open securableitem_group_and_permitable_ids;
                    fetch securableitem_group_and_permitable_ids into _securableitem_id, __group_id, _permitable_id;
                    while no_more_records = 0 do
                        call rebuild_a_permitable(munge_table_name, _securableitem_id, __group_id, _permitable_id, "G");
                        fetch securableitem_group_and_permitable_ids into _securableitem_id, __group_id, _permitable_id;
                    end while;
                    close securableitem_group_and_permitable_ids;
                    drop temporary table if exists rebuild_temp_table;
                end;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_roles`(
                                in model_table_name varchar(255),
                                in munge_table_name varchar(255)
                              )
begin
                call rebuild_roles_owned_securableitems                         (model_table_name, munge_table_name);
                call rebuild_roles_securableitem_with_explicit_user_permissions (model_table_name, munge_table_name);
                call rebuild_roles_securableitem_with_explicit_group_permissions(model_table_name, munge_table_name);
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_roles_for_users_in_group`(
                                in munge_table_name  varchar(255),
                                in _securableitem_id int(11),
                                in __group_id        int(11)
                              )
begin
                declare _role_id int(11);
                declare no_more_records tinyint default 0;
                declare role_ids cursor for
                    select role_id
                    from   _group__user, _user
                    where  _group__user._group_id = __group_id and
                           _user.id = _group__user._user_id;
                declare continue handler for not found
                    set no_more_records = 1;

                open role_ids;
                fetch role_ids into _role_id;
                while no_more_records = 0 do
                    call increment_parent_roles_counts(munge_table_name, _securableitem_id, _role_id);
                    fetch role_ids into _role_id;
                end while;
                close role_ids;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_roles_owned_securableitems`(
                                in model_table_name varchar(255),
                                in munge_table_name varchar(255)
                              )
begin
                set @select_statement  = concat("select role_id, ownedsecurableitem.securableitem_id
                                         from ", model_table_name, ", _user, ownedsecurableitem
                                         where ", model_table_name, ".ownedsecurableitem_id = ownedsecurableitem.id AND
                                         _user.id = ownedsecurableitem.owner__user_id and _user.role_id is not null");
                set @rebuild_roles_temp_table = CONCAT("create temporary table rebuild_temp_table as ", @select_statement);
                prepare statement FROM @rebuild_roles_temp_table;
                execute statement;
                deallocate prepare statement;
                   begin
                declare _role_id, _securableitem_id int(11);
                declare no_more_records tinyint default 0;
                declare role_and_securableitem_ids cursor for
                    select * from rebuild_temp_table;
                declare continue handler for not found
                    set no_more_records = 1;
                open role_and_securableitem_ids;
                fetch role_and_securableitem_ids into _role_id, _securableitem_id;
                while no_more_records = 0 do
                    call increment_parent_roles_counts(munge_table_name, _securableitem_id, _role_id);
                    fetch role_and_securableitem_ids into _role_id, _securableitem_id;
                end while;
                close role_and_securableitem_ids;
                drop temporary table if exists rebuild_temp_table;
                end;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_roles_securableitem_with_explicit_group_permissions`(
                                in model_table_name varchar(255),
                                in munge_table_name varchar(255)
                              )
begin
                set @select_statement  =  concat("select role.role_id, permission.securableitem_id
                                           from ", model_table_name, ", ownedsecurableitem, _user, _group, _group__user, permission, role
                                           where ", model_table_name, ".ownedsecurableitem_id = ownedsecurableitem.id and
                                           ownedsecurableitem.securableitem_id = permission.securableitem_id and
                                           _user.id = _group__user._user_id                and
                                           permission.permitable_id = _group.permitable_id and
                                           _group__user._group_id = _group.id              and
                                           _user.role_id = role.role_id                    and
                                           ((permission.permissions & 1) = 1)              and
                                           permission.type = 1");
                set @rebuild_roles_temp_table = CONCAT("create temporary table rebuild_temp_table as ", @select_statement);
                prepare statement FROM @rebuild_roles_temp_table;
                execute statement;
                deallocate prepare statement;
                begin
                    declare _role_id, _securableitem_id int(11);
                    declare no_more_records tinyint default 0;
                    declare role_and_securableitem_ids cursor for
                        select * from rebuild_temp_table;
                    declare continue handler for not found
                        set no_more_records = 1;
                    open role_and_securableitem_ids;
                    fetch role_and_securableitem_ids into _role_id, _securableitem_id;
                    while no_more_records = 0 do
                        call increment_count              (munge_table_name, _securableitem_id, _role_id, "R");
                        call increment_parent_roles_counts(munge_table_name, _securableitem_id, _role_id);
                        fetch role_and_securableitem_ids into _role_id, _securableitem_id;
                    end while;
                    close role_and_securableitem_ids;
                    drop temporary table if exists rebuild_temp_table;
                end;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_roles_securableitem_with_explicit_user_permissions`(
                                in model_table_name varchar(255),
                                in munge_table_name varchar(255)
                              )
begin
                set @select_statement  = concat("select role_id, permission.securableitem_id
                                         from ", model_table_name, ", ownedsecurableitem, permission, _user
                                         where ", model_table_name, ".ownedsecurableitem_id = ownedsecurableitem.id AND
                                         ownedsecurableitem.securableitem_id = permission.securableitem_id AND
                                         permission.permitable_id = _user.permitable_id and
                                         ((permission.permissions & 1) = 1) and permission.type = 1");
                set @rebuild_roles_temp_table = CONCAT("create temporary table rebuild_temp_table as ", @select_statement);
                prepare statement FROM @rebuild_roles_temp_table;
                execute statement;
                deallocate prepare statement;
                begin
                    declare _role_id, _securableitem_id int(11);
                    declare no_more_records tinyint default 0;
                    declare role_and_securableitem_ids cursor for
                        select * from rebuild_temp_table;
                    declare continue handler for not found
                        set no_more_records = 1;
                    open role_and_securableitem_ids;
                    fetch role_and_securableitem_ids into _role_id, _securableitem_id;
                    while no_more_records = 0 do
                        call increment_parent_roles_counts(munge_table_name, _securableitem_id, _role_id);
                        fetch role_and_securableitem_ids into _role_id, _securableitem_id;
                    end while;
                    close role_and_securableitem_ids;
                    drop temporary table if exists rebuild_temp_table;
                end;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_sub_groups`(
                                in munge_table_name  varchar(255),
                                in _securableitem_id int(11),
                                in __group_id        int(11)
                              )
begin
                declare sub_group_id int(11);
                declare no_more_records tinyint default 0;
                declare sub_group_ids cursor for
                    select id
                    from   _group
                    where  _group_id = __group_id;
                declare continue handler for not found
                    set no_more_records = 1;

                open sub_group_ids;
                fetch sub_group_ids into sub_group_id;
                while no_more_records = 0 do
                    call increment_count                 (munge_table_name, _securableitem_id, sub_group_id, "G");
                    call rebuild_roles_for_users_in_group(munge_table_name, _securableitem_id, sub_group_id);
                    call rebuild_sub_groups              (munge_table_name, _securableitem_id, sub_group_id);
                    fetch sub_group_ids into sub_group_id;
                end while;
                close sub_group_ids;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `rebuild_users`(
                                in model_table_name varchar(255),
                                in munge_table_name varchar(255)
                              )
begin
                set @select_statement  = concat("select permission.securableitem_id, _user.id, permission.permitable_id
                                         from ", model_table_name, ", ownedsecurableitem, permission, _user
                                         where ", model_table_name , ".ownedsecurableitem_id = ownedsecurableitem.id and
                                         ownedsecurableitem.securableitem_id = permission.securableitem_id and
                                         permission.permitable_id = _user.permitable_id");
                set @rebuild_users_temp_table = CONCAT("create temporary table rebuild_temp_table as ", @select_statement);
                prepare statement FROM @rebuild_users_temp_table;
                execute statement;
                deallocate prepare statement;
                begin
                    declare _securableitem_id, __user_id, _permitable_id int(11);
                    declare no_more_records tinyint default 0;
                    declare securableitem_user_and_permitable_ids cursor for
                        select * from rebuild_temp_table;
                    declare continue handler for not found
                        set no_more_records = 1;
                    open securableitem_user_and_permitable_ids;
                    fetch securableitem_user_and_permitable_ids into _securableitem_id, __user_id, _permitable_id;
                    while no_more_records = 0 do
                        call rebuild_a_permitable(munge_table_name, _securableitem_id, __user_id, _permitable_id, "U");
                        fetch securableitem_user_and_permitable_ids into _securableitem_id, __user_id, _permitable_id;
                    end while;
                    close securableitem_user_and_permitable_ids;
                    drop temporary table if exists rebuild_temp_table;
                end;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `recreate_tables`(
                                in munge_table_name varchar(255)
                              )
begin
                set @sql = concat("drop table if exists ", munge_table_name);
                prepare statement from @sql;
                execute statement;
                deallocate prepare statement;

                set @sql = concat("create table ", munge_table_name, " (",
                                        "securableitem_id      int(11)     unsigned not null, ",
                                        "munge_id              varchar(12)              null, ",
                                        "count                 int(8)      unsigned not null, ",
                                        "primary key (securableitem_id, munge_id))");
                prepare statement from @sql;
                execute statement;
                deallocate prepare statement;

                set @sql = concat("create index index_", munge_table_name, "_securableitem_id", " ",
                                        "on ", munge_table_name, " (securableitem_id)");
                prepare statement from @sql;
                execute statement;
                deallocate prepare statement;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `recursive_get_all_descendent_roles`(in _permitable_id int(11), in parent_role_id int(11))
begin
                declare child_role_id int(11);
                declare no_more_records tinyint default 0;
                declare child_role_ids cursor for
                    select id
                    from   role
                    where  role_id = parent_role_id;
                declare continue handler for not found
                    set no_more_records = 1;

                open child_role_ids;
                fetch child_role_ids into child_role_id;
                while no_more_records = 0 do
                    INSERT IGNORE INTO __role_children_cache VALUES (_permitable_id, child_role_id);
                    call recursive_get_all_descendent_roles(_permitable_id, child_role_id);
                    fetch child_role_ids into child_role_id;
                end while;
                close child_role_ids;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `recursive_get_securableitem_actual_permissions_for_permitable`(
                                in  _securableitem_id int(11),
                                in  _permitable_id    int(11),
                                in  class_name        varchar(255),
                                in  module_name       varchar(255),
                                out allow_permissions tinyint,
                                out deny_permissions  tinyint
                              )
begin
                declare propagated_allow_permissions                            tinyint default 0;
                declare nameditem_allow_permissions, nameditem_deny_permissions tinyint default 0;
                declare is_owner tinyint;
                begin
                    select _securableitem_id in
                        (select securableitem_id
                         from   _user, ownedsecurableitem
                         where  _user.id = ownedsecurableitem.owner__user_id and
                                permitable_id = _permitable_id)
                    into is_owner;
                end;
                if is_owner then
                    set allow_permissions = 31;
                    set deny_permissions  = 0;
                else                                                            # Not Coding Standard
                    set allow_permissions = 0;
                    set deny_permissions  = 0;
                    call get_securableitem_explicit_inherited_permissions_for_permitable(_securableitem_id, _permitable_id, allow_permissions, deny_permissions);
                    call get_securableitem_propagated_allow_permissions_for_permitable  (_securableitem_id, _permitable_id, class_name, module_name, propagated_allow_permissions);
                    call get_securableitem_module_and_model_permissions_for_permitable  (_securableitem_id, _permitable_id, class_name, module_name, nameditem_allow_permissions, nameditem_deny_permissions);
                    set allow_permissions = allow_permissions | propagated_allow_permissions | nameditem_allow_permissions;
                    set deny_permissions  = deny_permissions                                 | nameditem_deny_permissions;
                end if;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `recursive_get_securableitem_propagated_allow_permissions_permit`(
                                in  _securableitem_id int(11),
                                in  _permitable_id    int(11),
                                in  class_name        varchar(255),
                                in  module_name       varchar(255),
                                out allow_permissions tinyint
                              )
begin
                declare user_allow_permissions, user_deny_permissions, user_propagated_allow_permissions tinyint;

                set allow_permissions = 0;

                begin
                    declare sub_role_id int(11);
                    declare no_more_records tinyint default 0;
                    declare sub_role_ids cursor for
                        select role_id
                        from   __role_children_cache
                        where  permitable_id = _permitable_id;
                    declare continue handler for not found
                        begin
                            set no_more_records = 1;
                        end;

                    open sub_role_ids;
                    fetch sub_role_ids into sub_role_id;
                    while no_more_records = 0 do
                        begin
                            declare propagated_allow_permissions tinyint;
                            declare user_in_role_id, permitable_in_role_id int(11);
                            declare permitable_in_role_ids cursor for
                                select permitable_id
                                from   _user
                                where  role_id = sub_role_id;

                            open permitable_in_role_ids;
                            fetch permitable_in_role_ids into permitable_in_role_id;
                            while no_more_records = 0 do
                                call recursive_get_securableitem_actual_permissions_for_permitable  (_securableitem_id, permitable_in_role_id, class_name, module_name, user_allow_permissions, user_deny_permissions);
                                call recursive_get_securableitem_propagated_allow_permissions_permit(_securableitem_id, permitable_in_role_id, class_name, module_name, propagated_allow_permissions);
                                set allow_permissions =
                                        allow_permissions                                 |
                                        (user_allow_permissions & ~user_deny_permissions) |
                                        propagated_allow_permissions;
                                fetch permitable_in_role_ids into permitable_in_role_id;
                            end while;
                        end;
                        set no_more_records = 0;
                        fetch sub_role_ids into sub_role_id;
                    end while;
                    close sub_role_ids;
                end;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `recursive_get_user_actual_right`(
                                in  _user_id    int(11),
                                in  module_name varchar(255),
                                in  right_name  varchar(255),
                                out result      tinyint
                              )
begin
                declare _role_id tinyint;

                set result = 0;
                begin

                    select role_id
                    into   _role_id
                    from   _user
                    where  _user.id = _user_id;
                    if _role_id is not null then
                        call recursive_get_user_role_propagated_actual_allow_right(_role_id, module_name, right_name, result);
                        set result = result & 1;
                    end if;
                end;
                select get_user_explicit_actual_right (_user_id, module_name, right_name) |
                       get_user_inherited_actual_right(_user_id, module_name, right_name) |
                       result
                into result;

                if (result & 2) = 2 then
                    set result = 2;
                end if;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `recursive_get_user_role_propagated_actual_allow_right`(
                                in  _role_id    int(11),
                                in  module_name varchar(255),
                                in  right_name  varchar(255),
                                out result      tinyint
                              )
begin
                declare sub_role_id int(11);
                declare no_more_records tinyint default 0;
                declare sub_role_ids cursor for
                    select id
                    from   role
                    where  role.role_id = _role_id;
                declare continue handler for not found
                    set no_more_records = 1;

                set result = 0;
                open sub_role_ids;
                fetch sub_role_ids into sub_role_id;
                while result = 0 and no_more_records = 0 do
                  begin
                      declare _user_id int(11);
                      declare _user_ids cursor for
                          select id
                          from   _user
                          where  _user.role_id = sub_role_id;

                      open _user_ids;
                      fetch _user_ids into _user_id;
                      while result = 0 and no_more_records = 0 do
                          call recursive_get_user_actual_right(_user_id, module_name, right_name, result);
                          fetch _user_ids into _user_id;
                      end while;
                      close _user_ids;
                      if result = 0 then
                          call recursive_get_user_role_propagated_actual_allow_right(sub_role_id, module_name, right_name, result);
                      end if;
                      set no_more_records = 0;
                      fetch sub_role_ids into sub_role_id;
                  end;
                end while;
                close sub_role_ids;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `recursive_group_contains_group`(
                                in  group_id_1 int(11),
                                in  group_id_2 int(11),
                                out result     tinyint
                              )
begin
                declare group_2_parent_group_id, child_group_id int(11);
                declare no_more_records tinyint default 0;
                declare child_group_ids cursor for
                    select id
                    from   _group
                    where  _group._group_id = group_id_1;
                declare continue handler for not found
                    set no_more_records = 1;

                set result = 0;
                if group_id_1 = group_id_2 then
                    set result = 1;
                else                                                            # Not Coding Standard
                    select _group_id
                    into   group_2_parent_group_id
                    from   _group
                    where  id = group_id_2;
                    if group_id_1 = group_2_parent_group_id then
                        set result = 1;
                    else                                                        # Not Coding Standard
                        open child_group_ids;
                        fetch child_group_ids into child_group_id;
                        while result = 0 and no_more_records = 0 do
                            call recursive_group_contains_user(child_group_id, group_id_2, result);
                            fetch child_group_ids into child_group_id;
                        end while;
                        close child_group_ids;
                    end if;
                end if;
            end$$

CREATE DEFINER=`admin`@`localhost` PROCEDURE `recursive_group_contains_user`(
                                in  _group_id int(11),
                                in  _user_id  int(11),
                                out result    tinyint
                              )
begin
                declare child_group_id, count tinyint;
                declare no_more_records tinyint default 0;
                declare child_group_ids cursor for
                    select id
                    from   _group
                    where  _group._group_id = _group_id;
                declare continue handler for not found
                    set no_more_records = 1;

                set result = 0;
                select count(*)
                into count
                from _group__user
                where _group__user._group_id = _group_id and
                      _group__user._user_id  = _user_id;

                if count > 0 then
                    set result = 1;
                else                                                            # Not Coding Standard
                    open child_group_ids;
                    fetch child_group_ids into child_group_id;
                    while result = 0 and no_more_records = 0 do
                        call recursive_group_contains_user(child_group_id, _user_id, result);
                        fetch child_group_ids into child_group_id;
                    end while;
                    close child_group_ids;
                end if;
            end$$

--
-- Functions
--
CREATE DEFINER=`admin`@`localhost` FUNCTION `any_user_in_a_sub_role_has_read_permission`(
                                securableitem_id int(11),
                                role_id          int(11),
                                class_name       varchar(255),
                                module_name      varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare has_read tinyint default 0;

                call any_user_in_a_sub_role_has_read_permission(securableitem_id, role_id, class_name, module_name, has_read);
                return has_read;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_group_actual_right`(
                                _group_id   int(11),
                                module_name varchar(255),
                                right_name  varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint;

                select get_group_explicit_actual_right (_group_id, module_name, right_name) |
                       get_group_inherited_actual_right(_group_id, module_name, right_name)
                into result;
                if (result & 2) = 2 then
                    return 2;
                end if;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_group_explicit_actual_policy`(
                                _group_id   int(11),
                                module_name varchar(255),
                                policy_name varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint;
                declare _permitable_id tinyint;

                select permitable_id
                into   _permitable_id
                from   _group
                where  id = _group_id;
                select get_permitable_explicit_actual_policy(_permitable_id, module_name, policy_name)
                into result;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_group_explicit_actual_right`(
                                _group_id   int(11),
                                module_name varchar(255),
                                right_name  varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint;
                declare _permitable_id tinyint;

                select permitable_id
                into   _permitable_id
                from   _group
                where  id = _group_id;
                select get_permitable_explicit_actual_right(_permitable_id, module_name, right_name)
                into result;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_group_inherited_actual_right`(
                                _group_id   int(11),
                                module_name varchar(255),
                                right_name  varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare combined_right tinyint;

                call get_group_inherited_actual_right_ignoring_everyone(_group_id, module_name, right_name, combined_right);
                select combined_right |
                       get_named_group_explicit_actual_right('Everyone', module_name, right_name)
                into combined_right;
                if (combined_right & 2) = 2 then
                    return 2;
                end if;
                return combined_right;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_named_group_explicit_actual_policy`(
                                group_name  varchar(255),
                                module_name varchar(255),
                                policy_name varchar(255)
                             ) RETURNS varchar(255) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
begin                # but since PDO returns it as a string I am too, until I know if that is a bad thing.
                declare result tinyint;
                declare _permitable_id tinyint;

                select permitable_id
                into   _permitable_id
                from   _group
                where  name = group_name;
                select get_permitable_explicit_actual_policy(_permitable_id, module_name, policy_name)
                into result;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_named_group_explicit_actual_right`(
                                group_name  varchar(255),
                                module_name varchar(255),
                                right_name  varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint;
                declare _permitable_id tinyint;

                select permitable_id
                into   _permitable_id
                from   _group
                where  name = group_name;
                select get_permitable_explicit_actual_right(_permitable_id, module_name, right_name)
                into result;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_permitable_explicit_actual_policy`(
                                permitable_id int(11),
                                module_name   varchar(255),
                                policy_name   varchar(255)
                             ) RETURNS varchar(255) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
begin                # but since PDO returns it as a string I am too, until I know if that is a bad thing.
                declare result tinyint;

                select value
                into   result
                from   policy
                where  policy.modulename    = module_name and
                       name                 = policy_name and
                       policy.permitable_id = permitable_id
                limit  1;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_permitable_explicit_actual_right`(
                                permitable_id int(11),
                                module_name   varchar(255),
                                right_name    varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint;

                select max(type)
                into   result
                from   _right
                where  _right.modulename    = module_name and
                       name                 = right_name and
                       _right.permitable_id = permitable_id;
                if result is null then
                    return 0;
                end if;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_permitable_group_id`(
                                _permitable_id int(11)
                            ) RETURNS int(11)
begin
                declare result int(11);

                select id
                into   result
                from   _group
                where  _group.permitable_id = _permitable_id;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_permitable_user_id`(
                                _permitable_id int(11)
                            ) RETURNS int(11)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result int(11);

                select id
                into   result
                from   _user
                where  _user.permitable_id = _permitable_id;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_securableitem_actual_permissions_for_permitable`(
                                _securableitem_id int(11),
                                _permitable_id    int(11),
                                class_name        varchar(255),
                                module_name       varchar(255),
                                caching_on        tinyint
                             ) RETURNS smallint(6)
    READS SQL DATA
    DETERMINISTIC
begin
                declare allow_permissions, deny_permissions smallint default 0;
                declare is_super_administrator, is_owner tinyint;

                delete from __role_children_cache;

                select named_group_contains_permitable('Super Administrators', _permitable_id)
                into is_super_administrator;
                if is_super_administrator then
                    set allow_permissions = 31;
                    set deny_permissions  = 0;
                else                                                            # Not Coding Standard
                    begin
                        select _securableitem_id in
                            (select securableitem_id
                             from   _user, ownedsecurableitem
                             where  _user.id = ownedsecurableitem.owner__user_id and
                                    permitable_id = _permitable_id)
                        into is_owner;
                    end;
                    if is_owner then
                        set allow_permissions = 31;
                        set deny_permissions  = 0;
                    else                                                        # Not Coding Standard
                        if caching_on then
                            call get_securableitem_cached_actual_permissions_for_permitable(_securableitem_id, _permitable_id, allow_permissions, deny_permissions);
                            if allow_permissions is null then
                                call recursive_get_securableitem_actual_permissions_for_permitable(_securableitem_id, _permitable_id, class_name, module_name, allow_permissions, deny_permissions);
                                call cache_securableitem_actual_permissions_for_permitable(_securableitem_id, _permitable_id, allow_permissions, deny_permissions);
                            end if;
                        else                                                    # Not Coding Standard
                            call recursive_get_securableitem_actual_permissions_for_permitable(_securableitem_id, _permitable_id, class_name, module_name, allow_permissions, deny_permissions);
                        end if;
                    end if;
                end if;
                return (allow_permissions << 8) | deny_permissions;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_user_actual_right`(
                                _user_id    int(11),
                                module_name varchar(255),
                                right_name  varchar(255)
                            ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint;
                declare is_super_administrator tinyint;

                select named_group_contains_user('Super Administrators', _user_id)
                into   is_super_administrator;
                if is_super_administrator then
                    set result = 1;
                else                                                            # Not Coding Standard
                    call recursive_get_user_actual_right(_user_id, module_name, right_name, result);
                end if;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_user_explicit_actual_policy`(
                                _user_id    int(11),
                                module_name varchar(255),
                                policy_name varchar(255)
                             ) RETURNS varchar(255) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
begin                # but since PDO returns it as a string I am too, until I know if that is a bad thing.
                declare result tinyint;
                declare _permitable_id tinyint;

                select permitable_id
                into   _permitable_id
                from   _user
                where  id = _user_id;
                select get_permitable_explicit_actual_policy(_permitable_id, module_name, policy_name)
                into result;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_user_explicit_actual_right`(
                                _user_id    int(11),
                                module_name varchar(255),
                                right_name  varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint;
                declare _permitable_id tinyint;

                select permitable_id
                into   _permitable_id
                from   _user
                where  id = _user_id;
                select get_permitable_explicit_actual_right(_permitable_id, module_name, right_name)
                into result;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `get_user_inherited_actual_right`(
                                _user_id    int(11),
                                module_name varchar(255),
                                right_name  varchar(255)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare combined_right tinyint default 0;
                declare __group_id int(11);
                declare no_more_records tinyint default 0;
                declare _group_ids cursor for
                    select _group_id
                    from   _group__user
                    where  _group__user._user_id = _user_id;
                declare continue handler for not found
                    set no_more_records = 1;

                open _group_ids;
                fetch _group_ids into __group_id;
                while no_more_records = 0 do
                    select combined_right |
                           get_group_explicit_actual_right (__group_id, module_name, right_name) |
                           get_group_inherited_actual_right(__group_id, module_name, right_name)
                    into combined_right;
                    fetch _group_ids into __group_id;
                end while;
                close _group_ids;

                select combined_right |
                       get_named_group_explicit_actual_right('Everyone', module_name, right_name)
                into combined_right;

                if (combined_right & 2) = 2 then
                    return 2;
                end if;
                return combined_right;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `group_contains_permitable`(
                                _group_id      int(11),
                                _permitable_id int(11)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint default 0;
                declare _group_name varchar(255);
                declare is_everyone tinyint;
                declare _user_id int(11);
                declare group_id_2 int(11);

                select name
                into   _group_name
                from   _group
                where  _group.id = _group_id;
                if _group_name = 'Everyone' then
                    set result = 1;
                else                                                            # Not Coding Standard
                    set _user_id = get_permitable_user_id(_permitable_id);
                    if _user_id is not null then
                        call recursive_group_contains_user(_group_id, _user_id, result);
                    else                                                        # Not Coding Standard
                        set group_id_2 = get_permitable_group_id(_permitable_id);
                        if group_id_2 is not null then
                            call recursive_group_contains_group(_group_id, group_id_2, result);
                        end if;
                    end if;
                end if;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `group_contains_user`(
                                _group_id int(11),
                                _user_id  int(11)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint default 0;

                call recursive_group_contains_user(_group_id, _user_id, result);
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `named_group_contains_permitable`(
                                group_name     varchar(255),
                                _permitable_id int(11)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint default 0;
                declare group_id_1 int(11);
                declare _user_id   int(11);
                declare group_id_2 int(11);

                if group_name = 'Everyone' then
                    set result = 1;
                else                                                            # Not Coding Standard
                    select id
                    into   group_id_1
                    from   _group
                    where  _group.name = group_name;
                    set _user_id = get_permitable_user_id(_permitable_id);
                    if _user_id is not null then
                        call recursive_group_contains_user(group_id_1, _user_id, result);
                    else                                                        # Not Coding Standard
                        set group_id_2 = get_permitable_group_id(_permitable_id);
                        if group_id_2 is not null then
                            call recursive_group_contains_group(group_id_1, group_id_2, result);
                        end if;
                    end if;
                end if;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `named_group_contains_user`(
                                _group_name varchar(255),
                                _user_id    int(11)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint default 0;
                declare _group_id int(11);

                if _group_name = 'Everyone' then
                    set result = 1;
                else                                                            # Not Coding Standard
                    select id
                    into   _group_id
                    from   _group
                    where  _group.name = _group_name;
                    call recursive_group_contains_user(_group_id, _user_id, result);
                end if;
                return result;
            end$$

CREATE DEFINER=`admin`@`localhost` FUNCTION `permitable_contains_permitable`(
                                permitable_id_1 int(11),
                                permitable_id_2 int(11)
                             ) RETURNS tinyint(4)
    READS SQL DATA
    DETERMINISTIC
begin
                declare result tinyint;
                declare user_id_1, user_id_2, group_id_1, group_id_2 int(11);

                # If they are both users just compare if they are the same user.
                select get_permitable_user_id(permitable_id_1)
                into   user_id_1;
                select get_permitable_user_id(permitable_id_2)
                into   user_id_2;
                if user_id_1 is not null and user_id_2 is not null then
                    set result = permitable_id_1 = permitable_id_2;
                else                                                            # Not Coding Standard
                    # If the first is a user and the second is a group return false.
                    select get_permitable_group_id(permitable_id_2)
                    into   group_id_2;
                    if user_id_1 is not null and group_id_2 is not null then
                        set result = 0;
                    else                                                        # Not Coding Standard
                        # Otherwise the first is a group, just return if it contains
                        # the second.
                        select get_permitable_group_id(permitable_id_1)
                        into   group_id_1;
                        if group_id_1 is not null then
                            select group_contains_permitable(group_id_1, permitable_id_2)
                            into result;
                        end if;
                    end if;
                end if;
                return result;
            end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `annualrevenue` double DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `employees` int(11) DEFAULT NULL,
  `latestactivitydatetime` datetime DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `officephone` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `officefax` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `account_id` int(11) unsigned DEFAULT NULL,
  `billingaddress_address_id` int(11) unsigned DEFAULT NULL,
  `industry_customfield_id` int(11) unsigned DEFAULT NULL,
  `primaryemail_email_id` int(11) unsigned DEFAULT NULL,
  `secondaryemail_email_id` int(11) unsigned DEFAULT NULL,
  `shippingaddress_address_id` int(11) unsigned DEFAULT NULL,
  `type_customfield_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `annualrevenue`, `description`, `employees`, `latestactivitydatetime`, `name`, `officephone`, `officefax`, `website`, `ownedsecurableitem_id`, `account_id`, `billingaddress_address_id`, `industry_customfield_id`, `primaryemail_email_id`, `secondaryemail_email_id`, `shippingaddress_address_id`, `type_customfield_id`) VALUES
(1, 0, '', 0, NULL, 'ეწფსდფსდფდს', '', '', '', 4, NULL, 4, 5, NULL, NULL, 5, 6),
(2, 0, '', 0, NULL, 'fdsfsfdsfsdf', '', '', '', 5, NULL, 6, 7, NULL, NULL, 7, 8),
(3, 0, '', 0, NULL, 'fdsfsfdsfsdghsdgdsg', '', '', '', 6, NULL, 8, 9, NULL, NULL, 9, 10);

-- --------------------------------------------------------

--
-- Table structure for table `accountaccountaffiliation`
--

CREATE TABLE IF NOT EXISTS `accountaccountaffiliation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) unsigned DEFAULT NULL,
  `primaryaccountaffiliation_account_id` int(11) unsigned DEFAULT NULL,
  `secondaryaccountaffiliation_account_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `accountcontactaffiliation`
--

CREATE TABLE IF NOT EXISTS `accountcontactaffiliation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `primary` tinyint(1) unsigned DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `role_customfield_id` int(11) unsigned DEFAULT NULL,
  `accountaffiliation_account_id` int(11) unsigned DEFAULT NULL,
  `contactaffiliation_contact_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `accountstarred`
--

CREATE TABLE IF NOT EXISTS `accountstarred` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `basestarredmodel_id` int(11) unsigned DEFAULT NULL,
  `account_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `basestarredmodel_id_account_id` (`basestarredmodel_id`,`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `account_project`
--

CREATE TABLE IF NOT EXISTS `account_project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned DEFAULT NULL,
  `project_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_tcejorp_di_tnuocca` (`account_id`,`project_id`),
  KEY `di_tnuocca` (`account_id`),
  KEY `di_tcejorp` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `account_read`
--

CREATE TABLE IF NOT EXISTS `account_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `account_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `account_read`
--

INSERT INTO `account_read` (`id`, `securableitem_id`, `munge_id`, `count`) VALUES
(1, 4, 'G2', 1),
(2, 5, 'G2', 1),
(3, 6, 'G2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `account_read_subscription`
--

CREATE TABLE IF NOT EXISTS `account_read_subscription` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `modelid` int(11) unsigned NOT NULL,
  `modifieddatetime` datetime DEFAULT NULL,
  `subscriptiontype` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userid_modelid` (`userid`,`modelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activelanguage`
--

CREATE TABLE IF NOT EXISTS `activelanguage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nativename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activationdatetime` datetime DEFAULT NULL,
  `lastupdatedatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `activelanguage`
--

INSERT INTO `activelanguage` (`id`, `code`, `name`, `nativename`, `activationdatetime`, `lastupdatedatetime`) VALUES
(5, 'ka', 'Georgian', 'Georgian', '2014-04-15 12:49:12', '2014-04-24 09:48:45');

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `latestdatetime` datetime DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activity_item`
--

CREATE TABLE IF NOT EXISTS `activity_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) unsigned DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_meti_di_ytivitca` (`activity_id`,`item_id`),
  KEY `di_ytivitca` (`activity_id`),
  KEY `di_meti` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `actual_permissions_cache`
--

CREATE TABLE IF NOT EXISTS `actual_permissions_cache` (
  `securableitem_id` int(11) unsigned NOT NULL,
  `permitable_id` int(11) unsigned NOT NULL,
  `allow_permissions` tinyint(3) unsigned NOT NULL,
  `deny_permissions` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`securableitem_id`,`permitable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `city` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalid` tinyint(1) unsigned DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `postalcode` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street1` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street2` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `city`, `country`, `invalid`, `latitude`, `longitude`, `postalcode`, `street1`, `street2`, `state`) VALUES
(1, '', '', NULL, NULL, NULL, '', '', '', ''),
(2, '', '', NULL, NULL, NULL, '', '', '', ''),
(3, '', '', NULL, NULL, NULL, '', '', '', ''),
(4, '', '', NULL, NULL, NULL, '', '', '', ''),
(5, '', '', NULL, NULL, NULL, '', '', '', ''),
(6, '', '', NULL, NULL, NULL, '', '', '', ''),
(7, '', '', NULL, NULL, NULL, '', '', '', ''),
(8, '', '', NULL, NULL, NULL, '', '', '', ''),
(9, '', '', NULL, NULL, NULL, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `auditevent`
--

CREATE TABLE IF NOT EXISTS `auditevent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `eventname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modulename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modelclassname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modelid` int(11) DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=184 ;

--
-- Dumping data for table `auditevent`
--

INSERT INTO `auditevent` (`id`, `datetime`, `eventname`, `modulename`, `modelclassname`, `modelid`, `serializeddata`, `_user_id`) VALUES
(1, '2014-04-09 15:27:09', 'Item Created', 'EzcodxModule', 'User', 1, 's:10:"Super User";', 1),
(2, '2014-04-09 15:27:09', 'User Password Changed', 'UsersModule', 'User', 1, 's:5:"super";', 1),
(3, '2014-04-09 15:27:10', 'Item Created', 'EzcodxModule', 'Group', 1, 's:20:"Super Administrators";', 1),
(4, '2014-04-09 15:27:15', 'Item Created', 'EzcodxModule', 'Group', 2, 's:8:"Everyone";', 1),
(5, '2014-04-09 15:27:16', 'Item Created', 'EzcodxModule', 'User', 2, 's:11:"System User";', 1),
(6, '2014-04-09 15:27:16', 'User Password Changed', 'UsersModule', 'User', 2, 's:22:"backendjoboractionuser";', 1),
(7, '2014-04-09 15:27:17', 'Item Created', 'EzcodxModule', 'NotificationMessage', 1, 's:6:"(None)";', 1),
(8, '2014-04-09 15:27:17', 'Item Created', 'EzcodxModule', 'Notification', 1, 's:52:"Remove the api test entry script for production use.";', 1),
(9, '2014-04-09 15:27:18', 'Item Created', 'EzcodxModule', 'Notification', 2, 's:52:"Remove the api test entry script for production use.";', 1),
(10, '2014-04-09 15:30:01', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(11, '2014-04-09 15:30:01', 'Item Created', 'EzcodxModule', 'GameScore', 1, 's:9:"LoginUser";', 1),
(12, '2014-04-09 15:30:01', 'Item Created', 'EzcodxModule', 'GamePoint', 1, 's:12:"UserAdoption";', 1),
(13, '2014-04-09 15:30:02', 'Item Created', 'EzcodxModule', 'GameBadge', 1, 's:11:"LoginUser 1";', 1),
(14, '2014-04-09 15:30:47', 'Item Modified', 'EzcodxModule', 'User', 1, 'a:4:{i:0;s:10:"Super User";i:1;a:1:{i:0;s:8:"timeZone";}i:2;s:15:"America/Chicago";i:3;s:12:"Asia/Tbilisi";}', 1),
(15, '2014-04-09 17:11:44', 'Item Created', 'EzcodxModule', 'NotificationMessage', 2, 's:6:"(None)";', 1),
(16, '2014-04-09 17:11:44', 'Item Created', 'EzcodxModule', 'Notification', 3, 's:44:"Clear the assets folder on server(optional).";', 1),
(17, '2014-04-09 17:11:45', 'Item Created', 'EzcodxModule', 'Notification', 4, 's:44:"Clear the assets folder on server(optional).";', 1),
(18, '2014-04-09 17:25:13', 'Item Created', 'EzcodxModule', 'Dashboard', 1, 's:9:"Dashboard";', 1),
(19, '2014-04-09 17:25:15', 'Item Created', 'EzcodxModule', 'EmailFolder', 1, 's:5:"Draft";', 1),
(20, '2014-04-09 17:25:16', 'Item Created', 'EzcodxModule', 'EmailFolder', 2, 's:4:"Sent";', 1),
(21, '2014-04-09 17:25:16', 'Item Created', 'EzcodxModule', 'EmailFolder', 3, 's:6:"Outbox";', 1),
(22, '2014-04-09 17:25:16', 'Item Created', 'EzcodxModule', 'EmailFolder', 4, 's:12:"Outbox Error";', 1),
(23, '2014-04-09 17:25:16', 'Item Created', 'EzcodxModule', 'EmailFolder', 5, 's:14:"Outbox Failure";', 1),
(24, '2014-04-09 17:25:16', 'Item Created', 'EzcodxModule', 'EmailFolder', 6, 's:5:"Inbox";', 1),
(25, '2014-04-09 17:25:16', 'Item Created', 'EzcodxModule', 'EmailFolder', 7, 's:8:"Archived";', 1),
(26, '2014-04-09 17:25:16', 'Item Created', 'EzcodxModule', 'EmailFolder', 8, 's:18:"Archived Unmatched";', 1),
(27, '2014-04-09 17:25:16', 'Item Created', 'EzcodxModule', 'EmailBox', 1, 's:20:"System Notifications";', 1),
(28, '2014-04-10 00:45:28', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(29, '2014-04-10 03:12:57', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(30, '2014-04-10 03:13:28', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(31, '2014-04-10 03:13:28', 'Item Created', 'EzcodxModule', 'GameScore', 2, 's:8:"NightOwl";', 1),
(32, '2014-04-10 03:13:29', 'Item Created', 'EzcodxModule', 'GameBadge', 2, 's:10:"NightOwl 1";', 1),
(33, '2014-04-10 03:14:26', 'Item Created', 'EzcodxModule', 'GameCollection', 1, 's:7:"Airport";', 1),
(34, '2014-04-10 03:14:26', 'Item Created', 'EzcodxModule', 'GameCollection', 2, 's:10:"Basketball";', 1),
(35, '2014-04-10 03:14:27', 'Item Created', 'EzcodxModule', 'GameCollection', 3, 's:7:"Bicycle";', 1),
(36, '2014-04-10 03:14:27', 'Item Created', 'EzcodxModule', 'GameCollection', 4, 's:9:"Breakfast";', 1),
(37, '2014-04-10 03:14:27', 'Item Created', 'EzcodxModule', 'GameCollection', 5, 's:8:"Business";', 1),
(38, '2014-04-10 03:14:27', 'Item Created', 'EzcodxModule', 'GameCollection', 6, 's:8:"Camping2";', 1),
(39, '2014-04-10 03:14:27', 'Item Created', 'EzcodxModule', 'GameCollection', 7, 's:7:"Camping";', 1),
(40, '2014-04-10 03:14:28', 'Item Created', 'EzcodxModule', 'GameCollection', 8, 's:8:"CarParts";', 1),
(41, '2014-04-10 03:14:28', 'Item Created', 'EzcodxModule', 'GameCollection', 9, 's:21:"ChildrenPlayEquipment";', 1),
(42, '2014-04-10 03:14:28', 'Item Created', 'EzcodxModule', 'GameCollection', 10, 's:6:"Circus";', 1),
(43, '2014-04-10 03:14:28', 'Item Created', 'EzcodxModule', 'GameCollection', 11, 's:7:"Cooking";', 1),
(44, '2014-04-10 03:14:28', 'Item Created', 'EzcodxModule', 'GameCollection', 12, 's:6:"Drinks";', 1),
(45, '2014-04-10 03:14:28', 'Item Created', 'EzcodxModule', 'GameCollection', 13, 's:9:"Education";', 1),
(46, '2014-04-10 03:14:28', 'Item Created', 'EzcodxModule', 'GameCollection', 14, 's:7:"Finance";', 1),
(47, '2014-04-10 03:14:29', 'Item Created', 'EzcodxModule', 'GameCollection', 15, 's:7:"Fitness";', 1),
(48, '2014-04-10 03:14:29', 'Item Created', 'EzcodxModule', 'GameCollection', 16, 's:4:"Food";', 1),
(49, '2014-04-10 03:14:29', 'Item Created', 'EzcodxModule', 'GameCollection', 17, 's:4:"Golf";', 1),
(50, '2014-04-10 03:14:29', 'Item Created', 'EzcodxModule', 'GameCollection', 18, 's:6:"Health";', 1),
(51, '2014-04-10 03:14:29', 'Item Created', 'EzcodxModule', 'GameCollection', 19, 's:4:"Home";', 1),
(52, '2014-04-10 03:14:30', 'Item Created', 'EzcodxModule', 'GameCollection', 20, 's:5:"Hotel";', 1),
(53, '2014-04-10 03:14:30', 'Item Created', 'EzcodxModule', 'GameCollection', 21, 's:6:"Office";', 1),
(54, '2014-04-10 03:14:30', 'Item Created', 'EzcodxModule', 'GameCollection', 22, 's:6:"Racing";', 1),
(55, '2014-04-10 03:14:30', 'Item Created', 'EzcodxModule', 'GameCollection', 23, 's:7:"Science";', 1),
(56, '2014-04-10 03:14:31', 'Item Created', 'EzcodxModule', 'GameCollection', 24, 's:6:"Soccer";', 1),
(57, '2014-04-10 03:14:31', 'Item Created', 'EzcodxModule', 'GameCollection', 25, 's:11:"SocialMedia";', 1),
(58, '2014-04-10 03:14:31', 'Item Created', 'EzcodxModule', 'GameCollection', 26, 's:12:"SummerBeach2";', 1),
(59, '2014-04-10 03:14:31', 'Item Created', 'EzcodxModule', 'GameCollection', 27, 's:12:"SummerBeach3";', 1),
(60, '2014-04-10 03:14:31', 'Item Created', 'EzcodxModule', 'GameCollection', 28, 's:11:"SummerBeach";', 1),
(61, '2014-04-10 03:14:32', 'Item Created', 'EzcodxModule', 'GameCollection', 29, 's:7:"Traffic";', 1),
(62, '2014-04-10 03:14:32', 'Item Created', 'EzcodxModule', 'GameCollection', 30, 's:14:"Transportation";', 1),
(63, '2014-04-10 03:14:32', 'Item Created', 'EzcodxModule', 'GameCollection', 31, 's:13:"TravelHoliday";', 1),
(64, '2014-04-10 03:45:50', 'Item Created', 'EzcodxModule', 'GameCoin', 1, 's:6:"1 coin";', 1),
(65, '2014-04-10 11:27:02', 'Item Modified', 'EzcodxModule', 'Dashboard', 1, 'a:4:{i:0;s:21:"მთავარი";i:1;a:1:{i:0;s:4:"name";}i:2;s:9:"Dashboard";i:3;s:21:"მთავარი";}', 1),
(66, '2014-04-10 14:00:07', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(67, '2014-04-10 15:03:06', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(68, '2014-04-14 02:28:11', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(69, '2014-04-14 07:15:08', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(70, '2014-04-14 07:29:51', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(71, '2014-04-14 07:30:10', 'Item Modified', 'EzcodxModule', 'User', 1, 'a:4:{i:0;s:10:"Super User";i:1;a:1:{i:0;s:6:"locale";}i:2;N;i:3;s:5:"nl_nl";}', 1),
(72, '2014-04-14 07:30:11', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(73, '2014-04-14 07:30:20', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(74, '2014-04-14 07:30:24', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(75, '2014-04-14 07:30:28', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(76, '2014-04-14 07:30:39', 'Item Modified', 'EzcodxModule', 'User', 1, 'a:4:{i:0;s:10:"Super User";i:1;a:1:{i:0;s:6:"locale";}i:2;s:5:"nl_nl";i:3;s:5:"de_de";}', 1),
(77, '2014-04-14 07:30:40', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(78, '2014-04-14 07:31:02', 'Item Modified', 'EzcodxModule', 'User', 1, 'a:4:{i:0;s:10:"Super User";i:1;a:1:{i:0;s:6:"locale";}i:2;s:5:"de_de";i:3;s:0:"";}', 1),
(79, '2014-04-14 07:31:03', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(80, '2014-04-14 07:31:33', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(81, '2014-04-14 07:33:20', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(82, '2014-04-15 10:36:58', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(83, '2014-04-15 10:37:27', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(84, '2014-04-15 10:37:27', 'Item Created', 'EzcodxModule', 'GameScore', 3, 's:9:"EarlyBird";', 1),
(85, '2014-04-15 10:37:28', 'Item Created', 'EzcodxModule', 'GameBadge', 3, 's:11:"EarlyBird 1";', 1),
(86, '2014-04-15 10:37:28', 'Item Created', 'EzcodxModule', 'GameLevel', 1, 's:7:"General";', 1),
(87, '2014-04-15 10:38:05', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(88, '2014-04-15 10:38:41', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(89, '2014-04-15 10:39:10', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(90, '2014-04-15 10:39:20', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(91, '2014-04-15 10:39:20', 'Item Modified', 'EzcodxModule', 'GameBadge', 1, 'a:4:{i:0;s:11:"LoginUser 2";i:1;a:1:{i:0;s:5:"grade";}i:2;s:1:"1";i:3;i:2;}', 1),
(92, '2014-04-15 12:29:17', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(93, '2014-04-15 12:51:27', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(94, '2014-04-15 12:51:41', 'Item Modified', 'EzcodxModule', 'User', 1, 'a:4:{i:0;s:10:"Super User";i:1;a:1:{i:0;s:8:"language";}i:2;s:0:"";i:3;s:2:"ka";}', 1),
(95, '2014-04-15 12:51:43', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(96, '2014-04-15 14:14:46', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(97, '2014-04-15 14:16:22', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(98, '2014-04-16 07:50:09', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(99, '2014-04-16 07:50:54', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(100, '2014-04-16 10:36:50', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(101, '2014-04-16 10:37:06', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(102, '2014-04-16 10:40:16', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(103, '2014-04-16 11:12:00', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(104, '2014-04-16 11:45:47', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(105, '2014-04-16 11:54:53', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(106, '2014-04-16 13:17:05', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(107, '2014-04-16 13:17:58', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(108, '2014-04-16 14:46:57', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(109, '2014-04-16 14:47:34', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(110, '2014-04-16 15:40:07', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(111, '2014-04-16 15:40:11', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(112, '2014-04-16 15:40:29', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(113, '2014-04-17 08:42:33', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(114, '2014-04-17 08:55:43', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(115, '2014-04-17 13:57:21', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(116, '2014-04-22 07:37:45', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(117, '2014-04-22 07:37:46', 'Item Modified', 'EzcodxModule', 'GameBadge', 1, 'a:4:{i:0;s:11:"LoginUser 3";i:1;a:1:{i:0;s:5:"grade";}i:2;s:1:"2";i:3;i:3;}', 1),
(118, '2014-04-22 07:42:04', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(119, '2014-04-22 13:20:11', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(120, '2014-04-22 13:20:41', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(121, '2014-04-22 13:20:48', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(122, '2014-04-22 14:02:01', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(123, '2014-04-22 14:09:00', 'Item Created', 'EzcodxModule', 'GameScore', 4, 's:13:"CreateMission";', 1),
(124, '2014-04-22 14:09:00', 'Item Created', 'EzcodxModule', 'Mission', 1, 's:8:"sdadadad";', 1),
(125, '2014-04-22 14:09:00', 'Item Created', 'EzcodxModule', 'GamePoint', 2, 's:13:"Communication";', 1),
(126, '2014-04-22 14:09:00', 'Item Created', 'EzcodxModule', 'GameBadge', 4, 's:15:"CreateMission 1";', 1),
(127, '2014-04-22 14:09:02', 'Item Viewed', 'EzcodxModule', 'Mission', 1, 'a:2:{i:0;s:8:"sdadadad";i:1;s:14:"MissionsModule";}', 1),
(128, '2014-04-22 14:10:23', 'Item Created', 'EzcodxModule', 'GameScore', 5, 's:10:"CreateLead";', 1),
(129, '2014-04-22 14:10:23', 'Item Created', 'EzcodxModule', 'Contact', 1, 's:10:"cvxcvxcvxc";', 1),
(130, '2014-04-22 14:10:24', 'Item Created', 'EzcodxModule', 'GameScore', 6, 's:10:"UpdateLead";', 1),
(131, '2014-04-22 14:10:24', 'Item Created', 'EzcodxModule', 'GamePoint', 3, 's:11:"NewBusiness";', 1),
(132, '2014-04-22 14:10:24', 'Item Created', 'EzcodxModule', 'GameBadge', 5, 's:12:"CreateLead 1";', 1),
(133, '2014-04-22 14:10:26', 'Item Viewed', 'EzcodxModule', 'Contact', 1, 'a:2:{i:0;s:10:"cvxcvxcvxc";i:1;s:11:"LeadsModule";}', 1),
(134, '2014-04-22 14:48:06', 'Item Created', 'EzcodxModule', 'GameScore', 7, 's:13:"CreateAccount";', 1),
(135, '2014-04-22 14:48:06', 'Item Created', 'EzcodxModule', 'Account', 1, 's:33:"ეწფსდფსდფდს";', 1),
(136, '2014-04-22 14:48:07', 'Item Created', 'EzcodxModule', 'GameScore', 8, 's:13:"UpdateAccount";', 1),
(137, '2014-04-22 14:48:07', 'Item Created', 'EzcodxModule', 'GamePoint', 4, 's:17:"AccountManagement";', 1),
(138, '2014-04-22 14:48:07', 'Item Created', 'EzcodxModule', 'GameBadge', 6, 's:15:"CreateAccount 1";', 1),
(139, '2014-04-22 14:48:09', 'Item Viewed', 'EzcodxModule', 'Account', 1, 'a:2:{i:0;s:33:"ეწფსდფსდფდს";i:1;s:14:"AccountsModule";}', 1),
(140, '2014-04-22 15:00:20', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(141, '2014-04-23 07:40:56', 'Item Deleted', 'EzcodxModule', 'Notification', 4, 's:157:"გაასუფთავეთ სერვერზე ატივების საქაღალდე (არასავალდებულო).";', 1),
(142, '2014-04-23 07:40:56', 'Item Deleted', 'EzcodxModule', 'Notification', 2, 's:52:"Remove the api test entry script for production use.";', 1),
(143, '2014-04-23 08:11:45', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(144, '2014-04-23 08:57:11', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(145, '2014-04-23 08:58:33', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(146, '2014-04-23 08:59:00', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(147, '2014-04-23 08:59:40', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(148, '2014-04-23 08:59:49', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(149, '2014-04-23 09:00:13', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(150, '2014-04-23 09:00:23', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(151, '2014-04-23 09:00:47', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(152, '2014-04-23 09:01:00', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(153, '2014-04-23 09:01:01', 'Item Modified', 'EzcodxModule', 'GameBadge', 3, 'a:4:{i:0;s:11:"EarlyBird 2";i:1;a:1:{i:0;s:5:"grade";}i:2;s:1:"1";i:3;i:2;}', 1),
(154, '2014-04-23 09:07:02', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(155, '2014-04-23 09:07:11', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(156, '2014-04-23 09:07:35', 'Item Created', 'EzcodxModule', 'Account', 2, 's:12:"fdsfsfdsfsdf";', 1),
(157, '2014-04-23 09:07:38', 'Item Viewed', 'EzcodxModule', 'Account', 2, 'a:2:{i:0;s:12:"fdsfsfdsfsdf";i:1;s:14:"AccountsModule";}', 1),
(158, '2014-04-23 09:07:53', 'Item Viewed', 'EzcodxModule', 'Account', 2, 'a:2:{i:0;s:12:"fdsfsfdsfsdf";i:1;s:14:"AccountsModule";}', 1),
(159, '2014-04-23 09:08:06', 'Item Created', 'EzcodxModule', 'Account', 3, 's:19:"fdsfsfdsfsdghsdgdsg";', 1),
(160, '2014-04-23 09:08:08', 'Item Viewed', 'EzcodxModule', 'Account', 3, 'a:2:{i:0;s:19:"fdsfsfdsfsdghsdgdsg";i:1;s:14:"AccountsModule";}', 1),
(161, '2014-04-23 09:11:42', 'Item Created', 'EzcodxModule', 'GameScore', 9, 's:15:"MassEditContact";', 1),
(162, '2014-04-23 09:11:42', 'Item Modified', 'EzcodxModule', 'Contact', 1, 'a:4:{i:0;s:10:"cvxcvxcvxc";i:1;a:2:{i:0;s:6:"source";i:1;s:5:"value";}i:2;s:0:"";i:3;s:9:"Tradeshow";}', 1),
(163, '2014-04-23 09:11:43', 'Item Created', 'EzcodxModule', 'GameBadge', 7, 's:18:"MassEditContacts 1";', 1),
(164, '2014-04-23 09:12:50', 'Item Created', 'EzcodxModule', 'Dashboard', 2, 's:10:"cvbcvbncvn";', 1),
(165, '2014-04-23 11:32:01', 'Item Viewed', 'EzcodxModule', 'Account', 3, 'a:2:{i:0;s:19:"fdsfsfdsfsdghsdgdsg";i:1;s:14:"AccountsModule";}', 1),
(166, '2014-04-23 11:32:03', 'Item Viewed', 'EzcodxModule', 'Account', 3, 'a:2:{i:0;s:19:"fdsfsfdsfsdghsdgdsg";i:1;s:14:"AccountsModule";}', 1),
(167, '2014-04-23 12:22:25', 'Item Viewed', 'EzcodxModule', 'Account', 3, 'a:2:{i:0;s:19:"fdsfsfdsfsdghsdgdsg";i:1;s:14:"AccountsModule";}', 1),
(168, '2014-04-23 12:46:10', 'Item Created', 'EzcodxModule', 'FileModel', 1, 's:21:"აირჩიეთ";', 1),
(169, '2014-04-23 13:45:52', 'Item Created', 'EzcodxModule', 'Import', 1, 's:9:"(Unnamed)";', 1),
(170, '2014-04-24 09:56:33', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(171, '2014-04-24 09:56:59', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(172, '2014-04-24 09:58:20', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(173, '2014-04-24 09:58:54', 'Item Viewed', 'EzcodxModule', 'User', 1, 'a:2:{i:0;s:10:"Super User";i:1;s:11:"UsersModule";}', 1),
(174, '2014-04-24 10:04:06', 'Item Created', 'EzcodxModule', 'FileModel', 2, 's:21:"აირჩიეთ";', 1),
(175, '2014-04-24 10:04:09', 'Item Deleted', 'EzcodxModule', 'FileModel', 2, 's:21:"აირჩიეთ";', 1),
(176, '2014-04-25 11:39:40', 'Item Created', 'EzcodxModule', 'NotificationMessage', 3, 's:6:"(None)";', 1),
(177, '2014-04-25 11:39:40', 'Item Created', 'EzcodxModule', 'Notification', 4, 's:44:"Clear the assets folder on server(optional).";', 1),
(178, '2014-04-25 12:19:28', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(179, '2014-04-25 12:46:49', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(180, '2014-04-25 14:34:27', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1),
(181, '2014-04-25 15:11:21', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(182, '2014-04-25 15:12:51', 'User Logged Out', 'UsersModule', NULL, NULL, 'N;', 1),
(183, '2014-04-25 15:13:08', 'User Logged In', 'UsersModule', NULL, NULL, 'N;', 1);

-- --------------------------------------------------------

--
-- Table structure for table `autoresponder`
--

CREATE TABLE IF NOT EXISTS `autoresponder` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `textcontent` text COLLATE utf8_unicode_ci,
  `fromoperationdurationinterval` int(11) DEFAULT NULL,
  `fromoperationdurationtype` text COLLATE utf8_unicode_ci,
  `operationtype` int(11) DEFAULT NULL,
  `enabletracking` tinyint(1) unsigned DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `marketinglist_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `autoresponderitem`
--

CREATE TABLE IF NOT EXISTS `autoresponderitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `processdatetime` datetime DEFAULT NULL,
  `processed` tinyint(1) unsigned DEFAULT NULL,
  `contact_id` int(11) unsigned DEFAULT NULL,
  `emailmessage_id` int(11) unsigned DEFAULT NULL,
  `autoresponder_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `autoresponderitemactivity`
--

CREATE TABLE IF NOT EXISTS `autoresponderitemactivity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailmessageactivity_id` int(11) unsigned DEFAULT NULL,
  `autoresponderitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailmessageactivity_id_autoresponderitem_id` (`emailmessageactivity_id`,`autoresponderitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `basecustomfield`
--

CREATE TABLE IF NOT EXISTS `basecustomfield` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `data_customfielddata_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `basecustomfield`
--

INSERT INTO `basecustomfield` (`id`, `data_customfielddata_id`) VALUES
(1, 8),
(2, 8),
(3, 2),
(4, 4),
(5, 2),
(6, 3),
(7, 2),
(8, 3),
(9, 2),
(10, 3);

-- --------------------------------------------------------

--
-- Table structure for table `basestarredmodel`
--

CREATE TABLE IF NOT EXISTS `basestarredmodel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bytimeworkflowinqueue`
--

CREATE TABLE IF NOT EXISTS `bytimeworkflowinqueue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modelclassname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `processdatetime` datetime DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `modelitem_item_id` int(11) unsigned DEFAULT NULL,
  `savedworkflow_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `calculatedderivedattributemetadata`
--

CREATE TABLE IF NOT EXISTS `calculatedderivedattributemetadata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `derivedattributemetadata_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE IF NOT EXISTS `campaign` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `supportsrichtext` tinyint(1) unsigned DEFAULT NULL,
  `sendondatetime` datetime DEFAULT NULL,
  `fromname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromaddress` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `textcontent` text COLLATE utf8_unicode_ci,
  `enabletracking` tinyint(1) unsigned DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `marketinglist_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaignitem`
--

CREATE TABLE IF NOT EXISTS `campaignitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `processed` tinyint(1) unsigned DEFAULT NULL,
  `contact_id` int(11) unsigned DEFAULT NULL,
  `emailmessage_id` int(11) unsigned DEFAULT NULL,
  `campaign_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaignitemactivity`
--

CREATE TABLE IF NOT EXISTS `campaignitemactivity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailmessageactivity_id` int(11) unsigned DEFAULT NULL,
  `campaignitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailmessageactivity_id_campaignitem_id` (`emailmessageactivity_id`,`campaignitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `campaign_read`
--

CREATE TABLE IF NOT EXISTS `campaign_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `campaign_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `item_id` int(11) unsigned DEFAULT NULL,
  `relatedmodel_id` int(11) unsigned DEFAULT NULL,
  `relatedmodel_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `companyname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `latestactivitydatetime` datetime DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googlewebtrackingid` text COLLATE utf8_unicode_ci,
  `person_id` int(11) unsigned DEFAULT NULL,
  `account_id` int(11) unsigned DEFAULT NULL,
  `industry_customfield_id` int(11) unsigned DEFAULT NULL,
  `secondaryaddress_address_id` int(11) unsigned DEFAULT NULL,
  `secondaryemail_email_id` int(11) unsigned DEFAULT NULL,
  `source_customfield_id` int(11) unsigned DEFAULT NULL,
  `state_contactstate_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `companyname`, `description`, `latestactivitydatetime`, `website`, `googlewebtrackingid`, `person_id`, `account_id`, `industry_customfield_id`, `secondaryaddress_address_id`, `secondaryemail_email_id`, `source_customfield_id`, `state_contactstate_id`) VALUES
(1, '', '', NULL, '', NULL, 3, NULL, 3, 3, 3, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contactstarred`
--

CREATE TABLE IF NOT EXISTS `contactstarred` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `basestarredmodel_id` int(11) unsigned DEFAULT NULL,
  `contact_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `basestarredmodel_id_contact_id` (`basestarredmodel_id`,`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contactstate`
--

CREATE TABLE IF NOT EXISTS `contactstate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `serializedlabels` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `contactstate`
--

INSERT INTO `contactstate` (`id`, `name`, `order`, `serializedlabels`) VALUES
(1, 'New', 0, NULL),
(2, 'In Progress', 1, NULL),
(3, 'Recycled', 2, NULL),
(4, 'Dead', 3, NULL),
(5, 'Qualified', 4, NULL),
(6, 'Customer', 5, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contactwebform`
--

CREATE TABLE IF NOT EXISTS `contactwebform` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci,
  `redirecturl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submitbuttonlabel` text COLLATE utf8_unicode_ci,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `excludestyles` tinyint(1) unsigned DEFAULT NULL,
  `enablecaptcha` tinyint(1) unsigned DEFAULT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defaultpermissionsetting` tinyint(11) DEFAULT NULL,
  `defaultpermissiongroupsetting` int(11) DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `defaultstate_contactstate_id` int(11) unsigned DEFAULT NULL,
  `defaultowner__user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contactwebformentry`
--

CREATE TABLE IF NOT EXISTS `contactwebformentry` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `hashindex` text COLLATE utf8_unicode_ci,
  `item_id` int(11) unsigned DEFAULT NULL,
  `contact_id` int(11) unsigned DEFAULT NULL,
  `entries_contactwebform_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contactwebform_read`
--

CREATE TABLE IF NOT EXISTS `contactwebform_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `contactwebform_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_opportunity`
--

CREATE TABLE IF NOT EXISTS `contact_opportunity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) unsigned DEFAULT NULL,
  `opportunity_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_ytinutroppo_di_tcatnoc` (`contact_id`,`opportunity_id`),
  KEY `di_tcatnoc` (`contact_id`),
  KEY `di_ytinutroppo` (`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_project`
--

CREATE TABLE IF NOT EXISTS `contact_project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) unsigned DEFAULT NULL,
  `project_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_tcejorp_di_tcatnoc` (`contact_id`,`project_id`),
  KEY `di_tcatnoc` (`contact_id`),
  KEY `di_tcejorp` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_read`
--

CREATE TABLE IF NOT EXISTS `contact_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `contact_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact_read`
--

INSERT INTO `contact_read` (`id`, `securableitem_id`, `munge_id`, `count`) VALUES
(1, 3, 'G2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_read_subscription`
--

CREATE TABLE IF NOT EXISTS `contact_read_subscription` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `modelid` int(11) unsigned NOT NULL,
  `modifieddatetime` datetime DEFAULT NULL,
  `subscriptiontype` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userid_modelid` (`userid`,`modelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation`
--

CREATE TABLE IF NOT EXISTS `conversation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `latestdatetime` datetime DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownerhasreadlatest` tinyint(1) unsigned DEFAULT NULL,
  `isclosed` tinyint(1) unsigned DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversationparticipant`
--

CREATE TABLE IF NOT EXISTS `conversationparticipant` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hasreadlatest` tinyint(1) unsigned DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  `conversation_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversationstarred`
--

CREATE TABLE IF NOT EXISTS `conversationstarred` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `basestarredmodel_id` int(11) unsigned DEFAULT NULL,
  `conversation_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `basestarredmodel_id_conversation_id` (`basestarredmodel_id`,`conversation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_item`
--

CREATE TABLE IF NOT EXISTS `conversation_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(11) unsigned DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_meti_di_noitasrevnoc` (`conversation_id`,`item_id`),
  KEY `di_noitasrevnoc` (`conversation_id`),
  KEY `di_meti` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_read`
--

CREATE TABLE IF NOT EXISTS `conversation_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `conversation_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `code` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ratetobase` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_edoc` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `active`, `code`, `ratetobase`) VALUES
(1, 1, 'USD', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currencyvalue`
--

CREATE TABLE IF NOT EXISTS `currencyvalue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ratetobase` double DEFAULT NULL,
  `value` double DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customfield`
--

CREATE TABLE IF NOT EXISTS `customfield` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` text COLLATE utf8_unicode_ci,
  `basecustomfield_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `customfield`
--

INSERT INTO `customfield` (`id`, `value`, `basecustomfield_id`) VALUES
(1, '', 1),
(2, '', 2),
(3, '', 3),
(4, 'Tradeshow', 4),
(5, '', 5),
(6, '', 6),
(7, '', 7),
(8, '', 8),
(9, '', 9),
(10, '', 10);

-- --------------------------------------------------------

--
-- Table structure for table `customfielddata`
--

CREATE TABLE IF NOT EXISTS `customfielddata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defaultvalue` text COLLATE utf8_unicode_ci,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `serializedlabels` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_eman` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `customfielddata`
--

INSERT INTO `customfielddata` (`id`, `name`, `defaultvalue`, `serializeddata`, `serializedlabels`) VALUES
(1, 'AccountContactAffiliationRoles', NULL, 'a:6:{i:0;s:7:"Billing";i:1;s:8:"Shipping";i:2;s:7:"Support";i:3;s:9:"Technical";i:4;s:14:"Administrative";i:5;s:15:"Project Manager";}', NULL),
(2, 'Industries', NULL, 'a:9:{i:0;s:10:"Automotive";i:1;s:7:"Banking";i:2;s:17:"Business Services";i:3;s:6:"Energy";i:4;s:18:"Financial Services";i:5;s:9:"Insurance";i:6;s:13:"Manufacturing";i:7;s:6:"Retail";i:8;s:10:"Technology";}', NULL),
(3, 'AccountTypes', NULL, 'a:3:{i:0;s:8:"Prospect";i:1;s:8:"Customer";i:2;s:6:"Vendor";}', NULL),
(4, 'LeadSources', NULL, 'a:4:{i:0;s:14:"Self-Generated";i:1;s:12:"Inbound Call";i:2;s:9:"Tradeshow";i:3;s:13:"Word of Mouth";}', NULL),
(5, 'MeetingCategories', 'Meeting', 'a:2:{i:0;s:7:"Meeting";i:1;s:4:"Call";}', NULL),
(6, 'SalesStages', 'Prospecting', 'a:6:{i:0;s:11:"Prospecting";i:1;s:13:"Qualification";i:2;s:11:"Negotiating";i:3;s:6:"Verbal";i:4;s:10:"Closed Won";i:5;s:11:"Closed Lost";}', NULL),
(7, 'ProductStages', NULL, 'a:3:{i:0;s:4:"Open";i:1;s:4:"Lost";i:2;s:3:"Won";}', NULL),
(8, 'Titles', NULL, 'a:4:{i:0;s:3:"Mr.";i:1;s:4:"Mrs.";i:2;s:3:"Ms.";i:3;s:3:"Dr.";}', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customfieldvalue`
--

CREATE TABLE IF NOT EXISTS `customfieldvalue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` text COLLATE utf8_unicode_ci,
  `multiplevaluescustomfield_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dashboard`
--

CREATE TABLE IF NOT EXISTS `dashboard` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `isdefault` tinyint(1) unsigned DEFAULT NULL,
  `layoutid` int(11) DEFAULT NULL,
  `layouttype` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `dashboard`
--

INSERT INTO `dashboard` (`id`, `isdefault`, `layoutid`, `layouttype`, `name`, `ownedsecurableitem_id`) VALUES
(1, 1, 1, '50,50', 'მთავარი', 1),
(2, NULL, 2, '50,50', 'cvbcvbncvn', 7);

-- --------------------------------------------------------

--
-- Table structure for table `derivedattributemetadata`
--

CREATE TABLE IF NOT EXISTS `derivedattributemetadata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modelclassname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serializedmetadata` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `dropdowndependencyderivedattributemetadata`
--

CREATE TABLE IF NOT EXISTS `dropdowndependencyderivedattributemetadata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `derivedattributemetadata_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isinvalid` tinyint(1) unsigned DEFAULT NULL,
  `optout` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `emailaddress`, `isinvalid`, `optout`) VALUES
(1, '', NULL, NULL),
(2, '', NULL, 0),
(3, '', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `emailaccount`
--

CREATE TABLE IF NOT EXISTS `emailaccount` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fromname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `replytoname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outboundhost` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outboundusername` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outboundpassword` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outboundsecurity` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outboundtype` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `outboundport` int(11) DEFAULT NULL,
  `usecustomoutboundsettings` tinyint(1) unsigned DEFAULT NULL,
  `replytoaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailbox`
--

CREATE TABLE IF NOT EXISTS `emailbox` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `emailbox`
--

INSERT INTO `emailbox` (`id`, `name`, `item_id`, `_user_id`) VALUES
(1, 'System Notifications', 15, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `emailfolder`
--

CREATE TABLE IF NOT EXISTS `emailfolder` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `emailbox_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `emailfolder`
--

INSERT INTO `emailfolder` (`id`, `name`, `type`, `item_id`, `emailbox_id`) VALUES
(1, 'Draft', 'Draft', 16, 1),
(2, 'Sent', 'Sent', 17, 1),
(3, 'Outbox', 'Outbox', 18, 1),
(4, 'Outbox Error', 'OutboxError', 19, 1),
(5, 'Outbox Failure', 'OutboxFailure', 20, 1),
(6, 'Inbox', 'Inbox', 21, 1),
(7, 'Archived', 'Archived', 22, 1),
(8, 'Archived Unmatched', 'ArchivedUnmatched', 23, 1);

-- --------------------------------------------------------

--
-- Table structure for table `emailmessage`
--

CREATE TABLE IF NOT EXISTS `emailmessage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sendattempts` int(11) DEFAULT NULL,
  `sentdatetime` datetime DEFAULT NULL,
  `sendondatetime` datetime DEFAULT NULL,
  `headers` text COLLATE utf8_unicode_ci,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `folder_emailfolder_id` int(11) unsigned DEFAULT NULL,
  `content_emailmessagecontent_id` int(11) unsigned DEFAULT NULL,
  `sender_emailmessagesender_id` int(11) unsigned DEFAULT NULL,
  `error_emailmessagesenderror_id` int(11) unsigned DEFAULT NULL,
  `account_emailaccount_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessageactivity`
--

CREATE TABLE IF NOT EXISTS `emailmessageactivity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `latestdatetime` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `latestsourceip` text COLLATE utf8_unicode_ci,
  `item_id` int(11) unsigned DEFAULT NULL,
  `person_id` int(11) unsigned DEFAULT NULL,
  `emailmessageurl_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessagecontent`
--

CREATE TABLE IF NOT EXISTS `emailmessagecontent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `textcontent` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessagerecipient`
--

CREATE TABLE IF NOT EXISTS `emailmessagerecipient` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `toaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `toname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `emailmessage_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `remailmessage` (`emailmessage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessagerecipient_item`
--

CREATE TABLE IF NOT EXISTS `emailmessagerecipient_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailmessagerecipient_id` int(11) unsigned DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_meti_di_tneipiceregassemliame` (`emailmessagerecipient_id`,`item_id`),
  KEY `di_tneipiceregassemliame` (`emailmessagerecipient_id`),
  KEY `di_meti` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessagesender`
--

CREATE TABLE IF NOT EXISTS `emailmessagesender` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fromaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessagesenderror`
--

CREATE TABLE IF NOT EXISTS `emailmessagesenderror` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `createddatetime` datetime DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessagesender_item`
--

CREATE TABLE IF NOT EXISTS `emailmessagesender_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailmessagesender_id` int(11) unsigned DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_meti_di_rednesegassemliame` (`emailmessagesender_id`,`item_id`),
  KEY `di_rednesegassemliame` (`emailmessagesender_id`),
  KEY `di_meti` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessageurl`
--

CREATE TABLE IF NOT EXISTS `emailmessageurl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailmessageactivity_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailmessage_read`
--

CREATE TABLE IF NOT EXISTS `emailmessage_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `emailmessage_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailsignature`
--

CREATE TABLE IF NOT EXISTS `emailsignature` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `textcontent` text COLLATE utf8_unicode_ci,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailtemplate`
--

CREATE TABLE IF NOT EXISTS `emailtemplate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `modelclassname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `textcontent` text COLLATE utf8_unicode_ci,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `emailtemplate_read`
--

CREATE TABLE IF NOT EXISTS `emailtemplate_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `emailtemplate_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exportfilemodel`
--

CREATE TABLE IF NOT EXISTS `exportfilemodel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filemodel_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exportitem`
--

CREATE TABLE IF NOT EXISTS `exportitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `iscompleted` tinyint(1) unsigned DEFAULT NULL,
  `exportfiletype` text COLLATE utf8_unicode_ci,
  `exportfilename` text COLLATE utf8_unicode_ci,
  `modelclassname` text COLLATE utf8_unicode_ci,
  `processoffset` int(11) DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `exportfilemodel_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `filecontent`
--

CREATE TABLE IF NOT EXISTS `filecontent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `filecontent`
--

INSERT INTO `filecontent` (`id`, `content`) VALUES
(1, 0x0000010001002020000001002000a810000016000000280000002000000040000000010020000000000000100000120b0000120b000000000000000000007a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7a5112ffc9b89effffffffffffffffffffffffff7a5112ff7a5112ffffffffffffffffffffffffff7a5112ff7a5112ffffffffffffffffffd4c7b2ffffffffffc5b498fffcfbfaff7a5112ffffffffff8f6d38ff7a5112ff7a5112ff7a5112fffffffffff5f2eeff7a5112ff7a5112ff7a5112ffffffffffb09873ff7a5112ff7f581bffffffffff8b6730ff7a5112ffb7a07eff9f8255ffffffffffeee9e1ff7a5112fffffffffffcfbfafffefefeffffffffff7a5112ffffffffffffffffff7a5112fffbfaf8fffffffffffefefeff7a5112ff7a5112ff7a5112ff7a5112ffffffffffffffffffffffffff7a5112ff7a5112ffd4c6b2ffffffffff7a5112fff2eee8ffffffffff7a5112ff7a5112ff7a5112ff7a5112ffffffffffbca787ff7a5112ffffffffffffffffffffffffffffffffff7a5112ffffffffffffffffff7a5112ffdbd0c0ffffffffff7a5112ff7a5112ff7a5112ff7a5112ff7a5112fffffffffff5f2eeff7a5112ff7a5112ff7a5112ff7a5112ffffffffffffffffffc0ad90ffffffffff7a5112ff7a5112ff7a5112ff7a5112fff0ebe4ffffffffff7a5112ffffffffff835d22ff957442ffffffffff7a5112ffffffffffffffffff7a5112ffffffffff7a5112ffffffffff7a5112ff7a5112ff7a5112ff7a5112fffffffffffaf8f6ffbca888ffbca888ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ffffffffffffffffff7a5112ffffffffff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112fffefdfdffffffffff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112fffefdfdffffffffff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff7a5112ff0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000);

-- --------------------------------------------------------

--
-- Table structure for table `filemodel`
--

CREATE TABLE IF NOT EXISTS `filemodel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `type` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `filecontent_id` int(11) unsigned DEFAULT NULL,
  `relatedmodel_id` int(11) unsigned DEFAULT NULL,
  `relatedmodel_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `filemodel`
--

INSERT INTO `filemodel` (`id`, `name`, `size`, `type`, `item_id`, `filecontent_id`, `relatedmodel_id`, `relatedmodel_type`) VALUES
(1, 'favicon.ico', 4286, 'image/x-icon', 80, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gamebadge`
--

CREATE TABLE IF NOT EXISTS `gamebadge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grade` int(11) DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `gamebadge`
--

INSERT INTO `gamebadge` (`id`, `type`, `grade`, `item_id`, `person_item_id`) VALUES
(1, 'LoginUser', 3, 10, 1),
(2, 'NightOwl', 1, 25, 1),
(3, 'EarlyBird', 2, 59, 1),
(4, 'CreateMission', 1, 64, 1),
(5, 'CreateLead', 1, 69, 1),
(6, 'CreateAccount', 1, 74, 1),
(7, 'MassEditContacts', 1, 78, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gamecoin`
--

CREATE TABLE IF NOT EXISTS `gamecoin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` int(11) DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gamecoin`
--

INSERT INTO `gamecoin` (`id`, `value`, `item_id`, `person_item_id`) VALUES
(1, 33, 57, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gamecollection`
--

CREATE TABLE IF NOT EXISTS `gamecollection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `item_id` int(11) unsigned DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `gamecollection`
--

INSERT INTO `gamecollection` (`id`, `type`, `serializeddata`, `item_id`, `person_item_id`) VALUES
(1, 'Airport', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:4:"Gate";i:0;s:8:"Passport";i:1;s:5:"Pilot";i:0;s:7:"Luggage";i:0;s:8:"TowTruck";i:1;}}', 26, 1),
(2, 'Basketball', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:9:"Backboard";i:0;s:6:"Player";i:0;s:10:"ScoreBoard";i:0;s:7:"Uniform";i:1;s:6:"Trophy";i:1;}}', 27, 1),
(3, 'Bicycle', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:6:"Helmet";i:1;s:11:"RidingShirt";i:0;s:12:"RidingShorts";i:0;s:11:"Speedometer";i:0;s:7:"Bottles";i:0;}}', 28, 1),
(4, 'Breakfast', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:5:"Bread";i:1;s:3:"Jam";i:0;s:4:"Eggs";i:0;s:11:"OrangeJuice";i:1;s:7:"Toaster";i:0;}}', 29, 1),
(5, 'Business', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:5:"Chart";i:1;s:7:"Desktop";i:0;s:3:"Fax";i:0;s:9:"LightBulb";i:1;s:4:"News";i:0;}}', 30, 1),
(6, 'Camping2', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:10:"Binoculars";i:0;s:6:"Shovel";i:0;s:7:"Compass";i:1;s:5:"Signs";i:0;s:3:"SUV";i:2;}}', 31, 1),
(7, 'Camping', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:9:"ArmyKnife";i:0;s:10:"FlashLight";i:0;s:3:"Gas";i:0;s:8:"GasLight";i:0;s:7:"Lighter";i:1;}}', 32, 1),
(8, 'CarParts', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:6:"Breaks";i:1;s:4:"Seat";i:1;s:5:"Gauge";i:0;s:4:"Gear";i:1;s:10:"Suspension";i:1;}}', 33, 1),
(9, 'ChildrenPlayEquipment', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:4:"Ball";i:0;s:7:"Bicycle";i:0;s:6:"Bucket";i:0;s:5:"Swing";i:1;s:5:"Slide";i:0;}}', 34, 1),
(10, 'Circus', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:5:"Magic";i:1;s:6:"Cannon";i:2;s:5:"Clown";i:0;s:8:"Elephant";i:0;s:8:"Unicycle";i:0;}}', 35, 1),
(11, 'Cooking', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:7:"Cookies";i:0;s:5:"Glove";i:0;s:4:"Oven";i:0;s:6:"Server";i:0;s:5:"Grill";i:1;}}', 36, 1),
(12, 'Drinks', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:4:"Beer";i:0;s:3:"Ice";i:0;s:9:"Champagne";i:1;s:7:"Martini";i:1;s:5:"Water";i:0;}}', 37, 1),
(13, 'Education', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:9:"Textbooks";i:0;s:10:"Blackboard";i:0;s:4:"Exam";i:1;s:5:"Globe";i:1;s:5:"Ruler";i:0;}}', 38, 1),
(14, 'Finance', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:4:"Bank";i:1;s:6:"Abacus";i:1;s:5:"Check";i:0;s:4:"Safe";i:2;s:3:"ATM";i:0;}}', 39, 1),
(15, 'Fitness', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:5:"Apple";i:0;s:14:"StationaryBike";i:0;s:5:"Scale";i:0;s:7:"Weights";i:0;s:7:"Muscles";i:0;}}', 40, 1),
(16, 'Food', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:6:"Cheese";i:0;s:4:"Fish";i:1;s:5:"Fruit";i:1;s:6:"Spices";i:0;s:10:"Vegetables";i:0;}}', 41, 1),
(17, 'Golf', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:8:"GolfBall";i:0;s:7:"GolfBag";i:0;s:8:"GolfCart";i:1;s:8:"GolfShoe";i:1;s:7:"GolfBat";i:0;}}', 42, 1),
(18, 'Health', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:11:"FirstAidKit";i:1;s:5:"Heart";i:0;s:5:"Nurse";i:0;s:7:"Vaccine";i:1;s:8:"Vitamins";i:0;}}', 43, 1),
(19, 'Home', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:3:"Bed";i:1;s:9:"Bookshelf";i:0;s:7:"Flowers";i:0;s:6:"Lights";i:0;s:4:"Sofa";i:0;}}', 44, 1),
(20, 'Hotel', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:9:"CoffeeMug";i:0;s:12:"DoNotDisturb";i:0;s:11:"RoomKeyCard";i:0;s:11:"RoomService";i:1;s:6:"Towels";i:0;}}', 45, 1),
(21, 'Office', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:8:"Calendar";i:1;s:15:"CorrectionFluid";i:0;s:9:"DeskPlant";i:1;s:8:"Shredder";i:0;s:11:"CopyMachine";i:0;}}', 46, 1),
(22, 'Racing', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:9:"Champagne";i:0;s:6:"Helmet";i:0;s:9:"Dashboard";i:0;s:9:"StopWatch";i:0;s:5:"Track";i:0;}}', 47, 1),
(23, 'Science', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:5:"Alien";i:1;s:7:"Shuttle";i:0;s:10:"EarthsCore";i:0;s:9:"Chemistry";i:0;s:9:"Professor";i:0;}}', 48, 1),
(24, 'Soccer', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:8:"WorldCup";i:0;s:12:"PenaltyCards";i:0;s:4:"Goal";i:1;s:10:"ScoreBoard";i:0;s:10:"SoccerBall";i:0;}}', 49, 1),
(25, 'SocialMedia', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:5:"Email";i:1;s:6:"Photos";i:0;s:10:"Smartphone";i:0;s:4:"Blog";i:0;s:2:"TV";i:0;}}', 50, 1),
(26, 'SummerBeach2', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:10:"Volleyball";i:0;s:6:"Bikini";i:1;s:10:"SandCastle";i:1;s:10:"BeachChair";i:0;s:8:"Starfish";i:0;}}', 51, 1),
(27, 'SummerBeach3', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:10:"BananaBoat";i:0;s:13:"BathingTrunks";i:0;s:7:"Snorkel";i:0;s:9:"InnerTube";i:0;s:14:"InflatableBoat";i:0;}}', 52, 1),
(28, 'SummerBeach', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:8:"Sunshine";i:0;s:9:"Sunscreen";i:1;s:10:"SunGlasses";i:1;s:13:"BeachUmbrella";i:0;s:10:"SurfBoards";i:0;}}', 53, 1),
(29, 'Traffic', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:9:"GasNozzle";i:1;s:11:"TrafficCone";i:0;s:9:"PoliceCar";i:0;s:8:"RoadSign";i:0;s:13:"SteeringWheel";i:0;}}', 54, 1),
(30, 'Transportation', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:7:"Caravan";i:0;s:5:"Plane";i:0;s:8:"SailBoat";i:0;s:5:"Truck";i:0;s:4:"Taxi";i:0;}}', 55, 1),
(31, 'TravelHoliday', 'a:2:{s:14:"RedemptionItem";i:0;s:5:"Items";a:5:{s:8:"Suitcase";i:0;s:6:"Camera";i:0;s:5:"Hotel";i:0;s:9:"Landscape";i:0;s:3:"Map";i:0;}}', 56, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gamelevel`
--

CREATE TABLE IF NOT EXISTS `gamelevel` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `gamelevel`
--

INSERT INTO `gamelevel` (`id`, `type`, `value`, `item_id`, `person_item_id`) VALUES
(1, 'General', 3, 60, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gamenotification`
--

CREATE TABLE IF NOT EXISTS `gamenotification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gamepoint`
--

CREATE TABLE IF NOT EXISTS `gamepoint` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `gamepoint`
--

INSERT INTO `gamepoint` (`id`, `type`, `value`, `item_id`, `person_item_id`) VALUES
(1, 'UserAdoption', 1555, 9, 1),
(2, 'Communication', 20, 63, 1),
(3, 'NewBusiness', 50, 68, 1),
(4, 'AccountManagement', 30, 73, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gamepointtransaction`
--

CREATE TABLE IF NOT EXISTS `gamepointtransaction` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` int(11) DEFAULT NULL,
  `createddatetime` datetime DEFAULT NULL,
  `gamepoint_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=58 ;

--
-- Dumping data for table `gamepointtransaction`
--

INSERT INTO `gamepointtransaction` (`id`, `value`, `createddatetime`, `gamepoint_id`) VALUES
(1, 10, '2014-04-09 15:30:01', 1),
(2, 50, '2014-04-09 15:30:02', 1),
(3, 10, '2014-04-10 00:45:29', 1),
(4, 20, '2014-04-10 03:13:28', 1),
(5, 50, '2014-04-10 03:13:29', 1),
(6, 10, '2014-04-10 14:00:08', 1),
(7, 10, '2014-04-10 15:03:06', 1),
(8, 10, '2014-04-14 02:28:12', 1),
(9, 20, '2014-04-14 07:15:08', 1),
(10, 20, '2014-04-15 10:37:27', 1),
(11, 50, '2014-04-15 10:37:28', 1),
(12, 20, '2014-04-15 10:38:42', 1),
(13, 20, '2014-04-15 10:39:20', 1),
(14, 150, '2014-04-15 10:39:20', 1),
(15, 20, '2014-04-15 12:29:18', 1),
(16, 10, '2014-04-15 14:16:22', 1),
(17, 20, '2014-04-16 07:50:09', 1),
(18, 20, '2014-04-16 07:50:55', 1),
(19, 20, '2014-04-16 10:37:06', 1),
(20, 20, '2014-04-16 11:12:00', 1),
(21, 20, '2014-04-16 11:45:47', 1),
(22, 20, '2014-04-16 11:54:53', 1),
(23, 10, '2014-04-16 13:17:58', 1),
(24, 10, '2014-04-16 14:47:34', 1),
(25, 10, '2014-04-16 15:40:29', 1),
(26, 10, '2014-04-17 08:42:33', 1),
(27, 10, '2014-04-17 08:55:43', 1),
(28, 10, '2014-04-17 13:57:22', 1),
(29, 20, '2014-04-22 07:37:46', 1),
(30, 300, '2014-04-22 07:37:46', 1),
(31, 10, '2014-04-22 13:20:42', 1),
(32, 10, '2014-04-22 14:02:01', 1),
(33, 20, '2014-04-22 14:09:00', 2),
(34, 50, '2014-04-22 14:09:00', 1),
(35, 20, '2014-04-22 14:10:24', 3),
(36, 50, '2014-04-22 14:10:24', 1),
(37, 10, '2014-04-22 14:48:07', 3),
(38, 10, '2014-04-22 14:48:07', 4),
(39, 50, '2014-04-22 14:48:07', 1),
(40, 20, '2014-04-23 08:11:45', 1),
(41, 20, '2014-04-23 08:58:34', 1),
(42, 10, '2014-04-23 08:59:01', 1),
(43, 20, '2014-04-23 08:59:50', 1),
(44, 20, '2014-04-23 09:00:24', 1),
(45, 20, '2014-04-23 09:01:00', 1),
(46, 150, '2014-04-23 09:01:01', 1),
(47, 20, '2014-04-23 09:07:11', 1),
(48, 10, '2014-04-23 09:07:36', 3),
(49, 10, '2014-04-23 09:07:36', 4),
(50, 10, '2014-04-23 09:08:06', 3),
(51, 10, '2014-04-23 09:08:06', 4),
(52, 15, '2014-04-23 09:11:42', 1),
(53, 50, '2014-04-23 09:11:43', 1),
(54, 20, '2014-04-24 09:57:00', 1),
(55, 20, '2014-04-25 12:46:50', 1),
(56, 10, '2014-04-25 14:34:27', 1),
(57, 10, '2014-04-25 15:13:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gamereward`
--

CREATE TABLE IF NOT EXISTS `gamereward` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cost` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `expirationdatetime` datetime DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gamerewardtransaction`
--

CREATE TABLE IF NOT EXISTS `gamerewardtransaction` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `redemptiondatetime` datetime DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  `transactions_gamereward_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gamereward_read`
--

CREATE TABLE IF NOT EXISTS `gamereward_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `gamereward_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gamescore`
--

CREATE TABLE IF NOT EXISTS `gamescore` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `gamescore`
--

INSERT INTO `gamescore` (`id`, `type`, `value`, `item_id`, `person_item_id`) VALUES
(1, 'LoginUser', 38, 8, 1),
(2, 'NightOwl', 8, 24, 1),
(3, 'EarlyBird', 13, 58, 1),
(4, 'CreateMission', 1, 62, 1),
(5, 'CreateLead', 1, 66, 1),
(6, 'UpdateLead', 1, 67, 1),
(7, 'CreateAccount', 3, 71, 1),
(8, 'UpdateAccount', 3, 72, 1),
(9, 'MassEditContact', 1, 77, 1);

-- --------------------------------------------------------

--
-- Table structure for table `globalmetadata`
--

CREATE TABLE IF NOT EXISTS `globalmetadata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serializedmetadata` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_emaNssalc` (`classname`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `globalmetadata`
--

INSERT INTO `globalmetadata` (`id`, `classname`, `serializedmetadata`) VALUES
(1, 'ContactsModule', 'a:10:{s:17:"designerMenuItems";a:4:{s:14:"showFieldsLink";b:1;s:15:"showGeneralLink";b:1;s:15:"showLayoutsLink";b:1;s:13:"showMenusLink";b:1;}s:26:"globalSearchAttributeNames";a:4:{i:0;s:8:"fullName";i:1;s:8:"anyEmail";i:2;s:11:"officePhone";i:3;s:11:"mobilePhone";}s:13:"startingState";i:1;s:12:"tabMenuItems";a:1:{i:0;a:4:{s:5:"label";s:81:"eval:Ezcodx::t(''ContactsModule'', ''ContactsModulePluralLabel'', $translationParams)";s:3:"url";a:1:{i:0;s:17:"/contacts/default";}s:5:"right";s:19:"Access Contacts Tab";s:6:"mobile";b:1;}}s:24:"shortcutsCreateMenuItems";a:1:{i:0;a:4:{s:5:"label";s:83:"eval:Ezcodx::t(''ContactsModule'', ''ContactsModuleSingularLabel'', $translationParams)";s:3:"url";a:1:{i:0;s:24:"/contacts/default/create";}s:5:"right";s:15:"Create Contacts";s:6:"mobile";b:1;}}s:48:"updateLatestActivityDateTimeWhenATaskIsCompleted";b:1;s:46:"updateLatestActivityDateTimeWhenANoteIsCreated";b:1;s:55:"updateLatestActivityDateTimeWhenAnEmailIsSentOrArchived";b:1;s:51:"updateLatestActivityDateTimeWhenAMeetingIsInThePast";b:1;s:15:"startingStateId";i:5;}'),
(2, 'Currency', 'a:3:{s:7:"members";a:3:{i:0;s:6:"active";i:1;s:4:"code";i:2;s:10:"rateToBase";}s:5:"rules";a:9:{i:0;a:2:{i:0;s:6:"active";i:1;s:7:"boolean";}i:1;a:3:{i:0;s:6:"active";i:1;s:7:"default";s:5:"value";b:1;}i:2;a:2:{i:0;s:4:"code";i:1;s:8:"required";}i:3;a:2:{i:0;s:4:"code";i:1;s:6:"unique";}i:4;a:3:{i:0;s:4:"code";i:1;s:4:"type";s:4:"type";s:6:"string";}i:5;a:4:{i:0;s:4:"code";i:1;s:6:"length";s:3:"min";i:3;s:3:"max";i:3;}i:6;a:4:{i:0;s:4:"code";i:1;s:5:"match";s:7:"pattern";s:19:"/^[A-Z][A-Z][A-Z]$/";s:7:"message";s:35:"Code must be a valid currency code.";}i:7;a:2:{i:0;s:10:"rateToBase";i:1;s:8:"required";}i:8;a:3:{i:0;s:10:"rateToBase";i:1;s:4:"type";s:4:"type";s:5:"float";}}s:32:"lastAttemptedRateUpdateTimeStamp";i:1398670705;}'),
(3, 'EzcodxModule', 'a:12:{s:18:"configureMenuItems";a:7:{i:0;a:5:{s:8:"category";i:1;s:10:"titleLabel";s:54:"eval:Ezcodx::t(''EzcodxModule'', ''Global Configuration'')";s:16:"descriptionLabel";s:61:"eval:Ezcodx::t(''EzcodxModule'', ''Manage Global Configuration'')";s:5:"route";s:33:"/ezcodx/default/configurationEdit";s:5:"right";s:27:"Access Global Configuration";}i:1;a:5:{s:8:"category";i:1;s:10:"titleLabel";s:56:"eval:Ezcodx::t(''EzcodxModule'', ''Currency Configuration'')";s:16:"descriptionLabel";s:63:"eval:Ezcodx::t(''EzcodxModule'', ''Manage Currency Configuration'')";s:5:"route";s:34:"/ezcodx/currency/configurationList";s:5:"right";s:29:"Access Currency Configuration";}i:2;a:5:{s:8:"category";i:1;s:10:"titleLabel";s:35:"eval:Ezcodx::t(''Core'', ''Languages'')";s:16:"descriptionLabel";s:57:"eval:Ezcodx::t(''EzcodxModule'', ''Manage Active Languages'')";s:5:"route";s:34:"/ezcodx/language/configurationList";s:5:"right";s:27:"Access Global Configuration";}i:3;a:5:{s:8:"category";i:1;s:10:"titleLabel";s:49:"eval:Ezcodx::t(''EzcodxModule'', ''Developer Tools'')";s:16:"descriptionLabel";s:56:"eval:Ezcodx::t(''EzcodxModule'', ''Access Developer Tools'')";s:5:"route";s:20:"/ezcodx/development/";s:5:"right";s:27:"Access Global Configuration";}i:4;a:5:{s:8:"category";i:1;s:10:"titleLabel";s:62:"eval:Ezcodx::t(''EzcodxModule'', ''Authentication Configuration'')";s:16:"descriptionLabel";s:69:"eval:Ezcodx::t(''EzcodxModule'', ''Manage Authentication Configuration'')";s:5:"route";s:40:"/ezcodx/authentication/configurationEdit";s:5:"right";s:27:"Access Global Configuration";}i:5;a:5:{s:8:"category";i:1;s:10:"titleLabel";s:41:"eval:Ezcodx::t(''EzcodxModule'', ''Plugins'')";s:16:"descriptionLabel";s:65:"eval:Ezcodx::t(''EzcodxModule'', ''Manage Plugins and Integrations'')";s:5:"route";s:33:"/ezcodx/plugins/configurationEdit";s:5:"right";s:27:"Access Global Configuration";}i:6;a:5:{s:8:"category";i:1;s:10:"titleLabel";s:62:"eval:Ezcodx::t(''EzcodxModule'', ''User Interface Configuration'')";s:16:"descriptionLabel";s:69:"eval:Ezcodx::t(''EzcodxModule'', ''Manage User Interface Configuration'')";s:5:"route";s:46:"/ezcodx/default/userInterfaceConfigurationEdit";s:5:"right";s:27:"Access Global Configuration";}}s:15:"headerMenuItems";a:3:{i:0;a:5:{s:5:"label";s:48:"eval:Ezcodx::t(''EzcodxModule'', ''Administration'')";s:3:"url";a:1:{i:0;s:14:"/configuration";}s:5:"right";s:25:"Access Administration Tab";s:5:"order";i:1;s:6:"mobile";b:0;}i:1;a:4:{s:5:"label";s:47:"eval:Ezcodx::t(''EzcodxModule'', ''Need Support?'')";s:3:"url";s:37:"http://www.ezcodx.com/needSupport.php";s:5:"order";i:9;s:6:"mobile";b:1;}i:2;a:4:{s:5:"label";s:46:"eval:Ezcodx::t(''EzcodxModule'', ''About Ezcodx'')";s:3:"url";a:1:{i:0;s:21:"/ezcodx/default/about";}s:5:"order";i:10;s:6:"mobile";b:1;}}s:21:"configureSubMenuItems";a:1:{i:0;a:5:{s:8:"category";i:2;s:10:"titleLabel";s:52:"eval:Ezcodx::t(''EzcodxModule'', ''LDAP Configuration'')";s:16:"descriptionLabel";s:60:"eval:Ezcodx::t(''EzcodxModule'', ''Manage LDAP Authentication'')";s:5:"route";s:34:"/ezcodx/ldap/configurationEditLdap";s:5:"right";s:27:"Access Global Configuration";}}s:31:"adminTabMenuItemsModuleOrdering";a:9:{i:0;s:4:"home";i:1;s:13:"configuration";i:2;s:8:"designer";i:3;s:6:"import";i:4;s:6:"groups";i:5;s:5:"users";i:6;s:5:"roles";i:7;s:9:"workflows";i:8;s:15:"contactWebForms";}s:26:"tabMenuItemsModuleOrdering";a:10:{i:0;s:4:"home";i:1;s:13:"mashableInbox";i:2;s:8:"accounts";i:3;s:5:"leads";i:4;s:8:"contacts";i:5;s:13:"opportunities";i:6;s:9:"marketing";i:7;s:8:"projects";i:8;s:8:"products";i:9;s:7:"reports";}s:32:"lastAttemptedInfoUpdateTimeStamp";i:1398333420;s:18:"defaultFromAddress";s:29:"notification@ezcodxalerts.com";s:20:"defaultTestToAddress";s:29:"testJobEmail@ezcodxalerts.com";s:11:"globalState";s:88:"a:1:{s:34:"Yii.CSecurityManager.validationkey";s:32:"09ab6ef32163ad940965f5c6292801c6";}";s:22:"customThemeColorsArray";a:3:{i:0;s:7:"#d6d6f4";i:1;s:7:"#7cb830";i:2;s:7:"#464646";}s:16:"globalThemeColor";s:4:"blue";s:18:"forceAllUsersTheme";b:0;}'),
(4, 'UsersModule', 'a:6:{s:17:"adminTabMenuItems";a:1:{i:0;a:4:{s:5:"label";s:38:"eval:Ezcodx::t(''UsersModule'', ''Users'')";s:3:"url";a:1:{i:0;s:14:"/users/default";}s:5:"right";s:16:"Access Users Tab";s:5:"items";a:2:{i:0;a:3:{s:5:"label";s:44:"eval:Ezcodx::t(''UsersModule'', ''Create User'')";s:3:"url";a:1:{i:0;s:21:"/users/default/create";}s:5:"right";s:12:"Create Users";}i:1;a:3:{s:5:"label";s:38:"eval:Ezcodx::t(''UsersModule'', ''Users'')";s:3:"url";a:1:{i:0;s:14:"/users/default";}s:5:"right";s:16:"Access Users Tab";}}}}s:26:"globalSearchAttributeNames";a:3:{i:0;s:8:"fullName";i:1;s:8:"anyEmail";i:2;s:8:"username";}s:18:"configureMenuItems";a:1:{i:0;a:5:{s:8:"category";i:1;s:10:"titleLabel";s:38:"eval:Ezcodx::t(''UsersModule'', ''Users'')";s:16:"descriptionLabel";s:45:"eval:Ezcodx::t(''UsersModule'', ''Manage Users'')";s:5:"route";s:14:"/users/default";s:5:"right";s:16:"Access Users Tab";}}s:15:"headerMenuItems";a:1:{i:0;a:5:{s:5:"label";s:38:"eval:Ezcodx::t(''UsersModule'', ''Users'')";s:3:"url";a:1:{i:0;s:14:"/users/default";}s:5:"right";s:16:"Access Users Tab";s:5:"order";i:4;s:6:"mobile";b:0;}}s:19:"userHeaderMenuItems";a:2:{i:0;a:3:{s:5:"label";s:43:"eval:Ezcodx::t(''UsersModule'', ''My Profile'')";s:3:"url";a:1:{i:0;s:22:"/users/default/profile";}s:5:"order";i:1;}i:1;a:3:{s:5:"label";s:41:"eval:Ezcodx::t(''UsersModule'', ''Sign out'')";s:3:"url";a:1:{i:0;s:22:"/ezcodx/default/logout";}s:5:"order";i:4;}}s:17:"designerMenuItems";a:4:{s:14:"showFieldsLink";b:0;s:15:"showGeneralLink";b:0;s:15:"showLayoutsLink";b:1;s:13:"showMenusLink";b:0;}}');

-- --------------------------------------------------------

--
-- Table structure for table `import`
--

CREATE TABLE IF NOT EXISTS `import` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `import`
--

INSERT INTO `import` (`id`, `serializeddata`, `item_id`) VALUES
(1, 'a:7:{s:15:"importRulesType";s:8:"Accounts";s:14:"fileUploadData";N;s:18:"rowColumnDelimiter";s:1:",";s:18:"rowColumnEnclosure";s:1:""";s:19:"firstRowIsHeaderRow";N;s:11:"mappingData";N;s:33:"explicitReadWriteModelPermissions";N;}', 81);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `createddatetime` datetime DEFAULT NULL,
  `modifieddatetime` datetime DEFAULT NULL,
  `createdbyuser__user_id` int(11) unsigned DEFAULT NULL,
  `modifiedbyuser__user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=85 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `createddatetime`, `modifieddatetime`, `createdbyuser__user_id`, `modifiedbyuser__user_id`) VALUES
(1, '2014-04-09 15:27:09', '2014-04-15 12:51:41', NULL, 1),
(2, '2014-04-09 15:27:09', '2014-04-09 15:27:17', 1, 1),
(3, '2014-04-09 15:27:11', '2014-04-09 15:27:11', 1, 1),
(4, '2014-04-09 15:27:15', '2014-04-09 15:27:16', 1, 1),
(5, '2014-04-09 15:27:17', '2014-04-09 15:27:17', 1, 1),
(6, '2014-04-09 15:27:17', '2014-04-09 15:27:17', 1, 1),
(8, '2014-04-09 15:30:01', '2014-04-25 15:13:08', 1, 1),
(9, '2014-04-09 15:30:01', '2014-04-25 15:13:08', 1, 1),
(10, '2014-04-09 15:30:02', '2014-04-22 07:37:46', 1, 1),
(11, '2014-04-09 17:11:44', '2014-04-09 17:11:44', 1, 1),
(12, '2014-04-09 17:11:44', '2014-04-09 17:11:44', 1, 1),
(14, '2014-04-09 17:25:13', '2014-04-10 11:27:02', 1, 1),
(15, '2014-04-09 17:25:15', '2014-04-09 17:25:15', 1, 1),
(16, '2014-04-09 17:25:15', '2014-04-09 17:25:15', 1, 1),
(17, '2014-04-09 17:25:15', '2014-04-09 17:25:15', 1, 1),
(18, '2014-04-09 17:25:15', '2014-04-09 17:25:16', 1, 1),
(19, '2014-04-09 17:25:15', '2014-04-09 17:25:16', 1, 1),
(20, '2014-04-09 17:25:15', '2014-04-09 17:25:16', 1, 1),
(21, '2014-04-09 17:25:15', '2014-04-09 17:25:16', 1, 1),
(22, '2014-04-09 17:25:15', '2014-04-09 17:25:16', 1, 1),
(23, '2014-04-09 17:25:15', '2014-04-09 17:25:16', 1, 1),
(24, '2014-04-10 03:13:28', '2014-04-23 08:59:49', 1, 1),
(25, '2014-04-10 03:13:29', '2014-04-10 03:13:29', 1, 1),
(26, '2014-04-10 03:14:26', '2014-04-10 08:28:48', 1, 1),
(27, '2014-04-10 03:14:26', '2014-04-10 08:14:49', 1, 1),
(28, '2014-04-10 03:14:26', '2014-04-17 12:40:46', 1, 1),
(29, '2014-04-10 03:14:27', '2014-04-14 08:11:33', 1, 1),
(30, '2014-04-10 03:14:27', '2014-04-24 08:35:47', 1, 1),
(31, '2014-04-10 03:14:27', '2014-04-23 09:07:32', 1, 1),
(32, '2014-04-10 03:14:27', '2014-04-14 07:17:23', 1, 1),
(33, '2014-04-10 03:14:27', '2014-04-16 15:03:48', 1, 1),
(34, '2014-04-10 03:14:28', '2014-04-24 09:57:18', 1, 1),
(35, '2014-04-10 03:14:28', '2014-04-25 15:10:59', 1, 1),
(36, '2014-04-10 03:14:28', '2014-04-10 12:51:54', 1, 1),
(37, '2014-04-10 03:14:28', '2014-04-22 15:00:46', 1, 1),
(38, '2014-04-10 03:14:28', '2014-04-16 07:52:39', 1, 1),
(39, '2014-04-10 03:14:28', '2014-04-23 12:27:35', 1, 1),
(40, '2014-04-10 03:14:28', '2014-04-10 03:14:29', 1, 1),
(41, '2014-04-10 03:14:29', '2014-04-10 08:13:33', 1, 1),
(42, '2014-04-10 03:14:29', '2014-04-17 08:58:26', 1, 1),
(43, '2014-04-10 03:14:29', '2014-04-15 14:17:32', 1, 1),
(44, '2014-04-10 03:14:29', '2014-04-10 04:59:54', 1, 1),
(45, '2014-04-10 03:14:29', '2014-04-10 10:41:08', 1, 1),
(46, '2014-04-10 03:14:30', '2014-04-10 10:04:23', 1, 1),
(47, '2014-04-10 03:14:30', '2014-04-10 03:14:30', 1, 1),
(48, '2014-04-10 03:14:30', '2014-04-10 12:00:36', 1, 1),
(49, '2014-04-10 03:14:30', '2014-04-25 14:44:29', 1, 1),
(50, '2014-04-10 03:14:31', '2014-04-15 11:37:13', 1, 1),
(51, '2014-04-10 03:14:31', '2014-04-10 09:57:06', 1, 1),
(52, '2014-04-10 03:14:31', '2014-04-10 03:14:31', 1, 1),
(53, '2014-04-10 03:14:31', '2014-04-24 07:48:01', 1, 1),
(54, '2014-04-10 03:14:31', '2014-04-25 14:34:35', 1, 1),
(55, '2014-04-10 03:14:32', '2014-04-10 03:14:32', 1, 1),
(56, '2014-04-10 03:14:32', '2014-04-10 03:14:32', 1, 1),
(57, '2014-04-10 03:45:49', '2014-04-28 07:38:37', 1, 1),
(58, '2014-04-15 10:37:27', '2014-04-25 12:46:50', 1, 1),
(59, '2014-04-15 10:37:28', '2014-04-23 09:01:01', 1, 1),
(60, '2014-04-15 10:37:28', '2014-04-22 14:09:00', 1, 1),
(61, '2014-04-22 14:08:59', '2014-04-22 14:09:00', 1, 1),
(62, '2014-04-22 14:08:59', '2014-04-22 14:08:59', 1, 1),
(63, '2014-04-22 14:09:00', '2014-04-22 14:09:00', 1, 1),
(64, '2014-04-22 14:09:00', '2014-04-22 14:09:00', 1, 1),
(65, '2014-04-22 14:10:22', '2014-04-23 09:11:42', 1, 1),
(66, '2014-04-22 14:10:23', '2014-04-22 14:10:23', 1, 1),
(67, '2014-04-22 14:10:24', '2014-04-22 14:10:24', 1, 1),
(68, '2014-04-22 14:10:24', '2014-04-23 09:08:06', 1, 1),
(69, '2014-04-22 14:10:24', '2014-04-22 14:10:24', 1, 1),
(70, '2014-04-22 14:48:05', '2014-04-22 14:48:06', 1, 1),
(71, '2014-04-22 14:48:06', '2014-04-23 09:08:06', 1, 1),
(72, '2014-04-22 14:48:06', '2014-04-23 09:08:06', 1, 1),
(73, '2014-04-22 14:48:07', '2014-04-23 09:08:06', 1, 1),
(74, '2014-04-22 14:48:07', '2014-04-22 14:48:07', 1, 1),
(75, '2014-04-23 09:07:34', '2014-04-23 09:07:36', 1, 1),
(76, '2014-04-23 09:08:05', '2014-04-23 09:08:06', 1, 1),
(77, '2014-04-23 09:11:42', '2014-04-23 09:11:42', 1, 1),
(78, '2014-04-23 09:11:43', '2014-04-23 09:11:43', 1, 1),
(79, '2014-04-23 09:12:50', '2014-04-23 09:12:50', 1, 1),
(80, '2014-04-23 12:46:10', '2014-04-23 12:46:10', 1, 1),
(81, '2014-04-23 13:45:52', '2014-04-23 13:45:52', 1, 1),
(83, '2014-04-25 11:39:40', '2014-04-25 11:39:40', 1, 1),
(84, '2014-04-25 11:39:40', '2014-04-25 11:39:40', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jobinprocess`
--

CREATE TABLE IF NOT EXISTS `jobinprocess` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `joblog`
--

CREATE TABLE IF NOT EXISTS `joblog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `enddatetime` datetime DEFAULT NULL,
  `isprocessed` tinyint(1) unsigned DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `startdatetime` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kanbanitem`
--

CREATE TABLE IF NOT EXISTS `kanbanitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `sortorder` int(11) DEFAULT NULL,
  `kanbanrelateditem_item_id` int(11) unsigned DEFAULT NULL,
  `task_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `marketinglist`
--

CREATE TABLE IF NOT EXISTS `marketinglist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `fromname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromaddress` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `anyonecansubscribe` tinyint(1) unsigned DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `marketinglistmember`
--

CREATE TABLE IF NOT EXISTS `marketinglistmember` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `createddatetime` datetime DEFAULT NULL,
  `modifieddatetime` datetime DEFAULT NULL,
  `unsubscribed` tinyint(1) unsigned DEFAULT NULL,
  `contact_id` int(11) unsigned DEFAULT NULL,
  `marketinglist_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `marketinglist_read`
--

CREATE TABLE IF NOT EXISTS `marketinglist_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `marketinglist_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `meeting`
--

CREATE TABLE IF NOT EXISTS `meeting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `enddatetime` datetime DEFAULT NULL,
  `processedforlatestactivity` tinyint(1) unsigned DEFAULT NULL,
  `location` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logged` tinyint(1) unsigned DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startdatetime` datetime DEFAULT NULL,
  `activity_id` int(11) unsigned DEFAULT NULL,
  `category_customfield_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `meeting_read`
--

CREATE TABLE IF NOT EXISTS `meeting_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `meeting_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `meeting_read_subscription`
--

CREATE TABLE IF NOT EXISTS `meeting_read_subscription` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `modelid` int(11) unsigned NOT NULL,
  `modifieddatetime` datetime DEFAULT NULL,
  `subscriptiontype` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userid_modelid` (`userid`,`modelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `messagesource`
--

CREATE TABLE IF NOT EXISTS `messagesource` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sourceCategory` (`category`,`source`(767))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1101 ;

--
-- Dumping data for table `messagesource`
--

INSERT INTO `messagesource` (`id`, `category`, `source`) VALUES
(1, 'Default', 0x416476616e63656420536561726368),
(2, 'Default', 0x426173696320536561726368),
(3, 'Default', 0x496e626f78),
(4, 'Default', 0x416e6e75616c6c79),
(5, 'Core', 0x4265747765656e),
(6, 'Core', 0x596573746572646179),
(7, 'Core', 0x546f646179),
(8, 'Core', 0x546f6d6f72726f77),
(9, 'Core', 0x4265666f7265),
(10, 'Core', 0x4166746572),
(11, 'Core', 0x4e65787420372044617973),
(12, 'Core', 0x4c61737420372044617973),
(13, 'Core', 0x4e6f207b6d6f64756c654c6162656c506c7572616c4c6f776572436173657d20666f756e64),
(14, 'Core', 0x7b73746172747d2d7b656e647d206f66207b636f756e747d20726573756c742873292e),
(15, 'Core', 0x416476616e636564),
(16, 'Core', 0x436c656172),
(17, 'Core', 0x436f6c756d6e73),
(18, 'Core', 0x4f7074696f6e73),
(19, 'Core', 0x536561726368),
(20, 'Core', 0x436c6f7365),
(21, 'Core', 0x7375636365737366756c6c792064656c65746564),
(22, 'Core', 0x7b636f756e747d20726573756c74287329),
(23, 'Core', 0x4e616d65),
(24, 'Core', 0x52657475726e20746f204c697374),
(25, 'Core', 0x7b6174747269627574657d2063616e6e6f7420626520626c616e6b2e),
(26, 'Core', 0x284e6f6e6529),
(27, 'Core', 0x616e64),
(28, 'Core', 0x48696464656e20436f6c756d6e73),
(29, 'Core', 0x56697369626c6520436f6c756d6e73),
(30, 'Core', 0x4170706c79),
(31, 'Core', 0x5265736574),
(32, 'Core', 0x556e6b6e6f776e),
(33, 'Core', 0x596573),
(34, 'Core', 0x4e6f77),
(35, 'Core', 0x43726561746564204f6e),
(36, 'Core', 0x46756c6c204e616d65),
(37, 'Core', 0x4c617374204d6f646966696564),
(38, 'Core', 0x4c617374204d6f646966696564204461746520616e642055736572),
(39, 'Core', 0x437265617465),
(40, 'Core', 0x557064617465),
(41, 'Core', 0x53656c6563746564),
(42, 'Core', 0x416c6c20526573756c7473),
(43, 'Core', 0x436c6f6e65),
(44, 'Core', 0x45646974),
(45, 'HomeModule', 0x41646420506f72746c6574),
(46, 'Core', 0x44656c657465),
(47, 'Core', 0x41726520796f75207375726520796f752077616e7420746f2064656c6574652074686973207b6d6f64656c4c6162656c7d3f),
(48, 'Core', 0x44657461696c73),
(49, 'Core', 0x53617665),
(50, 'Core', 0x556e6c696e6b),
(51, 'Core', 0x41726520796f75207375726520796f752077616e7420746f20756e6c696e6b2074686973207b6d6f64656c4c6162656c7d3f),
(52, 'Core', 0x416476616e6365642053656172636820526f7773),
(53, 'Core', 0x536561726368204f70657261746f72),
(54, 'Core', 0x416c6c),
(55, 'Core', 0x596f75206d7573742073656c656374206174206c65617374206f6e65206669656c6420746f206d6f646966792e),
(56, 'Core', 0x5361766520616e6420436c6f7365),
(57, 'Core', 0x4164642046696c6573),
(58, 'Core', 0x4d61782075706c6f61642073697a653a207b6d617853697a657d),
(59, 'Core', 0x52656d6f766520506f72746c6574),
(60, 'Core', 0x436f6e66696775726520506f72746c6574),
(61, 'Core', 0x4564697420506f72746c6574),
(62, 'Core', 0x5468697320706f72746c65742077696c6c2062652072656d6f7665642c206f6b3f),
(63, 'MarketingListsModule', 0x4d7920537562736372697074696f6e73),
(64, 'MarketingListsModule', 0x7b636f756e747d20696e76616c696420656d61696c2061646472657373),
(65, 'MarketingListsModule', 0x41726520796f75207375726520796f752077616e7420746f20756e6c696e6b2074686973207265636f72643f),
(66, 'MarketingListsModule', 0x41646420436f6e74616374734d6f64756c6553696e67756c61724c6162656c2f4c656164734d6f64756c6553696e67756c61724c6162656c),
(67, 'EmailMessagesModule', 0x496e76616c696420656d61696c2061646472657373),
(68, 'EmailMessagesModule', 0x44656c65746564207375636365737366756c6c79),
(69, 'EmailMessagesModule', 0x41726520796f75207375726520796f752077616e7420746f2064656c6574653f),
(70, 'EmailMessagesModule', 0x456d61696c204163636f756e74),
(71, 'EmailMessagesModule', 0x456d61696c204163636f756e7473),
(72, 'EmailMessagesModule', 0x456d61696c),
(73, 'EmailMessagesModule', 0x496e626f78),
(74, 'EmailMessagesModule', 0x44656c65746520456d61696c73),
(75, 'EmailMessagesModule', 0x436f6d706f736520456d61696c),
(76, 'ConversationsModule', 0x4d61726b2073656c656374656420617320636c6f736564),
(77, 'ContactsModule', 0x44656c65746520436f6e766572736174696f6e73),
(78, 'InstallModule', 0x5573657260732070617373776f72642e),
(79, 'ReportsModule', 0x5265706f727473),
(80, 'ReportsModule', 0x526573756c7473),
(81, 'ReportsModule', 0x437265617465205265706f7274),
(82, 'ReportsModule', 0x53656c6563742046696c74657273),
(83, 'Core', 0x4964),
(84, 'ProductTemplatesModule', 0x4564697461626c65),
(85, 'NotificationsModule', 0x44656c6574652073656c6563746564),
(86, 'NotificationsModule', 0x44656c65746520616c6c),
(87, 'NotificationsModule', 0x4e6f74696669636174696f6e73),
(88, 'MashableInboxModule', 0x436f6d62696e6564),
(89, 'MashableInboxModule', 0x4d61726b2073656c65637465642061732072656164),
(90, 'MashableInboxModule', 0x4d61726b2073656c656374656420617320756e72656164),
(91, 'MashableInboxModule', 0x596f75206d7573742073656c656374206174206c65617374206f6e65207265636f7264),
(92, 'ContactsModule', 0x436f6e74616374734d6f64756c65506c7572616c4c6162656c),
(93, 'ContactsModule', 0x4d7920436f6e74616374734d6f64756c65506c7572616c4c6162656c),
(94, 'ContactsModule', 0x43726561746520436f6e74616374734d6f64756c6553696e67756c61724c6162656c),
(95, 'ContactsModule', 0x53656c6563746564207375636365737366756c6c79),
(96, 'ContactsModule', 0x43726561746520436f6e74616374734d6f64756c65506c7572616c4c6162656c),
(97, 'AccountsModule', 0x4163636f756e74734d6f64756c6553696e67756c61724c6162656c),
(98, 'ContactsModule', 0x436f6d70616e79204e616d65),
(99, 'ContactsModule', 0x536f75726365),
(100, 'ContactsModule', 0x436f6e74616374734d6f64756c6553696e67756c61724c6162656c204944),
(101, 'CustomField', 0x53656c662d47656e657261746564),
(102, 'CustomField', 0x576f7264206f66204d6f757468),
(103, 'AccountsModule', 0x7b6e7d204163636f756e74734d6f64756c6553696e67756c61724c6162656c20637265617465647c7b6e7d204163636f756e74734d6f64756c65506c7572616c4c6162656c2063726561746564),
(104, 'AccountsModule', 0x4163636f756e74734d6f64756c65506c7572616c4c6162656c),
(105, 'AccountsModule', 0x506172656e74204163636f756e74734d6f64756c6553696e67756c61724c6162656c),
(106, 'AccountsModule', 0x416e6e75616c20526576656e7565),
(107, 'AccountsModule', 0x42696c6c696e672041646472657373),
(108, 'AccountsModule', 0x456d706c6f79656573),
(109, 'AccountsModule', 0x4f66666963652050686f6e65),
(110, 'AccountsModule', 0x4f666669636520466178),
(111, 'AccountsModule', 0x5368697070696e672041646472657373),
(112, 'AccountsModule', 0x4163636f756e74),
(113, 'AccountsModule', 0x4163636f756e7473),
(114, 'AccountsModule', 0x4163636f756e74734d6f64756c6553696e67756c61724c6162656c204964),
(115, 'EzcodxModule', 0x4e616d65),
(116, 'ActivitiesModule', 0x4c61746573742041637469766974696573),
(117, 'WorkflowsModule', 0x557365722077686f206c617374206d6f646966696564207265636f7264),
(118, 'UsersModule', 0x5573657273),
(119, 'UsersModule', 0x4372656174652055736572),
(120, 'UsersModule', 0x4e6f6e65),
(121, 'UsersModule', 0x486964652077656c636f6d652070616765),
(122, 'UsersModule', 0x55736572),
(123, 'UsersModule', 0x437265617465205573657273),
(124, 'UsersModule', 0x4d792050726f66696c65),
(125, 'UsersModule', 0x5369676e206f7574),
(126, 'ContactWebFormsModule', 0x43726561746520436f6e74616374576562466f726d734d6f64756c6553696e67756c61724c6162656c),
(127, 'ImportModule', 0x57686f2063616e207265616420616e6420777269746520746865206e6577207b696d706f727452756c65734c6162656c7d),
(128, 'ImportModule', 0x4f74686572204964),
(129, 'DesignerModule', 0x506f72746c6574204c6973742056696577),
(130, 'DesignerModule', 0x53617665204c61796f7574),
(131, 'DesignerModule', 0x446174652054696d65),
(132, 'DesignerModule', 0x4c61796f7574207361766564207375636365737366756c6c792e),
(133, 'Core', 0x20416e6e75616c6c79),
(134, 'HomeModule', 0x41726520796f7520737572652077616e7420746f2064656c65746520746869732064617368626f6172643f),
(135, 'HomeModule', 0x4372656174652044617368626f61726473),
(136, 'HomeModule', 0x44656c6574652044617368626f61726473),
(137, 'HomeModule', 0x456469742044617368626f617264),
(138, 'HomeModule', 0x44656c6574652044617368626f617264),
(139, 'HomeModule', 0x4372656174652044617368626f617264),
(140, 'GamificationModule', 0x4c6561646572626f617264),
(141, 'ExportModule', 0x4578706f7274),
(142, 'ExportModule', 0x4578706f727473),
(143, 'Core', 0x4e6f20726573756c747320666f756e64),
(144, 'CustomField', 0x4d722e),
(145, 'CustomField', 0x4d72732e),
(146, 'MashableInboxModule', 0x556e72656164),
(147, 'Core', 0x546865726520617265206e6f206d6f726520706f72746c65747320746f20616464),
(148, 'NotificationsModule', 0x5669657720416c6c204e6f74696669636174696f6e73),
(149, 'ContactWebFormsModule', 0x456469742057656220466f726d),
(150, 'DesignerModule', 0x45646974204669656c64),
(151, 'DesignerModule', 0x45646974204c61796f75746f726761),
(152, 'Core', 0x53776974636820746f20427573696e65737320436172642056696577),
(153, 'ImportModule', 0x55706461746564),
(154, 'ContractsModule', 0x436f6e7472616374734d6f64756c65506c7572616c4c6162656c),
(155, 'HomeModule', 0x4261636b20746f204170706c69636174696f6e),
(156, 'EzcodxModule', 0x486f6d65),
(157, 'DesignerModule', 0x45646974204c61796f7574),
(158, 'ContractsModule', 0x73746172742064617465),
(159, 'Default', 0x4f7574626f756e6420456d61696c20436f6e66696775726174696f6e2028534d545029),
(160, 'Default', 0x4c6973742046696c74657273),
(161, 'Default', 0x496e76616c696420415049207265717565737420747970652e),
(162, 'Default', 0x496e76616c69642072657175657374),
(163, 'Default', 0x537562736372697074696f6e),
(164, 'Default', 0x496e616374697665),
(165, 'Default', 0x53657276696365),
(166, 'Default', 0x4f6e652054696d65),
(167, 'Commands', 0x416e206572726f72206f6363757220647572696e67206461746162617365206261636b75702f726573746f72653a207b6d6573736167657d),
(168, 'Commands', 0x4261636b75702066696c6520616c7265616479206578697374732e2041726520796f75207375726520796f752077616e7420746f206f766572777269746520746865206578697374696e672066696c653f2e),
(169, 'Commands', 0x4261636b7570206e6f7420636f6d706c657465642e),
(170, 'Commands', 0x506c656173652064656c657465206578697374696e672066696c65206f7220656e746572206e6577206f6e652c20616e64207374617274206261636b75702070726f6365737320616761696e2e),
(171, 'Commands', 0x5374617274696e67206461746162617365206261636b75702070726f636573732e),
(172, 'Commands', 0x4461746162617365206261636b757020636f6d706c657465642e),
(173, 'Commands', 0x54686572652077617320616e206572726f7220647572696e67206261636b75702e),
(174, 'Commands', 0x44656c6574696e67206261636b75702066696c652e),
(175, 'Commands', 0x506c65617365206261636b7570206461746162617365206d616e75616c6c792e),
(176, 'Commands', 0x5374617274696e6720646174616261736520726573746f72652070726f636573732e),
(177, 'Commands', 0x446174616261736520726573746f7265642e),
(178, 'Commands', 0x54686572652077617320616e206572726f7220647572696e6720726573746f72652e),
(179, 'Commands', 0x5374617274696e6720736368656d61207570646174652070726f636573732e),
(180, 'Commands', 0x536368656d612075706461746520636f6d706c6574652e),
(181, 'Commands', 0x546f74616c2072756e2074696d653a207b666f726d617474656454696d657d207365636f6e64732e),
(182, 'Commands', 0x5374617274696e6720457a636f647820757067726164652070726f636573732e),
(183, 'Commands', 0x457a636f64782075706772616465207068617365203120636f6d706c657465642e),
(184, 'Commands', 0x506c656173652065786563757465206e65787420636f6d6d616e643a20227b636f6d6d616e647d2220746f20636f6d706c65746520757067726164652070726f636573732e),
(185, 'Commands', 0x5374617274696e6720457a636f647820757067726164652070726f63657373202d20706861736520322e),
(186, 'Commands', 0x457a636f6478207570677261646520636f6d706c657465642e),
(187, 'Commands', 0x416e206572726f72206f6363757220647572696e6720757067726164653a207b6d6573736167657d),
(188, 'Commands', 0x546869732069732074686520457a636f647820757067726164652070726f636573732e20506c65617365206261636b75702066696c65732f6461746162617365206265666f726520796f7520636f6e74696e75652e),
(189, 'Commands', 0x41726520796f75207375726520796f752077616e7420746f207570677261646520457a636f64783f205b7965737c6e6f5d),
(190, 'Commands', 0x557067726164652070726f636573732068616c7465642e),
(191, 'Commands', 0x416e206572726f72206f6363757220647572696e67206d65746164617461206d616e6167653a207b6d6573736167657d),
(192, 'Core', 0x4e6f7420656e6f756768206461746120746f2072656e646572206368617274),
(193, 'Core', 0x4e6f20696d706f72742070726f63657373657320666f756e642e),
(194, 'Core', 0x426c7565),
(195, 'Core', 0x42726f776e),
(196, 'Core', 0x436865727279),
(197, 'Core', 0x486f6e6579),
(198, 'Core', 0x4c696d65),
(199, 'Core', 0x54757271756f697365),
(200, 'Core', 0x56696f6c6574),
(201, 'Core', 0x4578636c7573697665205061706572),
(202, 'Core', 0x4e6f697365),
(203, 'Core', 0x5061706572),
(204, 'Core', 0x43616e206e6f74206f70656e207468652066696c652e),
(205, 'Core', 0x4661696c656420746f2072656164207468652066696c65),
(206, 'Core', 0x4661696c65642070617273696e67207b66696c65536f757263657d3a2073796e746178206572726f72206f6e206c696e65207b6c696e654e756d6265727d),
(207, 'Core', 0x457175616c73),
(208, 'Core', 0x446f6573204e6f7420457175616c),
(209, 'Core', 0x5374617274732057697468),
(210, 'Core', 0x456e64732057697468),
(211, 'Core', 0x436f6e7461696e73),
(212, 'Core', 0x47726561746572205468616e204f7220457175616c20546f),
(213, 'Core', 0x4c657373205468616e204f7220457175616c20546f),
(214, 'Core', 0x47726561746572205468616e),
(215, 'Core', 0x4c657373205468616e),
(216, 'Core', 0x4f6e65204f66),
(217, 'Core', 0x4973204e756c6c),
(218, 'Core', 0x4973204e6f74204e756c6c),
(219, 'Core', 0x4265636f6d6573),
(220, 'Core', 0x576173),
(221, 'Core', 0x4265636f6d6573204f6e65204f66),
(222, 'Core', 0x576173204f6e65204f66),
(223, 'Core', 0x4368616e676573),
(224, 'Core', 0x446f6573204e6f74204368616e6765),
(225, 'Core', 0x497320456d707479),
(226, 'Core', 0x4973204e6f7420456d707479),
(227, 'Core', 0x4f6e7b646174657d),
(228, 'Core', 0x4973),
(229, 'Core', 0x324420486f72697a6f6e74616c20426172204772617068),
(230, 'Core', 0x334420486f72697a6f6e74616c20426172204772617068),
(231, 'Core', 0x324420566572746963616c20426172204772617068),
(232, 'Core', 0x334420566572746963616c20426172204772617068),
(233, 'Core', 0x446f6e7574203244),
(234, 'Core', 0x446f6e7574203344),
(235, 'Core', 0x5570646174696e67),
(236, 'Core', 0x75706461746564207375636365737366756c6c79),
(237, 'Core', 0x4d61737320557064617465),
(238, 'Core', 0x4d6f7265204f7074696f6e73),
(239, 'Core', 0x4665776572204f7074696f6e73),
(240, 'Core', 0x44656c6574696e67),
(241, 'Core', 0x4d6173732044656c657465),
(242, 'Core', 0x54686973206d6f64756c6520646f65736e2774206861766520616e797468696e6720746f20636f6e6669677572652e),
(243, 'Core', 0x4d6f72652044657461696c73),
(244, 'Core', 0x46657765722044657461696c73),
(245, 'Core', 0x546162),
(246, 'Core', 0x746f74616c),
(247, 'Core', 0x7b6174747269627574657d206d757374206265207b76616c75657d2e),
(248, 'Core', 0x7b6174747269627574657d206d757374206265207b747970657d2e),
(249, 'Core', 0x4e616d65206d757374206e6f7420636f6e7461696e20737061636573206f72207370656369616c2063686172616374657273),
(250, 'Core', 0x466972737420636861726163746572206d7573742062652061206c6f7765722063617365206c6574746572),
(251, 'Core', 0x4d697373696e672074686520617474726962757465206c6162656c732e),
(252, 'Core', 0x4661696c656420746f2073657420756e736166652061747472696275746520227b6174747269627574657d222e),
(253, 'Core', 0x7265636f726473),
(254, 'Core', 0x52656d6f7665),
(255, 'MarketingListsModule', 0x7b6e7d204d61726b6574696e67204c69737420637265617465647c7b6e7d204d61726b6574696e67204c697374732063726561746564),
(256, 'MarketingListsModule', 0x4c6973742044617368626f617264),
(257, 'MarketingListsModule', 0x5768617420697320676f696e67206f6e20776974682074686973206c6973743f),
(258, 'MarketingListsModule', 0x437265617465204c697374),
(259, 'MarketingListsModule', 0x4d616e61676520537562736372697074696f6e73),
(260, 'MarketingListsModule', 0x556e73756273637269626520416c6c2f4f70744f7574),
(261, 'MarketingListsModule', 0x3c68323e4e6f7420736f2066617374213c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703e546f206d616e616765204d61726b6574696e67204c6973747320796f75206d75737420686176652061636365737320746f2065697468657220636f6e7461637473206f72206c656164732e20436f6e74616374207468652043524d2061646d696e6973747261746f722061626f757420746869732069737375652e3c2f703e),
(262, 'MarketingListsModule', 0x7b636f756e747d2053756273637269626564),
(263, 'MarketingListsModule', 0x7b636f756e747d20556e73756273637269626564),
(264, 'MarketingListsModule', 0x4d61726b6574696e67204c697374),
(265, 'MarketingListsModule', 0x4d61726b6574696e67204c69737473),
(266, 'MarketingListsModule', 0x4d61726b6574696e67204c697374204d656d62657273),
(267, 'MarketingListsModule', 0x4d61726b6574696e67204c697374204d656d626572),
(268, 'MarketingListsModule', 0x4c69737473),
(269, 'MarketingListsModule', 0x437265617465204d61726b6574696e67204c69737473),
(270, 'MarketingListsModule', 0x44656c657465204d61726b6574696e67204c69737473),
(271, 'MarketingListsModule', 0x416363657373204d61726b6574696e67204c6973747320546162),
(272, 'MarketingListsModule', 0x596f752068617665206265656e20737562736372696265642e),
(273, 'MarketingListsModule', 0x596f752068617665206265656e20756e737562736372696265642066726f6d20616c6c206d61726b6574696e67206c6973747320616e64206f70746564206f75742066726f6d20616c6c2066757475726520656d61696c732e),
(274, 'MarketingListsModule', 0x596f752068617665206265656e20756e737562736372696265642e),
(275, 'MarketingListsModule', 0x7b73756273637269626564436f756e747d20737562736372696265642e),
(276, 'EmailMessagesModule', 0x436c656172204f6c642053656e74204e6f74696669636174696f6e7320456d61696c204a6f62),
(277, 'EmailMessagesModule', 0x50726f63657373204f7574626f756e6420456d61696c204a6f62),
(278, 'EmailMessagesModule', 0x45766572792031206d696e7574652e),
(279, 'EmailMessagesModule', 0x50726f6365737320496e626f756e6420456d61696c204a6f62),
(280, 'EmailMessagesModule', 0x4661696c656420746f20636f6e6e65637420746f206d61696c626f78),
(281, 'EmailMessagesModule', 0x456d61696c206164647265737320646f6573206e6f7420657869737420696e2073797374656d),
(282, 'EmailMessagesModule', 0x53656e64657220696e666f2063616e2774206265206578747261637465642066726f6d20656d61696c206d657373616765),
(283, 'EmailMessagesModule', 0x526563697069656e7420696e666f2063616e2774206265206578747261637465642066726f6d20656d61696c206d657373616765),
(284, 'EmailMessagesModule', 0x456d61696c206d65737361676520636f756c64206e6f742062652076616c696461746564),
(285, 'EmailMessagesModule', 0x456d61696c206d65737361676520636f756c64206e6f74206265207361766564),
(286, 'EmailMessagesModule', 0x54657374696e67204f7574626f756e6420456d61696c20436f6e6e656374696f6e204a6f62),
(287, 'EmailMessagesModule', 0x41207465737420656d61696c2066726f6d20457a636f6478),
(288, 'EmailMessagesModule', 0x4120746573742074657874206d6573736167652066726f6d20457a636f64782e),
(289, 'EmailMessagesModule', 0x4d657373616765207375636365737366756c6c792073656e74),
(290, 'EmailMessagesModule', 0x4d657373616765206661696c656420746f2073656e64),
(291, 'EmailMessagesModule', 0x526573706f6e73652066726f6d20536572766572),
(292, 'EmailMessagesModule', 0x4d6174636820617263686976656420656d61696c73),
(293, 'EmailMessagesModule', 0x3c7370616e20636c6173733d22656d61696c2d66726f6d223e3c7374726f6e673e46726f6d3a3c2f7374726f6e673e207b73656e646572436f6e74656e747d3c2f7370616e3e),
(294, 'EmailMessagesModule', 0x3c7370616e20636c6173733d22656d61696c2d746f223e3c7374726f6e673e546f3a3c2f7374726f6e673e207b726563697069656e74436f6e74656e747d3c2f7370616e3e),
(295, 'EmailMessagesModule', 0x5468657265206973206e6f207072696d61727920656d61696c206173736f6369617465642077697468207b636f6e746163744e616d657d2e20506c6561736520616464206f6e6520746f20636f6e74696e75652e),
(296, 'EmailMessagesModule', 0x53656e64),
(297, 'EmailMessagesModule', 0x456d61696c20436f6e66696775726174696f6e),
(298, 'EmailMessagesModule', 0x456d61696c20417263686976696e6720436f6e66696775726174696f6e2028494d415029),
(299, 'EmailMessagesModule', 0x3c68323e4e6f7420736f20666173743c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703e436f6e66696775726520796f757220656d61696c2073657474696e6773206265666f726520796f752063616e2073656e6420656d61696c732e3c2f703e),
(300, 'EmailMessagesModule', 0x3c68323e4e6f7468696e6720746f2073656520686572653c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703e546865726520617265206e6f20756e6d61746368656420656d61696c733c2f703e),
(301, 'EmailMessagesModule', 0x4f7574626f756e6420456d61696c20436f6e66696775726174696f6e2028534d545029),
(302, 'EmailMessagesModule', 0x3c68323e4e6f7420736f20666173743c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703e5468652061646d696e6973747261746f72206d75737420666972737420636f6e666967757265207468652073797374656d206f7574626f756e6420656d61696c2073657474696e67732e3c2f703e),
(303, 'EmailMessagesModule', 0x46726f6d2041646472657373),
(304, 'EmailMessagesModule', 0x46726f6d204e616d65),
(305, 'EmailMessagesModule', 0x4f7574626f756e6420486f7374),
(306, 'EmailMessagesModule', 0x4f7574626f756e642050617373776f7264),
(307, 'EmailMessagesModule', 0x4f7574626f756e6420506f7274),
(308, 'EmailMessagesModule', 0x4f7574626f756e64205365637572697479),
(309, 'EmailMessagesModule', 0x4f7574626f756e642054797065),
(310, 'EmailMessagesModule', 0x4f7574626f756e6420557365726e616d65),
(311, 'EmailMessagesModule', 0x5265706c7920546f2041646472657373),
(312, 'EmailMessagesModule', 0x5265706c7920546f204e616d65),
(313, 'EmailMessagesModule', 0x55736520437573746f6d204f7574626f756e642053657474696e6773),
(314, 'EmailMessagesModule', 0x54686973206669656c64206973207265717569726564),
(315, 'EmailMessagesModule', 0x436c69636b),
(316, 'EmailMessagesModule', 0x456d61696c204d657373616765204163746976697479),
(317, 'EmailMessagesModule', 0x456d61696c204d6573736167652041637469766974696573),
(318, 'EmailMessagesModule', 0x456d61696c2053656e646572),
(319, 'EmailMessagesModule', 0x456d61696c2053656e64657273),
(320, 'EmailMessagesModule', 0x4572726f72204d6573736167653a),
(321, 'EmailMessagesModule', 0x456d61696c2053656e64204572726f72),
(322, 'EmailMessagesModule', 0x456d61696c2053656e64204572726f7273),
(323, 'EzcodxModule', 0x4372656174656420446174652054696d65),
(324, 'EmailMessagesModule', 0x466f6c64657273),
(325, 'EmailMessagesModule', 0x456d61696c20426f78),
(326, 'EmailMessagesModule', 0x456d61696c20426f786573),
(327, 'EmailMessagesModule', 0x456d61696c205369676e6174757265),
(328, 'EmailMessagesModule', 0x456d61696c205369676e617475726573),
(329, 'EmailMessagesModule', 0x48746d6c20436f6e74656e74),
(330, 'EmailMessagesModule', 0x5465787420436f6e74656e74),
(331, 'EmailMessagesModule', 0x436f6e74656e74),
(332, 'EmailMessagesModule', 0x526563697069656e7473),
(333, 'EmailMessagesModule', 0x53656e646572),
(334, 'EmailMessagesModule', 0x53656e6420417474656d707473),
(335, 'EmailMessagesModule', 0x53656e7420446174652054696d65),
(336, 'EmailMessagesModule', 0x456d61696c204d6573736167652055726c),
(337, 'EmailMessagesModule', 0x456d61696c204d6573736167652055726c73),
(338, 'EmailMessagesModule', 0x456d61696c20526563697069656e74),
(339, 'EmailMessagesModule', 0x456d61696c20526563697069656e7473),
(340, 'EmailMessagesModule', 0x546f2041646472657373),
(341, 'EmailMessagesModule', 0x546f204e616d65),
(342, 'EmailMessagesModule', 0x4472616674),
(343, 'EmailMessagesModule', 0x4f7574626f78),
(344, 'EmailMessagesModule', 0x4f7574626f78204572726f72),
(345, 'EmailMessagesModule', 0x4f7574626f78204661696c757265),
(346, 'EmailMessagesModule', 0x417263686976656420556e6d617463686564),
(347, 'EmailMessagesModule', 0x456d61696c20466f6c646572),
(348, 'EmailMessagesModule', 0x456d61696c20466f6c64657273),
(349, 'EmailMessagesModule', 0x456d61696c20436f6e74656e74),
(350, 'EmailMessagesModule', 0x456d61696c20436f6e74656e7473),
(351, 'EmailMessagesModule', 0x41636365737320456d61696c20436f6e66696775726174696f6e),
(352, 'EmailMessagesModule', 0x43726561746520456d61696c73),
(353, 'EmailMessagesModule', 0x41636365737320456d61696c7320546162),
(354, 'EmailMessagesModule', 0x4d616e61676520456d61696c20436f6e66696775726174696f6e),
(355, 'EmailMessagesModule', 0x4461746120436c65616e7570),
(356, 'EmailMessagesModule', 0x456d61696c20534d545020436f6e66696775726174696f6e),
(357, 'EmailMessagesModule', 0x4d616e61676520456d61696c20534d545020436f6e66696775726174696f6e),
(358, 'EmailMessagesModule', 0x456d61696c20417263686976696e6720436f6e66696775726174696f6e),
(359, 'EmailMessagesModule', 0x4d616e61676520456d61696c20417263686976696e6720436f6e66696775726174696f6e),
(360, 'EmailMessagesModule', 0x456d61696c204d657373616765),
(361, 'EmailMessagesModule', 0x456d61696c204d65737361676573),
(362, 'EmailMessagesModule', 0x54657374204d65737361676520526573756c7473),
(363, 'EmailMessagesModule', 0x53656e64205465737420456d61696c),
(364, 'EmailMessagesModule', 0x426f6479),
(365, 'EmailMessagesModule', 0x4363),
(366, 'EmailMessagesModule', 0x426363),
(367, 'EmailMessagesModule', 0x54797065206e616d65206f7220656d61696c),
(368, 'EmailMessagesModule', 0x42636320526563697069656e7473),
(369, 'EmailMessagesModule', 0x436320526563697069656e7473),
(370, 'EmailMessagesModule', 0x546f20526563697069656e7473),
(371, 'EmailMessagesModule', 0x546f20616464726573732063616e6e6f7420626520626c616e6b),
(372, 'EmailMessagesModule', 0x4578747261204d61696c2053657474696e6773),
(373, 'EmailMessagesModule', 0x53656e642061207465737420656d61696c20746f),
(374, 'EmailMessagesModule', 0x53534c20636f6e6e656374696f6e),
(375, 'EmailMessagesModule', 0x5465737420494d415020636f6e6e656374696f6e),
(376, 'EmailMessagesModule', 0x4d616e61676520796f757220656d61696c20707265666572656e636573),
(377, 'ConversationsModule', 0x7b6e7d20436f6e766572736174696f6e20637265617465647c7b6e7d20436f6e766572736174696f6e732063726561746564),
(378, 'ConversationsModule', 0x50617274696369706174696e6720496e),
(379, 'ConversationsModule', 0x436c6f736564),
(380, 'ConversationsModule', 0x5061727469636970616e7473),
(381, 'ConversationsModule', 0x43726561746520436f6e766572736174696f6e),
(382, 'ConversationsModule', 0x436f6e766572736174696f6e73),
(383, 'ConversationsModule', 0x436f6e766572736174696f6e205061727469636970616e74),
(384, 'ConversationsModule', 0x436f6e766572736174696f6e205061727469636970616e7473),
(385, 'ActivitiesModule', 0x4c617465737420446174652054696d65),
(386, 'ConversationsModule', 0x497320436c6f736564),
(387, 'ConversationsModule', 0x436f6e766572736174696f6e204974656d73),
(388, 'CommentsModule', 0x436f6d6d656e7473),
(389, 'ConversationsModule', 0x436f6e766572736174696f6e),
(390, 'ConversationsModule', 0x436f6e766572736174696f6e204c6174657374),
(391, 'ConversationsModule', 0x54797065206120557365722773206e616d65),
(392, 'ConversationsModule', 0x436f6e766572736174696f6e2073746174757320776173206368616e6765642e),
(393, 'ConversationsModule', 0x5061727469636970616e74732075706461746564207375636365737366756c6c79),
(394, 'ContactsModule', 0x43726561746520436f6e766572736174696f6e73),
(395, 'ContactsModule', 0x41636365737320436f6e766572736174696f6e7320546162),
(396, 'ConversationsModule', 0x596f752068617665206265656e20696e766974656420746f20706172746963697061746520696e206120636f6e766572736174696f6e),
(397, 'SocialItemsModule', 0x7b6e7d20506f737420637265617465647c7b6e7d20506f7374732063726561746564),
(398, 'MeetingsModule', 0x43617465676f7279),
(399, 'MeetingsModule', 0x456e642054696d65),
(400, 'MeetingsModule', 0x4c6f636174696f6e),
(401, 'MeetingsModule', 0x417474656e64656573),
(402, 'MeetingsModule', 0x4d656574696e67),
(403, 'MeetingsModule', 0x4d656574696e6773),
(404, 'InstallModule', 0x54686572652077617320612070726f626c656d206372656174696e6720746865206461746162617365204572726f7220636f64653a),
(405, 'InstallModule', 0x54686572652077617320612070726f626c656d206372656174696e67207468652075736572204572726f7220636f64653a),
(406, 'InstallModule', 0x4372656174696e6720737570657220757365722e),
(407, 'InstallModule', 0x5374617274696e6720646174616261736520736368656d61206372656174696f6e2e),
(408, 'InstallModule', 0x446174616261736520736368656d61206372656174696f6e20636f6d706c6574652e),
(409, 'ReportsModule', 0x7b6e7d205265706f727420637265617465647c7b6e7d205265706f7274732063726561746564),
(410, 'ReportsModule', 0x437265617465205265706f727473),
(411, 'ProductTemplatesModule', 0x4372656174652043617465676f7279),
(412, 'DesignerModule', 0x53656c6c20507269636520466f726d756c6120496e666f726d6174696f6e),
(413, 'DesignerModule', 0x53656c6c20507269636520466f726d756c61206669656c6473),
(414, 'MashableInboxModule', 0x436f6d62696e656420496e626f78),
(415, 'MashableInboxModule', 0x496e626f786573),
(416, 'ContactsModule', 0x7b6e7d20436f6e74616374734d6f64756c6553696e67756c61724c6162656c20637265617465647c7b6e7d20436f6e74616374734d6f64756c65506c7572616c4c6162656c2063726561746564),
(417, 'ContactsModule', 0x7b6e7d20436f6e74616374734d6f64756c6553696e67756c61724c6162656c2073656172636820636f6d706c657465647c7b6e7d20436f6e74616374734d6f64756c6553696e67756c61724c6162656c20736561726368657320636f6d706c65746564),
(418, 'ContactsModule', 0x7b6e7d20436f6e74616374734d6f64756c6553696e67756c61724c6162656c206d61737320757064617465647c7b6e7d20436f6e74616374734d6f64756c65506c7572616c4c6162656c206d6173732075706461746564),
(419, 'ContactsModule', 0x436f6e74616374205374617475736573),
(420, 'ContactsModule', 0x4372656174656420436f6e74616374734d6f64756c6553696e67756c61724c6162656c207375636365737366756c6c79),
(421, 'ContactsModule', 0x436f6e74616374),
(422, 'ContactsModule', 0x436f6e7461637473),
(423, 'ContactsModule', 0x4120636f6e74616374206669656c64),
(424, 'ContactsModule', 0x5374617274696e6720537461747573),
(425, 'ContactsModule', 0x436f6e7461637420537461747573205472616e736c61746564204c6162656c73),
(426, 'ContactsModule', 0x436f6e74616374205374616765),
(427, 'ContactsModule', 0x54686520636f6e74616374207374616765206669656c64),
(428, 'ContactsModule', 0x5468652064656661756c74207374617475732073706563696669656420646f6573206e6f742065786973742e),
(429, 'ContactsModule', 0x546865207374617475732069732072657175697265642e20204e65697468657220612076616c7565206e6f7220612064656661756c7420776173207370656369666965642e),
(430, 'ConfigurationModule', 0x436f6e66696775726174696f6e),
(431, 'ConfigurationModule', 0x436f6e66696775726174696f6e73),
(432, 'CampaignsModule', 0x47656e65726174652063616d706169676e206974656d73),
(433, 'CampaignsModule', 0x4d61726b2063616d706169676e7320617320636f6d706c65746564),
(434, 'CampaignsModule', 0x50726f636573732063616d706169676e206d65737361676573),
(435, 'CampaignsModule', 0x7b6e7d2043616d706169676e20637265617465647c7b6e7d2043616d706169676e732063726561746564),
(436, 'CampaignsModule', 0x43616d706169676e73),
(437, 'CampaignsModule', 0x43616d706169676e2044617368626f617264),
(438, 'CampaignsModule', 0x5768617420697320676f696e67206f6e207769746820746869732063616d706169676e3f),
(439, 'CampaignsModule', 0x3c68323e4e6f7420736f2066617374213c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703e546f206d616e6167652063616d706169676e7320796f75206d75737420686176652061636365737320746f20656d61696c2074656d706c6174657320616e64206d61726b6574696e67206c697374732e20436f6e74616374207468652043524d2061646d696e6973747261746f722061626f757420746869732069737375652e3c2f703e),
(440, 'CampaignsModule', 0x4372656174652043616d706169676e),
(441, 'CampaignsModule', 0x43616d706169676e204974656d),
(442, 'CampaignsModule', 0x43616d706169676e204974656d73),
(443, 'CampaignsModule', 0x43616d706169676e204974656d204163746976697479),
(444, 'CampaignsModule', 0x43616d706169676e204974656d2041637469766974696573),
(445, 'CampaignsModule', 0x506175736564),
(446, 'CampaignsModule', 0x43616d706169676e),
(447, 'CampaignsModule', 0x53656e64204f6e),
(448, 'CampaignsModule', 0x537570706f7274732048544d4c),
(449, 'CampaignsModule', 0x4372656174652043616d706169676e73),
(450, 'CampaignsModule', 0x44656c6574652043616d706169676e73),
(451, 'CampaignsModule', 0x4163636573732043616d706169676e7320546162),
(452, 'AccountsModule', 0x416e206163636f756e74206669656c64),
(453, 'ActivitiesModule', 0x4163746976697479),
(454, 'ActivitiesModule', 0x41637469766974696573),
(455, 'ActivitiesModule', 0x666f72207b72656c617465644d6f64656c73537472696e67436f6e74656e747d),
(456, 'ActivitiesModule', 0x77697468207b72656c61746564436f6e7461637473537472696e67436f6e74656e747d),
(457, 'ActivitiesModule', 0x4163746976697479204974656d73),
(458, 'ActivitiesModule', 0x4d696e65),
(459, 'ActivitiesModule', 0x41637469766974792053756d6d617279),
(460, 'ActivitiesModule', 0x526f6c6c205570),
(461, 'ActivitiesModule', 0x4f66667b7669736962696c6974797d),
(462, 'ActivitiesModule', 0x4f6e7b7669736962696c6974797d),
(463, 'ActivitiesModule', 0x5475726e20726f6c6c207570206f6e20746f2073656520616374697669746965732066726f6d2072656c61746564207265636f7264732e),
(464, 'ActivitiesModule', 0x416c6c2041637469766974696573),
(465, 'AutorespondersModule', 0x50726f63657373206175746f726573706f6e646572206d65737361676573),
(466, 'AutorespondersModule', 0x4175746f726573706f6e64657273),
(467, 'AutorespondersModule', 0x4175746f726573706f6e646572),
(468, 'AutorespondersModule', 0x4175746f726573706f6e646572204974656d),
(469, 'AutorespondersModule', 0x4175746f726573706f6e646572204974656d73),
(470, 'AutorespondersModule', 0x4175746f726573706f6e646572204974656d204163746976697479),
(471, 'AutorespondersModule', 0x4175746f726573706f6e646572204974656d2041637469766974696573),
(472, 'AutorespondersModule', 0x537562736372697074696f6e20746f206c697374),
(473, 'AutorespondersModule', 0x556e737562736372696265642066726f6d206c697374),
(474, 'MarketingModule', 0x7b7175616e746974797d2073656e74),
(475, 'MarketingModule', 0x7b7175616e746974797d206f70656e7320287b6f70656e526174657d2529),
(476, 'EmailTemplatesModule', 0x50726f766964656420636f6e74656e7420636f6e7461696e732066657720696e76616c6964206d6572676520746167732e),
(477, 'WorkflowsModule', 0x43726561746520576f726b666c6f77),
(478, 'WorkflowsModule', 0x43726561746520576f726b666c6f7773),
(479, 'EmailTemplatesModule', 0x4372656174652054656d706c617465),
(480, 'WorkflowsModule', 0x557365722077686f2063726561746564207265636f7264),
(481, 'WorkflowsModule', 0x557365722773206d616e616765722077686f2063726561746564207265636f7264),
(482, 'ContactWebFormsModule', 0x436f6e746163742057656220466f726d73),
(483, 'ContactWebFormsModule', 0x4372656174652057656220466f726d),
(484, 'ContactWebFormsModule', 0x52656469726563742055726c),
(485, 'ContactWebFormsModule', 0x5375626d697420427574746f6e204c6162656c),
(486, 'ContactWebFormsModule', 0x44656661756c7420537461747573),
(487, 'ContactWebFormsModule', 0x436f6e746163742057656220466f726d20456e7472696573),
(488, 'ContactWebFormsModule', 0x57656220466f726d73),
(489, 'ContactWebFormsModule', 0x4d616e6167652057656220466f726d73),
(490, 'MapsModule', 0x416464726573732047656f636f646520557064617465204a6f62),
(491, 'MapsModule', 0x4576657279203330206d696e757465732e),
(492, 'MapsModule', 0x4d61707320436f6e66696775726174696f6e),
(493, 'MapsModule', 0x416363657373204d6170732041646d696e697374726174696f6e),
(494, 'MapsModule', 0x4d617073),
(495, 'MapsModule', 0x4d616e616765204d617020436f6e66696775726174696f6e),
(496, 'MapsModule', 0x4d6170),
(497, 'MapsModule', 0x476f6f676c65204d617020415049204b6579),
(498, 'ImportModule', 0x496d706f7274),
(499, 'ImportModule', 0x457a636f6478204669656c64),
(500, 'ImportModule', 0x486561646572),
(501, 'ImportModule', 0x52756c6573),
(502, 'ImportModule', 0x416464204669656c64),
(503, 'ImportModule', 0x436f6e67726174756c6174696f6e732120596f757220696d706f727420697320636f6d706c6574652e202042656c6f7720697320612073756d6d617279206f662074686520726573756c74732e),
(504, 'ImportModule', 0x457a636f647820557365726e616d65),
(505, 'ImportModule', 0x506c656173652073656c65637420746865206d6f64756c6520796f7520776f756c64206c696b6520746f20696d706f727420746f3a),
(506, 'ImportModule', 0x7b6669727374446174657d206f72207b7365636f6e64446174657d),
(507, 'ImportModule', 0x44656661756c74204e616d65),
(508, 'ImportModule', 0x54797065206f662056616c7565),
(509, 'ImportModule', 0x466f726d6174),
(510, 'ImportModule', 0x496e76616c6964206461746520666f726d61742e),
(511, 'ImportModule', 0x496e76616c696420636865636b20626f7820666f726d61742e),
(512, 'ImportModule', 0x496d706f72742066696c6520686173206e6f20726f777320746f207573652e),
(513, 'ImportModule', 0x596f7520646f206e6f742068617665207065726d697373696f6e20746f206372656174652f7570646174652074686973207265636f726420616e642f6f72206974732072656c61746564207265636f72642e),
(514, 'ImportModule', 0x5374617274696e6720696d706f727420666f722070726f636573733a207b70726f636573734e616d657d),
(515, 'ImportModule', 0x456e64696e6720696d706f72742e),
(516, 'ImportModule', 0x53616d706c6520526f77),
(517, 'ImportModule', 0x496d706f7274696e6720646174612e2e2e),
(518, 'ImportModule', 0x436f6d706c6574696e672e2e2e),
(519, 'ImportModule', 0x5265636f7264287329207b73746172744974656d436f756e747d202d207b656e644974656d436f756e747d206f66207b746f74616c4974656d436f756e747d),
(520, 'ImportModule', 0x52656d6f7665204669656c64),
(521, 'ApiModule', 0x415049),
(522, 'ApiModule', 0x41504973),
(523, 'ApiModule', 0x596f7520646f206e6f742068617665207065726d697373696f6e7320666f72207468697320616374696f6e2e),
(524, 'TasksModule', 0x4d79204f70656e205461736b734d6f64756c65506c7572616c4c6162656c),
(525, 'TasksModule', 0x5461736b73),
(526, 'DesignerModule', 0x44657461696c20616e6420456469742056696577),
(527, 'DesignerModule', 0x456469742056696577),
(528, 'DesignerModule', 0x506f707570204c6973742056696577),
(529, 'DesignerModule', 0x4c6973742056696577),
(530, 'DesignerModule', 0x44657461696c7320506f72746c65742056696577),
(531, 'DesignerModule', 0x506f72746c657420436f6e66696775726174696f6e2056696577),
(532, 'DesignerModule', 0x5365617263682056696577),
(533, 'DesignerModule', 0x52656c61746564204c6973742056696577),
(534, 'DesignerModule', 0x44657461696c732056696577),
(535, 'DesignerModule', 0x496e6c696e6520456469742056696577),
(536, 'DesignerModule', 0x506f707570205365617263682056696577),
(537, 'DesignerModule', 0x437573746f6d204669656c6473),
(538, 'DesignerModule', 0x5374616e64617264204669656c6473),
(539, 'DesignerModule', 0x5069636b204c6973742056616c756573),
(540, 'DesignerModule', 0x44657369676e6572),
(541, 'DesignerModule', 0x446973706c6179207b6c6162656c7d207768656e),
(542, 'DesignerModule', 0x596f75206d7573742066697273742073656c6563742061206669656c642074797065),
(543, 'DesignerModule', 0x437265617465204669656c64),
(544, 'DesignerModule', 0x53656c6563742061206669656c642074797065),
(545, 'DesignerModule', 0x4669656c642054797065),
(546, 'DesignerModule', 0x4669656c64204e616d65),
(547, 'DesignerModule', 0x466f726d756c61204e616d65),
(548, 'DesignerModule', 0x416c6c2070616e656c732076697369626c65),
(549, 'DesignerModule', 0x46697273742070616e656c2076697369626c652c207468656e206073686f77206d6f726560206c696e6b),
(550, 'DesignerModule', 0x50616e656c732061726520746162626564),
(551, 'DesignerModule', 0x4669656c6473),
(552, 'DesignerModule', 0x4c61796f757473),
(553, 'DesignerModule', 0x4163636573732044657369676e657220546f6f6c),
(554, 'DesignerModule', 0x4d616e616765206d6f64756c65206669656c64732c206c61796f7574732c20616e64206c6162656c732e),
(555, 'DesignerModule', 0x436865636b20426f78),
(556, 'DesignerModule', 0x4120636865636b20626f78),
(557, 'DesignerModule', 0x456d61696c204164647265737320496e666f726d6174696f6e),
(558, 'DesignerModule', 0x456d61696c2061646472657373206669656c6473),
(559, 'DesignerModule', 0x4120646174652f74696d65206669656c64),
(560, 'DesignerModule', 0x54657874),
(561, 'DesignerModule', 0x412074657874206669656c64),
(562, 'DesignerModule', 0x507265636973696f6e),
(563, 'DesignerModule', 0x446563696d616c),
(564, 'DesignerModule', 0x4120646563696d616c206669656c64),
(565, 'DesignerModule', 0x466f726d756c61),
(566, 'DesignerModule', 0x43616c63756c61746564204e756d626572),
(567, 'DesignerModule', 0x412063616c63756c61746564206e756d626572206261736564206f6e206f74686572206669656c642076616c756573),
(568, 'DesignerModule', 0x54686520666f726d756c6120697320696e76616c69642e),
(569, 'DesignerModule', 0x41206669656c6420776974682074686973206e616d6520697320616c726561647920757365642e),
(570, 'DesignerModule', 0x496e7465676572),
(571, 'DesignerModule', 0x416e20696e7465676572206669656c64),
(572, 'DesignerModule', 0x412063757272656e6379206669656c64),
(573, 'DesignerModule', 0x41206669656c64207468617420636f6e7461696e7320612055524c),
(574, 'DesignerModule', 0x5265717569726564204669656c64),
(575, 'DesignerModule', 0x227b246174747269627574654e616d657d22206669656c64206e616d65206973206120646174616261736520726573657276656420776f72642e20506c6561736520656e746572206120646966666572656e74206f6e652e),
(576, 'DesignerModule', 0x227b246174747269627574654e616d657d22206669656c64206e616d6520636f6e7461696e7320726573657276656420636861726163746572732e20456974686572207b7265736572766564317d206f72207b7265736572766564327d2e),
(577, 'DesignerModule', 0x546578742041726561),
(578, 'DesignerModule', 0x41206465736372697074696f6e20626f78),
(579, 'DesignerModule', 0x50686f6e65),
(580, 'DesignerModule', 0x412070686f6e65206669656c64),
(581, 'DesignerModule', 0x596f75206d7573742073656c656374206174206c656173742032207069636b2d6c697374732e),
(582, 'DesignerModule', 0x596f752063616e206f6e6c792068617665206174206d6f73742034207069636b2d6c697374732073656c65637465642e),
(583, 'DesignerModule', 0x41646472657373206669656c6473),
(584, 'DesignerModule', 0x4d696e696d756d2056616c7565),
(585, 'DesignerModule', 0x4d6178696d756d2056616c7565),
(586, 'DesignerModule', 0x44617465),
(587, 'DesignerModule', 0x412064617465206669656c64),
(588, 'DesignerModule', 0x596f75206d7573742068617665206174206c65617374206f6e65206669656c6420706c6163656420696e206f7264657220746f20736176652061206c61796f75742e),
(589, 'DesignerModule', 0x596f75206d7573742068617665206174206c65617374206f6e652070616e656c20696e206f7264657220746f20736176652061206c61796f75742e),
(590, 'DesignerModule', 0x416c6c207265717569726564206669656c6473206d75737420626520706c6163656420696e2074686973206c61796f75742e),
(591, 'DesignerModule', 0x4c6576656c3a207b6e756d6265727d),
(592, 'DesignerModule', 0x46697273742073656c656374206c6576656c207b6e756d6265727d),
(593, 'ProductsModule', 0x50726f6475637473),
(594, 'ProductsModule', 0x50726f64756374),
(595, 'ProductTemplatesModule', 0x50726f647563742043617465676f72696573),
(596, 'MarketingModule', 0x4d61726b6574696e67),
(597, 'MarketingModule', 0x5768617420697320676f696e67206f6e2077697468204d61726b6574696e673f),
(598, 'MarketingModule', 0x4f766572616c6c204c69737420506572666f726d616e6365),
(599, 'MarketingModule', 0x4c6973742047726f777468),
(600, 'MarketingModule', 0x486f7720646f657320456d61696c204d61726b6574696e6720776f726b20696e20457a636f64783f),
(601, 'MarketingModule', 0x4d61726b6574696e672044617368626f617264),
(602, 'MarketingModule', 0x456e642044617465),
(603, 'MarketingModule', 0x416363657373204d61726b6574696e6720546162),
(604, 'MarketingModule', 0x556e6971756520435452),
(605, 'MarketingModule', 0x435452),
(606, 'MarketingModule', 0x556e69717565204f70656e2052617465),
(607, 'MarketingModule', 0x4f70656e2052617465),
(608, 'MarketingModule', 0x517565756564),
(609, 'MarketingModule', 0x4f70656e6564),
(610, 'MarketingModule', 0x436c69636b6564),
(611, 'MarketingModule', 0x426f756e636564),
(612, 'MarketingModule', 0x4578697374696e67205375627363726962657273),
(613, 'MarketingModule', 0x4e6577205375627363726962657273),
(614, 'HomeModule', 0x48656c7066756c204c696e6b73),
(615, 'HomeModule', 0x4a6f696e2074686520666f72756d),
(616, 'HomeModule', 0x52656164207468652077696b69),
(617, 'HomeModule', 0x566965772061207475746f7269616c),
(618, 'HomeModule', 0x5761746368206120766964656f),
(619, 'HomeModule', 0x57656c636f6d6520746f20457a636f6478),
(620, 'HomeModule', 0x5573696e6720612043524d2073686f756c646e277420626520612063686f72652e205769746820457a636f64782c20796f752063616e206561726e20706f696e74732c20636f6c6c656374206261646765732c20616e6420636f6d7065746520616761696e737420636f2d776f726b657273207768696c652067657474696e6720796f7572206a6f6220646f6e652e),
(621, 'HomeModule', 0x546970206f662074686520446179),
(622, 'HomeModule', 0x4e65787420546970),
(623, 'HomeModule', 0x476f20746f207468652064617368626f617264),
(624, 'HomeModule', 0x446f6e27742073686f77206d6520746869732073637265656e20616761696e),
(625, 'HomeModule', 0x446f6e277420776f72727920796f752063616e207475726e206974206f6e20616761696e),
(626, 'HomeModule', 0x4163636573732044617368626f61726473),
(627, 'HomeModule', 0x5377697463682044617368626f617264),
(628, 'OpportunitiesModule', 0x4f70706f7274756e697479),
(629, 'OpportunitiesModule', 0x4f70706f7274756e6974696573),
(630, 'GamificationModule', 0x53616c6573),
(631, 'GamificationModule', 0x4e657720427573696e657373),
(632, 'GamificationModule', 0x436f6d6d756e69636174696f6e),
(633, 'GamificationModule', 0x4163636f756e74204d616e6167656d656e74),
(634, 'GamificationModule', 0x54696d65204d616e6167656d656e74),
(635, 'MissionsModule', 0x7b6e7d204d697373696f6e20637265617465647c7b6e7d204d697373696f6e732063726561746564),
(636, 'MissionsModule', 0x4d79204d697373696f6e73),
(637, 'MissionsModule', 0x41206d697373696f6e20796f75206372656174656420686173206265656e20636f6d706c65746564),
(638, 'MissionsModule', 0x437265617465204d697373696f6e73),
(639, 'CommentsModule', 0x436f6d6d656e74),
(640, 'CommentsModule', 0x7b6e7d20436f6d6d656e7420637265617465647c7b6e7d20436f6d6d656e74732063726561746564),
(641, 'CommentsModule', 0x53686f77206f6c64657220636f6d6d656e7473),
(642, 'CommentsModule', 0x4e657720636f6d6d656e74206f6e207b6d6f64656c4e616d657d3a207b7375626a6563747d),
(643, 'EzcodxModule', 0x4d6f64656c20776173206e6f7420637265617465642e),
(644, 'EzcodxModule', 0x526563656e746c7920566965776564),
(645, 'EzcodxModule', 0x4372656174652047726f7570),
(646, 'EzcodxModule', 0x43726561746520526f6c65),
(647, 'EzcodxModule', 0x437265617465),
(648, 'EzcodxModule', 0x43757272656e636965733a20437265617465),
(649, 'EzcodxModule', 0x4163746976652063757272656e636965732063616e2062652075736564207768656e206372656174696e67206e6577207265636f72647320616e6420617320612064656661756c742063757272656e637920666f72206120757365722e),
(650, 'EzcodxModule', 0x437265617465642042792055736572),
(651, 'EzcodxModule', 0x4372656174652047726f757073),
(652, 'EzcodxModule', 0x43726561746520526f6c6573),
(653, 'EzcodxModule', 0x546865726520617265206e6f20726563656e746c7920766965776564206974656d732e),
(654, 'EmailTemplatesModule', 0x7b6e7d20456d61696c2054656d706c61746520637265617465647c7b6e7d20456d61696c2054656d706c617465732063726561746564),
(655, 'EmailTemplatesModule', 0x456d61696c2054656d706c61746573),
(656, 'EmailTemplatesModule', 0x41206d65726765207461672073746172747320776974683a207b7461675072656669787d20616e6420656e64732077697468207b7461675375666669787d2e),
(657, 'EmailTemplatesModule', 0x4265747765656e207374617274696e6720616e6420636c6f73696e6720746167732069742063616e2068617665206669656c64206e616d65732e205468657365206e616d657320617265207772697474656e20696e20616c6c2063617073207265676172646c657373206f662061637475616c206669656c64206e616d6520636173652e),
(658, 'EmailTemplatesModule', 0x546f2061636365737320612072656c61746564206669656c642c207573652074686520666f6c6c6f77696e67207072656669783a207b70726f706572747944656c696d697465727d),
(659, 'EmailTemplatesModule', 0x43726561746520456d61696c2054656d706c61746573),
(660, 'MarketingModule', 0x456d61696c7320696e2074686973204c697374),
(661, 'MarketingListsModule', 0x3c68323e4e6f7420736f2066617374213c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703e4163636573732064656e6965642064756520746f2070726576696577206d6f6465206265696e67206163746976652e3c2f703e),
(662, 'EmailMessagesModule', 0x45766572792035206d696e757465732e),
(663, 'EmailMessagesModule', 0x426f756e6365),
(664, 'EmailMessagesModule', 0x426f756e636520436f6e66696775726174696f6e),
(665, 'EmailMessagesModule', 0x4d616e61676520426f756e636520436f6e66696775726174696f6e),
(666, 'MarketingModule', 0x4f766572616c6c2043616d706169676e20506572666f726d616e6365),
(667, 'MarketingModule', 0x456d61696c7320696e20746869732043616d706169676e),
(668, 'CampaignsModule', 0x5361766520616e64205363686564756c65),
(669, 'CampaignsModule', 0x596f752063616e6e6f7420736565207468697320636f6e746163742064756520746f206c696d6974656420616363657373),
(670, 'MarketingModule', 0x53656e64204661696c6564),
(671, 'EmailMessagesModule', 0x426f756e636520436f6e66696775726174696f6e2028494d415029),
(672, 'MarketingModule', 0x7b7175616e746974797d20556e7375627363726962656420287b756e73756273637269626564526174657d2529),
(673, 'ContactWebFormsModule', 0x4578636c756465205374796c6573),
(674, 'ContactWebFormsModule', 0x57656220466f726d204e616d6532),
(675, 'ContactWebFormsModule', 0x57656220466f726d204e616d65),
(676, 'DesignerModule', 0x41206669656c6420776974682074686973206e616d6520616e64206461746120697320616c7265616479207573656420696e20616e6f74686572206d6f64756c652e),
(677, 'MarketingModule', 0x4f766572616c6c20506572666f726d616e6365),
(678, 'MarketingModule', 0x4f766572616c6c20456d61696c20506572666f726d616e6365),
(679, 'MarketingListsModule', 0x4e616d652077696c6c206265207075626c69636c79207669657761626c6520627920436f6e74616374732f4c6561647320746f206d616e61676520746865697220737562736372697074696f6e73),
(680, 'Default', 0x437265617465204d61726b6574696e67204c697374),
(681, 'MarketingListsModule', 0x50726f63657373696e673a207b70657263656e74616765436f6d706c6574657d20636f6d706c657465),
(682, 'EmailMessagesModule', 0x4661696c656420746f2070726f63657373204d6573736167652069643a207b7569647d),
(683, 'EmailMessagesModule', 0x456d61696c20636f6e66696775726174696f6e207361766564207375636365737366756c6c792e),
(684, 'EmailMessagesModule', 0x456d61696c20617263686976696e6720636f6e66696775726174696f6e207361766564207375636365737366756c6c792e),
(685, 'EmailMessagesModule', 0x426f756e636520636f6e66696775726174696f6e207361766564207375636365737366756c6c792e),
(686, 'EmailMessagesModule', 0x41207465737420656d61696c2061646472657373206d75737420626520656e7465726564206265666f726520796f752063616e2073656e642061207465737420656d61696c2e),
(687, 'EmailMessagesModule', 0x436f756c64206e6f7420636f6e6e65637420746f20494d4150207365727665722e),
(688, 'EmailMessagesModule', 0x5375636365737366756c6c7920636f6e6e656374656420746f20494d4150207365727665722e),
(689, 'EmailMessagesModule', 0x556e6d61746368656420417263686976656420456d61696c73),
(690, 'CampaignsModule', 0x596f752063616e6e6f74207365652074686520706572666f726d616e6365206d6574726963732064756520746f206c696d6974656420616363657373),
(691, 'CampaignsModule', 0x43616d706169676e732077696c6c206e6f742072756e2070726f7065726c7920756e74696c207363686564756c6564206a6f627320617265207365742075702e20436f6e7461637420796f75722061646d696e6973747261746f722e),
(692, 'AutorespondersModule', 0x41726520796f75207375726520796f752077616e7420746f2064656c6574652074686973206175746f726573706f6e6465723f),
(693, 'AutorespondersModule', 0x4175746f726573706f6e646572732077696c6c206e6f742072756e2070726f7065726c7920756e74696c207363686564756c6564206a6f627320617265207365742075702e20436f6e7461637420796f75722061646d696e6973747261746f722e),
(694, 'ContactWebFormsModule', 0x436f6e746163742057656220466f726d),
(695, 'ContactWebFormsModule', 0x436f6e746163742057656220466f726d20456e747279),
(696, 'MapsModule', 0x4d61707320636f6e66696775726174696f6e207361766564207375636365737366756c6c792e),
(697, 'ImportModule', 0x496d706f7274206d6f64656c206661696c656420746f20736176652e),
(698, 'DesignerModule', 0x417661696c61626c65204d6f64756c6573),
(699, 'DesignerModule', 0x496e76616c69642070616e656c20636f6e66696775726174696f6e2074797065),
(700, 'MissionsModule', 0x437265617465204d697373696f6e),
(701, 'CampaignsModule', 0x5768656e20636865636b65642c20656d61696c2077696c6c2062652073656e7420696e20626f7468207465787420616e642048544d4c20666f726d61742e2020556e636865636b20746f206f6e6c792073656e64207465787420656d61696c73),
(702, 'CampaignsModule', 0x53617665),
(703, 'CommentsModule', 0x41646420436f6d6d656e74),
(704, 'ContactsModule', 0x52656379636c6564),
(705, 'ContactsModule', 0x44656164),
(706, 'ContactsModule', 0x5175616c6966696564),
(707, 'ContactsModule', 0x5365636f6e646172792041646472657373);
INSERT INTO `messagesource` (`id`, `category`, `source`) VALUES
(708, 'EmailMessagesModule', 0x4d61746368696e6720417263686976656420456d61696c73),
(709, 'ContactWebFormsModule', 0x43686f73656e204669656c6473),
(710, 'ContactWebFormsModule', 0x417661696c61626c65204669656c6473),
(711, 'ContactWebFormsModule', 0x466f726d204c61796f7574),
(712, 'Core', 0x43726561746564),
(713, 'AutorespondersModule', 0x53656e6420696d6d6564696174656c79206166746572207b6f7065726174696f6e7d),
(714, 'AutorespondersModule', 0x53656e64207b696e74657276616c7d207b747970657d206166746572207b6f7065726174696f6e7d),
(715, 'CampaignsModule', 0x4d6f64656c732070726f6365737365643a207b636f756e747d204d656d6f727920636f737420706572206d6f64656c3a207b636f73747d),
(716, 'CampaignsModule', 0x46696e616c206d656d6f72792075736167653a207b75736167657d),
(717, 'CampaignsModule', 0x43616d706169676e7320466f72204d61726b6574696e67204c697374),
(718, 'ImportModule', 0x497320696e76616c69642e),
(719, 'ContactsModule', 0x53746174757320737065636966696564206973206e6f7420756e6971756520616e6420697320696e76616c69642e),
(720, 'ContactsModule', 0x5374617475732073706563696669656420646f6573206e6f742065786973742e),
(721, 'DesignerModule', 0x417661696c61626c65204669656c647320466f7220466f726d756c613a),
(722, 'DesignerModule', 0x43616e206265207573656420696e206d6174682065787072657373696f6e),
(723, 'DesignerModule', 0x546865726520617265206e6f206669656c647320696e2074686973206d6f64756c6520746f206265207573656420696e20616e2065787072657373696f6e2e),
(724, 'HomeModule', 0x7b6e756d6265727d20436f6c756d6e),
(725, 'HomeModule', 0x7b6e756d6265727d20436f6c756d6e73),
(726, 'ImportModule', 0x416e616c797a696e67),
(727, 'ImportModule', 0x46696e616c697a696e6720416e616c797369732e2e2e),
(728, 'ImportModule', 0x497320616e20696e76616c696420656d61696c2e),
(729, 'ImportModule', 0x497320746f6f206c6f6e672e204d696e696d756d206c656e677468206973207b6d696e696d756d4c656e6774687d),
(730, 'ImportModule', 0x497320616e206578697374696e67207265636f726420616e642077696c6c20626520757064617465642e),
(731, 'ImportModule', 0x497320746f6f206c6f6e672e),
(732, 'ImportModule', 0x497320746f6f2073686f72742e),
(733, 'ImportModule', 0x46756c6c204e616d65206d75737420636f6e7461696e2061204c617374204e616d652c2077686963682069732072657175697265642e),
(734, 'ImportModule', 0x497320746f6f2073686f72742e204d696e696d756d206c656e677468206973207b6d696e696d756d4c656e6774687d2e),
(735, 'ImportModule', 0x56616c756520697320746f6f206c6f6e672e),
(736, 'ImportModule', 0x576173206e6f7420666f756e6420616e642077696c6c206372656174652061206e6577207265636f726420647572696e6720696d706f72742e),
(737, 'ImportModule', 0x4973202072657175697265642e),
(738, 'ImportModule', 0x497320746f6f206c6f6e672e204d6178696d756d206c656e677468206973207b6d6178696d756d4c656e6774687d2e20546869732076616c75652077696c6c207472756e63617465642075706f6e20696d706f72742e),
(739, 'ImportModule', 0x497320746f6f206c6f6e672e204d696e696d756d206c656e677468206973207b6d696e696d756d4c656e6774687d2e),
(740, 'ImportModule', 0x557365722049642073706563696669656420646964206e6f74206d6174636820616e79206578697374696e67207265636f7264732e),
(741, 'ImportModule', 0x557365726e616d652073706563696669656420646964206e6f74206d6174636820616e79206578697374696e67207265636f7264732e),
(742, 'ImportModule', 0x416e616c797a652044617461),
(743, 'ImportModule', 0x496d706f72742044617461),
(744, 'ImportModule', 0x436f6c756d6e207b6e7d),
(745, 'ImportModule', 0x4461746120416e616c7973697320697320636f6d706c6574652e20506c65617365207265766965772074686520726573756c74732062656c6f772e205768656e20796f75206172652072656164792c20636c69636b20224e6578742220746f20696d706f727420796f757220646174612e),
(746, 'MapsModule', 0x476f6f676c65204d6170),
(747, 'MarketingListsModule', 0x7b736b6970706564436f756e747d20736b69707065642c20616c726561647920696e20746865206c6973742e),
(748, 'MarketingListsModule', 0x7b636f6e74616374537472696e677d20697320616c7265616479207375627363726962656420746f207b6d6f64656c537472696e677d2e),
(749, 'MarketingListsModule', 0x4d61726b6574696e67204c6973747320466f7220436f6e74616374734d6f64756c6553696e67756c61724c6162656c2f4c656164734d6f64756c6553696e67756c61724c6162656c),
(750, 'MarketingListsModule', 0x4d61726b6574696e67204c69737420536561726368),
(751, 'ImportModule', 0x5374617475732076616c756520697320696e76616c69642e2054686973207374617475732077696c6c2062652073657420746f206163746976652075706f6e20696d706f72742e),
(752, 'Commands', 0x536b697070696e67206578697374696e672072656164205461626c65732e),
(753, 'Commands', 0x4f76657277726974696e6720616e79206578697374696e672072656164205461626c65732e),
(754, 'AccountsModule', 0x4163636f756e742053746172726564),
(755, 'AccountsModule', 0x4163636f756e74732053746172726564),
(756, 'CampaignsModule', 0x4e6f206175746f726573706f6e646572732063726561746564),
(757, 'MarketingModule', 0x546865207072696d61727920656d61696c2061646472657373206973206f70746564206f75742e20546865207365636f6e6461727920656d61696c2061646472657373206973206e6f74206f70746564206f75742062757420746865207072696d61727920656d61696c206164647265737320697320746865206f6e65207573656420746f2073656e642074686520656d61696c206d6573736167652e),
(758, 'MarketingModule', 0x4576656e74),
(759, 'MarketingModule', 0x5175616e74697479),
(760, 'MarketingModule', 0x4c617465737420736f75726365204950),
(761, 'CampaignsModule', 0x546869732063616d706169676e2068617320616c726561647920737461727465642c20796f752063616e206f6e6c79206564697420697473206e616d652c2072696768747320616e64207065726d697373696f6e732e),
(762, 'CampaignsModule', 0x4f7574626f756e6420656d61696c2073657474696e677320617265206e6f7420776f726b696e6720636f72726563746c792e20436f6e7461637420796f75722061646d696e6973747261746f722e),
(763, 'CampaignsModule', 0x596f752063686f6f736520746f20737570706f72742048544d4c20627574206469646e27742073657420616e792048544d4c20636f6e74656e742e),
(764, 'CampaignsModule', 0x596f752063686f6f7365206e6f7420746f20737570706f72742048544d4c20627574206469646e27742073657420616e79207465787420636f6e74656e742e),
(765, 'ConfigurationModule', 0x41646d696e697374726174696f6e20486f6d65),
(766, 'ContactsModule', 0x596f75206d7573742073656c6563742061204d61726b6574696e674c697374734d6f64756c6553696e67756c61724c6162656c),
(767, 'ContactsModule', 0x436f6e746163742053746172726564),
(768, 'ContactsModule', 0x436f6e74616374732053746172726564),
(769, 'ContactWebFormsModule', 0x52657175697265643f),
(770, 'ContactWebFormsModule', 0x48696464656e3f),
(771, 'ContactWebFormsModule', 0x4c6162656c),
(772, 'ConversationsModule', 0x436f6e766572736174696f6e2053746172726564),
(773, 'ConversationsModule', 0x436f6e766572736174696f6e732053746172726564),
(774, 'DesignerModule', 0x4d617373205570646174652056696577),
(775, 'DesignerModule', 0x5069636b204c69737420446570656e64656e6379204d617070696e67),
(776, 'EmailMessagesModule', 0x576520747269656420746f2073656e642074686973206d65737361676520666f72207b636f756e747d2074696d657320287b6572726f727d292e),
(777, 'EmailMessagesModule', 0x4d6573736167652069732071756575656420746f2062652073656e742e),
(778, 'EmailMessagesModule', 0x4f776e6572204f6620546865204d65737361676520446f6573204e6f74204578697374),
(779, 'HomeModule', 0x4c61796f7574204964),
(780, 'HomeModule', 0x4c61796f75742054797065),
(781, 'HomeModule', 0x49732044656661756c74),
(782, 'ImportModule', 0x5468652066696c652068617320746f6f206d616e7920636f6c756d6e732e20546865206d6178696d756d20697320313030),
(783, 'ImportModule', 0x7b6d6f64656c4c6162656c7d20736176656420636f72726563746c793a207b6c696e6b546f4d6f64656c7d),
(784, 'ImportModule', 0x7b6d6f64656c4c6162656c7d2049442073706563696669656420646964206e6f74206d6174636820616e79206578697374696e67207265636f7264732e),
(785, 'ImportModule', 0x7b6d6f64656c4c6162656c7d206f746865722049442073706563696669656420646964206e6f74206d6174636820616e79206578697374696e67207265636f7264732e),
(786, 'ImportModule', 0x49442073706563696669656420646964206e6f74206d6174636820616e79206578697374696e67207265636f7264732e),
(787, 'ImportModule', 0x4f746865722049442073706563696669656420646964206e6f74206d6174636820616e79206578697374696e67207265636f7264732e),
(788, 'MarketingModule', 0x4d61726b6574696e6720436f6e66696775726174696f6e),
(789, 'MarketingModule', 0x43616d706169676e204974656d73206372656174696f6e20706167652073697a65),
(790, 'MarketingModule', 0x53746172742044617465),
(791, 'MarketingModule', 0x416363657373204d61726b6574696e6720436f6e66696775726174696f6e),
(792, 'MarketingModule', 0x4d616e616765204d61726b6574696e6720436f6e66696775726174696f6e),
(793, 'ProjectsModule', 0x4372656174652050726f6a656374734d6f64756c6553696e67756c61724c6162656c),
(794, 'ProjectsModule', 0x50726f6a65637473),
(795, 'ProjectsModule', 0x4372656174652050726f6a656374),
(796, 'ProjectsModule', 0x50726f6a656374),
(797, 'ProjectsModule', 0x412070726f6a656374206669656c64),
(798, 'ProjectsModule', 0x4372656174652050726f6a656374734d6f64756c65506c7572616c4c6162656c),
(799, 'ProjectsModule', 0x4e6f2050726f6a656374732063726561746564),
(800, 'ProjectsModule', 0x486f7720646f2050726f6a6563747320776f726b20696e20457a636f64783f),
(801, 'TasksModule', 0x4e455720434f4d4d454e54207b72656c617465644d6f64656c7d3a207b7461736b7d),
(802, 'TasksModule', 0x7b757365727d2068617320636f6d6d656e746564206f6e20746865207461736b20277b7461736b7d273a),
(803, 'AccountAccountAffiliationsModule', 0x506172746e6572),
(804, 'AccountAccountAffiliationsModule', 0x437573746f6d6572),
(805, 'AccountsModule', 0x436f6e74616374734d6f64756c6553696e67756c61724c6162656c),
(806, 'AccountsModule', 0x6f7220),
(807, 'AccountsModule', 0x4372656174652061206e6577204163636f756e74734d6f64756c6553696e67756c61724c6162656c),
(808, 'AccountAccountAffiliationsModule', 0x7b7072696d6172794163636f756e747d20416666696c696174696f6e73),
(809, 'AccountsModule', 0x4d6f64616c204372656174652056696577),
(810, 'AccountsModule', 0x4d65726765204163636f756e74734d6f64756c65506c7572616c4c6162656c),
(811, 'EmailMessages', 0x4e6f205375626a656374),
(812, 'EmailMessagesModule', 0x506f7765726564204279203c6120687265663d27687474703a2f2f7777772e657a636f64782e636f6d273e457a636f64783c2f613e),
(813, 'EmailMessagesModule', 0x506f776572656420427920457a636f6478),
(814, 'ImportModule', 0x5768656e2061206d6174636820697320666f756e642c20736b697020726f77),
(815, 'ImportModule', 0x5768656e2061206d6174636820697320666f756e642c20757064617465206578697374696e67207265636f7264),
(816, 'MeetingsModule', 0x437265617465204d656574696e67),
(817, 'MeetingsModule', 0x4e6f206d656574696e67207363686564756c6564),
(818, 'TasksModule', 0x416c6c205461736b73),
(819, 'EzcodxModule', 0x5461736b73),
(820, 'Core', 0x43616e63656c),
(821, 'Core', 0x43616e63656c204368616e676573),
(822, 'Core', 0x436865636b696e67207065726d697373696f6e732c2066696c65732c20757067726164652076657273696f6e2e2e2e2e),
(823, 'Core', 0x52756e6e696e67207461736b73206265666f7265207570646174696e6720736368656d612e),
(824, 'Core', 0x52756e6e696e67207461736b7320616674657220736368656d6120697320757064617465642e),
(825, 'SocialItemsModule', 0x57686174277320676f696e67206f6e3f),
(826, 'MeetingsModule', 0x4d79205570636f6d696e67204d656574696e67734d6f64756c65506c7572616c4c6162656c),
(827, 'InstallModule', 0x496e7374616c6c),
(828, 'InstallModule', 0x52656275696c64696e67205065726d697373696f6e732e),
(829, 'InstallModule', 0x49662074686973207765627369746520697320696e2070726f64756374696f6e206d6f64652c20706c656173652072656d6f766520746865206170702f746573742e7068702066696c652e),
(830, 'InstallModule', 0x506c656173652064656c65746520616c6c2066696c65732066726f6d2061737365747320666f6c646572206f6e207365727665722e),
(831, 'ReportsModule', 0x5265706f7274204368617274),
(832, 'ReportsModule', 0x53656c6563742061207265706f727420776974682061206368617274),
(833, 'ReportsModule', 0x596f75206861766520747269656420746f206163636573732061207265706f727420796f7520646f206e6f7420686176652061636365737320746f),
(834, 'ReportsModule', 0x596f75206861766520747269656420746f206163636573732061207265706f72742074686174206973206e6f206c6f6e67657220617661696c61626c65),
(835, 'ReportsModule', 0x53756d6d6174696f6e205265706f7274),
(836, 'ReportsModule', 0x53617665205265706f7274),
(837, 'ReportsModule', 0x7b6d6f64756c654c6162656c7d207b747970654c6162656c7d205265706f7274),
(838, 'ReportsModule', 0x44656c657465205265706f727473),
(839, 'ReportsModule', 0x416363657373205265706f72747320546162),
(840, 'ReportsModule', 0x5265706f7274),
(841, 'ReportsModule', 0x5265706f7274204964),
(842, 'ReportsModule', 0x5265706f7274204e616d65),
(843, 'RssReaderModule', 0x457a636f6478204e657773),
(844, 'ImportModule', 0x4d6f64656c205065726d697373696f6e73),
(845, 'TasksModule', 0x5461736b),
(846, 'JobsManagerModule', 0x417661696c61626c65204a6f6273),
(847, 'OpportunitiesModule', 0x4f70706f7274756e69746965732042792053616c6573205374616765),
(848, 'MissionsModule', 0x4174206c65617374206f6e65206d697373696f6e2068617320756e7265616420636f6e74656e74),
(849, 'MissionsModule', 0x41206d697373696f6e2073746174757320686173206368616e676564),
(850, 'MissionsModule', 0x4d697373696f6e73),
(851, 'MissionsModule', 0x41206d697373696f6e20796f7520636f6d706c6574656420686173206265656e2072656a6563746564),
(852, 'MissionsModule', 0x41206d697373696f6e20796f7520636f6d706c6574656420686173206265656e206163636570746564),
(853, 'MissionsModule', 0x4d697373696f6e),
(854, 'MissionsModule', 0x4d697373696f6e204c6174657374),
(855, 'MissionsModule', 0x44656c657465204d697373696f6e73),
(856, 'MissionsModule', 0x416363657373204d697373696f6e7320546162),
(857, 'MissionsModule', 0x4e6577206d697373696f6e),
(858, 'EzcodxModule', 0x4d616e61676520416374697665204c616e677561676573),
(859, 'EzcodxModule', 0x596f75206e6f206c6f6e6765722068617665207065726d697373696f6e7320746f20616363657373207b6d6f64656c4e616d657d2e),
(860, 'EzcodxModule', 0x4c616e6775616765206e6f74206163746976652e),
(861, 'EzcodxModule', 0x52696768747320616e64205065726d697373696f6e73),
(862, 'EzcodxModule', 0x536561726368206279206e616d652c2070686f6e652c206f7220652d6d61696c),
(863, 'EzcodxModule', 0x416c6c6f77),
(864, 'EzcodxModule', 0x416c6c),
(865, 'EzcodxModule', 0x596f75206d7573742068617665206174206c65617374206f6e65206163746976652063757272656e63792e),
(866, 'EzcodxModule', 0x46696e64207768617420796f75206e65656420717569636b6c79207573696e672074686520676c6f62616c207365617263682061742074686520746f70206f66207468652073637265656e2e),
(867, 'EzcodxModule', 0x736b6970706564206265636175736520796f7520646f206e6f7420686176652073756666696369656e74207065726d697373696f6e732e),
(868, 'EzcodxModule', 0x54686973206973206f6e6c7920617661696c61626c6520696e204368726f6d652e),
(869, 'UsersModule', 0x47726f7570205265636f7264205065726d697373696f6e73),
(870, 'EzcodxModule', 0x50726576696577),
(871, 'EzcodxModule', 0x5265636f7264205065726d697373696f6e73),
(872, 'EzcodxModule', 0x5265636f7264205065726d697373696f6e73205361766564205375636365737366756c6c792e),
(873, 'ReportsModule', 0x53656c656374205265706f72742054797065),
(874, 'Core', 0x56696577),
(875, 'Core', 0x7b6e7d206461792061676f7c7b6e7d20646179732061676f),
(876, 'Core', 0x53756d6d617279),
(877, 'Core', 0x46696c74657273),
(878, 'Core', 0x416374697665),
(879, 'EzcodxModule', 0x537461747573),
(880, 'EzcodxModule', 0x5374617475732073706563696669656420697320696e76616c69642e),
(881, 'Core', 0x417661696c61626c65),
(882, 'UsersModule', 0x497320416374697665),
(883, 'EzcodxModule', 0x5065726d697373696f6e73),
(884, 'EzcodxModule', 0x53656c656374205065726d697373696f6e73),
(885, 'Core', 0x566965772061732044657461696c73),
(886, 'GamificationModule', 0x4261736b657462616c6c),
(887, 'GamificationModule', 0x42616c6c),
(888, 'GamificationModule', 0x476f6c662042616c6c),
(889, 'GamificationModule', 0x536f636365722042616c6c),
(890, 'GamificationModule', 0x566f6c6c657962616c6c),
(891, 'TasksModule', 0x41535349474e4d454e54207b72656c617465644d6f64656c7d3a207b7461736b7d),
(892, 'TasksModule', 0x44454c495645524544207b72656c617465644d6f64656c7d3a207b7461736b7d),
(893, 'TasksModule', 0x4143434550544544207b72656c617465644d6f64656c7d3a207b7461736b7d),
(894, 'TasksModule', 0x52454a4543544544207b72656c617465644d6f64656c7d3a207b7461736b7d),
(895, 'TasksModule', 0x546865207461736b2c20277b7461736b7d),
(896, 'EzcodxModule', 0x746f2076696577),
(897, 'Core', 0x43726561746564204461746520616e642055736572),
(898, 'InstallModule', 0x4461746162617365206e616d65),
(899, 'InstallModule', 0x446174616261736520757365726e616d65),
(900, 'NotificationsModule', 0x412073696d706c65206e6f74696669636174696f6e),
(901, 'NotificationsModule', 0x4e6f74696669636174696f6e204d657373616765),
(902, 'NotificationsModule', 0x4e6f74696669636174696f6e204d65737361676573),
(903, 'NotificationsModule', 0x4e6f74696669636174696f6e),
(904, 'NotificationsModule', 0x546865726520617265206e6f20726563656e74206e6f74696669636174696f6e732e),
(905, 'NotificationsModule', 0x596f7520686176652061206e6577206e6f74696669636174696f6e),
(906, 'AccountsModule', 0x437265617465204163636f756e74734d6f64756c6553696e67756c61724c6162656c),
(907, 'UsersModule', 0x5475726e206f666620656d61696c206e6f74696669636174696f6e73),
(908, 'UsersModule', 0x456e61626c65204465736b746f70206e6f74696669636174696f6e73),
(909, 'JobsManagerModule', 0x436c656172207468652061737365747320666f6c646572206f6e20736572766572286f7074696f6e616c292e),
(910, 'JobsManagerModule', 0x4a6f62204e616d65),
(911, 'GamificationModule', 0x47616d65204e6f74696669636174696f6e),
(912, 'GamificationModule', 0x47616d65204e6f74696669636174696f6e73),
(913, 'ExportModule', 0x4578706f72742046696c65204e616d65),
(914, 'EzcodxModule', 0x44656163746976617465),
(915, 'EzcodxModule', 0x4163746976617465),
(916, 'EzcodxModule', 0x4669727374204e616d65),
(917, 'EzcodxModule', 0x4c617374204e616d65),
(918, 'EzcodxModule', 0x4170706c69636174696f6e204e616d65),
(919, 'EzcodxModule', 0x557365726e616d65),
(920, 'EzcodxModule', 0x496e636f727265637420757365726e616d65206f722070617373776f72642e),
(921, 'EzcodxModule', 0x496e76616c696420757365726e616d65206f722070617373776f72642e),
(922, 'EzcodxModule', 0x7b6c616e67756167654e616d657d20616374697661746564207375636365737366756c6c79),
(923, 'EzcodxModule', 0x7b6c616e67756167654e616d657d2061637469766174696f6e206661696c65642e204572726f723a207b6572726f724d6573736167657d),
(924, 'EzcodxModule', 0x7b6c616e67756167654e616d657d2061637469766174696f6e206661696c65642e20556e6578706563746564206572726f722e),
(925, 'EzcodxModule', 0x7b6c616e67756167654e616d657d2075706461746564207375636365737366756c6c79),
(926, 'EzcodxModule', 0x7b6c616e67756167654e616d657d20757064617465206661696c65642e204572726f723a207b6572726f724d6573736167657d),
(927, 'EzcodxModule', 0x7b6c616e67756167654e616d657d20757064617465206661696c65642e20556e6578706563746564206572726f722e),
(928, 'EzcodxModule', 0x7b6c616e67756167654e616d657d206465616374697661746564207375636365737366756c6c79),
(929, 'EzcodxModule', 0x7b6c616e67756167654e616d657d2064656163746976617465206661696c65642e204572726f723a207b6572726f724d6573736167657d),
(930, 'EzcodxModule', 0x7b6c616e67756167654e616d657d2064656163746976617465206661696c65642e20556e6578706563746564206572726f722e),
(931, 'EzcodxModule', 0x596f75206861766520626c6f636b6564206465736b746f70206e6f74696669636174696f6e7320666f7220746869732062726f777365722e),
(932, 'EzcodxModule', 0x596f75206861766520616c726561647920616374697661746564206465736b746f70206e6f74696669636174696f6e7320666f72204368726f6d65),
(933, 'EzcodxModule', 0x5468657265206d757374206265206174206c65617374206f6e652073757065722061646d696e6973747261746f72),
(934, 'TasksModule', 0x57686f20697320726563656976696e67206e6f74696669636174696f6e73),
(935, 'EzcodxModule', 0x53757065722041646d696e6973747261746f7273),
(936, 'EzcodxModule', 0x536b697070696e67207b7b7461626c654e616d657d7d),
(937, 'GamificationModule', 0x596f7520646973636f766572656420746865207b6e616d657d),
(938, 'Core', 0x7b6d6f64756c6553696e67756c61724c6162656c7d20536561726368),
(939, 'Core', 0x54797065),
(940, 'EzcodxModule', 0x4465736372697074696f6e),
(941, 'MeetingsModule', 0x437265617465204d656574696e67734d6f64756c6553696e67756c61724c6162656c),
(942, 'MeetingsModule', 0x437265617465204d656574696e67734d6f64756c65506c7572616c4c6162656c),
(943, 'InstallModule', 0x5570677261646520696e2070726f67726573732e20506c6561736520776169742e),
(944, 'InstallModule', 0x496e7374616c6c6174696f6e20696e2070726f67726573732e20506c6561736520776169742e),
(945, 'ReportsModule', 0x4f776e6572204964),
(946, 'ReportsModule', 0x4f776e6572204e616d65),
(947, 'EzcodxModule', 0x4f776e6572),
(948, 'AccountsModule', 0x437265617465204163636f756e74734d6f64756c65506c7572616c4c6162656c),
(949, 'CustomField', 0x42616e6b696e67),
(950, 'CustomField', 0x456e65726779),
(951, 'CustomField', 0x46696e616e6369616c205365727669636573),
(952, 'CustomField', 0x496e737572616e6365),
(953, 'CustomField', 0x4d616e75666163747572696e67),
(954, 'CustomField', 0x52657461696c),
(955, 'CustomField', 0x546563686e6f6c6f6779),
(956, 'EzcodxModule', 0x57686f2063616e207265616420616e64207772697465),
(957, 'WorkflowsModule', 0x4e6577205265636f726473204f6e6c79),
(958, 'EzcodxModule', 0x47726f757073),
(959, 'EzcodxModule', 0x57686f2063616e207265616420616e64207772697465202d2044656661756c74),
(960, 'TasksModule', 0x437265617465205461736b734d6f64756c6553696e67756c61724c6162656c),
(961, 'TasksModule', 0x437265617465205461736b734d6f64756c65506c7572616c4c6162656c),
(962, 'ProductsModule', 0x4372656174652050726f64756374734d6f64756c6553696e67756c61724c6162656c),
(963, 'ProductsModule', 0x4372656174652050726f64756374734d6f64756c65506c7572616c4c6162656c),
(964, 'OpportunitiesModule', 0x437265617465204f70706f7274756e69746965734d6f64756c6553696e67756c61724c6162656c),
(965, 'OpportunitiesModule', 0x437265617465204f70706f7274756e69746965734d6f64756c65506c7572616c4c6162656c),
(966, 'CustomField', 0x5175616c696669636174696f6e),
(967, 'CustomField', 0x4e65676f74696174696e67),
(968, 'LeadsModule', 0x437265617465204c656164734d6f64756c6553696e67756c61724c6162656c),
(969, 'LeadsModule', 0x43726561746564204c656164734d6f64756c6553696e67756c61724c6162656c207375636365737366756c6c79),
(970, 'LeadsModule', 0x437265617465204c656164734d6f64756c65506c7572616c4c6162656c),
(971, 'LeadsModule', 0x4c656164),
(972, 'LeadsModule', 0x4c65616473),
(973, 'GamificationModule', 0x4e6577204261646765),
(974, 'GamificationModule', 0x492072656365697665642061206e65772062616467653a207b6261646765436f6e74656e747d),
(975, 'ExportModule', 0x4578706f72742046696c652054797065),
(976, 'EzcodxModule', 0x4163636573732041646d696e697374726174696f6e20546162),
(977, 'EzcodxModule', 0x5361766520736561726368),
(978, 'EzcodxModule', 0x4465706172746d656e74),
(979, 'EzcodxModule', 0x4a6f62205469746c65),
(980, 'EzcodxModule', 0x4d6f62696c652050686f6e65),
(981, 'EzcodxModule', 0x4f66666963652050686f6e65),
(982, 'EzcodxModule', 0x4f666669636520466178),
(983, 'EzcodxModule', 0x5072696d6172792041646472657373),
(984, 'EzcodxModule', 0x5072696d61727920456d61696c),
(985, 'EzcodxModule', 0x43697479),
(986, 'EzcodxModule', 0x436f756e747279),
(987, 'EzcodxModule', 0x506f7374616c20436f6465),
(988, 'EzcodxModule', 0x5374726565742031),
(989, 'EzcodxModule', 0x5374726565742032),
(990, 'EzcodxModule', 0x5374617465),
(991, 'EzcodxModule', 0x536176656420536561726368),
(992, 'EzcodxModule', 0x4f776e6572204f6e6c79),
(993, 'EzcodxModule', 0x43686172742054797065),
(994, 'EzcodxModule', 0x5365727665722054797065),
(995, 'EzcodxModule', 0x44656c6574652047726f757073),
(996, 'EzcodxModule', 0x4163636573732047726f75707320546162),
(997, 'EzcodxModule', 0x4d616e6167652047726f757073),
(998, 'CustomField', 0x4d732e),
(999, 'CustomField', 0x44722e),
(1000, 'WorkflowsModule', 0x53656c65637420576f726b666c6f772054797065),
(1001, 'EzcodxModule', 0x437573746f6d6572),
(1002, 'EzcodxModule', 0x416e792043697479),
(1003, 'EzcodxModule', 0x416e7920537472656574),
(1004, 'EzcodxModule', 0x416e79205374617465),
(1005, 'EzcodxModule', 0x416e7920506f7374616c20436f6465),
(1006, 'EzcodxModule', 0x416e7920436f756e747279),
(1007, 'EzcodxModule', 0x496e647573747279),
(1008, 'EzcodxModule', 0x5365636f6e6461727920456d61696c),
(1009, 'Core', 0x45766572796f6e65),
(1010, 'EzcodxModule', 0x41646d696e697374726174696f6e),
(1011, 'Core', 0x4e6577),
(1012, 'Core', 0x496e2050726f6772657373),
(1013, 'EzcodxModule', 0x63726561746564206279207b6f776e6572537472696e67436f6e74656e747d),
(1014, 'EzcodxModule', 0x547970652063616e6e6f7420626520626c616e6b2e),
(1015, 'ProductTemplatesModule', 0x4372656174652050726f6475637454656d706c617465734d6f64756c6553696e67756c61724c6162656c),
(1016, 'EzcodxModule', 0x476c6f62616c20536561726368),
(1017, 'EzcodxModule', 0x57656273697465),
(1018, 'EzcodxModule', 0x44656661756c74202d2057686f2063616e207265616420616e64207772697465),
(1019, 'GamificationModule', 0x4e6577737061706572),
(1020, 'GamificationModule', 0x46696e616e6365),
(1021, 'CustomField', 0x537570706f7274),
(1022, 'CustomField', 0x546563686e6963616c),
(1023, 'CustomField', 0x41646d696e697374726174697665),
(1024, 'CustomField', 0x50726f6a656374204d616e61676572),
(1025, 'Default', 0x43726561746520456d61696c54656d706c617465734d6f64756c6553696e67756c61724c6162656c),
(1026, 'InstallModule', 0x436f6e67726174756c6174696f6e73212054686520696e7374616c6c6174696f6e206f6620457a636f647820697320636f6d706c6574652e),
(1027, 'InstallModule', 0x436f6e67726174756c6174696f6e7321205468652064656d6f206461746120686173206265656e207375636365737366756c6c79206c6f616465642e),
(1028, 'WorkflowsModule', 0x4d616e61676520576f726b666c6f7773),
(1029, 'WorkflowsModule', 0x7b6e7d20576f726b666c6f7720637265617465647c7b6e7d20576f726b666c6f77732063726561746564),
(1030, 'WorkflowsModule', 0x576f726b666c6f7773),
(1031, 'WorkflowsModule', 0x44656c65746520576f726b666c6f7773),
(1032, 'WorkflowsModule', 0x41636365737320576f726b666c6f777320546162),
(1033, 'WorkflowsModule', 0x426f7468204e657720616e64204578697374696e67205265636f726473),
(1034, 'NotesModule', 0x4e6f746573),
(1035, 'NotesModule', 0x437265617465204e6f7465734d6f64756c6553696e67756c61724c6162656c),
(1036, 'NotesModule', 0x437265617465204e6f7465734d6f64756c65506c7572616c4c6162656c),
(1037, 'NotesModule', 0x44656c657465204e6f7465734d6f64756c65506c7572616c4c6162656c),
(1038, 'NotesModule', 0x416363657373204e6f7465734d6f64756c65506c7572616c4c6162656c),
(1039, 'OpportunitiesModule', 0x4f70706f7274756e6974696573204279204c65616420536f75726365),
(1040, 'OpportunitiesModule', 0x416d6f756e74),
(1041, 'OpportunitiesModule', 0x436c6f73652044617465),
(1042, 'GamificationModule', 0x596f7520686176652072656163686564206c6576656c207b6e6578744c6576656c7d),
(1043, 'EzcodxModule', 0x41757468656e7469636174696f6e20436f6e66696775726174696f6e),
(1044, 'EzcodxModule', 0x526f6c6573),
(1045, 'EzcodxModule', 0x47726f7570),
(1046, 'EzcodxModule', 0x456d61696c73),
(1047, 'EzcodxModule', 0x53656c65637420612047726f7570),
(1048, 'EzcodxModule', 0x44656c6574652047726f7570),
(1049, 'EzcodxModule', 0x41726520796f75207375726520796f752077616e7420746f2064656c65746520746869732067726f75703f),
(1050, 'EzcodxModule', 0x44656c65746520526f6c6573),
(1051, 'EzcodxModule', 0x41636365737320526f6c657320546162),
(1052, 'EzcodxModule', 0x4d616e61676520526f6c6573),
(1053, 'EmailTemplatesModule', 0x4d65726765207461677320617265206120717569636b2077617920746f20696e74726f64756365207265616465722d73706563696669632064796e616d696320696e666f726d6174696f6e20696e746f20656d61696c732e),
(1054, 'EmailTemplatesModule', 0x44656c65746520456d61696c2054656d706c61746573),
(1055, 'EmailTemplatesModule', 0x41636365737320456d61696c2054656d706c6174657320546162),
(1056, 'UsersModule', 0x47726f7570204d6f64756c6520526967687473),
(1057, 'MarketingModule', 0x437265617465207468652074656d706c61746520666f722074686520656d61696c20796f752061726520676f696e6720746f2073656e642c20696d706f727420616e6420757365206569746865722066756c6c2c20726963682048544d4c2074656d706c61746573206f7220706c61696e2074657874),
(1058, 'Core', 0x536b6970706564),
(1059, 'EzcodxModule', 0x4c616e6775616765),
(1060, 'Core', 0x54656d706c61746573),
(1061, 'Core', 0x436f6e67726174756c6174696f6e7321),
(1062, 'Core', 0x536b6970),
(1063, 'EzcodxModule', 0x506f737420746f2050726f66696c65),
(1064, 'Core', 0x4c616e6775616765),
(1065, 'ProductsModule', 0x53656c6563742050726f6475637454656d706c617465734d6f64756c6553696e67756c61724c6162656c),
(1066, 'GamificationModule', 0x596f752068617665207265636569766564207b6e7d20636f696e207c20596f752068617665207265636569766564207b6e7d20636f696e73),
(1067, 'Core', 0x4c616e677561676573),
(1068, 'EzcodxModule', 0x4e6f746573),
(1069, 'JobsManagerModule', 0x4a6f62204d616e61676572),
(1070, 'EzcodxModule', 0x476c6f62616c20436f6e66696775726174696f6e),
(1071, 'EzcodxModule', 0x43757272656e637920436f6e66696775726174696f6e),
(1072, 'EzcodxModule', 0x446576656c6f70657220546f6f6c73),
(1073, 'EzcodxModule', 0x506c7567696e73),
(1074, 'GameRewardsModule', 0x47616d652052657761726473),
(1075, 'EzcodxModule', 0x5573657220496e7465726661636520436f6e66696775726174696f6e),
(1076, 'Core', 0x53656c656374204d6f64756c65),
(1077, 'ProductTemplatesModule', 0x436174616c6f67204974656d73),
(1078, 'EzcodxModule', 0x55706c6f61642046696c65),
(1079, 'EzcodxModule', 0x4d6170204669656c6473),
(1080, 'EzcodxModule', 0x486f7720646f657320736563757269747920776f726b20696e20457a636f64783f),
(1081, 'AccountAccountAffiliationsModule', 0x4163636f756e7420746f204163636f756e7420416666696c696174696f6e73),
(1082, 'EzcodxModule', 0x526f6c657320657870616e64207669736962696c69747920616c6c6f77696e67206d616e616765727320746f20726561642f777269746520746865697220656d706c6f7965657327207265636f7264732e),
(1083, 'EzcodxModule', 0x52656164206d6f7265206f6e20616476616e6365642073656375726974792066656174757265733c2f753e3c2f623e),
(1084, 'AccountContactAffiliationsModule', 0x4163636f756e7420746f20436f6e7461637420416666696c696174696f6e73),
(1085, 'EzcodxModule', 0x4469736d697373),
(1086, 'GamificationModule', 0x4765742074686973206974656d),
(1087, 'Core', 0x436f6e666967757265),
(1088, 'EzcodxModule', 0x52756e),
(1089, 'UsersModule', 0x4d616e616765205573657273),
(1090, 'ImportModule', 0x496d706f7274206461746120696e746f20457a636f6478),
(1091, 'JobsManagerModule', 0x4d616e616765205363686564756c6564204a6f6273),
(1092, 'EzcodxModule', 0x4d616e61676520476c6f62616c20436f6e66696775726174696f6e),
(1093, 'EzcodxModule', 0x4d616e6167652043757272656e637920436f6e66696775726174696f6e),
(1094, 'EzcodxModule', 0x41636365737320446576656c6f70657220546f6f6c73),
(1095, 'EzcodxModule', 0x4d616e616765205573657220496e7465726661636520436f6e66696775726174696f6e),
(1096, 'EzcodxModule', 0x4d616e6167652041757468656e7469636174696f6e20436f6e66696775726174696f6e),
(1097, 'EzcodxModule', 0x4d616e61676520506c7567696e7320616e6420496e746567726174696f6e73),
(1098, 'GameRewardsModule', 0x4d616e6167652047616d652052657761726473),
(1099, 'WorkflowsModule', 0x54696d652d426173656420576f726b666c6f77),
(1100, 'ContactWebFormsModule', 0x456e61626c652043617074636861);

-- --------------------------------------------------------

--
-- Table structure for table `messagetranslation`
--

CREATE TABLE IF NOT EXISTS `messagetranslation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `translation` blob,
  `language` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `messagesource_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sourceLanguageTranslation` (`messagesource_id`,`language`,`translation`(767))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1099 ;

--
-- Dumping data for table `messagetranslation`
--

INSERT INTO `messagetranslation` (`id`, `translation`, `language`, `messagesource_id`) VALUES
(1, 0xe18393e18390e1839be18390e183a2e18394e18391e18398e18397e1839820e183abe18398e18394e18391e18390, 'ka', 1),
(2, 0xe183abe18398e183a0e18398e18397e18390e18393e1839820e183abe18398e18394e18391e18390, 'ka', 2),
(3, 0xe183a8e18394e1839be1839de183a1e183a3e1839ae18398, 'ka', 3),
(4, 0xe183ace1839ae18398e183a3e183a0e18390e18393, 'ka', 4),
(5, 0xe183a8e1839de183a0e18398e183a1, 'ka', 5),
(6, 0xe18392e183a3e183a8e18398e1839c, 'ka', 6),
(7, 0xe18393e183a6e18394e183a1, 'ka', 7),
(8, 0xe183aee18395e18390e1839a, 'ka', 8),
(9, 0xe1839be18393e18394, 'ka', 9),
(10, 0xe183a8e18394e1839be18393e18394e18392, 'ka', 10),
(11, 0xe183a8e18394e1839be18393e18394e18392e18398203720e18393e183a6e18394, 'ka', 11),
(12, 0xe18391e1839de1839ae1839d203720e18393e183a6e18394, 'ka', 12),
(13, 0x7b6d6f64756c654c6162656c506c7572616c4c6f776572436173657d20e18390e183a020e1839be1839de18398e183abe18394e18391e1839ce18390, 'ka', 13),
(14, 0xe183a8e18394e18393e18394e1839228e18394e1839129e18398, 'ka', 14),
(15, 0xe18393e18390e1839be18390e183a2e18394e18391e18398e18397e18398, 'ka', 15),
(16, 0xe18392e18390e183a1e183a3e183a4e18397e18390e18395e18394e18391e18390, 'ka', 16),
(17, 0xe183a1e18395e18394e183a2e18394e18391e18398, 'ka', 17),
(18, 0xe1839ee18390e183a0e183902e2e2e2ee18394e18391e18398, 'ka', 18),
(19, 0xe183abe18398e18394e18391e18390, 'ka', 19),
(20, 0xe18393e18390e183aee183a3e183a0e18395e18390, 'ka', 20),
(21, 0xe183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e183ace18390e18398e183a8e18390e1839ae18390, 'ka', 21),
(22, 0xe183a8e18394e18393e18394e1839228e18394e1839129e18398, 'ka', 22),
(23, 0xe183a1e18390e183aee18394e1839ae18398, 'ka', 23),
(24, 0xe183a1e18398e18398e183a120e18392e18395e18394e183a0e18393e18396e1839420e18393e18390e18391e183a0e183a3e1839ce18394e18391e18390, 'ka', 24),
(25, 0x7b6174747269627574657d20e18392e18397e183aee1839de18395e1839720e183a8e18394e18390e18395e183a1e1839de1839720e18395e18394e1839ae18398, 'ka', 25),
(26, 0xe18390e18398e183a0e183a9e18398e18394e18397, 'ka', 26),
(27, 0xe18393e18390, 'ka', 27),
(28, 0xe183a4e18390e183a0e183a3e1839ae1839820e183a1e18395e18394e183a2e18394e18391e18398, 'ka', 28),
(29, 0xe183aee18398e1839ae183a3e1839ae1839820e183a1e18395e18394e183a2e18394e18391e18398, 'ka', 29),
(30, 0xe18393e18390e1839be1839de183ace1839be18394e18391e18390, 'ka', 30),
(31, 0xe18392e18390e18393e18390e183a2e18395e18398e183a0e18397e18395e18390, 'ka', 31),
(32, 0xe183a3e183aae1839ce1839de18391e18398, 'ka', 32),
(33, 0xe18393e18398e18390e183ae, 'ka', 33),
(34, 0xe18390e183aee1839ae18390, 'ka', 34),
(35, 0xe183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 35),
(36, 0xe183a1e183a0e183a3e1839ae1839820e183a1e18390e183aee18394e1839ae18398, 'ka', 36),
(37, 0xe18391e1839de1839ae1839d20e183aae18395e1839ae18398e1839ae18394e18391e18390, 'ka', 37),
(38, 0xe18391e1839de1839ae1839d20e183aae18395e1839ae18398e1839ae18394e18391e18398e183a120e18393e183a0e1839d20e18393e1839020e1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae18398, 'ka', 38),
(39, 0xe18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 39),
(40, 0xe18392e18390e1839ce18390e183aee1839ae18394e18391e18390, 'ka', 40),
(41, 0xe183a8e18394e183a0e183a9e18394e183a3e1839ae18398, 'ka', 41),
(42, 0xe183a7e18395e18394e1839ae1839020e183a8e18394e18393e18394e18392e18398, 'ka', 42),
(43, 0xe18399e1839ae1839de1839ce18398, 'ka', 43),
(44, 0xe183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390, 'ka', 44),
(45, 0xe18391e1839ae1839de18399e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 45),
(46, 0xe183ace18390e183a8e1839ae18390, 'ka', 46),
(47, 0xe18393e18390e183a0e183ace1839be183a3e1839ce18394e18391e183a3e1839ae1839820e183aee18390e183a0e1839720e183a0e1839de1839b20e18392e183a1e183a3e183a0e1839720e183ace18390e183a8e18390e1839ae1839de183973f, 'ka', 47),
(48, 0xe18393e18394e183a2e18390e1839ae183a3e183a0e18390e18393, 'ka', 48),
(49, 0xe183a8e18394e1839ce18390e183aee18395e18390, 'ka', 49),
(50, 0xe18392e18390e1839ce1839ae18398e1839ce18399e18395e18390, 'ka', 50),
(51, 0xe18393e18390e183a0e183ace1839be183a3e1839ce18394e18391e183a3e1839ae1839820e183aee18390e183a0e1839720e183a0e1839de1839b20e18392e183a1e183a3e183a0e1839720e18391e1839be183a3e1839ae18398e183a120e1839be1839de183a8e1839de183a0e18394e18391e183903f, 'ka', 51),
(52, 0xe18393e18390e1839be18390e183a2e18394e18391e18398e18397e1839820e183abe18398e18394e18391e18398e183a120e183a0e18398e18392e18398, 'ka', 52),
(53, 0xe183abe18398e18394e18391e18398e183a120e1839de1839ee18394e183a0e18390e183a2e1839de183a0e18398, 'ka', 53),
(54, 0xe183a7e18395e18394e1839ae18390, 'ka', 54),
(55, 0xe183a3e1839ce18393e1839020e183a8e18394e18390e183a0e183aae18398e1839de1839720e1839be18398e1839ce18398e1839be183a3e1839b20e18394e183a0e18397e1839820e18395e18394e1839ae1839820e183a8e18394e183a1e18390e183aae18395e1839ae18394e1839ae18390e18393, 'ka', 55),
(56, 0xe183a8e18394e1839ce18390e183aee18395e1839020e18393e1839020e18393e18390e183aee183a3e183a0e18395e18390, 'ka', 56),
(57, 0xe183a4e18390e18398e1839ae18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 57),
(58, 0xe18390e183a2e18395e18398e183a0e18397e18395e18398e183a120e1839be18390e183a5e183a1e18398e1839be18390e1839ae183a3e183a0e1839820e18396e1839de1839be183903a20207b6d617853697a657d, 'ka', 58),
(59, 0xe18391e1839ae1839de18399e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 59),
(60, 0xe18391e1839ae1839de18399e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 60),
(61, 0xe18391e1839ae1839de18399e18398e183a120e183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390, 'ka', 61),
(62, 0xe18391e1839ae1839de18399e1839820e183ace18390e18398e183a8e1839ae18394e18391e183902c20e18397e18390e1839ce18390e183aee1839be1839020e183aee18390e183a0e183973f, 'ka', 62),
(63, 0xe183a9e18394e1839be1839820e18392e18390e1839be1839de183ace18394e183a0e18394e18391e18398, 'ka', 63),
(64, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e1839820e18390e183a0e18390e183a1e183ace1839de183a0e18398e18390, 'ka', 64),
(65, 0xe18393e18390e183a0e183ace1839be183a3e1839ce18394e18391e183a3e1839ae1839820e183aee18390e183a0e1839720e183a0e1839de1839b20e18392e183a1e183a3e183a0e1839720e18390e1839b20e183a9e18390e1839ce18390e183ace18394e183a0e18396e1839420e18391e1839be183a3e1839ae18398e183a120e1839be1839de183a8e1839de183a0e18394e18391e183903f, 'ka', 65),
(66, 0xe18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 66),
(67, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e1839820e18390e183a0e18390e183a1e183ace1839de183a0e18398e18390, 'ka', 67),
(68, 0xe183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e183ace18390e18398e183a8e18390e1839ae18390, 'ka', 68),
(69, 0xe18393e18390e183a0e183ace1839be183a3e1839ce18394e18391e183a3e1839ae1839820e183aee18390e183a0e1839720e183a0e1839de1839b20e18392e183a1e183a3e183a0e1839720e183ace18390e183a8e18390e1839ae1839de183973f, 'ka', 69),
(70, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e18390e1839ce18392e18390e183a0e18398e183a8e18398, 'ka', 70),
(71, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e18390e1839ce18392e18390e183a0e18398e183a8e18398, 'ka', 71),
(72, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18390, 'ka', 72),
(73, 0xe183a8e18394e1839be1839de183a1e183a3e1839ae18398, 'ka', 73),
(74, 0xe18398e1839be18394e18398e1839ae18394e18391e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 74),
(75, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e183a8e18394e18393e18392e18394e1839ce18390, 'ka', 75),
(76, 0xe1839be1839de1839ce18398e183a8e1839ce1839420e183a8e18394e183a0e183a9e18394e183a3e1839ae1839820e183a0e1839de18392e1839de183a0e183aa20e18393e18390e183aee183a3e183a0e183a3e1839ae18398, 'ka', 76),
(77, 0xe183a1e18390e183a3e18391e183a0e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 77),
(78, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e1839ee18390e183a0e1839de1839ae18398, 'ka', 78),
(79, 0xe183a0e18394e1839ee1839de183a0e183a2e18398, 'ka', 79),
(80, 0xe183a8e18394e18393e18394e18392e18394e18391e18398, 'ka', 80),
(81, 0xe183a8e18394e183a5e1839be18394e1839ce1839820e183a0e18394e1839ee1839de183a0e183a2e18398, 'ka', 81),
(82, 0xe183a4e18398e1839ae183a2e183a0e18390e183aae18398e18398e183a120e18390e183a0e183a9e18394e18395e18390, 'ka', 82),
(83, 0xe183a12fe18399, 'ka', 83),
(84, 0xe183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390e18393e18398, 'ka', 84),
(85, 0xe1839be1839de1839ce18398e183a8e1839ce183a3e1839ae18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 85),
(86, 0xe183a7e18395e18394e1839ae18390e183a120e183ace18390e183a8e1839ae18390, 'ka', 86),
(87, 0xe183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398, 'ka', 87),
(88, 0xe183a7e18395e18394e1839ae18390, 'ka', 88),
(89, 0xe1839be1839de1839ce18398e183a8e1839ce1839420e183a8e18394e183a0e183a9e18394e183a3e1839ae18394e18391e1839820e183a0e1839de18392e1839de183a0e183aa20e183ace18390e18399e18398e18397e183aee183a3e1839ae18398, 'ka', 89),
(90, 0xe1839be1839de1839ce18398e183a8e1839ce1839420e183a8e18394e183a0e183a9e18394e183a3e1839ae18394e18391e1839820e183a0e1839de18392e1839de183a0e183aa20e183ace18390e183a3e18399e18398e18397e183aee18390e18395e18398, 'ka', 90),
(91, 0xe183a3e1839ce18393e1839020e183a8e18394e18390e183a0e183a9e18398e1839de1839720e1839be18398e1839ce18398e1839be183a3e1839b20e18394e183a0e18397e1839820e183a9e18390e1839ce18390e183ace18394e183a0e18398, 'ka', 91),
(92, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18394e18391e18398, 'ka', 92),
(93, 0xe183a9e18394e1839be1839820e18399e1839de1839ce183a2e18390e183a5e183a2e18394e18391e18398, 'ka', 93),
(94, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 94),
(95, 0xe183a8e18394e18398e183a0e183a9e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e183a3e1839ae18390e18393, 'ka', 95),
(96, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 96),
(97, 0xe1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18390, 'ka', 97),
(98, 0xe18399e1839de1839be1839ee18390e1839ce18398e18398e183a120e18393e18390e183a1e18390e183aee18394e1839ae18394e18391e18390, 'ka', 98),
(99, 0xe183ace183a7e18390e183a0e1839d, 'ka', 99),
(100, 0xe183a12fe18399, 'ka', 100),
(101, 0xe18397e18395e18398e1839720e183ace18390e183a0e1839be1839de18394e18391e18390, 'ka', 101),
(102, 0xe183a1e18398e183a2e183a7e18395e18398e18394e183a0e18398, 'ka', 102),
(103, 0x7b6e7d20e1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e1839020e183a8e18394e183a5e1839be1839ce18398e1839ae18398e183907c7b6e7d20e183a8e18394e183a5e1839be1839ce18398e1839ae1839820e1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18394e18391e1839820e18398e183aee18398e1839ae18394e183973a2022e1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18394e18391222de183a8e18398, 'ka', 103),
(104, 0xe1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18394e18391e18398, 'ka', 104),
(105, 0xe1839be183a8e1839de18391e18394e1839ae1839820e1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18390, 'ka', 105),
(106, 0xe183ace1839ae18398e183a3e183a0e1839820e183a8e18394e1839be1839de183a1e18390e18395e18390e1839ae18398, 'ka', 106),
(107, 0xe18398e183a3e183a0e18398e18393e18398e183a3e1839ae1839820e1839be18398e183a1e18390e1839be18390e183a0e18397e18398, 'ka', 107),
(108, 0xe18397e18390e1839ce18390e1839be183a8e183a0e1839de1839be1839ae18394e18391e18398, 'ka', 108),
(109, 0xe1839de183a4e18398e183a1e18398e183a120e183a2e18394e1839a2e, 'ka', 109),
(110, 0xe183a4e18390e183a5e183a1e18398, 'ka', 110),
(111, 0xe183a4e18390e183a5e183a2e18398e183a3e183a0e1839820e1839be18398e183a1e18390e1839be18390e183a0e18397e18398, 'ka', 111),
(112, 0xe1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18390, 'ka', 112),
(113, 0xe1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18394e18391e18398, 'ka', 113),
(114, 0xe183a12fe18399, 'ka', 114),
(115, 0xe18391e1839de1839ae1839d20e18390e183a5e183a2e18398e18395e1839de18391e18394e18391e18398, 'ka', 116),
(116, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae183982c20e183a0e1839de1839be18394e1839ae1839be18390e183aa20e18391e1839de1839ae1839d20e183aae18395e1839ae18398e1839ae18394e18391e1839020e183a8e18394e18398e183a2e18390e1839ce18390, 'ka', 117),
(117, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18394e18391e18398, 'ka', 118),
(118, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 119),
(119, 0xe18390e18398e183a0e183a9e18398e18394e18397, 'ka', 120),
(120, 0xe1839be18398e183a1e18390e183a1e18390e1839ae1839be18394e18391e18394e1839ae1839820e18392e18395e18394e183a0e18393e18398e183a120e18393e18390e1839be18390e1839ae18395e18390, 'ka', 121),
(121, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae18398, 'ka', 122),
(122, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 123),
(123, 0xe183a9e18394e1839be1839820e1839ee183a0e1839de183a4e18398e1839ae18398, 'ka', 124),
(124, 0xe18392e18390e1839be1839de183a1e18395e1839ae18390, 'ka', 125),
(125, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 126),
(126, 0xe18395e18398e183a120e183a8e18394e183a3e183abe1839ae18398e1839020e183ace18390e18398e18399e18398e18397e183aee1839de183a120e18393e1839020e18393e18390e183ace18394e183a0e1839de183a1, 'ka', 127),
(127, 0xe183a1e183aee18395e1839020e183a12fe18399, 'ka', 128),
(128, 0xe18391e1839ae1839de18399e18398e183a120e183a9e18390e1839be1839de1839ce18390e18397e18395e18390e1839ae18397e1839020e183a1e18398e18390, 'ka', 129),
(129, 0xe18392e18390e1839ce1839ae18390e18392e18394e18391e18398e183a120e183a8e18394e1839ce18390e183aee18395e18390, 'ka', 130),
(130, 0xe18393e183a0e1839d, 'ka', 131),
(131, 0xe18392e18390e1839ce1839ae18390e18392e18394e18391e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e183a8e18394e18398e1839ce18390e183aee18390, 'ka', 132),
(132, 0x20e183ace1839ae18398e183a3e183a0e18390e18393, 'ka', 133),
(133, 0xe18393e18390e183a0e183ace1839be183a3e1839ce18394e18391e183a3e1839ae1839820e183aee18390e183a0e1839720e183a0e1839de1839b20e18392e183a1e183a3e183a0e1839720e18390e1839b20e183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18398e183a120e183ace18390e183a8e1839ae183903f, 'ka', 134),
(134, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 135),
(135, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 136),
(136, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18398e183a120e183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390, 'ka', 137),
(137, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 138),
(138, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 139),
(139, 0xe1839ae18398e18393e18394e183a0e18391e1839de183a0e18393e18398, 'ka', 140),
(140, 0xe18394e183a5e183a1e1839ee1839de183a0e183a2e18398, 'ka', 141),
(141, 0xe18394e183a5e183a1e1839ee1839de183a0e183a2e18398, 'ka', 142),
(142, 0xe183a8e18394e18393e18394e18392e18394e18391e1839820e18390e183a020e1839be1839de18398e183abe18394e18391e1839ce18390, 'ka', 143),
(143, 0xe18391e18390e183a2e1839de1839ce18398, 'ka', 144),
(144, 0xe183a5e18390e1839ae18391e18390e183a2e1839de1839ce18398, 'ka', 145),
(145, 0xe183ace18390e183a3e18399e18398e18397e183aee18390e18395e18398, 'ka', 146),
(146, 0xe18390e183a6e18390e183a020e18390e183a0e18398e183a120e18391e1839ae1839de18399e1839820e18393e18390e183a1e18390e1839be18390e183a2e18394e18391e1839ae18390e18393, 'ka', 147),
(147, 0xe183a7e18395e18394e1839ae1839020e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e183a120e1839ce18390e183aee18395e18390, 'ka', 148),
(148, 0xe18395e18394e1839120e183a4e1839de183a0e1839be18398e183a120e183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390, 'ka', 149),
(149, 0xe18395e18394e1839ae18398e183a120e183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390, 'ka', 150),
(150, 0xe18392e18390e1839ce1839ae18390e18392e18394e18391e18398e183a120e183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390, 'ka', 157),
(151, 0xe183a1e18390e18395e18398e18396e18398e183a2e1839d20e18391e18390e183a0e18390e18397e18398e183a120e183a1e18390e183aee18398e1839720e183a9e18395e18394e1839ce18394e18391e18390, 'ka', 152),
(152, 0xe18392e18390e1839ce18390e183aee1839ae18394e18391e18390, 'ka', 153),
(153, 0xe18393e18390e183ace183a7e18394e18391e18398e183a120e18397e18390e183a0e18398e183a6e18398, 'ka', 158),
(154, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e18392e18390e183a1e18390e18392e18396e18390e18395e1839ce1839820e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 159),
(155, 0xe183a4e18398e1839ae183a2e183a0e18394e18391e18398e183a120e183a1e18398e18390, 'ka', 160),
(156, 0x4150492de18398e183a120e18390e183a0e18390e183a1e183ace1839de183a0e1839820e1839be1839de18397e183aee1839de18395e1839ce18398e183a120e183a2e18398e1839ee18398, 'ka', 161),
(157, 0xe18390e183a0e18390e183a1e183ace1839de183a0e1839820e1839be1839de18397e183aee1839de18395e1839ce18390, 'ka', 162),
(158, 0xe18392e18390e1839be1839de183ace18394e183a0e18390, 'ka', 163),
(159, 0xe18390e183a0e18390e18390e183a5e183a2e18398e183a3e183a0e18398, 'ka', 164),
(160, 0xe183a1e18394e183a0e18395e18398e183a1e18398, 'ka', 165),
(161, 0xe18394e183a0e18397e183aee18394e1839a, 'ka', 166),
(162, 0xe183a8e18394e183aae18393e1839de1839be1839020e1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a0e18394e18396e18394e183a0e18395e18390e183aae18398e18398e183a12fe18390e183a6e18393e18392e18394e1839ce18398e183a120e18393e183a0e1839de183a12e, 'ka', 167),
(163, 0xe183a0e18394e18396e18394e183a0e18395e18390e183aae18398e18398e183a120e183a4e18390e18398e1839ae1839820e183a3e18399e18395e1839420e18390e183a0e183a1e18394e18391e1839de18391e183a12e20e18393e18390e183a0e183ace1839be183a3e1839ce18394e18391e183a3e1839ae1839820e183aee18390e183a0e183972c20e183a0e1839de1839b20e18392e18398e1839ce18393e18390e1839720e183a4e18390e18398e1839ae18398e183a120e183aee18394e1839ae18390e183aee1839ae1839020e18392e18390e18393e18390e183ace18394e183a0e183903f, 'ka', 168),
(164, 0xe183a0e18394e18396e18394e183a0e18395e18390e183aae18398e1839020e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae1839820e18390e183a020e18390e183a0e18398e183a12e, 'ka', 169),
(165, 0xe18392e18397e183aee1839de18395e183972c20e183ace18390e183a8e18390e1839ae18394e1839720e18390e183a0e183a1e18394e18391e183a3e1839ae1839820e183a4e18390e18398e1839ae1839820e18390e1839c20e183a8e18394e18398e183a7e18395e18390e1839ce18394e1839720e18390e183aee18390e1839ae1839820e18393e1839020e18393e18390e18398e183ace183a7e18394e1839720e183a0e18394e18396e18394e183a0e18395e18390e183aae18398e18398e183a120e1839ee183a0e1839de183aae18394e183a1e1839820e18397e18390e18395e18398e18393e18390e1839c202e, 'ka', 170),
(166, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a0e18394e18396e18394e183a0e18395e18390e183aae18398e18398e183a120e18393e18390e183ace183a7e18394e18391e183902e, 'ka', 171),
(167, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a0e18394e18396e18394e183a0e18395e18390e183aae18398e1839020e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae18398e183902e, 'ka', 172),
(168, 0xe183a8e18394e183aae18393e1839de1839be1839020e18391e18394e183a5e18390e183a4e18398e183a120e183a8e18394e183a5e1839be1839ce18398e183a120e18393e183a0e1839de183a12e, 'ka', 173),
(169, 0xe183a0e18394e18396e18394e183a0e18395e18390e183aae18398e18398e183a120e183a4e18390e18398e1839ae18398e183a120e183ace18390e183a8e1839ae183902e, 'ka', 174),
(170, 0xe18392e18397e183aee1839de18395e183972c20e183a8e18394e18390e183a1e183a0e183a3e1839ae1839de1839720e1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a0e18394e18396e18394e183a0e18395e18390e183aae18398e1839020e18398e1839ce18393e18398e18395e18398e18393e183a3e18390e1839ae183a3e183a0e18390e183932e, 'ka', 175),
(171, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a0e18394e18396e18394e183a0e18395e18390e183aae18398e18398e183a120e18390e183a6e18393e18392e18394e1839ce18398e183a120e18393e18390e183ace183a7e18394e18391e183902e, 'ka', 176),
(172, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e1839020e18390e183a6e18393e18392e18394e1839ce18398e1839ae18398e18390, 'ka', 177),
(173, 0xe183a8e18394e183aae18393e1839de1839be183902c20e18390e183a6e18393e18392e18394e1839ce18398e183a120e18393e183a0e1839de183a12e, 'ka', 178),
(174, 0xe183a1e183a5e18394e1839be18398e183a120e18392e18390e1839ce18390e183aee1839ae18394e18391e18398e183a120e1839ee183a0e1839de183aae18394e183a1e18398e183a120e18393e18390e183ace183a7e18394e18391e183902e, 'ka', 179),
(175, 0xe183a1e183a5e18394e1839be18398e183a120e18392e18390e1839ce18390e183aee1839ae18394e18391e1839020e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae18398e183902e, 'ka', 180),
(176, 0xe183afe18390e1839be183a3e183a0e1839820e18393e183a0e1839d3a207b666f726d617474656454696d657d20e183ace18390e1839be18394e18391e183982e, 'ka', 181),
(177, 0x457a636f64782de18398e183a120e18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e18398e183a120e1839ee183a0e1839de183aae18394e183a1e18398e183a120e18393e18390e183ace183a7e18394e18391e183902e, 'ka', 182),
(178, 0x457a636f64782de18398e183a120e18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e18398e183a120e1839ee18398e183a0e18395e18394e1839ae1839820e183a4e18390e18396e1839020e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae18398e183902e, 'ka', 183),
(179, 0xe18392e18397e183aee1839de18395e183972c20e18392e18390e1839ce18390e183aee1839de183a0e183aae18398e18394e1839ae1839de1839720e183a8e18394e1839be18393e18394e18392e1839820e18391e183a0e183abe18390e1839ce18394e18391e183903a20227b636f6d6d616e647d2220e18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e18398e183a120e1839ee183a0e1839de183aae18394e183a1e18398e183a120e18393e18390e183a1e18390e183a1e183a0e183a3e1839ae18394e18391e1839ae18390e183932e, 'ka', 184),
(180, 0x457a636f64782de18398e183a120e18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e18398e183a120e18393e18390e183ace183a7e18394e18391e183902e20e183a4e18390e18396e183902d32, 'ka', 185),
(181, 0x457a636f64782de18398e183a120e18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e1839020e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae18398e183902e, 'ka', 186),
(182, 0xe183a8e18394e183aae18393e1839de1839be183902c20e18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e18398e183a120e1839ee183a0e1839de183aae18394e183a1e183a8e183983a207b6d6573736167657d, 'ka', 187),
(183, 0xe18394e183a120e18390e183a0e18398e183a120457a636f64782de18398e183a120e18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e18398e183a120e1839ee183a0e1839de183aae18394e183a1e183982e20e18392e18397e183aee1839de18395e183972c20e1839be1839de18390e183aee18393e18398e1839ce1839de1839720e183a4e18390e18398e1839ae18394e18391e18398e183a12fe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a0e18394e18396e18394e183a0e18395e18390e183aae18398e183902c20e1839ee183a0e1839de183aae18394e183a1e18398e183a120e18392e18390e18392e183a0e183abe18394e1839ae18394e18391e18390e1839be18393e183942e, 'ka', 188),
(184, 0xe1839ce18390e1839be18393e18395e18398e1839ae18390e1839320e18392e183a1e183a3e183a0e1839720457a636f64782de18398e183a120e18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e183903f, 'ka', 189),
(185, 0xe18392e18390e183a3e1839be183afe1839de18391e18394e183a1e18394e18391e18398e183a120e1839ee183a0e1839de183aae18394e183a1e1839820e183a8e18394e183ace183a7e18395e18394e183a2e18398e1839ae18398e183902e, 'ka', 190),
(186, 0xe183a8e18394e183aae18393e1839de1839be183902c20e1839be18394e183a2e18390e18393e18390e183a2e18398e183a120e1839be18390e183a0e18397e18395e18398e183a120e18393e183a0e1839de183a12e, 'ka', 191),
(187, 0xe18390e183a0e18390e183a1e18390e18399e1839ce18390e183a0e18398e183a1e1839820e1839be1839de1839ce18390e183aae18394e1839be18394e18391e1839820e18392e183a0e18390e183a4e18398e18399e18398e183a120e183a8e18394e183a1e18390e183a5e1839be1839ce18394e1839ae18390e18393, 'ka', 192),
(188, 0xe18398e1839be1839ee1839de183a0e183a2e18398e183a120e1839ee183a0e1839de183aae18394e183a1e1839820e18390e183a6e1839be1839de183a9e18394e1839ce18398e1839ae1839820e18390e183a020e18390e183a0e18398e183a1, 'ka', 193),
(189, 0xe1839ae183a3e183a0e183afe18398, 'ka', 194),
(190, 0xe183a7e18390e18395e18398e183a1e183a4e18394e183a0e18398, 'ka', 195),
(191, 0xe18390e1839ae183a3e18391e18390e1839ae18398, 'ka', 196),
(192, 0xe18397e18390e183a4e1839ae18398, 'ka', 197),
(193, 0xe1839ae18390e18398e1839be18390, 'ka', 198),
(194, 0xe183a4e18398e183a0e183a3e18396e18398, 'ka', 199),
(195, 0xe18398e18390, 'ka', 200),
(196, 0xe18394e183a5e183a1e18399e1839ae183a3e18396e18398e183a3e183a0e1839820e183a4e183a3e183a0e183aae18394e1839ae18398, 'ka', 201),
(197, 0xe183aee1839be18390e183a3e183a0e18398, 'ka', 202),
(198, 0xe183a4e183a3e183a0e183aae18394e1839ae18398, 'ka', 203),
(199, 0xe183a4e18390e18398e1839ae18398e183a120e18392e18390e183aee183a1e1839ce1839020e183a8e18394e183a3e183abe1839ae18394e18391e18394e1839ae18398e18390, 'ka', 204),
(200, 0xe183a8e18394e183aae18393e1839de1839be1839020e183a4e18390e18398e1839ae18398e183a120e18399e18398e18397e183aee18395e18398e183a1e18390e183a1, 'ka', 205),
(201, 0xe183a8e18394e183aae18393e1839de1839be1839020e18392e183a0e18390e1839be18390e183a2e18398e18399e183a3e1839a20e18392e18390e183a0e183a9e18394e18395e18390e183a8e18398207b66696c65536f757263657d3a20e183a1e18398e1839ce183a2e18390e183a5e183a1e183a3e183a0e1839820e183a8e18394e183aae18393e1839de1839be1839020e18395e18394e1839ae183a8e18398, 'ka', 206),
(202, 0xe18397e18390e1839ce18390e18391e183a0e18394e18391e18398, 'ka', 207),
(203, 0xe18390e183a020e183a3e18393e183a0e18398e183a1, 'ka', 208),
(204, 0xe18398e183ace183a7e18394e18391e18390, 'ka', 209),
(205, 0xe1839be18397e18390e18395e183a0e18393e18394e18391e18390, 'ka', 210),
(206, 0xe1839be1839de18398e183aae18390e18395e183a1, 'ka', 211),
(207, 0xe1839be18394e183a2e1839820e18390e1839c20e18397e18390e1839ce18390e18391e18390e183a0e18398, 'ka', 212),
(208, 0xe1839ce18390e18399e1839ae18394e18391e1839820e18390e1839c20e18397e18390e1839ce18390e18391e18390e183a0e18398, 'ka', 213),
(209, 0xe183a3e183a4e183a0e1839d20e1839be18394e183a2e18398, 'ka', 214),
(210, 0xe183a3e183a4e183a0e1839d20e1839ce18390e18399e1839ae18394e18391e18398, 'ka', 215),
(211, 0xe18394e183a0e183972de18394e183a0e18397e18398, 'ka', 216),
(212, 0xe18391e18390e18397e18398e1839ae18398e18390, 'ka', 217),
(213, 0xe18391e18390e18397e18398e1839ae1839820e18390e183a020e18390e183a0e18398e183a1, 'ka', 218),
(214, 0xe18399e18394e18397e18393e18394e18391e18390, 'ka', 219),
(215, 0xe18398e183a7e1839d, 'ka', 220),
(216, 0xe18399e18394e18397e18393e18394e18391e1839020e18394e183a0e183972de18394e183a0e18397e18398, 'ka', 221),
(217, 0xe18398e183a7e1839d20e18394e183a0e183972de18394e183a0e18397e18398, 'ka', 222),
(218, 0xe183aae18395e1839ae18398e1839ae18394e18391e18394e18391e18398, 'ka', 223),
(219, 0xe18390e183a020e18398e183aae18395e1839ae18394e18391e18390, 'ka', 224),
(220, 0xe183aae18390e183a0e18398e18394e1839ae18398e18390, 'ka', 225),
(221, 0xe183aae18390e183a0e18398e18394e1839ae1839820e18390e183a020e18390e183a0e18398e183a1, 'ka', 226),
(222, 0xe18394e183a0e18397e183987b646174657d, 'ka', 227),
(223, 0xe18390e183a0e18398e183a1, 'ka', 228),
(224, 0x324420e183b0e1839de183a0e18398e18396e1839de1839ce183a2e18390e1839ae183a3e183a0e1839820e18392e183a0e18390e183a4e18398e18399e18398e183a120e183aee18390e18396e18398, 'ka', 229),
(225, 0x334420e183b0e1839de183a0e18398e18396e1839de1839ce183a2e18390e1839ae183a3e183a0e1839820e18392e183a0e18390e183a4e18398e18399e18398e183a120e183aee18390e18396e18398, 'ka', 230),
(226, 0x324420e18395e18394e183a0e183a2e18398e18399e18390e1839ae183a3e183a0e1839820e18392e183a0e18390e183a4e18398e18399e18398, 'ka', 231),
(227, 0x334420e18395e18394e183a0e183a2e18398e18399e18390e1839ae183a3e183a0e1839820e18392e183a0e18390e183a4e18398e18399e18398e183a120e183aee18390e18396e18398, 'ka', 232),
(228, 0xe18393e1839de1839ce18390e183a2e18398203244, 'ka', 233),
(229, 0xe18393e1839de1839ce18390e183a2e18398203344, 'ka', 234),
(230, 0xe18392e18390e1839ce18390e183aee1839ae18394e18391e18390, 'ka', 235),
(231, 0xe18392e18390e1839ce18390e183aee1839ae18394e18391e1839020e183ace18390e183a0e18390e1839be183a2e18394e18391e18398e1839720e18392e18390e1839ce183aee1839de183a0e183aae18398e18394e1839ae18393e18390, 'ka', 236),
(232, 0xe1839be18390e183a1e18398e183a3e183a0e1839820e18392e18390e1839ce18390e183aee1839ae18394e18391e18390, 'ka', 237),
(233, 0xe1839be18394e183a2e1839820e1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e18398, 'ka', 238),
(234, 0xe1839be183aae18398e183a0e1839420e1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e18398, 'ka', 239),
(235, 0xe183ace18390e183a8e1839ae18390, 'ka', 240),
(236, 0xe1839be18390e183a1e18398e183a3e183a0e1839820e183ace18390e183a8e1839ae18390, 'ka', 241),
(237, 0xe1839be1839de18393e183a3e1839ae1839820e18390e183a020e183a1e18390e183ade18398e183a0e1839de18394e18391e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390e183a1, 'ka', 242),
(238, 0xe1839be18394e183a2e1839820e18393e18394e183a2e18390e1839ae18398, 'ka', 243),
(239, 0xe1839be183aae18398e183a0e1839420e18393e18394e183a2e18390e1839ae18394e18391e18398, 'ka', 244),
(240, 0xe183a2e18390e18391e18398, 'ka', 245),
(241, 0xe183afe18390e1839be18398, 'ka', 246),
(242, 0x7b6174747269627574657d20e183a3e1839ce18393e1839020e18398e183a7e1839de183a1207b76616c75657d2e, 'ka', 247),
(243, 0x7b6174747269627574657d20e183a3e1839ce18393e1839020e18398e183a7e1839de183a1207b747970657d2e, 'ka', 248),
(244, 0xe183a1e18390e183aee18394e1839ae1839820e18390e183a020e183a3e1839ce18393e1839020e183a8e18394e18398e183aae18390e18395e18393e18394e183a120e18392e18390e1839be1839de183a2e1839de18395e18394e18391e18390e183a120e18393e1839020e183a1e1839ee18394e183aae18398e18390e1839ae183a3e183a020e183a1e18398e1839be18391e1839de1839ae1839de18394e18391e183a1, 'ka', 249),
(245, 0xe1839ee18398e183a0e18395e18394e1839ae1839820e183a1e18398e1839be18391e1839de1839ae1839d20e183a3e1839ce18393e1839020e18398e183a7e1839de183a120e1839ee18390e183a2e18390e183a0e1839020e18390e183a1e1839d, 'ka', 250),
(246, 0xe18390e183a2e183a0e18398e18391e183a3e183a2e18398e183a120e18398e18390e183a0e1839ae18398e183a7e18394e18391e1839820e18390e18399e1839ae18398e18390, 'ka', 251),
(247, 0xe183a8e18394e183aae18393e1839de1839be18390e183902c20e18390e183a0e18390e183a1e18390e18398e1839be18394e18393e1839d20e18390e183a2e183a0e18398e18391e183a3e183a2e18398e183a120e183a8e18394e183a7e18395e18390e1839ce1839020227b6174747269627574657d22, 'ka', 252),
(248, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398, 'ka', 253),
(249, 0xe1839be1839de183a8e1839de183a0e18394e18391e18390, 'ka', 254),
(250, 0x7b6e7d20e1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e1839020e183a8e18394e183a5e1839be1839ce18398e1839ae18398e183907b6e7de1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e1839020e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 255),
(251, 0xe183a1e18398e18398e183a120e18393e18390e183a4e18390, 'ka', 256),
(252, 0xe183a0e1839020e183aee18393e18394e18391e1839020e18390e1839b20e183a1e18398e18390e183a8e183983f, 'ka', 257),
(253, 0xe183a9e18390e1839be1839de1839ce18390e18397e18395e18390e1839ae18398e183a120e183a8e18394e18393e18392e18394e1839ce18390, 'ka', 258),
(254, 0xe18392e18390e1839be1839de183ace18394e183a0e18394e18391e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 259),
(255, 0xe183a7e18395e18394e1839ae1839020e18392e18390e1839be1839de183ace18394e183a0e18398e183a120e18392e18390e183a3e183a5e1839be18394e18391e18390, 'ka', 260),
(256, 0x3c68323ee18390e183a1e1839420e183a1e183ace183a0e18390e183a4e18390e1839320e18390e183a0e18390213c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703e20e183a0e1839de1839b20e1839be1839de18390e183aee18394e183a0e183aee1839de1839720e1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e18398e183a120e1839be18390e183a0e18397e18395e183902c20e183a3e1839ce18393e1839020e18392e183a5e1839de1839ce18393e18394e1839720e183ace18395e18393e1839de1839be1839020e18390e1839c20e18399e1839de1839ce183a2e18390e183a5e183a2e18394e18391e18397e18390e1839c20e18390e1839c20e183aee18394e1839ae1839be183abe183a6e18395e18390e1839ce18394e1839ae1839de18391e18390e183a1e18397e18390e1839c2e20e18393e18390e183a3e18399e18390e18395e183a8e18398e183a0e18393e18398e18397202043524d202de18398e183a120e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183aae18398e18390e183a120e18390e1839b20e183a1e18390e18399e18398e18397e183aee18397e18390e1839c20e18393e18390e18399e18390e18395e183a8e18398e183a0e18394e18391e18398e183972e3c2f703e, 'ka', 261),
(257, 0x7b636f756e747d20e18392e18390e1839be1839de183ace18394e183a0e18398e1839ae18398, 'ka', 262),
(258, 0x7b636f756e747d20e18392e18390e1839be1839de183ace18394e183a0e18398e183a120e18392e18390e183a3e183a5e1839be18394e18391e18390, 'ka', 263),
(259, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e18390, 'ka', 264),
(260, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e18394e18391e18398, 'ka', 265),
(261, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e18398e183a120e183ace18394e18395e183a0e18394e18391e18398, 'ka', 266),
(262, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e18398e183a120e183ace18394e18395e183a0e18398, 'ka', 267),
(263, 0xe183a9e18390e1839be1839de1839ce18390e18397e18395e18390e1839ae18398, 'ka', 268),
(264, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a3e1839ae1839820e183a1e18398e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 269),
(265, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a3e1839ae1839820e183a1e18398e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 270),
(266, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a3e1839a20e1839ce183a3e183a1e183aee18390e183a8e1839820e183ace18395e18393e1839de1839be18390, 'ka', 271),
(267, 0xe18397e183a5e18395e18394e1839c20e183aee18390e183a0e1839720e18392e18390e1839be1839de183ace18394e183a0e18398e1839ae18398, 'ka', 272),
(268, 0xe18397e183a5e18395e1839ce1839420e183aee18390e183a0e1839720e18392e18390e183a3e183a5e1839be18394e18391e183a3e1839ae183982020e183a7e18395e18394e1839ae1839020e1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a3e1839ae1839820e183a1e18398e18398e18393e18390e1839c20e18393e1839020e18390e1839be1839de183ace18394e183a0e18398e1839ae1839820e183a7e18395e18394e1839ae1839020e18398e1839be18394e18398e1839ae18394e18391e18398e18393e18390e1839c20e1839be1839de1839be18390e18395e18390e1839ae183a8e183982e, 'ka', 273),
(269, 0xe18397e183a5e18395e18394e1839c20e183aee18390e183a0e1839720e18390e1839be1839de183ace18394e183a0e18398e1839ae18398, 'ka', 274),
(270, 0x7b73756273637269626564436f756e747d20e18392e18390e1839be1839de183ace18394e183a0e18398e1839ae18398, 'ka', 275),
(271, 0xe18392e18390e18390e183a1e183a3e183a4e18397e18390e18395e1839420e183abe18395e18394e1839ae1839820e18392e18390e18392e18396e18390e18395e1839ce18398e1839ae1839820e183a1e18390e1839be183a3e183a8e18390e1839d20e18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398, 'ka', 276),
(272, 0xe18392e18390e18392e18396e18390e18395e1839ce18398e1839ae1839820e1839be18394e18398e1839ae18398e183a120e18393e18390e1839be183a3e183a8e18390e18395e18394e18391e18390, 'ka', 277),
(273, 0xe183a7e1839de18395e18394e1839a203120e183ace183a3e18397e183a8e183982e, 'ka', 278),
(274, 0xe183a8e18394e1839be1839de183a1e183a3e1839ae1839820e1839be18394e18398e1839ae18398e183a120e18393e18390e1839be183a3e183a8e18390e18395e18394e18391e18390, 'ka', 279),
(275, 0xe183a1e18390e183a4e1839de183a1e183a2e1839d20e183a7e183a3e18397e18397e18390e1839c20e18393e18390e18399e18390e18395e183a8e18398e183a0e18394e18391e18398e183a120e183a8e18394e183aae18393e1839de1839be18390, 'ka', 280),
(276, 0xe18394e1839a2ee183a4e1839de183a1e183a2e18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e1839820e18390e183a020e18390e183a0e183a1e18394e18391e1839de18391e183a120e183a1e18398e183a1e183a2e18394e1839be18390e183a8e18398, 'ka', 281),
(277, 0xe18392e18390e1839be1839de1839be18392e18396e18390e18395e1839ce18396e1839420e18398e1839ce183a4e1839de183a0e1839be18390e183aae18398e1839020e18395e18394e183a020e18398e183a5e1839ce1839020e1839be1839de1839ee1839de18395e18394e18391e183a3e1839ae1839820e18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e18393e18390e1839c, 'ka', 282),
(278, 0xe18390e18393e183a0e18394e183a1e18390e183a2e18398e183a120e18398e1839ce183a4e1839de183a0e1839be18390e183aae18398e18398e183a120e18398e1839be18394e18398e1839ae18398e183a120e18392e18390e183a0e18394e1839720e18392e18390e183a2e18390e1839ce1839020e183a8e18394e183a3e183abe1839ae18394e18391e18394e1839ae18398e18390, 'ka', 283),
(279, 0xe18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e1839020e18395e18394e183a020e18392e18390e18393e18390e1839be1839de183ace1839be18393e18390, 'ka', 284),
(280, 0xe18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e183a120e183a8e18394e1839ce18390e183aee18395e1839020e18395e18394e183a020e1839be1839de183aae18394e183a0e183aee18393e18390, 'ka', 285),
(281, 0xe18392e18390e18392e18396e18390e18395e1839ce18398e1839ae1839820e18398e1839be18394e18398e1839ae18398e183a120e183a2e18394e183a1e183a2e18398e183a0e18394e18391e18390, 'ka', 286),
(282, 0xe183a1e18390e183a2e18394e183a1e183a2e1839d20e18398e1839be18394e18398e1839ae1839820457a636f64782de18398e183a1e18392e18390e1839c, 'ka', 287),
(283, 0xe183a1e18390e183a2e18394e183a1e183a2e1839d20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e1839020457a636f64782de18398e183a1e18392e18390e1839c, 'ka', 288),
(284, 0xe183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e18392e18390e18398e18392e18396e18390e18395e1839ce18390, 'ka', 289),
(285, 0xe183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e1839020e18395e18394e183a020e18392e18390e18398e18392e18396e18390e18395e1839ce18390, 'ka', 290),
(286, 0xe18392e18390e1839be1839de183aee1839be18390e183a3e183a0e18394e18391e1839020e183a1e18394e183a0e18395e18394e183a0e18398e18393e18390e1839c, 'ka', 291),
(287, 0xe18393e18390e18390e183a0e183a5e18398e18395e18394e18391e183a3e1839ae1839820e18398e1839be18394e18398e1839ae18394e18391e1839820e18394e1839be18397e183aee18395e18394e18395e18390, 'ka', 292),
(288, 0x3c7370616e20636c6173733d22656d61696c2d66726f6d223e3c7374726f6e673ee18392e18390e1839be1839de1839be18392e18396e18390e18395e1839ce183983c2f7374726f6e673e207b73656e646572436f6e74656e747d3c2f7370616e3e, 'ka', 293),
(289, 0x3c7370616e20636c6173733d22656d61696c2d746f223e3c7374726f6e673ee18390e18393e183a0e18394e183a1e18390e183a2e183983a3c2f7374726f6e673e207b726563697069656e74436f6e74656e747d3c2f7370616e3e, 'ka', 294),
(290, 0xe18393e18390e18399e18390e18395e183a8e18398e183a0e18394e18391e183a3e1839ae1839820e18390e183a020e18390e183a0e18398e183a120e183abe18398e183a0e18398e18397e18390e18393e1839820e18398e1839be18394e18398e1839ae18398207b636f6e746163744e616d657de183a1e18397e18390e1839c2e20e18392e18397e183aee1839de18395e1839720e18393e18390e18390e1839be18390e183a2e1839de183972c20e183a0e1839de1839b20e18392e18390e18390e18392e183a0e183abe18394e1839ae1839de183972e, 'ka', 295),
(291, 0xe18392e18390e18392e18396e18390e18395e1839ce18390, 'ka', 296),
(292, 0xe18398e1839be18394e18398e1839ae18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 297),
(293, 0xe18398e1839be18394e18398e1839ae18398e183a120e18390e183a0e183a5e18398e18395e18390e183aae18398e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390202028494d415029, 'ka', 298),
(294, 0x3c68323ee18390e183a0e183aa20e18398e183a1e1839420e183a1e183ace183a0e18390e183a4e18390e183933c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703ee18390e18398e183a0e183a9e18398e1839420e18398e1839be18394e18398e1839ae1839820e1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e1839820e1839be18394e18398e1839ae18398e183a120e18392e18390e18392e18396e18390e18395e1839ce18390e1839be18393e183942e3c2f703e, 'ka', 299),
(295, 0x3c68323ee18390e183a0e18390e183a4e18394e183a0e18398e1839020e183a1e18390e1839ce18390e183aee18390e18395e183983c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703ee18390e183a020e18390e183a0e183a1e18394e18391e1839de18391e183a120e18393e18390e183a3e18399e18390e18395e183a8e18398e183a0e18394e18391e18394e1839ae1839820e1839be18394e18398e1839ae18394e18391e183983c2f703e, 'ka', 300),
(296, 0xe18392e18390e18392e18396e18390e18395e1839ce18398e1839ae1839820e1839be18394e18398e1839ae18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e183902028534d545029, 'ka', 301),
(297, 0x3c68323ee1839ce183a320e18398e183a9e183a5e18390e183a0e18394e183913c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703e20e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183a2e1839de183a0e1839be1839020e1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e1839820e183a3e1839ce18393e1839020e18392e18390e18390e183a1e183ace1839de183a0e1839de183a120e18392e18390e183a1e18390e18392e18396e18390e18395e1839ce1839820e1839be18394e18398e1839ae18398e183a120e1839be1839de1839ce18390e183aae18394e1839be18394e18391e183a8e183982e3c2f703e, 'ka', 302),
(298, 0xe1839be18398e183a1e18390e1839be18390e183a0e18397e18398e18393e18390e1839c, 'ka', 303),
(299, 0xe18391e18390e183a2e1839de1839ce183982fe183a5e18390e1839ae18391e18390e183a2e1839de1839ce18398e183a1e18392e18390e1839c, 'ka', 304),
(300, 0xe18392e18396e18390e18395e1839ce18398e1839ae18398e183a120e1839be18398e1839be183a6e18394e18391e18398, 'ka', 305),
(301, 0xe18392e18396e18390e18395e1839ce18398e1839ae18398e183a120e1839ee18390e183a0e1839de1839ae18398, 'ka', 306),
(302, 0xe18392e18396e18390e18395e1839ce18398e1839ae18398e183a120e1839ee1839de183a0e183a2e18398, 'ka', 307),
(303, 0xe18392e18396e18390e18395e1839ce18398e1839ae18398e183a120e183a3e183a1e18390e183a4e183a0e18397e183aee1839de18394e18391e18390, 'ka', 308),
(304, 0xe18392e18396e18390e18395e1839ce18398e1839ae18398e183a120e183a2e18398e1839ee18398, 'ka', 309),
(305, 0xe18392e18396e18390e18395e1839ce18398e1839ae18398e183a120757365726e616d65, 'ka', 310),
(306, 0xe1839ee18390e183a1e183a3e183aee1839820e1839be18398e183a1e18390e1839be18390e183a0e18397e18396e18394, 'ka', 311),
(307, 0xe183a3e1839ee18390e183a1e183a3e183aee18394, 'ka', 312),
(308, 0xe18392e18390e1839be1839de18398e183a7e18394e1839ce1839420e18392e18396e18390e18395e1839ce18398e1839ae18398e183a120e18398e1839ce18393e18398e18395e18398e18393e183a3e18390e1839ae183a3e183a0e183982020e1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e18398, 'ka', 313),
(309, 0xe18390e183a3e183aae18398e1839ae18394e18391e18394e1839ae1839820e18395e18394e1839ae18398, 'ka', 314),
(310, 0xe18393e18390e18390e18399e1839ae18398e18399e18394, 'ka', 315),
(311, 0x456d61696c20e1839be18394e183a1e18398e183afe18398e183a120e18390e183a5e183a2e18398e18395e1839de18391e18390, 'ka', 316),
(312, 0x456d61696c20e1839be18394e183a1e18398e183afe18398e183a120e18390e183a5e183a2e18398e18395e1839de18391e18394e18391e18398, 'ka', 317),
(313, 0xe18398e1839be18394e18398e1839ae18398e183a120e18392e18390e1839be18392e18396e18390e18395e1839ce18398, 'ka', 318),
(314, 0xe18398e1839be18394e18398e1839ae18398e183a120e18392e18390e1839be18392e18396e18390e18395e1839ce18394e18391e18398, 'ka', 319),
(315, 0xe183a8e18394e183aae18393e1839de1839be18398e183a120e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e183903a, 'ka', 320),
(316, 0xe183a8e18394e183aae18393e1839de1839be1839020e18398e1839be18394e18398e1839ae18398e183a120e18392e18390e18392e18396e18390e18395e1839ce18398e183a1e18390e183a1, 'ka', 321),
(317, 0xe183a8e18394e183aae18393e1839de1839be18394e18391e1839820e18398e1839be18394e18398e1839ae18398e183a120e18392e18390e18392e18396e18390e18395e1839ce18398e183a1e18390e183a1, 'ka', 322),
(318, 0xe183a8e18394e183a5e1839be1839ce18398e183a120e18397e18390e183a0e18398e183a6e1839820e18393e1839020e18393e183a0e1839d, 'ka', 323),
(319, 0xe183a1e18390e183a5e18390e183a6e18390e1839ae18393e18394e18394e18391e18398, 'ka', 324),
(320, 0xe183a1e18390e183a4e1839de183a1e183a2e1839d20e183a7e183a3e18397e18398, 'ka', 325),
(321, 0xe183a1e18390e183a4e1839de183a1e183a2e1839d20e183a7e183a3e18397e18394e18391e18398, 'ka', 326),
(322, 0xe18398e1839be18394e18398e1839ae18398e183a120e183aee18394e1839ae1839be1839de183ace18394e183a0e18390, 'ka', 327),
(323, 0xe18398e1839be18394e18398e1839ae18398e183a120e183aee18394e1839ae1839be1839de183ace18394e183a0e18394e18391e18398, 'ka', 328),
(324, 0x48746d6c20e18399e1839de1839ce183a2e18394e1839ce183a2e18398, 'ka', 329),
(325, 0xe183a2e18394e183a5e183a1e183a2e18398e183a120e183a8e18398e1839ce18390e18390e183a0e183a1e18398, 'ka', 330),
(326, 0xe183a8e18398e1839ce18390e18390e183a0e183a1e18398, 'ka', 331),
(327, 0xe1839be18398e1839be183a6e18394e18391e18394e18391e18398, 'ka', 332),
(328, 0xe18392e18390e1839be18392e18396e18390e18395e1839ce18398, 'ka', 333),
(329, 0xe18392e18390e18392e18396e18390e18395e1839ce18398e183a120e1839be183aae18393e18394e1839ae1839de18391e18390, 'ka', 334),
(330, 0xe18392e18390e18392e18396e18390e18395e1839ce18398e183a120e18397e18390e183a0e18398e183a6e1839820e18393e1839020e18393e183a0e1839d, 'ka', 335),
(331, 0xe18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e183a12055726c, 'ka', 336),
(332, 0xe18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e183a12055726c73, 'ka', 337),
(333, 0xe18398e1839be18394e18398e1839ae18398e183a120e1839be18398e1839be183a6e18394e18391e18398, 'ka', 338),
(334, 0xe18398e1839be18394e18398e1839ae18398e183a120e1839be18398e1839be183a6e18394e18391e18394e18391e18398, 'ka', 339),
(335, 0xe1839be18398e183a1e18390e1839be18390e183a0e18397e18396e18394, 'ka', 340),
(336, 0xe1839be18398e183a1e18390e1839be18390e183a0e18397e18396e18394, 'ka', 341),
(337, 0xe18393e183a0e18390e183a4e183a2e18398, 'ka', 342),
(338, 0xe18392e18390e18392e18396e18390e18395e1839ce18398e1839ae18394e18391e18398, 'ka', 343),
(339, 0xe18392e18396e18390e18395e1839ce1839ae18398e18396e1839420e183a8e18394e183aae18393e1839de1839be18390e18390, 'ka', 344),
(340, 0xe183ace18390e183a0e183a3e1839be18390e183a2e18394e18391e18394e1839ae1839820e18392e18396e18390e18395e1839ce18398e1839ae18398, 'ka', 345),
(341, 0xe18393e18390e18390e183a0e183a5e18398e18395e18394e18391e18398e183a1e18390e183a120e183a8e18394e183aae18393e1839de1839be18390e18390, 'ka', 346),
(342, 0x456d61696c20e183a1e18390e183a5e18390e183a6e18390e1839ae18393e18394, 'ka', 347),
(343, 0x456d61696c20e183a1e18390e183a5e18390e183a6e18390e1839ae18393e18394e18394e18391e18398, 'ka', 348),
(344, 0xe18398e1839be18394e18398e1839ae18398e183a120e18399e1839de1839ce183a2e18394e1839ce183a2e18398, 'ka', 349),
(345, 0xe18398e1839be18394e18398e1839ae18398e183a120e18399e1839de1839ce183a2e18394e1839ce183a2e18394e18391e18398, 'ka', 350),
(346, 0xe183ace18395e18393e1839de1839be1839020e18398e1839be18394e18398e1839ae18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390e18396e18394, 'ka', 351),
(347, 0xe18398e1839be18394e18398e1839ae18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 352),
(348, 0x456d61696c20e183a2e18390e18391e18394e18391e18398, 'ka', 353),
(349, 0xe1839be18390e183a0e18397e1839420e18398e1839be18394e18398e1839ae18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 354),
(350, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e183ace18390e183a8e1839ae18390, 'ka', 355),
(351, 0xe18398e1839be18394e18398e1839ae18398e183a120534d545020e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 356),
(352, 0xe1839be18390e183a0e18397e1839420e18398e1839be18394e18398e1839ae18398e183a120534d545020e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 357),
(353, 0xe18398e1839be18394e18398e1839ae18398e183a120e18393e18390e18390e183a0e183a5e18398e18395e18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 358),
(354, 0xe1839be18390e183a0e18397e18394e1839720e18393e18390e18390e183a0e183a5e18398e18395e18394e18391e183a3e1839ae1839820e18398e1839be18394e18398e1839ae18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 359),
(355, 0xe18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18390, 'ka', 360),
(356, 0xe18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398, 'ka', 361),
(357, 0xe183a2e18394e183a1e183a220e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e183a120e183a8e18394e18393e18394e18392e18394e18391e18398, 'ka', 362),
(358, 0xe183a1e18390e183a2e18394e183a1e183a2e1839d20e18398e1839be18394e18398e1839ae18398e183a120e18392e18390e18392e18396e18390e18395e1839ce18390, 'ka', 363),
(359, 0xe183a2e18394e183a5e183a1e183a2e18398, 'ka', 364),
(360, 0x4363, 'ka', 365),
(361, 0x426363, 'ka', 366),
(362, 0xe183a8e18394e18398e183a7e18395e18390e1839ce18394e1839720e183a1e18390e183aee18394e1839ae1839820e18390e1839c20e18394e1839a2e20e183a4e1839de183a1e183a2e18390, 'ka', 367),
(363, 0xe1839be18398e1839be183a6e18394e18391e1839820426363, 'ka', 368),
(364, 0xe1839be18398e1839be183a6e18394e18391e18398204363, 'ka', 369),
(365, 0xe1839be18398e1839be183a6e18394e18391e183a1, 'ka', 370),
(366, 0xe1839be18398e183a1e18390e1839be18390e183a0e18397e18398e183a120e18392e183a0e18390e183a4e1839020e183aae18390e183a0e18398e18394e1839ae18398e18390, 'ka', 371);
INSERT INTO `messagetranslation` (`id`, `translation`, `language`, `messagesource_id`) VALUES
(367, 0xe1839be18394e18398e1839ae18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18398e18397e1839820e1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e18398, 'ka', 372),
(368, 0xe18392e18390e183a3e18392e18396e18390e18395e1839ce18394e1839720e183a1e18390e183a2e18394e183a1e183a2e1839d20e18398e1839be18394e18398e1839ae18398, 'ka', 373),
(369, 0x53534c20e18399e18390e18395e183a8e18398e183a0e18398, 'ka', 374),
(370, 0xe183a1e18390e183a2e18394e183a1e183a2e1839d20494d415020e18399e18390e18395e183a8e18398e183a0e18398, 'ka', 375),
(371, 0xe1839be18390e183a0e18397e18394e1839720e18397e183a5e18395e18394e1839ce1839820e18398e1839be18394e18398e1839ae18398e183a120e1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e18398, 'ka', 376),
(372, 0x7b6e7d20e18393e18398e18390e1839ae1839de18392e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e183907c7b6e7d20e18393e18398e18390e1839ae1839de18392e18394e18391e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 377),
(373, 0xe1839be1839de1839ce18390e183ace18398e1839ae18394, 'ka', 378),
(374, 0xe18393e18390e183aee183a3e183a0e183a3e1839ae18398, 'ka', 379),
(375, 0xe1839be1839de1839ce18390e183ace18398e1839ae18394e18394e18391e18398, 'ka', 380),
(376, 0xe18393e18398e18390e1839ae1839de18392e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 381),
(377, 0xe18393e18398e18390e1839ae1839de18392e18394e18391e18398, 'ka', 382),
(378, 0xe18393e18398e18390e1839ae1839de18392e18398e183a120e1839be1839de1839ce18390e183ace18398e1839ae18394, 'ka', 383),
(379, 0xe18393e18398e18390e1839ae1839de18392e18394e18391e18398e183a120e1839be1839de1839ce18390e183ace18398e1839ae18394e18394e18391e18398, 'ka', 384),
(380, 0xe183a3e18390e183aee1839ae18394e183a1e1839820e18397e18390e183a0e18398e183a6e18398, 'ka', 385),
(381, 0xe18393e18390e183aee183a3e183a0e183a3e1839ae18398e18390, 'ka', 386),
(382, 0xe18393e18398e18390e1839ae1839de18392e18398e183a120e18394e183a0e18397e18394e183a3e1839ae18398, 'ka', 387),
(383, 0xe18399e1839de1839be18394e1839ce183a2e18390e183a0e18394e18391e18398, 'ka', 388),
(384, 0xe18393e18398e18390e1839ae1839de18392e18398, 'ka', 389),
(385, 0xe183a3e18390e183aee1839ae18394e183a1e1839820e18393e18398e18390e1839ae1839de18392e18398, 'ka', 390),
(386, 0xe18390e18399e183a0e18398e183a4e18394e1839720e1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e183a1e18390e183aee18394e1839ae18398, 'ka', 391),
(387, 0xe183a8e18394e18398e183aae18395e18390e1839ae1839020e18393e18398e18390e1839ae1839de18392e18398e183a120e183a1e183a2e18390e183a2e183a3e183a1e18398, 'ka', 392),
(388, 0xe1839be1839de1839ce18390e183ace18398e1839ae18394e18394e18391e18398202d20e183ace18390e183a0e1839be18390e183a2e18394e18398e1839720e18392e18390e1839ce18390e183aee1839ae18393e183902e, 'ka', 393),
(389, 0xe18393e18398e18390e1839ae1839de18392e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 394),
(390, 0xe18393e18398e18390e1839ae1839de18392e18398e183a120e18393e18390e183a4e18390e183a1e18397e18390e1839c20e183ace18395e18393e1839de1839be18390, 'ka', 395),
(391, 0xe18397e183a5e18395e18394e1839c20e1839be1839de18398e183ace18395e18398e18394e1839720e1839be1839de1839ce18390e183ace18398e1839ae1839420e18393e18398e18390e1839ae1839de18392e183a8e18398, 'ka', 396),
(392, 0x7b6e7d20e1839ee1839de183a1e183a2e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e183907c7b6e7d20e1839ee1839de183a1e183a2e18394e18391e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 397),
(393, 0xe18399e18390e183a2e18394e18392e1839de183a0e18398e18390, 'ka', 398),
(394, 0xe18393e183a0e1839de18398e183a120e18393e18390e183a1e183a0e183a3e1839ae18394e18391e18390, 'ka', 399),
(395, 0xe1839be18393e18394e18391e18390e183a0e18394e1839de18391e18390, 'ka', 400),
(396, 0xe1839be1839de1839ce18390e183ace18398e1839ae18394e1839ce18398, 'ka', 401),
(397, 0xe183a8e18394e183aee18395e18394e18393e183a0e18390, 'ka', 402),
(398, 0xe183a8e18394e183aee18395e18394e18393e183a0e18394e18391e18398, 'ka', 403),
(399, 0xe18393e18390e183a4e18398e183a5e183a1e18398e183a0e18393e1839020e1839ee183a0e1839de18391e1839ae18394e1839be1839020e1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a8e18394e183a5e1839be1839ce18398e183a1e18390e183a12c20e183a8e18394e183aae18393e1839be18398e183a120e18399e1839de18393e183983a, 'ka', 404),
(400, 0xe18393e18390e183a4e18398e183a5e183a1e18398e183a0e18393e1839020e1839ee183a0e1839de18391e1839ae18394e1839be1839020e1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e183a8e18394e183a5e1839be1839ce18398e183a1e18390e183a12c20e183a8e18394e183aae18393e1839de1839be18398e183a120e18399e1839de18393e183983a, 'ka', 405),
(401, 0xe183a1e183a3e1839ee18394e183a020e1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e183a8e18394e183a5e1839be1839ce183902e, 'ka', 406),
(402, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a8e18394e183a5e1839be1839ce18398e183a120e18393e18390e183ace183a7e18394e18391e183902e, 'ka', 407),
(403, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a8e18394e183a5e1839be1839ce18398e183a120e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183902e, 'ka', 408),
(404, 0x7b6e7d20e183a0e18394e1839ee1839de183a0e183a2e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e183907c7b6e7d20e183a0e18394e1839ee1839de183a0e183a2e18394e18391e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 409),
(405, 0xe183a0e18394e1839ee1839de183a0e183a2e18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 410),
(406, 0xe18399e18390e183a2e18394e18392e1839de183a0e18398e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 411),
(407, 0xe18392e18390e183a1e18390e183a7e18398e18393e1839820e183a4e18390e183a1e18398e183a120e183a4e1839de183a0e1839be183a3e1839ae18398e183a120e18398e1839ce183a4e1839de183a0e1839be18390e183aae18398e18390, 'ka', 412),
(408, 0xe18392e18390e183a1e18390e183a7e18398e18393e1839820e183a4e18390e183a1e18398e183a120e183a4e1839de183a0e1839be183a3e1839ae18390e18397e1839020e18395e18394e1839ae18394e18391e18398, 'ka', 413),
(409, 0xe18392e18390e18394e183a0e18397e18398e18390e1839ce18394e18391e183a3e1839ae18390e1839320e183a8e18394e1839be1839de183a1e183a3e1839ae1839820e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398, 'ka', 414),
(410, 0xe183a8e18394e1839be1839de183a1e183a3e1839ae1839820e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398, 'ka', 415),
(411, 0x7b6e7d20436f6e74616374734d6f64756c6553696e67756c61724c6162656c20637265617465647c7b6e7d20436f6e74616374734d6f64756c65506c7572616c4c6162656c2063726561746564, 'ka', 416),
(412, 0x7b6e7d20436f6e74616374734d6f64756c6553696e67756c61724c6162656c2073656172636820636f6d706c657465647c7b6e7d20436f6e74616374734d6f64756c6553696e67756c61724c6162656c20736561726368657320636f6d706c65746564, 'ka', 417),
(413, 0x7b6e7d20436f6e74616374734d6f64756c6553696e67756c61724c6162656c206d61737320757064617465647c7b6e7d20436f6e74616374734d6f64756c65506c7572616c4c6162656c206d6173732075706461746564, 'ka', 418),
(414, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e183a1e183a2e18390e183a2e183a3e183a1e18398, 'ka', 419),
(415, 0xe183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 420),
(416, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18398, 'ka', 421),
(417, 0xe18399e1839de1839ce18390e183a2e18390e183a5e183a2e18394e18391e18398, 'ka', 422),
(418, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e18395e18394e1839ae18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 423),
(419, 0xe183a1e183a2e18390e183a2e183a3e183a1e1839820e18393e18390e183ace183a7e18394e18391e18390, 'ka', 424),
(420, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e183a1e183a2e18390e183a2e183a3e183a1e1839820e18392e18390e18393e18390e18397e18390e183a0e18392e1839be1839ce18398e1839ae1839820e18394e183a2e18398e18399e18394e183a2e18394e18391e18398, 'ka', 425),
(421, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e18394e183a2e18390e1839ee18398, 'ka', 426),
(422, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e18394e183a2e18390e1839ee18398e183a120e18395e18394e1839ae18398, 'ka', 427),
(423, 0xe18392e18390e1839ce183a1e18390e18396e183a6e18395e183a0e183a3e1839ae1839820e183a1e183a2e18390e1839ce18393e18390e183a0e183a2e183a3e1839ae1839820e183a1e183a2e18390e183a2e183a3e183a1e1839820e18390e183a020e18390e183a0e183a1e18394e18391e1839de18391e183a12e, 'ka', 428),
(424, 0xe183a1e183a2e18390e183a2e183a3e183a1e1839820e18390e183a0e18398e183a120e18390e183a3e183aae18398e1839ae18394e18391e18394e1839ae183982e, 'ka', 429),
(425, 0xe18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 430),
(426, 0xe18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18394e18391e18398, 'ka', 431),
(427, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e18394e183a0e18397e18394e183a3e1839ae18394e18391e18398e183a120e18392e18394e1839ce18394e183a0e18398e183a0e18394e18391e18390, 'ka', 432),
(428, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e1839be1839de1839ce18398e183a8e18395e1839ce1839020e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae18390e18393, 'ka', 433),
(429, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398e183a120e1839ee183a0e1839de183aae18394e183a1e18398, 'ka', 434),
(430, 0xe18399e18390e1839be1839ee18390e1839ce18398e1839020e183a8e18394e183a5e1839be1839ce18398e1839ae18398e1839020e18399e18390e1839be1839ee18390e1839ce18398e18394e18391e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 435),
(431, 0xe18399e18390e1839be1839ee18390e1839ce18398e18394e18391e18398, 'ka', 436),
(432, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18390, 'ka', 437),
(433, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e1839be18398e1839be18393e18398e1839ce18390e183a0e1839420e1839ee183a0e1839de183aae18394e183a1e18398e183a120e1839ce18390e183aee18395e18390, 'ka', 438),
(434, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e1839be18390e183a0e18397e18395e18398e183a1e18397e18395e18398e183a120e18397e183a5e18395e18394e1839c20e183a3e1839ce18393e1839020e18392e183a5e1839de1839ce18393e18394e1839720e183ace18395e18393e1839de1839be1839020e18394e1839a2de183a4e1839de183a1e183a2e18398e183a120e183a8e18390e18391e1839ae1839de1839ce18394e18391e183a1e1839020e18393e1839020e1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a3e1839a20e183a1e18398e18394e18391e18396e183942e20e18393e18390e183a3e18399e18390e18395e183a8e18398e183a0e18393e18398e1839720e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183a2e1839de183a0e183a12e, 'ka', 439),
(435, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 440),
(436, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e18394e183a0e18397e18394e183a3e1839ae18398, 'ka', 441),
(437, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e18394e183a0e18397e18394e183a3e1839ae18394e18391e18398, 'ka', 442),
(438, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e18394e183a0e18397e18394e183a3e1839ae18398e183a120e18390e183a5e183a2e18398e18395e1839de18391e18390, 'ka', 443),
(439, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e18394e183a0e18397e18394e183a3e1839ae18398e183a120e18390e183a5e183a2e18398e18395e1839de18391e18394e18391e18398, 'ka', 444),
(440, 0xe183a8e18394e183a9e18394e183a0e18394e18391e183a3e1839ae18398e18390, 'ka', 445),
(441, 0xe18399e18390e1839be1839ee18390e1839ce18398e18390, 'ka', 446),
(442, 0xe18392e18390e18392e18396e18390e18395e1839ce18390, 'ka', 447),
(443, 0xe1839be183aee18390e183a0e18393e18390e183ade183a0e183a0e18398e183a12048544d4c, 'ka', 448),
(444, 0xe18399e18390e1839be1839ee18390e1839ce18398e18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 449),
(445, 0xe18399e18390e1839be1839ee18390e1839ce18398e18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 450),
(446, 0xe183ace18395e18393e1839de1839be1839020e18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e18395e18394e1839ae183a8e18398, 'ka', 451),
(447, 0xe18390e1839ce18392e18390e183a0e18398e183a8e18398e183a120e18395e18394e1839ae18398, 'ka', 452),
(448, 0xe18390e183a5e183a2e18398e18395e1839de18391e18390, 'ka', 453),
(449, 0xe18390e183a5e183a2e18398e18395e1839de18391e18394e18391e18398, 'ka', 454),
(450, 0xe18397e18395e18398e183a1, 'ka', 455),
(451, 0xe18397e18390e1839c, 'ka', 456),
(452, 0xe183a1e18390e183a5e1839be18398e18390e1839ce1839de18391e18398e183a120e18394e183a0e18397e18394e183a3e1839ae18394e18391e18398, 'ka', 457),
(453, 0xe183a9e18394e1839be18398, 'ka', 458),
(454, 0xe183afe18390e1839be183a3e183a0e1839820e18390e183a5e183a2e18398e18395e1839de18395e18390, 'ka', 459),
(455, 0xe18390e1839be1839de18399e18394e183aae18395e18390, 'ka', 460),
(456, 0xe18392e18390e18397e18398e183a8e18395e18390, 'ka', 461),
(457, 0xe18396e18394, 'ka', 462),
(458, 0xe18390e1839be1839de18399e18394e183aae18395e18398e183a120e183a9e18390e183a0e18397e18395e1839020e18390e183a5e183a2e18398e18395e1839de18391e18394e18391e18398e183a120e183a1e18390e1839ce18390e183aee18390e18395e18390e18393, 'ka', 463),
(459, 0xe183a7e18395e18394e1839ae1839020e18390e183a5e183a2e18398e18395e1839de18391e18390, 'ka', 464),
(460, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18398e183a120e1839ee183a0e1839de183aae18394e183a1e18398, 'ka', 465),
(461, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18394e18394e18391e18398, 'ka', 466),
(462, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18394, 'ka', 467),
(463, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18398e183a120e18394e183a0e18397e18394e183a3e1839ae18398, 'ka', 468),
(464, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18398e183a120e18394e183a0e18397e18394e183a3e1839ae18394e18391e18398, 'ka', 469),
(465, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18398e183a120e18394e183a0e18397e18394e183a3e1839ae18398e183a120e18390e183a5e183a2e18398e18395e1839de18391e18390, 'ka', 470),
(466, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18398e183a120e18394e183a0e18397e18394e183a3e1839ae18398e183a120e18390e183a5e183a2e18398e18395e1839de18391e18394e18391e18398, 'ka', 471),
(467, 0xe183a1e18398e18398e183a120e18392e18390e1839be1839de183ace18394e183a0e18390, 'ka', 472),
(468, 0xe183a1e18398e18398e183a120e18392e18390e1839be1839de183ace18394e183a0e18398e183a120e18392e18390e183a3e183a5e1839be18394e18391e18390, 'ka', 473),
(469, 0x7b7175616e746974797d20e18392e18390e18392e18396e18390e18395e1839ce18390, 'ka', 474),
(470, 0x7b7175616e746974797d20e18398e183aee183a1e1839ce18394e18391e1839020287b6f70656e526174657d2529, 'ka', 475),
(471, 0xe18390e183a6e1839ce18398e183a8e1839ce183a3e1839ae1839820e18399e1839de1839ce183a2e18394e1839ce183a2e1839820e183a8e18394e18398e183aae18390e18395e183a120e183a0e18390e1839be1839de18393e18394e1839ce18398e1839be1839420e183a8e18394e183a0e183ace183a7e1839be183a3e1839a20e183a2e18394e18392e183a1, 'ka', 476),
(472, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 477),
(473, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 478),
(474, 0xe183a8e18390e18391e1839ae1839de1839ce18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 479),
(475, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae1839820e183a0e1839de1839be18394e1839ae1839be18390e183aa20e183a8e18394e183a5e1839be1839ce1839020e183a9e18390e1839ce18390e183ace18394e183a0e18398, 'ka', 480),
(476, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e1839be18394e1839ce18394e183afe18394e183a0e1839820e183a0e1839de1839be18394e1839ae1839be18390e183aa20e183a8e18394e183a5e1839be1839ce1839020e183a9e18390e1839ce18390e183ace18394e183a0e18398, 'ka', 481),
(477, 0xe18395e18394e1839120e183a4e1839de183a0e1839be18398e183a120e18399e1839de1839ce183a2e18390e183a5e183a2e18398, 'ka', 482),
(478, 0xe18395e18394e1839120e183a4e1839de183a0e1839be18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 483),
(479, 0x55524c2de183a120e18392e18390e18393e18390e1839be18398e183a1e18390e1839be18390e183a0e18397e18394e18391e18390, 'ka', 484),
(480, 0xe18394e183a2e18398e18399e18394e183a2e18398e183a120e183a6e18398e1839ae18390e18399e18398e183a120e18393e18390e1839be183a2e18399e18398e183aae18394e18391e18390, 'ka', 485),
(481, 0xe183a1e183a2e18390e1839ce18393e18390e183a0e183a2e183a3e1839ae1839820e183a1e183a2e18390e183a2e183a3e183a1e18398, 'ka', 486),
(482, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e1839820e18395e18394e1839120e183a4e1839de183a0e1839be18398e183a120e183a8e18394e183a1e18390e183a1e18395e1839ae18394e1839ae18394e18391e18397e18390e1839c, 'ka', 487),
(483, 0xe18395e18394e1839120e183a4e1839de183a0e1839be18394e18391e18398, 'ka', 488),
(484, 0xe18395e18394e1839120e183a4e1839de183a0e1839be18394e18391e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 489),
(485, 0xe183afe18394e1839de18399e1839de18393e18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e18398e183a120e18392e18390e1839ce18390e183aee1839ae18394e18391e18398e183a120e183a1e18390e1839be183a1e18390e183aee183a3e183a0e18398, 'ka', 490),
(486, 0xe183a7e1839de18395e18394e1839a20333020e183ace183a3e18397e183a8e183982e, 'ka', 491),
(487, 0xe183a0e183a3e18399e18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 492),
(488, 0xe183a0e183a3e18399e18394e18391e18398e183a120e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183aae18398e18390e183a1e18397e18390e1839c20e183ace18395e18393e1839de1839be18390, 'ka', 493),
(489, 0xe183a0e183a3e18399e18394e18391e18398, 'ka', 494),
(490, 0xe183a0e183a3e18399e18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 495),
(491, 0xe183a0e183a3e18399e18390, 'ka', 496),
(492, 0x476f6f676c65202de18398e183a120e183a0e183a3e18399e18398e183a1204150492de18398e183a120e18392e18390e183a1e18390e183a6e18394e18391e18398, 'ka', 497),
(493, 0xe18398e1839be1839ee1839de183a0e183a2e18398, 'ka', 498),
(494, 0x457a636f647820e18395e18394e1839ae18398, 'ka', 499),
(495, 0xe183b0e18394e18393e18394e183a0e18398, 'ka', 500),
(496, 0xe18399e18390e1839ce1839de1839ce18394e18391e18398, 'ka', 501),
(497, 0xe18395e18394e1839ae18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 502),
(498, 0xe18392e18398e1839ae1839de183aae18390e18395e183972e20e18398e1839be1839ee1839de183a0e183a2e1839820e18393e18390e183a1e183a0e183a3e1839ae18393e183902e20e183a5e18395e18394e1839be1839de1839720e1839be1839de183aae18394e1839be183a3e1839ae18398e1839020e183a8e18394e18393e18394e18392e18398e183a120e183a8e18398e1839ce18390e18390e183a0e183a1e183982e, 'ka', 503),
(499, 0x457a636f647820e1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e183a1e18390e183aee18394e1839ae18398, 'ka', 504),
(500, 0xe18392e18397e183aee1839de18395e1839720e18390e18398e183a0e183a9e18398e1839de1839720e1839be1839de18393e183a3e1839ae183982c20e183a0e1839de1839be1839ae18398e183a120e18398e1839be1839ee1839de183a0e183a2e18398e183a120e18392e183a1e183a3e183a0e183973a, 'ka', 505),
(501, 0x7b6669727374446174657d20e18390e1839c207b7365636f6e64446174657d, 'ka', 506),
(502, 0xe183a1e18390e183ace183a7e18398e183a1e1839820e183a1e18390e183aee18394e1839ae18398, 'ka', 507),
(503, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e183a2e18398e1839ee18394e18391e18398, 'ka', 508),
(504, 0xe183a4e1839de183a0e1839be18390e183a2e18398, 'ka', 509),
(505, 0xe18397e18390e183a0e18398e183a6e18398e183a120e18390e183a0e18390e183a1e183ace1839de183a0e1839820e183a4e1839de183a0e1839be18390e183a2e183982e, 'ka', 510),
(506, 0xe1839be1839de1839ce18398e183a8e18395e1839ce18398e183a120e1839ee18390e1839ce18394e1839ae18398e183a120e18390e183a0e18390e183a1e183ace1839de183a0e1839820e183a4e1839de183a0e1839be18390e183a2e183982e, 'ka', 511),
(507, 0xe18398e1839be1839ee1839de183a1e183a2e18398e183a120e183a4e18390e18398e1839ae18394e18391e183a120e18390e183a020e18390e183a5e18395e183a120e183a0e18398e18392e18394e18391e1839820e18392e18390e1839be1839de183a1e18390e183a7e18394e1839ce18394e18391e1839ae18390e183932e, 'ka', 512),
(508, 0xe18390e183a020e18392e18390e183a5e18395e1839720e183a3e183a4e1839ae18394e18391e1839020e183a0e1839de1839b20e183a8e18394e183a5e1839be1839ce18390e183972fe18392e18390e1839ce18390e18390e183aee1839ae1839de1839720e18394e183a120e183a9e18390e1839ce18390e183ace18394e183a0e1839820e18393e183902fe18390e1839c20e1839be18398e183a1e1839820e1839be183a1e18392e18390e18395e183a1e1839820e183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e183982e, 'ka', 513),
(509, 0xe18398e1839be1839ee1839de183a0e183a2e18398e183a120e1839ee183a0e1839de183aae18394e183a1e18398e183a120e18393e18390e183ace183a7e18394e18391e183903a207b70726f636573734e616d657d, 'ka', 514),
(510, 0xe18398e1839be1839ee1839de183a0e183a2e18398e183a120e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183902e, 'ka', 515),
(511, 0xe183a0e18398e18392e18398e183a120e1839ce18398e1839be183a3e183a8e18398, 'ka', 516),
(512, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18398e1839be1839ee1839de183a0e183a2e183982e2e2e, 'ka', 517),
(513, 0xe18393e18390e183a1e183a0e183a3e1839ae18394e18391e183902e2e2e, 'ka', 518),
(514, 0xe183a9e18390e1839ce18390e183ace18394e183a028e18394e1839129e18398207b73746172744974656d436f756e747d202d207b656e644974656d436f756e747d206f66207b746f74616c4974656d436f756e747d, 'ka', 519),
(515, 0xe18395e18394e1839ae18398e183a120e18390e1839be1839de183a6e18394e18391e18390, 'ka', 520),
(516, 0x415049, 'ka', 521),
(517, 0x41504973, 'ka', 522),
(518, 0xe18397e183a5e18395e18394e1839c20e18390e183a020e18392e18390e183a5e1839720e183a3e183a4e1839ae18394e18391e1839020e18390e1839b20e1839be1839de183a5e1839be18394e18393e18394e18391e1839820e183a8e18394e18390e183a1e183a0e183a3e1839ae18394e18391e1839ae18390e18393, 'ka', 523),
(519, 0xe183a9e18394e1839be1839820e18392e18390e183aee183a1e1839ce18398e1839ae1839820e18393e18390e18395e18390e1839ae18394e18391e18394e18391e18398, 'ka', 524),
(520, 0xe18393e18390e18395e18390e1839ae18394e18391e18394e18391e18398, 'ka', 525),
(521, 0xe18393e18394e183a2e18390e1839ae183a3e183a0e1839820e18393e1839020e183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390e18393e1839820e183a0e18394e1839fe18398e1839be18398, 'ka', 526),
(522, 0xe183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18390, 'ka', 527),
(523, 0xe1839ee1839de1839ee18390e1839ee18398e183a120e183a1e18398e18390, 'ka', 528),
(524, 0xe183a1e18398e18398e183a120e183a0e18394e1839fe18398e1839be18398, 'ka', 529),
(525, 0xe18393e18394e183a2e18390e1839ae183a3e183a0e1839820e18391e1839ae1839de18399e18394e18391e18398e183a120e183a0e18394e1839fe18398e1839be18398, 'ka', 530),
(526, 0xe18391e1839ae1839de18399e18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 531),
(527, 0xe183abe18398e18394e18391e18390, 'ka', 532),
(528, 0xe1839be183a1e18392e18390e18395e183a1e1839820e183a1e18398e18390, 'ka', 533),
(529, 0xe18393e18394e183a2e18390e1839ae183a3e183a0e18390e18393, 'ka', 534),
(530, 0xe18395e18394e1839ae18398e183a120e183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e18398e183a120e183a0e18394e1839fe18398e1839be18398, 'ka', 535),
(531, 0xe183abe18398e18394e18391e1839020e1839ee1839de1839ee18390e1839ee18398e183a120e183a0e18394e1839fe18398e1839be183a8e18398, 'ka', 536),
(532, 0xe18393e18390e1839be18390e183a2e18394e18391e18398e18397e1839820e18395e18394e1839ae18394e18391e18398, 'ka', 537),
(533, 0xe183a1e183a2e18390e1839ce18393e18390e183a0e183a2e183a3e1839ae1839820e18395e18394e1839ae18394e18391e18398, 'ka', 538),
(534, 0xe18390e183a1e18390e183a0e183a9e18394e18395e1839820e183a1e18398e18398e183a120e1839be1839de1839ce18390e183aae18394e1839be18398, 'ka', 539),
(535, 0xe18393e18398e18396e18390e18398e1839ce18394e183a0e18398, 'ka', 540),
(536, 0xe18390e183a9e18395e18394e1839ce18394207b6c6162656c7d20e183a0e1839de18393e18394e183a1e18390e183aa, 'ka', 541),
(537, 0xe183afe18394e183a020e183a3e1839ce18393e1839020e183a8e18394e18390e183a0e183a9e18398e1839de1839720e18395e18394e1839ae18398e183a120e183a2e18398e1839ee18398, 'ka', 542),
(538, 0xe18395e18394e1839ae18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 543),
(539, 0xe18390e18398e183a0e183a9e18398e18394e1839720e18395e18394e1839ae18398e183a120e183a2e18398e1839ee18398, 'ka', 544),
(540, 0xe18395e18394e1839ae18398e183a120e183a2e18398e1839ee18398, 'ka', 545),
(541, 0xe18395e18394e1839ae18398e183a120e18393e18390e183a1e18390e183aee18394e1839ae18394e18391e18390, 'ka', 546),
(542, 0xe183a4e1839de183a0e1839be183a3e1839ae18398e183a120e18393e18390e183a1e18390e183aee18394e1839ae18394e18391e18390, 'ka', 547),
(543, 0xe183a7e18395e18394e1839ae1839020e1839ee18390e1839ce18394e1839ae1839820e183a8e18394e183a1e18390e1839be183a9e1839ce18394e18395e18398, 'ka', 548),
(544, 0xe1839ee18398e183a0e18395e18394e1839ae1839820e1839ee18390e1839ce18394e1839ae1839820e183aee18398e1839ae183a3e1839ae183982c20e183a8e18394e1839be18393e18394e183922022e18398e183aee18398e1839ae1839420e1839be18390e183a2e18398222de183a120e18391e1839be183a3e1839ae18398, 'ka', 549),
(545, 0xe1839ee18390e1839ce18394e1839ae18394e18391e1839820e18390e183a0e18398e183a120e183a9e18390e1839ce18390e183a0e18397e18394e18391e18398e1839720e18390e183a6e183ade183a3e183a0e18395e18398e1839ae18398, 'ka', 550),
(546, 0xe18395e18394e1839ae18394e18391e18398, 'ka', 551),
(547, 0xe18392e18390e1839ce1839ae18390e18392e18394e18391e18390, 'ka', 552),
(548, 0xe183ace18395e18393e1839de1839be1839020e18393e18398e18396e18390e18398e1839ce18394e183a0e18398e183a120e18398e1839ce183a1e183a2e183a0e183a3e1839be18394e1839ce183a2e18394e18391e18396e18394, 'ka', 553),
(549, 0xe1839be18390e183a0e18397e1839420e1839be1839de18393e183a3e1839ae18398e183a120e18395e18394e1839ae18394e18391e183982c20e18392e18390e1839ce1839ae18390e18392e18394e18391e1839020e18393e1839020e18398e18390e183a0e1839ae18398e183a7e18394e18391e18398, 'ka', 554),
(550, 0xe1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e18398e183a120e183a8e18394e183a0e183a9e18394e18395e18390, 'ka', 555),
(551, 0xe1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e18398e183a120e183a8e18394e183a0e183a9e18394e18395e18390, 'ka', 556),
(552, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e18398e183a120e18398e1839ce183a4e1839de183a0e1839be18390e183aae18398e18390, 'ka', 557),
(553, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e18398e183a120e18395e18394e1839ae18394e18391e18398, 'ka', 558),
(554, 0xe18397e18390e183a0e18398e183a6e18398e183a12fe18393e183a0e1839de18398e183a120e18395e18394e1839ae18398, 'ka', 559),
(555, 0xe183a2e18394e183a5e183a1e183a2e18398, 'ka', 560),
(556, 0xe183a2e18394e183a5e183a1e183a2e183a3e183a0e1839820e18395e18394e1839ae18398, 'ka', 561),
(557, 0xe183a1e18398e18396e183a3e183a1e183a2e18394, 'ka', 562),
(558, 0xe18390e18397e183ace18398e1839ae18390e18393e18398, 'ka', 563),
(559, 0xe18390e18397e183ace18398e1839ae18390e18393e18398e183a120e18395e18394e1839ae18398, 'ka', 564),
(560, 0xe183a4e1839de183a0e1839be183a3e1839ae18390, 'ka', 565),
(561, 0xe18392e18390e1839be1839de18397e18395e1839ae18398e1839ae1839820e183a0e18398e183aae183aee18395e18394e18391e18398, 'ka', 566),
(562, 0xe18392e18390e1839be1839de18397e18395e1839ae18398e1839ae1839820e183a0e18398e183aae183aee18395e18394e18391e1839820e18393e18390e1839be1839de18399e18398e18393e18394e18391e183a3e1839ae1839820e183a1e183aee18395e1839020e18395e18394e1839ae18394e18391e18398e183a120e183a6e18398e183a0e18394e18391e183a3e1839ae18394e18391e18390e18396e18394, 'ka', 567),
(563, 0xe183a4e1839de183a0e1839be183a3e1839ae1839020e18390e183a0e18390e183a1e183ace1839de183a0e18398e18390, 'ka', 568),
(564, 0xe18395e18394e1839ae1839820e18390e1839b20e183a1e18390e183aee18394e1839ae183ace1839de18393e18394e18391e18398e1839720e183a3e18399e18395e1839420e18392e18390e1839be1839de183a7e18394e1839ce18394e18391e183a3e1839ae18398e18390, 'ka', 569),
(565, 0xe1839be18397e18394e1839ae1839820e183a0e18398e183aae183aee18395e18398, 'ka', 570),
(566, 0xe1839be18397e18394e1839ae1839820e183a0e18398e183aae183aee18395e18398e183a120e18395e18394e1839ae18398, 'ka', 571),
(567, 0xe18395e18390e1839ae183a3e183a2e18398e183a120e18395e18394e1839ae18398, 'ka', 572),
(568, 0xe18395e18394e1839ae1839820e183a0e1839de1839be18394e1839ae18398e183aa20e183a8e18394e18398e183aae18390e18395e183a12055524c2de183a1, 'ka', 573),
(569, 0xe1839be1839de18397e183aee1839de18395e1839ce18390e18393e1839820e18395e18394e1839ae18398, 'ka', 574),
(570, 0x227b246174747269627574654e616d657d2220e18395e18394e1839ae18398e183a120e183a1e18390e183aee18394e1839ae183ace1839de18393e18394e18391e1839020e18390e183a0e18398e183a120e1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e183a1e18390e18397e18390e18393e18390e183a0e18398e18392e1839d20e183a1e18398e183a2e183a7e18395e183902e20e18392e18397e183aee1839de18395e1839720e183a8e18394e18398e183a7e18395e18390e1839ce1839de1839720e18392e18390e1839ce183a1e183aee18395e18390e18395e18394e18391e183a3e1839ae1839820e183a1e18398e183a2e183a7e18395e183902e, 'ka', 575),
(571, 0x227b246174747269627574654e616d657d2220e18395e18394e1839ae18398e183a120e183a1e18390e183aee18394e1839ae183ace1839de18393e18394e18391e1839020e183a8e18394e18398e183aae18390e18395e183a120e183a1e18390e18397e18390e18393e18390e183a0e18398e18392e1839d20e183a1e18398e1839be18391e1839de1839ae1839de18394e18391e183a12e2020e18390e1839c207b7265736572766564317d20e18390e1839c207b7265736572766564327d2e, 'ka', 576),
(572, 0xe183a2e18394e183a5e183a1e183a2e18398e1839020e183a1e18398e18395e183a0e183aae18394, 'ka', 577),
(573, 0xe18390e183a6e183ace18394e183a0e18398e183a120e18391e1839ae1839de18399e18398, 'ka', 578),
(574, 0xe183a2e18394e1839ae18394e183a4e1839de1839ce18398, 'ka', 579),
(575, 0xe183a2e18394e1839ae18394e183a4e1839de1839ce18398e183a120e18395e18394e1839ae18398, 'ka', 580),
(576, 0xe183a3e1839ce18393e1839020e183a8e18394e18390e183a0e183a9e18398e1839de1839720e183a1e183a3e1839a20e1839be183aae18398e183a0e18394203220e18390e183a1e18390e183a0e183a9e18394e18395e1839820e183a1e18398e18390, 'ka', 581),
(577, 0xe183a3e1839ce18393e1839020e183a8e18394e18390e183a0e183a9e18398e1839de1839720e1839be18390e183a5e183a1e18398e1839be183a3e1839b203420e18390e183a1e18390e183a0e183a9e18394e18395e1839820e183a1e18398e183902e, 'ka', 582),
(578, 0xe1839be18398e183a1e18390e1839be18390e183a0e18397e18398e183a120e18395e18394e1839ae18398, 'ka', 583),
(579, 0xe1839be18390e183a5e183a1e18398e1839be18390e1839ae183a3e183a0e1839820e1839be1839de1839ce18390e183aae18394e1839be18398, 'ka', 584),
(580, 0xe1839be18390e183a5e183a1e18398e1839be18390e1839ae183a3e183a0e1839820e1839be1839de1839ce18390e183aae18394e1839be18398, 'ka', 585),
(581, 0xe18397e18390e183a0e18398e183a6e18398, 'ka', 586),
(582, 0xe18397e18390e183a0e18398e183a6e18398e183a120e18395e18394e1839ae18398, 'ka', 587),
(583, 0xe183a1e183a3e1839a20e1839be183aae18398e183a0e1839420e18394e183a0e18397e1839820e18395e18394e1839ae1839820e1839be18390e18398e1839ce183aa20e183a3e1839ce18393e1839020e18392e183a5e1839de1839ce18393e18394e1839720e18392e18390e1839ce18397e18390e18395e183a1e18394e18391e183a3e1839ae183982c20e183a0e1839de1839b20e183a8e18394e18398e1839ce18390e183aee1839de1839720e18392e18390e1839ce1839ae18390e18392e18394e18391e183902e, 'ka', 588),
(584, 0xe183a1e183a3e1839a20e1839be183aae18398e183a0e1839420e18394e183a0e18397e1839820e1839ee18390e1839ce18394e1839ae1839820e1839be18390e18398e1839ce183aa20e183a3e1839ce18393e1839020e18392e183a5e1839de1839ce18393e18394e183972c20e183a0e1839de1839b20e183a8e18394e18398e1839ce18390e183aee1839de1839720e18392e18390e1839ce1839ae18390e18392e18394e18391e183902e, 'ka', 589),
(585, 0xe183a7e18395e18394e1839ae1839020e1839be1839de18397e183aee1839de18395e1839ce18390e18393e1839820e18395e18394e1839ae1839820e183a3e1839ce18393e1839020e18392e18390e1839ce18397e18390e18395e183a1e18393e18394e183a120e18392e18390e1839ce1839ae18390e18392e18394e18391e18390e183a8e18398, 'ka', 590),
(586, 0xe18393e1839de1839ce183943a20207b6e756d6265727d, 'ka', 591),
(587, 0xe1839ee18398e183a0e18395e18394e1839ae1839820e183a8e18394e183a0e183a9e18394e183a3e1839ae1839820e18393e1839de1839ce1839420207b6e756d6265727d, 'ka', 592),
(588, 0xe1839ee183a0e1839de18393e183a3e183a5e183aae18398e18394e18391e18398, 'ka', 593),
(589, 0xe1839ee183a0e1839de18393e183a3e183a5e183aae18398e18390, 'ka', 594),
(590, 0xe1839ee183a0e1839de18393e183a3e183a5e183aae18398e18398e183a120e18399e18390e183a2e18394e18392e1839de183a0e18398e18394e18391e18398, 'ka', 595),
(591, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398, 'ka', 596),
(592, 0xe183a0e1839020e183aee18393e18394e18391e1839020e1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a8e18398, 'ka', 597),
(593, 0xe183a1e183a0e183a3e1839ae1839820e183a1e18398e18398e183a120e1839ee18394e183a0e183a4e1839de1839be18390e1839ce183a1e18398, 'ka', 598),
(594, 0xe183a1e18398e18398e183a120e18396e183a0e18393e18390, 'ka', 599),
(595, 0xe183a0e1839de18392e1839de183a020e1839be183a3e183a8e18390e1839de18391e183a120e18398e1839be18394e18398e1839a20e1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e1839820457a636f64782de183a8e183983f, 'ka', 600),
(596, 0xe1839be18390e183a0e18399e18394e183a2e18398e183a1e18392e18398e183a120e183a1e18398e18398e183a120e18393e18390e183a4e18390, 'ka', 601),
(597, 0xe18393e18390e183a1e183a0e183a3e1839ae18394e18391e18398e183a120e18397e18390e183a0e18398e183a6e18398, 'ka', 602),
(598, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a3e1839ae1839820e183a2e18390e18391e18398e183a120e183ace18395e18393e1839de1839be18390, 'ka', 603),
(599, 0xe183a3e1839ce18398e18399e18390e1839ae183a3e183a0e1839820435452, 'ka', 604),
(600, 0x435452, 'ka', 605),
(601, 0xe183a3e1839ce18398e18399e18390e1839ae183a3e183a0e1839820e183a6e18398e1839020e183a8e18394e183a4e18390e183a1e18394e18391e18390, 'ka', 606),
(602, 0xe183a6e18398e1839020e183a8e18394e183a4e18390e183a1e18394e18391e18390, 'ka', 607),
(603, 0xe183a0e18398e18392e183a8e18398e18390, 'ka', 608),
(604, 0xe183a6e18398e18390e18390, 'ka', 609),
(605, 0xe18393e18390e18399e1839ae18398e18399e18394e18391e183a3e1839ae18398, 'ka', 610),
(606, 0xe18392e18390e1839be1839de183a2e1839de18395e18394e18391e183a3e1839ae18398, 'ka', 611),
(607, 0xe18390e183a0e183a1e18394e18391e183a3e1839ae1839820e18392e18390e1839be1839de1839be183ace18394e183a0e18394e18391e18398, 'ka', 612),
(608, 0xe18390e183aee18390e1839ae1839820e18392e18390e1839be1839de1839be183ace18394e183a0e18394e18391e18398, 'ka', 613),
(609, 0xe183a1e18390e183ade18398e183a0e1839d20e18391e1839be183a3e1839ae18394e18391e18398, 'ka', 614),
(610, 0xe183a8e18394e183a3e18394e183a0e18397e18393e18398e1839720e183a4e1839de183a0e183a3e1839be183a1, 'ka', 615),
(611, 0xe183ace18390e18398e18399e18398e18397e183aee18394e1839720e18395e18398e18399e18398, 'ka', 616),
(612, 0xe18398e183aee18398e1839ae18394e1839720e18398e1839ce183a1e183a2e183a0e183a3e183a5e183aae18398e18390, 'ka', 617),
(613, 0xe18398e183aee18398e1839ae18394e1839720e18395e18398e18393e18394e1839d, 'ka', 618),
(614, 0xe18399e18394e18397e18398e1839ae1839820e18398e183a7e1839de183a120e18397e183a5e18395e18394e1839ce1839820e1839be1839de18391e183a0e183abe18390e1839ce18394e18391e1839020457a636f64782de183a8e18398, 'ka', 619),
(615, 0x43524d2de18398e183a120e18392e18390e1839be1839de183a7e18394e1839ce18394e18391e1839020e18390e183a020e18390e183a0e18398e183a120e183a0e183a3e183a2e18398e1839ce183902e20457a636f64782de18398e183a120e1839be18394e183a8e18395e18394e1839de18391e18398e1839720e183a8e18394e18392e18398e183abe1839ae18398e18390e1839720e18392e18390e1839be1839de18398e1839be183a3e183a8e18390e1839de1839720e183a5e183a3e1839ae18394e18391e183982c20e183a8e18394e18390e18392e183a0e1839de18395e1839de1839720e18394e1839be18391e1839ae18394e1839be18394e18391e183982c20e183a8e18394e18394e183afe18398e18391e183a0e1839de1839720e18397e18390e1839ce18390e1839be183a8e183a0e1839de1839be1839ae18394e18391e183a120e18393e1839020e18397e18390e1839c20e18392e18390e18390e18399e18394e18397e1839de1839720e18397e183a5e18395e18394e1839ce1839820e183a1e18390e183a5e1839be183942e, 'ka', 620),
(616, 0xe18393e183a6e18398e183a120e1839be18398e18397e18398e18397e18394e18391e18390, 'ka', 621),
(617, 0xe183a8e18394e1839be18393e18394e18392e1839820e1839be18398e18397e18398e18397e18394e18391e18390, 'ka', 622),
(618, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18390e18396e1839420e18393e18390e18391e183a0e183a3e1839ce18394e18391e18390, 'ka', 623),
(619, 0xe18390e183a6e18390e183a020e1839be18390e183a9e18395e18394e1839ce1839de1839720e18394e183a120e183a4e18390e1839ce183afe18390e183a0e18390, 'ka', 624),
(620, 0xe1839ce183a320e183a6e18394e1839ae18390e18395e183972c20e183a8e18394e18392e18398e183abe1839ae18398e18390e1839720e18399e18398e18393e18394e1839520e183a9e18390e183a0e18397e1839de18397, 'ka', 625),
(621, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18398e183a120e18392e18390e183a1e183ace18395e183a0e18398e18395, 'ka', 626),
(622, 0xe18393e18390e18391e183a0e183a3e1839ce18394e18391e18390, 'ka', 155),
(623, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e18393e18390e183a4e18398e183a120e18392e18390e18393e18390e183a0e18397e18395e18390, 'ka', 627),
(624, 0xe183a8e18394e183a1e18390e183abe1839ae18394e18391e1839ae1839de18391e18390, 'ka', 628),
(625, 0xe183a8e18394e183a1e18390e183abe1839ae18394e18391e1839ae1839de18391e18394e18391e18398, 'ka', 629),
(626, 0xe18392e18390e183a7e18398e18393e18395e18394e18391e18398, 'ka', 630),
(627, 0xe18390e183aee18390e1839ae1839820e183a1e18390e183a5e1839be18394, 'ka', 631),
(628, 0xe18399e1839de1839be183a3e1839ce18398e18399e18390e183aae18398e18390, 'ka', 632),
(629, 0xe18390e1839ce18392e18390e183a0e18398e183a8e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 633),
(630, 0xe18393e183a0e1839de18398e183a120e1839be18394e1839ce18394e183afe1839be18394e1839ce183a2e18398, 'ka', 634),
(631, 0x7b6e7d20e1839be18398e183a1e18398e1839020e183a8e18394e18398e183a5e1839be1839ce183907c7b6e7d20e1839be18398e183a1e18398e18394e18391e1839820e183a8e18394e18398e183a5e1839be1839ce18390, 'ka', 635),
(632, 0xe183a9e18394e1839be1839820e1839be18398e183a1e18398e18390, 'ka', 636),
(633, 0xe1839be18398e183a1e18398e1839020e183a0e1839de1839be18394e1839ae18398e183aa20e183a8e18394e183a5e1839be18394e1839ce18398e1839720e18393e18390e183a1e183a0e183a3e1839ae18393e18390, 'ka', 637),
(634, 0xe1839be18398e183a1e18398e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 638),
(635, 0xe18399e1839de1839be18394e1839ce183a2e18390e183a0e18398, 'ka', 639),
(636, 0x7b6e7d20e18399e1839de1839be18394e1839ce183a2e18390e183a0e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e183907b6e7d20e18399e1839de1839be18394e1839ce183a2e18390e183a0e18394e18391e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 640),
(637, 0xe183abe18395e18394e1839ae1839820e18399e1839de1839be18394e1839ce183a2e18390e183a0e18394e18391e18398e183a120e1839ce18390e183aee18395e18390, 'ka', 641),
(638, 0xe18390e183aee18390e1839ae1839820e18399e1839de1839be18394e1839ce183a2e18390e183a0e18398207b6d6f64656c4e616d657d3a207b7375626a6563747d, 'ka', 642),
(639, 0xe1839be1839de18393e18394e1839ae1839820e18390e183a020e183a8e18394e183a5e1839be1839ce18398e1839ae18390, 'ka', 643),
(640, 0xe1839be18397e18390e18395e18390e183a0e18398, 'ka', 156),
(641, 0xe18390e183aee1839ae18390e183aee18390e1839c20e1839ce18390e1839ce18390e183aee18398, 'ka', 644),
(642, 0xe183afe18392e183a3e183a4e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 645),
(643, 0xe183a0e1839de1839ae18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 646),
(644, 0xe183a8e18394e183a5e1839be1839ce18390, 'ka', 647),
(645, 0xe18395e18390e1839ae183a3e183a2e18394e18391e183983a20e183a8e18394e183a5e1839be1839ce18390, 'ka', 648),
(646, 0xe18390e183a5e183a2e18398e183a3e183a0e1839820e18395e18390e1839ae183a3e183a2e18394e18391e1839820e183a8e18394e18398e183abe1839ae18394e18391e1839020e18392e18390e1839be1839de183a7e18394e1839ce18394e18391e183a3e18398e1839ae1839820e18398e183a5e1839ce18394e183a120e18390e183aee18390e1839ae1839820e183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18398e183a1e18390e183a120e18393e1839020e183a0e1839de18392e1839de183a0e183aa20e183a1e18390e183ace183a7e18398e183a1e1839820e18395e18390e1839ae183a3e183a2e1839020e1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a1e18397e18395e18398e183a1, 'ka', 649),
(647, 0xe183a8e18394e18398e183a5e1839be1839ce1839020e1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e1839be18398e18394e183a0, 'ka', 650),
(648, 0xe183afe18392e183a3e183a4e18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 651),
(649, 0xe183a0e1839de1839ae18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 652),
(650, 0xe18390e183a020e18390e183a0e18398e183a120e18390e183aee1839ae18390e183aee18390e1839c20e1839ce18390e1839ce18390e183aee1839820e18394e1839ae18394e1839be18394e1839ce183a2e18394e18391e183982e, 'ka', 653),
(651, 0x7b6e7d20e18394e1839a2de183a4e1839de183a1e183a2e18398e183a120e183a8e18390e18391e1839ae1839de1839ce1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e183907c7b6e7d20e18394e1839a2de183a4e1839de183a1e183a2e18398e183a120e183a8e18390e18391e1839ae1839de1839ce1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 654),
(652, 0xe18394e1839a2de183a4e1839de183a1e183a2e18398e183a120e183a8e18390e18391e1839ae1839de1839ce18394e18391e18398, 'ka', 655),
(653, 0xe183a8e18394e18394e183a0e18397e18394e18391e183a3e1839ae1839820e183a2e18394e18392e18394e18391e1839820e18398e183ace183a7e18394e18391e183903a207b7461675072656669787d20e18393e1839020e1839be18397e18390e18395e183a0e18393e18394e18391e18390207b7461675375666669787d2de18398e183972e, 'ka', 656),
(654, 0xe183a2e18390e18392e18394e18391e18398e183a120e183a9e18390e183a8e18395e18394e18391e1839020e18393e1839020e18393e18390e183aee183a3e183a0e18395e18390e183a120e183a8e1839de183a0e18398e183a12c20e183a8e18394e183a1e18390e183abe1839ae18394e18391e18394e1839ae18398e1839020e183a1e18390e183aee18394e1839ae1839820e18392e183a0e18390e183a4e18398e183a120e18390e183a0e183a1e18394e18391e1839de18391e183902e20e183a1e18390e183aee18394e1839ae1839820e183a3e1839ce18393e1839020e18393e18390e18398e183ace183a7e1839de183a120e18393e18398e18393e1839820e1839ae18390e18397e18398e1839ce183a3e183a0e1839820e18390e183a1e1839de18397e18398, 'ka', 657),
(655, 0xe183a1e18390e183a1e183a3e183a0e18395e18394e1839a20e18392e183a0e18390e183a4e18390e183a1e18397e18390e1839c20e18393e18390e183a1e18390e18399e18390e18395e183a8e18398e183a0e18394e18391e1839ae18390e1839320e18392e18390e1839be1839de18398e183a7e18394e1839ce18394e1839720e183a8e18394e1839be18393e18394e18392e1839820e1839ee183a0e18394e183a4e18398e183a5e183a1e1839820207b70726f706572747944656c696d697465727d, 'ka', 658),
(656, 0xe18398e1839be18394e18398e1839ae18398e183a120e183a8e18390e18391e1839ae1839de1839ce18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 659),
(657, 0xe18398e1839be18394e18398e1839ae18394e18391e1839820e183a1e18398e18390e183a8e18398, 'ka', 660),
(658, 0x3c68323ee18390e183a1e1839420e183a1e183ace183a0e18390e183a4e18390e1839320e18390e183a0e18390213c2f68323e3c64697620636c6173733d226c617267652d69636f6e223e3c2f6469763e3c703ee183ace18395e18393e1839de1839be1839020e183a3e18390e183a0e183a7e1839de183a4e18398e1839ae18398e1839020e183ace18398e1839ce18390e183a1e183ace18390e183a020e18392e18390e18393e18390e183a1e18398e1839ce183afe18395e18398e183a120e1839be18394e18397e1839de18393e18398e183a120e18392e18390e18390e183a5e183a2e18398e183a3e183a0e18394e18391e18398e183a120e18392e18390e1839be1839d2e3c2f703e, 'ka', 661),
(659, 0xe183a7e1839de18395e18394e1839a203520e183ace183a3e18397e183a8e18398, 'ka', 662),
(660, 0xe1839be1839de18391e183a0e183a3e1839ce18394e18391e183a3e1839ae1839820e183ace18394e183a0e18398e1839ae18398, 'ka', 663),
(661, 0xe1839be1839de18391e183a0e183a3e1839ce18394e18391e183a3e1839ae1839820e1839be18394e18398e1839ae18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 664),
(662, 0xe1839be18390e183a0e18397e1839420e18393e18390e18391e183a0e183a3e1839ce18394e18391e183a3e1839ae1839820e1839be18394e18398e1839ae18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 665),
(663, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a3e1839ae183982020e18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e1839ee18394e183a0e183a4e1839de1839be18390e1839ce183a1e18398e183a120e183a8e18394e183a4e18390e183a1e18394e18391e18390, 'ka', 666),
(664, 0xe18398e1839be18394e18398e1839ae18394e18391e1839820e18390e1839b2020e18399e18390e1839be1839ee18390e1839ce18398e18390e183a8e18398, 'ka', 667),
(665, 0xe183a8e18394e1839ce18390e183aee18395e1839020e18393e1839020e18392e18390e1839ce183a0e18398e18392e18398, 'ka', 668),
(666, 0xe183a8e18394e18396e183a6e183a3e18393e183a3e1839ae1839820e183ace18395e18393e1839de1839be18398e183a120e18392e18390e1839be1839d2c20e18397e183a5e18395e18394e1839c20e18390e183a020e183a8e18394e18392e18398e183abe1839ae18398e18390e1839720e18390e1839b20e18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e1839ce18390e183aee18395e183902e, 'ka', 669),
(667, 0xe18392e18390e18392e18396e18390e18395e1839ce1839020e18395e18394e183a020e18392e18390e1839ce183aee1839de183a0e183aae18398e18394e1839ae18393e18390, 'ka', 670),
(668, 0xe18393e18390e18391e183a0e183a3e1839ce18394e18391e183a3e1839ae1839820e1839be18394e18398e1839ae18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 671),
(669, 0x7b7175616e746974797d20e18390e183a0e18390e18392e1839de1839be1839de183ace18394e183a0e18398e1839ae18394e18391e1839820287b756e73756273637269626564526174657d2529, 'ka', 672),
(670, 0xe183a1e183a2e18398e1839ae18394e18391e18398e183a120e18392e18390e1839be1839de183a0e18398e183aae183aee18395e18390, 'ka', 673),
(671, 0xe18395e18394e1839120e183a4e1839de183a0e1839be1839020e183a1e18390e183aee18394e1839ae183982d32, 'ka', 674),
(672, 0xe18395e18394e1839120e183a4e1839de183a0e1839be18398e183a120e183a1e18390e183aee18394e1839ae18398, 'ka', 675),
(673, 0xe18395e18394e1839ae183982c20e18390e183a1e18394e18397e1839820e183a1e18390e183aee18394e1839ae183ace1839de18393e18394e18391e18398e18397e1839020e18393e1839020e1839be1839de1839ce18390e183aae18394e1839be18394e18391e18398e1839720e183a3e18399e18395e1839420e18392e18390e1839be1839de183a7e18394e1839ce18394e18391e18398e1839ae18398e1839020e183a1e183aee18395e1839020e1839be1839de18393e183a3e1839ae183a8e18398, 'ka', 676),
(674, 0xe183a8e18394e183a1e183a0e183a3e1839ae18394e18391e18398e183a120e183a8e18394e183a4e18390e183a1e18394e18391e18390, 'ka', 677),
(675, 0xe18398e1839be18394e18398e1839ae18394e18391e18398e183a120e1839ee18394e183a0e183a4e1839de183a0e1839be18390e1839ce183a1e18398e183a120e183a8e18394e183a4e18390e183a1e18394e18391e18390, 'ka', 678),
(676, 0xe183a1e18390e183aee18394e1839ae18394e18391e1839820e18398e183a5e1839ce18394e18391e1839020e183a1e18390e183afe18390e183a0e1839d20e18399e1839de1839ce183a2e18390e183a5e183a2e18394e18391e18398e183a1e18397e18395e18398e183a12fe183aee18394e1839ae1839be183abe183a6e18395e18390e1839ce18394e1839ae18394e18391e18398e183a1e18397e18395e18398e183a12c20e183a0e18390e18397e1839020e1839be18390e183a0e18397e1839de1839c20e18392e18390e1839be1839de183ace18394e183a0e18394e18391e18398, 'ka', 679),
(677, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 680),
(678, 0xe1839ee183a0e1839de183aae18394e183a1e183983a207b70657263656e74616765436f6d706c6574657d20e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae18398e18390, 'ka', 681),
(679, 0xe183ace18390e183a0e183a3e1839be18390e183a2e18394e18391e1839ae18390e1839320e18392e18390e18392e18396e18390e18395e1839ce18398e1839ae1839820e1839be18394e18398e1839ae18398e183a120e1839ce183a3e1839be18394e183a0e18390e183aae18398e18390, 'ka', 682),
(680, 0xe183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e183a8e18394e1839ce18390e183aee183a3e1839ae1839820e1839be18394e18398e1839ae18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 683),
(681, 0xe18398e1839be18394e18398e1839ae18398e183a120e18393e18390e18390e183a0e183a5e18398e18395e18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e1839020e183a8e18394e1839ce18390e183aee183a3e1839ae18398e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e18397, 'ka', 684),
(682, 0xe18393e18390e18391e183a0e183a3e1839ce18394e18391e183a3e1839ae1839820e1839be18394e18398e1839ae18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e1839020e183a8e18394e1839ce18390e183aee183a3e1839ae18398e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e18397, 'ka', 685),
(683, 0xe183a1e18390e1839ce18390e1839b20e183a8e18394e183abe1839ae18394e18391e1839720e18392e18390e18390e18392e18396e18390e18395e1839ce1839de1839720e183a1e18390e183a2e18394e183a1e183a2e1839d20e18398e1839be18394e18398e1839ae1839820e183a3e1839ce18393e1839020e183a8e18394e18398e183a7e18395e18390e1839ce1839de1839720e183a1e18390e183a2e18394e183a1e183a2e1839d20e18398e1839be18394e18398e1839ae1839820e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e183982e, 'ka', 686),
(684, 0xe18395e18394e183a020e183a3e18399e18390e18395e183a8e18398e183a0e18393e18394e18391e1839020494d415020e183a1e18394e183a0e18395e18394e183a0e183a12e, 'ka', 687),
(685, 0xe183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e18393e18390e183a3e18399e18390e18395e183a8e18398e183a0e18393e1839020494d415020e183a1e18394e183a0e18395e18394e183a0e183a12e, 'ka', 688),
(686, 0xe18390e183a020e18394e1839be18397e183aee18395e18394e18395e1839020e18393e18390e18390e183a0e183a5e18398e18395e18394e18391e183a3e1839a20e18398e1839be18394e18398e1839ae18394e18391e183a1, 'ka', 689);
INSERT INTO `messagetranslation` (`id`, `translation`, `language`, `messagesource_id`) VALUES
(687, 0xe183a8e18394e18396e183a6e183a3e18393e183a3e1839ae1839820e183ace18395e18393e1839de1839be18398e183a120e18392e18390e1839be1839d2c20e18397e183a5e18395e18394e1839c20e18390e183a020e183a8e18394e18392e18398e183abe1839ae18398e18390e1839720e183a8e18394e183a1e183a0e183a3e1839ae18394e18391e18398e183a120e1839be18390e183a9e18395e18394e1839ce18394e18391e1839ae18398e183a120e1839ce18390e183aee18395e183902e, 'ka', 690),
(688, 0xe18393e18390e18392e18394e18392e1839be18398e1839ae1839820e183a1e18390e1839be183a3e183a8e18390e1839de18394e18391e18398e183a120e183a8e18394e183a5e1839be1839ce18390e1839be18393e183942c20e18399e18390e1839be1839ee18390e1839ce18398e1839020e18390e183a020e1839be1839de183a5e1839be18394e18393e18394e18391e183a12e20e1839be18398e1839be18390e183a0e18397e18394e1839720e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183a2e1839de183a0e183a12e, 'ka', 691),
(689, 0xe1839ce18390e1839be18393e18395e18398e1839ae18390e1839320e18392e18398e1839ce18393e18390e1839720e18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18398e183a120e183ace18390e183a8e1839ae183903f, 'ka', 692),
(690, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee1839820e18390e183a020e18398e1839be183a3e183a8e18390e18395e18394e18391e183a120e1839be18390e1839ce18390e1839b20e183a1e18390e1839ce18390e1839b20e18390e183a020e1839be18398e18398e183a6e18394e18391e183a120e1839be18398e18397e18398e18397e18394e18391e18390e183a12e20e18393e18390e183a3e18399e18390e18395e183a8e183a0e18393e18398e1839720e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183a2e1839de183a0e183a12e, 'ka', 693),
(691, 0xe18395e18394e1839120e183a4e1839de183a0e1839be18398e183a120e18399e1839de1839ce183a2e18390e183a5e183a2e18398, 'ka', 694),
(692, 0xe18399e1839de1839ce183a2e18390e183a5e183a2e183a1e1839820e18395e18394e1839120e183a4e1839de183a0e1839be18398e183a120e183a8e18394e183a1e18390e183a1e18395e1839ae18394e1839ae18397e18390e1839c2e, 'ka', 695),
(693, 0xe183a0e183a3e18399e18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18398e183a120e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e183a8e18394e1839ce18390e183aee18395e18390, 'ka', 696),
(694, 0xe18398e1839be1839ee1839de183a0e183a2e1839820e183a8e18394e183a1e18390e1839ce18390e183aee18390e1839320e18390e183a020e18392e18390e1839ce183aee1839de183a0e183aae18398e18394e1839ae18393e183902e, 'ka', 697),
(695, 0xe183aee18394e1839ae1839be18398e183a1e18390e183ace18395e18393e1839de1839be1839820e1839be1839de18393e183a3e1839ae18394e18391e18398, 'ka', 698),
(696, 0xe1839ee18390e1839ce18394e1839ae18398e183a120e18390e183a0e18390e183a1e183ace1839de183a0e1839820e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18398e183a120e183a2e18398e1839ee18398, 'ka', 699),
(697, 0xe1839be18398e183a1e18398e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 700),
(698, 0xe183a0e1839de18393e18394e183a1e18390e183aa20e1839be1839de1839ce18398e183a8e1839ce183a3e1839ae18398e183902c20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e1839020e18398e18392e18396e18390e18395e1839ce18394e18391e1839020e183a2e18394e183a5e183a1e183a2e183a3e183a0e18390e1839320e18393e183902048544d4c20e183a4e1839de183a0e1839be18390e183a2e183a8e183982e20e1839be183aee1839de1839ae1839de1839320e183a2e18394e183a5e183a1e183a2e183a3e183a0e1839820e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e183a120e18392e18390e183a1e18390e18392e18396e18390e18395e1839ce18390e183932c20e18392e18390e18390e183a3e183a5e1839be18394e1839720e1839be1839de1839ce18398e183a8e18395e1839ce183902e, 'ka', 701),
(699, 0xe183a8e18394e1839ce18390e183aee18395e18390, 'ka', 702),
(700, 0xe18399e1839de1839be18394e1839ce183a2e18390e183a0e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 703),
(701, 0xe18392e18390e18393e18390e1839be183a3e183a8e18390e18395e18394e18391e183a3e1839ae18398, 'ka', 704),
(702, 0xe183aae18390e183a0e18398e18394e1839ae18398, 'ka', 705),
(703, 0xe18399e18395e18390e1839ae18398e183a4e18398e183aae18398e183a3e183a0e18398, 'ka', 706),
(704, 0xe1839be18394e1839de183a0e18390e18393e1839820e1839be18398e183a1e18390e1839be18390e183a0e18397e18398, 'ka', 707),
(705, 0xe18394e1839be18397e183aee18395e18394e18395e1839020e18393e18390e18390e183a0e183a5e18398e18395e18394e18391e183a3e1839a20e18398e1839be18394e18398e1839ae18394e18391e183a1, 'ka', 708),
(706, 0xe183a8e18394e183a0e183a9e18394e183a3e1839ae1839820e18395e18394e1839ae18394e18391e18398, 'ka', 709),
(707, 0xe183aee18394e1839ae1839be18398e183a1e18390e183ace18395e18393e1839de1839be1839820e18395e18394e1839ae18394e18391e18398, 'ka', 710),
(708, 0xe183a1e183a5e18394e1839be18398e183a120e183a4e1839de183a0e1839be18390, 'ka', 711),
(709, 0xe183a8e18394e18398e183a5e1839be1839ce18390, 'ka', 712),
(710, 0xe18392e18390e18390e18392e18396e18390e18395e1839ce18394e1839720e1839be18390e183a8e18398e1839ce18395e183942c20e183a0e18390e183aa2028e1839de1839ee18394e183a0e18390e183aae18398e1839029, 'ka', 713),
(711, 0xe18392e18390e18392e18396e18390e18395e1839ce183902028e18398e1839ce183a2e18394e183a0e18395e18390e1839ae18398292028e183a2e18398e1839ee183982920e183a8e18394e1839be18393e18394e183922028e1839de1839ee18394e183a0e18390e183aae18398e1839029, 'ka', 714),
(712, 0xe1839be1839de18393e18394e1839ae18394e18391e18398e183a120e18393e18390e1839be183a3e183a8e18390e18395e18394e18391e183902e20e1839be18394e183aee183a1e18398e18394e183a0e18394e18391e18398e183a120e1839be1839de18393e18394e1839ae18398e183a120e183a6e183a0e18394e18391e183a3e1839ae18394e18391e183902e, 'ka', 715),
(713, 0xe183a1e18390e18391e1839de1839ae1839de1839d20e1839be18394e183aee183a1e18398e18394e183a0e18394e18391e18398e183a120e1839be18390e183a9e18395e18394e1839ce18394e18391e18394e1839ae183982e, 'ka', 716),
(714, 0xe18399e18390e1839be1839ee18390e1839ce18398e1839020e1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e183a3e1839ae1839820e183a1e18398e18398e183a1e18397e18395e18398e183a12e, 'ka', 717),
(715, 0xe18390e183a0e18390e183a1e183ace1839de183a0e18398e183902e, 'ka', 718),
(716, 0xe183a1e183a2e18390e183a2e183a3e183a1e1839820e18390e183a0e18390e183a1e183ace1839de183a0e18398e1839020e18393e1839020e18390e183a020e18390e183a0e18398e183a120e183a3e1839ce18398e18399e18390e1839ae183a3e183a0e183982e, 'ka', 719),
(717, 0xe18392e18390e1839ce183a1e18390e18396e183a6e18395e183a0e183a3e1839ae1839820e183a1e183a2e18390e183a2e183a3e183a1e1839820e18390e183a020e18390e183a0e183a1e18394e18391e1839de18391e183a12e, 'ka', 720),
(718, 0xe183a4e1839de183a0e1839be183a3e1839ae18390e183a1e18397e18395e18398e183a120e183aee18394e1839ae1839be18398e183a1e18390e183ace18395e18393e1839de1839be1839820e18395e18394e1839ae18394e18391e18398, 'ka', 721),
(719, 0xe183a8e18394e18398e183abe1839ae18394e18391e1839020e18392e18390e1839be1839de183a7e18394e1839ce18394e18391e183a3e1839ae1839820e18398e183a5e1839ce18394e183a120e1839be18390e18397e18394e1839be18390e183a2e18398e18399e183a3e183a0e1839820e18392e18390e1839be1839de183a1e18390e183aee183a3e1839ae18394e18391e18398e183a1e18397e18395e18398e183a1, 'ka', 722),
(720, 0xe18390e1839b20e1839be1839de18393e183a3e1839ae183a8e1839820e18390e183a020e18390e183a0e18398e183a120e18395e18394e1839ae1839820e183a0e1839de1839b20e18392e18390e1839be1839de18398e183a7e18394e1839ce18394e18391e1839de18393e18394e183a120e18392e18390e1839be1839de183a1e18390e183aee183a3e1839ae18394e18391e18398e183a1e18397e18395e18398e183a1, 'ka', 723),
(721, 0x7b6e756d6265727d20e183a1e18395e18394e183a2e18398, 'ka', 724),
(722, 0x7b6e756d6265727d20e183a1e18395e18394e183a2e18394e18391e18398, 'ka', 725),
(723, 0xe18390e1839ce18390e1839ae18398e18396e18398, 'ka', 726),
(724, 0xe183a1e18390e18391e1839de1839ae1839de1839d20e18390e1839ce18390e1839ae18398e18396e183982e2e2e, 'ka', 727),
(725, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e1839020e18390e183a0e18390e183a1e183ace1839de183a0e18398e18390, 'ka', 728),
(726, 0xe183abe18390e1839ae18398e18390e1839c20e18392e183a0e183abe18394e1839ae18398e183902e20e1839be18390e183a5e183a1e18398e1839be18390e1839ae183a3e183a0e1839820e183a1e18398e18392e183a0e183abe18394e1839020207b6d6178696d756d4c656e6774687d2e, 'ka', 729),
(727, 0xe18390e183a0e183a1e18394e18391e183a3e1839a20e183a9e18390e1839ce18390e183ace18394e183a0e18398e1839020e18393e1839020e18392e18390e1839ce18390e183aee1839ae18393e18394e18391e183902e, 'ka', 730),
(728, 0xe183abe18390e1839ae18398e18390e1839c20e18392e183a0e183abe18394e1839ae18398e183902e, 'ka', 731),
(729, 0xe183abe18390e1839ae18398e18390e1839c20e1839be1839de18399e1839ae18394e183902e, 'ka', 732),
(730, 0xe183a3e1839ce18393e1839020e183a8e18394e18398e183aae18390e18395e18393e18394e183a120e18392e18395e18390e183a0e183a12c20e183a0e1839de1839be18394e1839ae18398e183aa20e18390e183a0e18398e183a120e1839be1839de18397e183aee1839de18395e1839ce18390e18393e183982e, 'ka', 733),
(731, 0xe183abe18390e1839ae18398e18390e1839c20e1839be1839de18399e1839ae18394e183902e20e1839be18398e1839ce18398e1839be18390e1839ae183a3e183a0e1839820e183a1e18398e18392e183a0e183abe1839420e18390e183a0e18398e183a1207b6d696e696d756d4c656e6774687d2e, 'ka', 734),
(732, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e183a1e18398e18393e18398e18393e1839420e18390e183a6e18394e1839be18390e183a2e18394e18391e1839020e18393e18390e183a8e18395e18394e18391e183a3e1839ae183a12e, 'ka', 735),
(733, 0xe18395e18394e183a020e18398e183a5e1839ce1839020e1839ce18390e1839ee1839de18395e1839ce1839820e18393e1839020e183a8e18394e18398e183a5e1839be1839ce18394e18391e1839020e18390e183aee18390e1839ae1839820e183a9e18390e1839ce18390e183ace18394e183a0e1839820e18398e1839be1839ee1839de183a0e183a2e18398e183a120e18393e183a0e1839de183a12e, 'ka', 736),
(734, 0xe1839be1839de18397e183aee1839de18395e1839ce18390e18393e18398e183902e, 'ka', 737),
(735, 0xe1839be1839de1839ce18390e183aae18394e1839be1839820e18393e18398e18393e1839820e1839de18393e18394e1839ce1839de18391e18398e183a1e18390e183902e20e1839be18390e183a5e183a1e18398e1839be18390e1839ae183a3e183a0e1839820e183a1e18398e18392e183a0e183abe1839420e18390e183a0e18398e183a120207b6d6178696d756d4c656e6774687d2e, 'ka', 738),
(736, 0xe183abe18390e1839ae18398e18390e1839c20e18392e183a0e183abe18394e1839ae18398e183902e20e1839be18390e183a5e183a1e18398e1839be18390e1839ae183a3e183a0e1839820e183a1e18398e18392e183a0e183abe18394e1839020207b6d6178696d756d4c656e6774687d2e, 'ka', 739),
(737, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120496420e18390e183a020e18394e1839be18397e183aee18395e18394e18395e1839020e18390e183a0e183a1e18394e18391e183a3e1839a20e183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e183a12e, 'ka', 740),
(738, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e183a1e18390e183aee18394e1839ae1839820e18390e183a020e18394e1839be18397e183aee18395e18394e18395e1839020e183a0e1839de1839be18394e1839ae18398e1839be1839420e18390e183a0e183a1e18394e18391e183a3e1839a20e183a9e18390e1839ce18390e183ace18394e183a0e183a12e, 'ka', 741),
(739, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18390e1839ce18390e1839ae18398e18396e18398, 'ka', 742),
(740, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18398e1839be1839ee1839de183a0e183a2e18398, 'ka', 743),
(741, 0xe183a1e18395e18394e183a2e18398207b6e7d, 'ka', 744),
(742, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18390e1839ce18390e1839ae18398e18396e1839820e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae18398e183902e20e18392e18397e183aee1839de18395e1839720e18392e18390e18393e18390e183aee18394e18393e1839de1839720e183a8e18394e18393e18394e18392e18394e18391e183a120e183a5e18395e18394e1839be1839de183972e20e183a0e1839de183aae1839020e1839be18396e18390e1839320e18398e183a5e1839ce18394e18391e18398e1839720e18393e18390e18390e18399e1839ae18398e18399e18394e183972022e183a8e18394e1839be18393e18394e18392e183a12220e1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18398e1839be1839ee1839de183a0e183a2e18398e183a1e18397e18395e18398e183a12e, 'ka', 745),
(743, 0x476f6f676c652de18398e183a120e183a0e183a3e18399e18390, 'ka', 746),
(744, 0x7b736b6970706564436f756e747d20e18392e18390e1839be1839de183a2e1839de18395e18394e18391e183a3e1839ae18398e1839020e183a3e18399e18395e1839420e183a1e18398e18398e18393e18390e1839c2e, 'ka', 747),
(745, 0x7b636f6e74616374537472696e677d20e183a3e18399e18395e1839420e18392e18390e1839be1839de183ace18394e183a0e18398e1839ae18398e18390207b6d6f64656c537472696e677d2de183a8e18398, 'ka', 748),
(746, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e1839020436f6e74616374734d6f64756c6553696e67756c61724c6162656c2f4c656164734d6f64756c6553696e67756c61724c6162656c2de18397e18395e18398e183a1, 'ka', 749),
(747, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e183a1e18398e18398e183a120e183abe18394e18391e1839ce18390, 'ka', 750),
(748, 0xe183a1e183a2e18390e183a2e183a3e183a1e1839820e18390e183a0e18390e183a1e183ace1839de183a0e18398e183902e20e183a1e183a2e18390e183a2e183a3e183a1e1839820e1839be18398e18394e18397e18398e18397e18394e18391e1839020e18398e1839be1839ee1839de183a0e183a2e18398e183a120e18390e183a5e183a2e18398e18395e18390e183aae18398e18398e183a1e18390e183a12e, 'ka', 751),
(749, 0xe18390e183a0e183a1e18394e18391e183a3e1839ae1839820e183ace18390e183a1e18390e18399e18398e18397e183aee1839820e18393e18390e183a4e18398e183a120e18392e18390e1839be1839de183a2e1839de18395e18394e18391e183902e, 'ka', 752),
(750, 0xe18390e183a0e183a1e18394e18391e183a3e1839ae1839820e183ace18390e183a1e18390e18399e18398e18397e183aee1839820e1839be18390e18392e18398e18393e18394e18391e18398e183a120e18392e18390e18393e18390e183ace18394e183a0e183902e, 'ka', 753),
(751, 0xe1839be1839de1839ce18398e183a8e1839ce183a3e1839ae1839820e18390e1839ce18392e18390e183a0e18398, 'ka', 754),
(752, 0xe1839be1839de1839ce18398e183a8e1839ce183a3e1839ae1839820e18390e1839ce18392e18390e183a0e18398e183a8e18394e18391e18398, 'ka', 755),
(753, 0xe18390e18395e183a2e1839de1839be1839de1839ee18390e183a1e183a3e183aee18394e18394e18391e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae1839820e18390e183a020e18390e183a0e18398e183a12e, 'ka', 756),
(754, 0xe1839ee18398e183a0e18395e18394e1839ae18390e18393e1839820e18398e1839be18394e18398e1839ae18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e1839820e18390e183a0e18398e183a120e18390e1839be1839de183a0e183a9e18394e183a3e1839ae183982e20e1839be18394e1839de183a0e18390e18393e1839820e18398e1839be18394e18398e1839ae18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e1839820e18390e183a020e18390e183a0e18398e183a120e18390e1839be1839de183a0e183a9e18394e183a3e1839ae183982c20e1839be18390e18392e183a0e18390e1839b20e1839ee18398e183a0e18395e18394e1839ae18390e18393e1839820e18398e1839be18394e18398e183a120e1839be18398e183a1e18390e1839be18390e183a0e18397e18398e1839720e18390e183a0e18398e183a120e1839be18394e18398e1839ae18398e183a120e18392e18390e18392e18396e18390e18395e1839ce1839020e183a8e18394e183a1e18390e183abe1839ae18394e18391e18394e1839ae183982e, 'ka', 757),
(755, 0xe18397e18390e183a0e18398e183a6e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 758),
(756, 0xe183a0e18390e1839de18393e18394e1839ce1839de18391e18390, 'ka', 759),
(757, 0x49502de18398e183a120e183a3e18399e18390e1839ce18390e183a1e18399e1839ce18394e1839ae1839820e183ace183a7e18390e183a0e1839d, 'ka', 760),
(758, 0xe18399e18390e1839ee18390e1839ce18398e1839020e18393e18390e183ace183a7e18394e18391e183a3e1839ae18398e183902e20e183a8e18394e183a1e18390e183abe1839ae18394e18391e18394e1839ae18398e1839020e1839be183aee1839de1839ae1839de1839320e183a1e18390e183aee18394e1839ae18398e183a12c20e183ace18394e183a1e18394e18391e18398e183a120e18393e1839020e183a3e183a4e1839ae18394e18391e18394e18391e18394e18391e18398e183a120e183a0e18394e18393e18390e183a5e183a2e18398e183a0e18394e18391e183902e, 'ka', 761),
(759, 0xe18392e18390e1839be18390e18395e18390e1839ae1839820e183a4e1839de183a1e183a2e18398e183a120e1839ee18390e183a0e18390e1839be18394e183a2e183a0e18394e18391e1839820e18390e183a020e1839be183a3e183a8e18390e1839de18391e183a120e18392e18390e1839be18390e183a0e18397e183a3e1839ae18390e183932e20e18393e18390e183a3e18399e18390e18395e183a8e18398e183a0e18393e18398e1839720e18390e18393e1839be18398e1839ce18398e183a2e183a0e18390e183a2e1839de183a0e183a12e, 'ka', 762),
(760, 0xe18397e183a5e18395e18394e1839c20e18398e183a0e183a9e18394e18395e183972048544d4c2de18398e183a120e1839be183aee18390e183a0e18393e18390e183ade18394e183a0e18390e183a120e1839ce18394e18391e18398e183a1e1839be18398e18394e183a0e183982048544d4c20e183a8e18398e1839ce18390e18390e183a0e183a1e18398e183a1e18397e18395e18398e183a12e, 'ka', 763),
(761, 0xe18397e183a5e18395e18394e1839c20e18390e183a020e18398e183a0e183a9e18394e18395e183972048544d4c2de18398e183a120e1839be183aee18390e183a0e18393e18390e183ade18394e183a0e18390e183a12c20e1839be18390e18392e183a0e18390e1839b20e18390e183a020e183a3e18397e18398e18397e18394e18391e1839720e1839ce18394e18391e18398e183a1e1839be18398e18394e183a0e1839820e183a2e18394e183a5e183a1e183a2e18398e183a120e183a8e18398e1839ce18390e18390e183a0e183a1e183a12e, 'ka', 764),
(762, 0xe18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183aae18398e1839020e1839be18397e18390e18395e18390e183a0e18398, 'ka', 765),
(763, 0xe18397e183a5e18395e18394e1839c20e183a3e1839ce18393e1839020e1839be18398e183a3e18397e18398e18397e1839de18397, 'ka', 766),
(764, 0xe1839be1839de1839ce18398e183a8e1839ce183a3e1839ae1839820e18399e1839de1839ce183a2e18390e183a5e183a2e18398, 'ka', 767),
(765, 0xe1839be1839de1839ce18398e183a8e1839ce183a3e1839ae1839820e18399e1839de1839ce183a2e18390e183a5e183a2e18394e18391e18398, 'ka', 768),
(766, 0xe1839be1839de18397e183aee1839de18395e1839ce18398e1839ae18398e183903f, 'ka', 769),
(767, 0xe18393e18390e1839be18390e1839ae183a3e1839ae18398e183903f, 'ka', 770),
(768, 0xe18394e183a2e18398e18399e18394e183a2e18398, 'ka', 771),
(769, 0xe1839be1839de1839ce18398e183a8e1839ce183a3e1839ae1839820e18393e18398e18390e1839ae1839de18392e18398, 'ka', 772),
(770, 0xe1839be1839de1839ce18398e183a8e1839ce183a3e1839ae1839820e18393e18398e18390e1839ae1839de18392e18394e18391e18398, 'ka', 773),
(771, 0xe1839be18390e183a1e1839de18391e183a0e18398e18395e1839820e18392e18390e1839ce18390e183aee1839ae18394e18391e18398e183a120e183a0e18394e1839fe18398e1839be18398, 'ka', 774),
(772, 0xe18390e183a1e18390e183a0e183a9e18394e18395e1839820e183a1e18398e1839020e18393e18390e1839be1839de18399e18398e18393e18394e18391e183a3e1839ae18398e1839020e183a0e183a3e18399e18398e183a120e183a8e18394e18393e18392e18394e1839ce18390e18396e18394, 'ka', 775),
(773, 0xe18395e18394e183aae18390e18393e18394e1839720e18392e18390e18392e18395e18394e18392e18396e18390e18395e1839ce1839020e18394e183a120e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18390207b636f756e747d20e183afe18394e183a020287b6572726f727d292e, 'ka', 776),
(774, 0xe183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e1839020e183a0e18398e18392e183a8e18398e1839020e18392e18390e183a1e18390e18392e18396e18390e18395e1839ce18390e183932e, 'ka', 777),
(775, 0xe1839be18394e183a1e18398e183afe18398e183a120e1839be183a4e1839ae1839de18391e18394e1839ae1839820e18390e183a020e18390e183a0e183a1e18394e18391e1839de18391e183a1, 'ka', 778),
(776, 0xe18392e18390e1839ce1839ae18390e18392e18394e18391e18398e183a1204964, 'ka', 779),
(777, 0xe18392e18390e1839ce1839ae18390e18392e18394e18391e18398e183a120e183a2e18398e1839ee18398, 'ka', 780),
(778, 0xe18390e183a0e18398e183a120e183a1e18390e183ace183a7e18398e183a1e18398, 'ka', 781),
(779, 0xe183a4e18390e18398e1839ae183a120e18390e183a5e18395e183a120e183abe18390e1839ae18398e18390e1839c20e18391e18394e18395e183a0e1839820e183a1e18395e18394e183a2e183982c20e1839be18390e183a5e183a1e18398e1839be183a3e1839be1839820e18390e183a0e18398e183a120313030, 'ka', 782),
(780, 0x7b6d6f64656c4c6162656c7d20e183a8e18394e18398e1839ce18390e183aee1839020e183a1e183ace1839de183a0e18390e183933a207b6c696e6b546f4d6f64656c7d, 'ka', 783),
(781, 0x7b6d6f64656c4c6162656c7d20494420e18390e183a020e18394e1839be18397e183aee18395e18394e18395e1839020e18390e183a0e183aae18394e183a0e1839720e18390e183a0e183a1e18394e18391e183a3e1839a20e183a9e18390e1839ce18390e183ace18394e183a0e183a12e, 'ka', 784),
(782, 0x7b6d6f64656c4c6162656c7d20e1839be18398e18397e18398e18397e18394e18391e183a3e1839ae1839820e183a1e183aee18395e1839020494420e18390e183a020e18394e1839be18397e183aee18395e18394e18395e1839020e18390e183a0e183a1e18394e18391e183a3e1839a20e183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e183a12e, 'ka', 785),
(783, 0x494420e18390e183a020e18394e1839be18397e183aee18395e18394e18395e1839020e18390e183a0e183aae18394e183a0e1839720e18390e183a0e183a1e18394e18391e183a3e1839a20e183a9e18390e1839ce18390e183ace18394e183a0e183a12e, 'ka', 786),
(784, 0xe1839be18398e18397e18398e18397e18394e18391e183a3e1839ae1839820e183a1e183aee18395e1839020494420e18390e183a020e18394e1839be18397e183aee18395e18394e18395e1839020e18390e183a0e183a1e18394e18391e183a3e1839a20e183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e183a12e, 'ka', 787),
(785, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 788),
(786, 0xe18399e18390e1839be1839ee18390e1839ce18398e18398e183a120e183a1e18390e18399e18398e18397e183aee18394e18391e1839820e18392e18395e18394e183a0e18393e18398e183a120e183a8e18394e183a5e1839be1839ce18398e183a120e18396e1839de1839be18390e183a1e18397e18390e1839c20e18393e18390e18399e18390e18395e183a8e18398e183a0e18394e18391e18398e18397, 'ka', 789),
(787, 0xe18393e18390e183ace183a7e18394e18391e18398e183a120e18397e18390e183a0e18398e183a6e18398, 'ka', 790),
(788, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18398e183a120e1839be18398e183a6e18394e18391e18390, 'ka', 791),
(789, 0xe1839be18390e183a0e18399e18394e183a2e18398e1839ce18392e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 792),
(790, 0xe1839ee183a0e1839de18394e183a5e183a2e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 793),
(791, 0xe1839ee183a0e1839de18394e183a5e183a2e18394e18391e18398, 'ka', 794),
(792, 0xe1839ee183a0e1839de18394e183a5e183a2e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 795),
(793, 0x7367322d70726f6a65637473, 'ka', 796),
(794, 0xe1839ee183a0e1839de18394e183a5e183a2e18398e183a120e18395e18394e1839ae18398, 'ka', 797),
(795, 0xe1839ee183a0e1839de18394e183a5e183a2e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 798),
(796, 0xe1839ee183a0e1839de18394e183a5e183a2e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae1839820e18390e183a020e18390e183a0e18398e183a1, 'ka', 799),
(797, 0xe183a0e1839de18392e183a0e1839d20e1839be183a3e183a8e18390e1839de18391e183a120e1839ee183a0e1839de18394e183a5e183a2e183982020457a636f64782de183a8e183983f, 'ka', 800),
(798, 0xe18390e183aee18390e1839ae1839820e18399e1839de1839be18394e1839ce183a2e18390e183a0e18398207b72656c617465644d6f64656c7d3a207b7461736b7d, 'ka', 801),
(799, 0x7b757365727d20e18393e18390e18390e18399e1839de1839be18394e1839ce183a2e18390e183a0e1839020e18393e18390e18395e18390e1839ae18394e18391e18390e18396e1839420277b7461736b7d273a, 'ka', 802),
(800, 0xe1839ee18390e183a0e183a2e1839ce18398e1839de183a0e18398, 'ka', 803),
(801, 0xe18399e1839ae18398e18394e1839ce183a2e18398, 'ka', 804),
(802, 0xe1839be183aee1839de1839ae1839de18391e18398e18397e1839820e18394e183a2e18398e18399e18394e183a2e18398e183a120e18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e1839be1839de18393e183a3e1839ae18398, 'ka', 805),
(803, 0xe18390e1839c20, 'ka', 806),
(804, 0xe1839be183aee1839de1839ae1839de18391e18398e18397e1839820e18394e183a2e18398e18399e18394e183a2e18398e183a120e18399e1839de1839ce183a2e18390e183a5e183a2e18398e183a120e1839be1839de18393e183a3e1839ae18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 807),
(805, 0x28e1839ee18398e183a0e18395e18394e1839ae18390e18393e1839820e18390e1839ce18392e18390e183a0e18398e183a8e183982920e183ace18394e18395e183a0e1839de18391e18390, 'ka', 808),
(806, 0xe1839be1839de18393e18390e1839ae183a3e183a0e1839820e183a8e18394e183a5e1839be1839ce18398e183a120e18393e18390e18397e18395e18390e1839ae18398e18394e183a0e18394e18391e18390, 'ka', 809),
(807, 0xe1839be183a0e18390e18395e1839ae1839de18391e18398e18397e1839820e18394e183a2e18398e18399e18394e183a2e18398e183a120e18390e1839ce18392e18390e183a0e18398e183a8e18394e18391e18398e183a120e183a8e18394e183a0e183ace183a7e1839be18390, 'ka', 810),
(808, 0xe18397e18394e1839be18398e183a120e18392e18390e183a0e18394e183a8e18394, 'ka', 811),
(809, 0xe18393e18390e1839be18396e18390e18393e18394e18391e183a3e1839ae18398e18390203c6120687265663d27687474703a2f2f7777772e657a636f64782e636f6d273e457a636f64783c2f613e2de183a120e1839be18398e18394e183a0, 'ka', 812),
(810, 0xe18393e18390e1839be18396e18390e18393e18394e18391e183a3e1839ae18398e183902020457a636f64782de18398e183a120e1839be18398e18394e183a0, 'ka', 813),
(811, 0xe183a0e1839de18393e18394e183a1e18390e183aa20e18393e18390e1839be18397e183aee18395e18394e18395e1839020e1839be1839de18398e183abe18394e18391e1839ce18394e18391e1839020e18392e18390e1839be1839de183a2e1839de18395e1839420e183a0e18398e18392e18398, 'ka', 814),
(812, 0xe183a0e1839de18393e18394e183a1e18390e183aa20e18393e18390e1839be18397e183aee18395e18394e18395e1839020e1839be1839de18398e183abe18394e18391e1839ce18394e18391e1839020e18392e18390e1839ce18390e18390e183aee1839ae1839420e18390e183a0e183a1e18394e18391e183a3e1839ae1839820e183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398, 'ka', 815),
(813, 0xe183a8e18394e183aee18395e18394e18393e183a0e18398e183a120e183a8e18394e183a5e1839be1839ce18390, 'ka', 816),
(814, 0xe18390e183a020e18390e183a0e18398e183a120e183a8e18394e183aee18395e18394e18393e183a0e1839020e18393e18390e18392e18394e18392e1839be18398e1839ae18398, 'ka', 817),
(815, 0xe183a7e18395e18394e1839ae1839020e18393e18390e18395e18390e1839ae18394e18391e18390, 'ka', 818),
(816, 0xe18393e18390e18395e18390e1839ae18394e18391e18394e18391e18398, 'ka', 819),
(817, 0xe18392e18390e183a3e183a5e1839be18394e18391e18390, 'ka', 820),
(818, 0xe183aae18395e1839ae18398e1839ae18394e18391e18394e18391e18398e183a120e18392e18390e183a3e183a5e1839be18394e18391e18390, 'ka', 821),
(819, 0xe183a8e18394e1839be1839de183ace1839be18394e18391e1839020e183a3e183a4e1839ae18394e18391e18394e18391e18398e183a12c20e183a4e18390e18398e1839ae18394e18391e18398e183a12c20e18395e18394e183a0e183a1e18398e18398e183a120e18392e18390e1839ce18390e183aee1839ae18394e18391e183902e2e2e, 'ka', 822),
(820, 0xe18393e18390e18395e18390e1839ae18394e18391e18394e18391e18398e183a120e18392e18390e183a8e18395e18394e18391e1839020e183a1e183a5e18394e1839be18398e183a120e18392e18390e1839ce18390e183aee1839ae18394e18391e18390e1839be18393e183942e, 'ka', 823),
(821, 0xe18393e18390e18395e18390e1839ae18394e18391e18394e18391e18398e183a120e18392e18390e183a8e18395e18394e18391e1839020e183a1e183a5e18394e1839be18398e183a120e18392e18390e1839ce18390e183aee1839ae18394e18391e18398e183a120e183a8e18394e1839be18393e18394e183922e, 'ka', 824),
(822, 0xe183a0e1839020e183aee18393e18394e18391e183903f, 'ka', 825),
(823, 0xe183a9e18394e1839be1839820e1839be1839de1839be18393e18394e18395e1839ce1839d, 'ka', 826),
(824, 0xe18398e1839ce183a1e183a2e18390e1839ae18390e183aae18398e18390, 'ka', 827),
(825, 0xe183a3e183a4e1839ae18394e18391e18394e18391e18398e183a120e18392e18390e1839ce18390e183aee1839ae18394e18391e183902e, 'ka', 828),
(826, 0xe18397e183a320e18395e18394e1839120e18392e18395e18394e183a0e18393e1839820e18390e183a020e18390e183a0e18398e183a120e183ace18390e183a0e1839be1839de18394e18391e18398e183a120e183a0e18394e18396e18398e1839be183a8e183982c20e18392e18397e183aee1839de18395e1839720e183ace18390e183a8e18390e1839ae1839de1839720206170702f746573742e70687020e183a4e18390e18398e1839ae183982e, 'ka', 829),
(827, 0xe18392e18397e183aee1839de18395e1839720e183a1e18394e183a0e18395e18394e183a0e18396e1839420e18390e183a0e183a1e18394e18391e183a3e1839ae1839820e18390e183a5e183a2e18398e18395e18394e18391e18398e183a120e183a7e18395e18394e1839ae1839020e183a4e18390e18398e1839ae1839820e183ace18390e183a8e18390e1839ae1839de183972e, 'ka', 830),
(828, 0xe183a0e18394e1839ee1839de183a0e183a2e18398e183a120e18393e18398e18390e18392e183a0e18390e1839be18390, 'ka', 831),
(829, 0xe18390e18398e183a0e183a9e18398e18394e1839720e183a0e18394e1839ee1839de183a0e183a2e1839820e18393e18398e18390e18392e183a0e18390e1839be18398e18397, 'ka', 832),
(830, 0xe18397e183a5e18395e18394e1839c20e183a1e183aae18390e18393e18394e1839720e183ace18395e18393e1839de1839be1839020e18392e183a5e1839de1839ce1839de18393e18390e1839720e183a0e18394e1839ee1839de183a0e183a2e18396e183942c20e183a0e1839de1839be1839ae18398e183a120e183ace18395e18393e1839de1839be18398e183a120e183a3e183a4e1839ae18394e18391e18390e183aa20e18390e183a020e18392e18390e183a5e18395e18397, 'ka', 833),
(831, 0xe18397e183a5e18395e18394e1839c20e183a1e183aae18390e18393e18394e1839720e183ace18395e18393e1839de1839be1839020e18392e183a5e1839de1839ce1839de18393e18390e1839720e183a0e18394e1839ee1839de183a0e183a2e18396e183942c20e183a0e1839de1839be18394e1839ae18398e183aa20e183aee18394e1839ae1839be18398e183a1e18390e183ace18395e18393e1839de1839be1839820e18390e183a6e18390e183a0e18390e18390, 'ka', 834),
(832, 0xe183a0e18394e1839ee1839de183a0e183a2e18398e183a120e183a8e18394e183afe18390e1839be18394e18391e18390, 'ka', 835),
(833, 0xe183a0e18394e1839ee1839de183a0e183a2e18398e183a120e183a8e18394e1839ce18390e183aee18395e18390, 'ka', 836),
(834, 0x7b6d6f64756c654c6162656c7d207b747970654c6162656c7d20e183a0e18394e1839ee1839de183a0e183a2e18398, 'ka', 837),
(835, 0xe183a0e18394e1839ee1839de183a0e183a2e18394e18391e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 838),
(836, 0xe183a0e18394e1839ee1839de183a0e183a2e18394e18391e18398e183a120e183a2e18390e18391e18396e1839420e183ace18395e18393e1839de1839be18390, 'ka', 839),
(837, 0xe183a0e18394e1839ee1839de183a0e183a2e18398, 'ka', 840),
(838, 0xe183a0e18394e1839ee1839de183a0e183a2204964, 'ka', 841),
(839, 0xe183a0e18394e1839ee1839de183a0e183a2e18398e183a120e18393e18390e183a1e18390e183aee18394e1839ae18394e18391e18390, 'ka', 842),
(840, 0x457a636f64782de18398e183a120e183a1e18398e18390e183aee1839ae18394e18394e18391e18398, 'ka', 843),
(841, 0xe1839be1839de18393e18394e1839ae18398e183a120e183a3e183a4e1839ae18394e18391e18394e18391e18398, 'ka', 844),
(842, 0xe18393e18390e18395e18390e1839ae18394e18391e18390, 'ka', 845),
(843, 0xe183aee18394e1839ae1839be18398e183a1e18390e183ace18395e18393e1839de1839be1839820e183a1e18390e1839be183a3e183a8e18390e1839de18394e18391e18398, 'ka', 846),
(844, 0xe183a8e18394e183a1e18390e183abe1839ae18394e18391e1839ae1839de18391e18394e18391e1839820e18392e18390e183a7e18398e18393e18395e18394e18391e18398e183a120e1839be18398e183aee18394e18393e18395e18398e18397, 'ka', 847),
(845, 0xe183a1e183a3e1839a20e1839be183aae18398e183a0e1839420e18394e183a0e1839720e1839be18398e183a1e18398e18390e183a120e1839be18390e18398e1839ce183aa20e18390e183a5e18395e183a120e183ace18390e183a3e18399e18398e18397e183aee18390e18395e1839820e183a8e18398e1839ce18390e18390e183a0e183a1e18398, 'ka', 848),
(846, 0xe1839be18398e183a1e18398e18398e183a120e183a1e183a2e18390e183a2e183a3e183a1e1839820e183a8e18394e18398e183aae18395e18390e1839ae18390, 'ka', 849),
(847, 0xe1839be18398e183a1e18398e18394e18391e18398, 'ka', 850),
(848, 0xe1839be18398e183a1e18398e183902c20e183a0e1839de1839be18394e1839ae18398e183aa20e18393e18390e18390e183a1e183a0e183a3e1839ae18394e1839720e183a3e18390e183a0e183a7e1839de183a4e18398e1839ae1839820e18398e183a5e1839ce18390, 'ka', 851),
(849, 0xe1839be18398e183a1e18398e183902c20e183a0e1839de1839be18394e1839ae18398e183aa20e18393e18390e183a1e183a0e183a3e1839ae18394e1839720e18393e18390e1839be18398e183ace1839be18394e18391e183a3e1839ae1839820e18398e183a5e1839ce18390, 'ka', 852),
(850, 0xe1839be18398e183a1e18398e18390, 'ka', 853),
(851, 0xe18391e1839de1839ae1839d20e1839be18398e183a1e18398e18390, 'ka', 854),
(852, 0xe1839be18398e183a1e18398e18394e18391e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 855),
(853, 0xe183ace18395e18393e1839de1839be1839020e1839be18398e183a1e18398e18394e18391e18398e183a120e183a2e18390e18391e18396e18394, 'ka', 856),
(854, 0xe18390e183aee18390e1839ae1839820e1839be18398e183a1e18398e18390, 'ka', 857),
(855, 0xe1839be18390e183a0e18397e1839420e18390e183a5e183a2e18398e183a3e183a0e1839820e18394e1839ce18394e18391e18398, 'ka', 858),
(856, 0xe18397e183a5e18395e18394e1839c20e18390e183a6e18390e183a020e18392e18390e183a5e18395e1839720e183ace18395e18393e1839de1839be18398e183a120e183a3e183a4e1839ae18394e18391e18390207b6d6f64656c4e616d657d2e, 'ka', 859),
(857, 0xe18394e1839ce1839020e18390e183a5e183a2e18398e183a3e183a0e1839820e18390e183a0e18390e183902e, 'ka', 860),
(858, 0xe183a3e183a4e1839ae18394e18391e18394e18391e1839820e18393e1839020e1839ce18394e18391e18390e183a0e18397e18395e18394e18391e18398, 'ka', 861),
(859, 0xe1839be1839de183abe18394e18391e1839ce18394e1839720e183a1e18390e183aee18394e1839ae18398e183a12c20e183a2e18394e1839ae18394e183a4e1839de1839ce18398e183a120e18390e1839c20e18394e1839a2ee183a4e1839de183a1e183a2e18398e183a120e1839be18398e183aee18394e18393e18395e18398e18397, 'ka', 862),
(860, 0xe1839ce18394e18391e18390e183a0e18397e18395e18390, 'ka', 863),
(861, 0xe183a7e18395e18394e1839ae18390, 'ka', 864),
(862, 0xe183a3e1839ce18393e1839020e18392e183a5e1839de1839ce18393e18394e1839720e183a1e183a3e1839a20e1839be183aae18398e183a0e1839420e18394e183a0e18397e1839820e18390e183a5e183a2e18398e183a3e183a0e1839820e18395e18390e1839ae183a3e183a2e183902e, 'ka', 865),
(863, 0xe18392e1839ae1839de18391e18390e1839ae183a3e183a0e1839820e183a1e18390e183abe18398e18394e18391e1839d20e183a1e18398e183a1e183a2e18394e1839be1839020e18394e18399e183a0e18390e1839ce18398e183a120e18397e18390e18395e183a8e183982c20e18393e18390e18392e18394e183aee1839be18390e183a0e18394e18391e18390e1839720e18398e1839ee1839de18395e1839de1839720e18397e183a5e18395e18394e1839ce18397e18395e18398e183a120e183a1e18390e183ade18398e183a0e1839d20e18398e1839ce183a4e1839de183a0e1839be18390e183aae18398e1839020e183a1e183ace183a0e18390e183a4e18390e183932e, 'ka', 866),
(864, 0xe18392e18390e1839be1839de183a2e1839de18395e18394e1839720e183a0e18390e18393e18392e18390e1839c20e18397e183a5e18395e18394e1839c20e18390e183a020e18392e18390e183a5e18395e1839720e183a1e18390e18399e1839be18390e183a0e18398e183a1e1839820e183a3e183a4e1839ae18394e18391e18394e18391e183982e, 'ka', 867),
(865, 0xe18394e183a120e183aee18394e1839ae1839be18398e183a1e18390e183ace18395e18393e1839de1839be18398e1839020e1839be183aee1839de1839ae1839de18393204368726f6d652de183a8e183982e, 'ka', 868),
(866, 0xe1839ce18394e18391e18390e183a0e18397e18395e18394e18391e18398e183a120e183afe18392e183a3e183a4e183a3e183a0e1839820e183a9e18390e183ace18394e183a0e18390, 'ka', 869),
(867, 0xe183ace18398e1839ce18390, 'ka', 870),
(868, 0xe1839ce18394e18391e18390e183a0e18397e18395e18394e18391e18398e183a120e183a9e18390e183ace18394e183a0e18390, 'ka', 871),
(869, 0xe1839ce18394e18391e18390e183a0e18397e18395e18394e18391e18398e183a120e183a9e18390e1839ce18390e183ace18394e183a0e1839820e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e183a8e18394e18398e1839ce18390e183aee183902e, 'ka', 872),
(870, 0xe183a0e18394e1839ee1839de183a0e183a2e18398e183a120e183a2e18398e1839ee18398e183a120e18390e183a0e183a9e18394e18395e18390, 'ka', 873),
(871, 0xe1839ce18390e183aee18395e18390, 'ka', 874),
(872, 0x7b6e7d20e18393e183a6e18398e183a120e183ace18398e1839c207c7b6e7d20e18393e183a6e18398e183a120e183ace18398e1839c, 'ka', 875),
(873, 0xe183a8e18398e1839ce18390e18390e183a0e183a1e18398, 'ka', 876),
(874, 0xe183a4e18398e1839ae183a2e183a0e18390e183aae18398e18394e18391e18398, 'ka', 877),
(875, 0xe18390e183a5e183a2e18398e183a3e183a0e18398, 'ka', 878),
(876, 0xe183a1e183a2e18390e183a2e183a3e183a1e18398, 'ka', 879),
(877, 0xe183a1e183a2e18390e183a2e183a3e183a1e18398e183a120e18392e18390e1839ce183a1e18390e18396e183a6e18395e183a0e1839020e18390e183a0e18390e183a1e183ace1839de183a0e18398e183902e, 'ka', 880),
(878, 0xe183aee18394e1839ae1839be18398e183a1e18390e183ace18395e18393e1839de1839be18398, 'ka', 881),
(879, 0xe18390e183a5e183a2e18398e183a3e183a0e18398e18390, 'ka', 882),
(880, 0xe183a3e183a4e1839ae18394e18391e18394e18391e18398, 'ka', 883),
(881, 0xe183a3e183a4e1839ae18394e18391e18394e18391e18398e183a120e18390e183a0e183a9e18394e18395e18390, 'ka', 884),
(882, 0xe18393e18394e183a2e18390e1839ae18394e18391e18398e183a120e1839ce18390e183aee18395e18390, 'ka', 885),
(883, 0xe18399e18390e1839ae18390e18397e18391e183a3e183a0e18397e18398, 'ka', 886),
(884, 0xe18391e183a3e183a0e18397e18398, 'ka', 887),
(885, 0xe18392e1839de1839ae183a4e18398e183a120e18391e183a3e183a0e18397e18398, 'ka', 888),
(886, 0xe183a4e18394e183aee18391e183a3e183a0e18397e18398e183a120e18391e183a3e183a0e18397e18398, 'ka', 889),
(887, 0xe183a4e183a0e18394e1839ce18391e183a3e183a0e18397e18398, 'ka', 890),
(888, 0xe18393e18390e1839ce18398e183a8e1839ce183a3e1839ae18398207b72656c617465644d6f64656c7d3a207b7461736b7d, 'ka', 891),
(889, 0xe1839be18398e183ace1839de18393e18394e18391e183a3e1839ae18398e18390207b72656c617465644d6f64656c7d3a207b7461736b7d, 'ka', 892),
(890, 0xe18393e18390e18393e18390e183a1e183a2e183a3e183a0e18394e18391e183a3e1839ae18398e18390207b72656c617465644d6f64656c7d3a207b7461736b7d, 'ka', 893),
(891, 0xe183a3e18390e183a0e183a7e1839de183a4e18398e1839ae18398e18390207b72656c617465644d6f64656c7d3a207b7461736b7d, 'ka', 894),
(892, 0xe18393e18390e18395e18390e1839ae18394e18391e183902c20277b7461736b7d, 'ka', 895),
(893, 0xe183a0e1839de1839b20e1839ce18390e183aee1839de18397, 'ka', 896),
(894, 0xe18397e18390e183a0e18398e183a6e1839820e18393e1839020e1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae1839820e183a8e18394e18398e183a5e1839be1839ce18390, 'ka', 897),
(895, 0xe183a1e18390e183aee18394e1839ae18398, 'ka', 115),
(896, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e18393e18390e183a1e18390e183aee18394e1839ae18394e18391e18390, 'ka', 898),
(897, 0xe1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18391e18390e18396e18398e183a120e1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae18398, 'ka', 899),
(898, 0xe183a3e18391e183a0e18390e1839ae1839d20e183a8e18394e183a2e183a7e183a3e1839de18391e18398e1839ce18394e18391e18390, 'ka', 900),
(899, 0xe183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e183a120e183ace18394e183a0e18398e1839ae18398, 'ka', 901),
(900, 0xe183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18398e183a120e183ace18394e183a0e18398e1839ae18394e18391e18398, 'ka', 902),
(901, 0xe183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18390, 'ka', 903),
(902, 0xe18390e183a020e18390e183a0e18398e183a120e18390e183aee18390e1839ae1839820e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e183982e, 'ka', 904),
(903, 0xe18397e183a5e18395e18394e1839c20e18392e18390e183a5e18395e1839720e18390e183aee18390e1839ae1839820e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18390, 'ka', 905),
(904, 0xe1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 906),
(905, 0xe18398e1839be18394e18398e1839a20e183a8e18394e183a2e183a7e1839de18398e1839ce18394e18391e18394e18391e18398e183a120e18392e18390e18397e18398e183a8e18395e18390, 'ka', 907),
(906, 0xe18393e18394e183a1e18399e183a2e1839de1839e20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398e183a120e18392e18390e18390e183a5e183a2e18398e183a3e183a0e18394e18391e18390, 'ka', 908),
(907, 0xe18392e18390e18390e183a1e183a3e183a4e18397e18390e18395e18394e1839720e183a1e18394e183a0e18395e18394e183a0e18396e1839420e18390e183a2e18398e18395e18394e18391e18398e183a120e183a1e18390e183a5e18390e183a6e18390e1839ae18393e183942028e18390e183a0e18390e183a1e18390e18395e18390e1839ae18393e18394e18391e183a3e1839ae1839d292e, 'ka', 909),
(908, 0xe183a1e18390e183a5e1839be18398e18390e1839ce1839de18391e18398e183a120e18393e18390e183a1e18390e183aee18394e1839ae18394e18391e18390, 'ka', 910),
(909, 0xe18397e18390e1839be18390e183a8e18398e183a120e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18390, 'ka', 911),
(910, 0xe18397e18390e1839be18390e183a8e18398e183a120e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398, 'ka', 912),
(911, 0xe183a4e18390e18398e1839ae18398e183a120e18393e18390e183a1e18390e183aee18394e1839ae18394e18391e18398e183a120e18394e183a5e183a1e1839ee1839de183a0e183a2e18398, 'ka', 913),
(912, 0xe18393e18394e18390e183a5e183a2e18398e18395e18390e183aae18398e18390, 'ka', 914),
(913, 0xe18392e18390e18390e183a5e183a2e18398e183a3e183a0e18394e18391e18390, 'ka', 915),
(914, 0xe183a1e18390e183aee18394e1839ae18398, 'ka', 916),
(915, 0xe18392e18395e18390e183a0e18398, 'ka', 917),
(916, 0xe18390e1839ee1839ae18398e18399e18390e183aae18398e18398e183a120e18393e18390e183a1e18390e183aee18394e1839ae18394e18391e18390, 'ka', 918),
(917, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e183a1e18390e183aee18394e1839ae18398, 'ka', 919),
(918, 0xe18390e183a0e18390e183a1e183ace1839de183a0e1839820e1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae1839820e18390e1839c20e1839ee18390e183a0e1839de1839ae183982e, 'ka', 920),
(919, 0xe18390e183a0e18390e183a1e183ace1839de183a0e1839820e1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae1839820e18390e1839c20e1839ee18390e183a0e1839de1839ae183982e, 'ka', 921),
(920, 0x7b6c616e67756167654e616d657d20e18392e18390e18390e183a5e183a2e18398e183a3e183a0e18393e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e18397, 'ka', 922),
(921, 0x7b6c616e67756167654e616d657d20e18390e183a5e183a2e18398e18395e18390e183aae18398e1839020e18395e18394e183a020e18392e18390e1839ce183aee1839de183a0e183aae18398e18394e1839ae18393e183902e20e183a8e18394e183aae18393e1839de1839be183903a207b6572726f724d6573736167657d, 'ka', 923),
(922, 0x7b6c616e67756167654e616d657d20e18390e183a5e183a2e18398e18395e18390e183aae18398e1839020e18395e18394e183a020e18392e18390e1839ce183aee1839de183a0e183aae18398e18394e1839ae18393e183902e20e18393e18390e183a4e18398e183a5e183a1e18398e183a0e18393e1839020e1839be1839de183a3e1839ae18393e1839ce18394e1839ae1839820e183a8e18394e183aae18393e1839de1839be183902e, 'ka', 924),
(923, 0x7b6c616e67756167654e616d657d20e18392e18390e1839ce18390e183aee1839ae18393e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e18397, 'ka', 925),
(924, 0x7b6c616e67756167654e616d657d20e18392e18390e1839ce18390e183aee1839ae18394e18391e1839020e18395e18394e183a020e18392e18390e1839ce183aee1839de183a0e183aae18398e18394e1839ae18393e183902e20e183a8e18394e183aae18393e1839de1839be183903a207b6572726f724d6573736167657d, 'ka', 926),
(925, 0x7b6c616e67756167654e616d657d20e18392e18390e1839ce18390e183aee1839ae18394e18391e1839020e18395e18394e183a020e18392e18390e1839ce183aee1839de183a0e183aae18398e18394e1839ae18393e183902e20e18393e18390e183a4e18398e183a5e183a1e18398e183a0e18393e1839020e1839be1839de183a3e1839ae1839de18393e1839ce18394e1839ae1839820e183a8e18394e183aae18393e1839de1839be183902e, 'ka', 927),
(926, 0x7b6c616e67756167654e616d657d20e18392e18390e183a3e183a5e1839be1839ce18393e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e18397, 'ka', 928),
(927, 0x7b6c616e67756167654e616d657d20e18392e18390e183a3e183a5e1839be18394e18391e1839020e18395e18394e183a020e1839be1839de183aee18394e183a0e183aee18393e183902e20e183a8e18394e183aae18393e1839de1839be183903a207b6572726f724d6573736167657d, 'ka', 929),
(928, 0x7b6c616e67756167654e616d657d20e18392e18390e183a3e183a5e1839be18394e18391e1839020e18395e18394e183a020e1839be1839de183aee18394e183a0e183aee18393e183902e20e18393e18390e183a4e18398e183a5e183a1e18398e183a0e18393e1839020e1839be1839de183a3e1839ae1839de18393e1839ce18394e1839ae1839820e183a8e18394e183aae18393e1839de1839be183902e, 'ka', 930),
(929, 0xe18397e183a5e18395e18394e1839c20e183a3e18399e18395e1839420e18393e18390e18391e1839ae1839de18399e18394e1839720e18393e18394e183a1e18399e183a2e1839de1839e20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e1839820e18390e1839b20e18391e183a0e18390e183a3e18396e18394e183a0e18398e183a1e18397e18395e18398e183a12e, 'ka', 931),
(930, 0xe18397e183a5e18395e18394e1839c20e183a3e18399e18395e1839420e18392e18390e18390e183a5e183a2e18398e183a3e183a0e18394e1839720e18393e18394e183a1e18399e183a2e1839de1839e20e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e18398204368726f6d652de18398e183a1e18397e18395e18398e183a1, 'ka', 932),
(931, 0xe183a3e1839ce18393e1839020e18398e183a7e1839de183a120e1839be18398e1839ce18398e1839be183a3e1839b20e18394e183a0e18397e1839820e183a1e183a3e1839ee18394e183a020e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183a2e1839de183a0e1839820e1839be18390e18398e1839ce183aa, 'ka', 933),
(932, 0xe18395e18398e1839c20e18398e183a6e18394e18391e183a120e183a8e18394e183a2e183a7e1839de18391e18398e1839ce18394e18391e18394e18391e183a1, 'ka', 934),
(933, 0xe183a1e183a3e1839ee18394e183a020e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183a2e1839de183a0e18394e18391e18398, 'ka', 935),
(934, 0xe18392e18390e1839be1839de183a2e1839de18395e18394e18391e18390207b7b7461626c654e616d657d7d, 'ka', 936),
(935, 0xe18397e183a5e18395e18394e1839c20e18390e183a6e1839be1839de18390e183a9e18398e1839ce18394e18397207b6e616d657d, 'ka', 937),
(936, 0x7b6d6f64756c6553696e67756c61724c6162656c7d20e183abe18398e18394e18391e18390, 'ka', 938),
(937, 0xe183a1e18390e183aee18394e1839de18391e18390, 'ka', 939),
(938, 0xe18390e183a6e183ace18394e183a0e18398e1839ae1839de18391e18390, 'ka', 940),
(939, 0xe183a8e18394e183aee18395e18394e18393e183a0e18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 941),
(940, 0xe183a8e18394e183aee18395e18394e18393e183a0e18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 942),
(941, 0xe1839be18398e1839be18393e18398e1839ce18390e183a0e18394e1839de18391e183a120e18392e18390e1839ce18390e183aee1839ae18394e18391e183902e20e18392e18397e183aee1839de18395e1839720e18393e18390e18398e183aae18390e18393e1839de183972e, 'ka', 943),
(942, 0xe1839be18398e1839be18393e18398e1839ce18390e183a0e18394e1839de18391e183a120e18398e1839ce183a1e183a2e18390e1839ae18390e183aae18398e183902e20e18392e18397e183aee1839de18395e1839720e18393e18390e18398e183aae18390e18393e1839de183972e, 'ka', 944),
(943, 0xe1839be183a4e1839ae1839de18391e18394e1839ae18398e183a1204964, 'ka', 945),
(944, 0xe1839be183a4e1839ae1839de18391e18394e1839ae18398e183a120e183a1e18390e183aee18394e1839ae18398, 'ka', 946),
(945, 0xe1839be183a4e1839ae1839de18391e18394e1839ae18398, 'ka', 947),
(946, 0xe1839de183a0e18392e18390e1839ce18398e18396e18390e183aae18398e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 948),
(947, 0xe183a1e18390e18391e18390e1839ce18399e1839d, 'ka', 949),
(948, 0xe18394e1839ce18394e183a0e18392e18394e183a2e18398e18399e18390, 'ka', 950),
(949, 0xe183a4e18398e1839ce18390e1839ce183a1e183a3e183a0e1839820e1839be1839de1839be183a1e18390e183aee183a3e183a0e18394e18391e18390, 'ka', 951),
(950, 0xe18393e18390e18396e183a6e18395e18394e18395e18390, 'ka', 952),
(951, 0xe1839be183a0e18394e183ace18395e18394e1839ae1839de18391e18390, 'ka', 953),
(952, 0xe183a1e18390e183aae18390e1839ae1839d20e18395e18390e183ade183a0e1839de18391e18390, 'ka', 954),
(953, 0xe183a2e18394e183a5e1839ce1839de1839ae1839de18392e18398e18390, 'ka', 955),
(954, 0xe18395e18398e183a120e183a8e18394e18394e183abe1839ae1839de183a120e1839ce18390e183aee18395e1839020e18393e1839020e183a9e18390e183a1e183ace1839de183a0e18394e18391e18390, 'ka', 956),
(955, 0xe1839be183aee1839de1839ae1839de1839320e18390e183aee18390e1839ae1839820e183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398, 'ka', 957),
(956, 0xe183afe18392e183a3e183a4e18394e18391e18398, 'ka', 958),
(957, 0xe18395e18398e183a120e183a8e18394e18394e183abe1839ae1839de183a120e1839ce18390e183aee18395e1839020e18393e1839020e183a9e18390e183a1e183ace1839de183a0e18394e18391e18390202d20e183a1e18390e183ace183a7e18398e183a1e18390e18393, 'ka', 959),
(958, 0xe18393e18390e18395e18390e1839ae18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 960),
(959, 0xe18393e18390e18395e18390e1839ae18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 961),
(960, 0xe1839ee183a0e1839de18393e183a3e183a5e183aae18398e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 962),
(961, 0xe1839ee183a0e1839de18393e183a3e183a5e183aae18398e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 963),
(962, 0xe183a8e18394e183a1e18390e183abe1839ae18394e18391e1839ae1839de18391e18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 964),
(963, 0xe183a8e18394e183a1e18390e183abe1839ae18394e18391e1839ae1839de18391e18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 965),
(964, 0xe18399e18395e18390e1839ae18398e183a4e18398e18399e18390e183aae18398e18390, 'ka', 966),
(965, 0xe1839be1839de1839ae18390e1839ee18390e183a0e18390e18399e18394e18391e18398e183a120e183ace18390e183a0e1839be1839de18394e18391e18390, 'ka', 967),
(966, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 968),
(967, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 969),
(968, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 970),
(969, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18398, 'ka', 971),
(970, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398, 'ka', 972),
(971, 0xe18390e183aee18390e1839ae1839820e18394e1839be18391e1839ae18394e1839be18390, 'ka', 973),
(972, 0xe1839be18398e18395e18398e183a6e1839420e18390e183aee18390e1839ae1839820e18394e1839be18391e1839ae18394e1839be183903a207b6261646765436f6e74656e747d, 'ka', 974),
(973, 0xe18394e183a5e183a1e1839ee1839de183a0e183a2e18398e183a120e183a4e18390e18398e1839ae18398e183a120e183a2e18398e1839ee18398, 'ka', 975),
(974, 0xe183ace18395e18393e1839de1839be1839020e18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183aae18398e18398e183a120e183a2e18390e18391e18396e18394, 'ka', 976),
(975, 0xe183abe18398e18394e18391e18398e183a120e183a8e18394e1839ce18390e183aee18395e18390, 'ka', 977),
(976, 0xe18393e18394e1839ee18390e183a0e183a2e18390e1839be18394e1839ce183a2e18398, 'ka', 978),
(977, 0xe18397e18390e1839ce18390e1839be18393e18394e18391e1839de18391e18390, 'ka', 979),
(978, 0xe1839be1839de18391e18398e1839ae183a3e183a0e1839820e183a2e18394e1839a, 'ka', 980),
(979, 0xe1839de183a4e18398e183a1e18398e183a120e183a2e18394e1839a, 'ka', 981),
(980, 0xe183a4e18390e183a5e183a1e18398, 'ka', 982);
INSERT INTO `messagetranslation` (`id`, `translation`, `language`, `messagesource_id`) VALUES
(981, 0xe183abe18398e183a0e18398e18397e18390e18393e1839820e1839be18398e183a1e18390e1839be18390e183a0e18397e18398, 'ka', 983),
(982, 0xe183abe18398e183a0e18398e18397e18390e18393e1839820e18394e1839a2e20e183a4e1839de183a1e183a2e18390, 'ka', 984),
(983, 0xe183a5e18390e1839ae18390e183a5e18398, 'ka', 985),
(984, 0xe183a5e18395e18394e183a7e18390e1839ce18390, 'ka', 986),
(985, 0xe183a1e18390e183a4e1839de183a1e183a2e1839d20e18399e1839de18393e18398, 'ka', 987),
(986, 0xe183a5e183a3e183a9e183902031, 'ka', 988),
(987, 0xe183a5e183a3e183a9e183902032, 'ka', 989),
(988, 0xe183a8e183a2e18390e183a2e18398, 'ka', 990),
(989, 0xe183abe18398e18394e18391e1839020e183a8e18394e1839ce18390e183aee183a3e1839ae18398e18390, 'ka', 991),
(990, 0xe1839be183aee1839de1839ae1839de1839320e1839be183a4e1839ae1839de18391e18394e1839ae18398, 'ka', 992),
(991, 0xe183aae183aee183a0e18398e1839ae18398e183a120e183a2e18398e1839ee18398, 'ka', 993),
(992, 0xe1839be183a0e18390e18395e18390e1839ae1839820e183a1e18390e183aee18394e1839de18391e18390, 'ka', 994),
(993, 0xe183afe18392e183a3e183a4e18394e18391e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 995),
(994, 0xe183afe18392e183a3e183a4e18394e18391e18398e183a120e183a2e18390e18391e18396e1839420e183ace18395e18393e1839de1839be18390, 'ka', 996),
(995, 0xe183afe18392e183a3e183a4e18394e18391e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 997),
(996, 0xe183a5e18390e1839ae18391e18390e183a2e1839de1839ce18398, 'ka', 998),
(997, 0xe18393e1839de183a5e183a2e1839de183a0e18398, 'ka', 999),
(998, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e18398e183a120e183a1e18390e183aee18394e1839de18391e18398e183a120e18390e183a0e183a9e18394e18395e18390, 'ka', 1000),
(999, 0xe18399e1839ae18398e18394e1839ce183a2e18398, 'ka', 1001),
(1000, 0xe1839ce18394e18391e18398e183a1e1839be18398e18394e183a0e1839820e183a5e18390e1839ae18390e183a5e18398, 'ka', 1002),
(1001, 0xe1839ce18394e18391e18398e183a1e1839be18398e18394e183a0e1839820e183a5e183a3e183a9e18390, 'ka', 1003),
(1002, 0xe1839ce18394e18391e18398e183a1e1839be18398e18394e183a0e1839820e183a8e183a2e18390e183a2e18398, 'ka', 1004),
(1003, 0xe1839ce18394e18391e18398e183a1e1839be18398e18394e183a0e1839820e183a1e18390e183a4e1839de183a1e18390e183a2e1839d20e18399e1839de18393e18398, 'ka', 1005),
(1004, 0xe1839ce18394e18391e18398e183a1e1839be18398e18394e183a0e1839820e183a5e18395e18394e183a7e18390e1839ce18390, 'ka', 1006),
(1005, 0xe183a1e18390e183a5e1839be18398e18390e1839ce1839de18391e18390, 'ka', 1007),
(1006, 0xe1839be18394e1839de183a0e18390e18393e1839820e18394e1839a2e20e183a4e1839de183a1e183a2e18390, 'ka', 1008),
(1007, 0xe183a7e18395e18394e1839ae18390, 'ka', 1009),
(1008, 0xe18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183aae18398e18390, 'ka', 1010),
(1009, 0xe18390e183aee18390e1839ae18398, 'ka', 1011),
(1010, 0xe1839be18398e1839be18393e18398e1839ce18390e183a0e18394, 'ka', 1012),
(1011, 0xe183a8e18394e18398e183a5e1839be1839ce18390207b6f776e6572537472696e67436f6e74656e747d20e1839be18398e18394e183a0, 'ka', 1013),
(1012, 0xe183a1e18390e183aee18394e1839de18391e1839020e18390e183a020e183a3e1839ce18393e1839020e18398e183a7e1839de183a120e183aae18390e183a0e18398e18394e1839ae18398, 'ka', 1014),
(1013, 0xe1839ee183a0e1839de18393e183a3e183a5e183aae18398e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 1015),
(1014, 0xe1839be18390e183a8e183a2e18390e18391e183a3e183a0e1839820e183abe18398e18394e18391e18390, 'ka', 1016),
(1015, 0xe18395e18394e1839120e18392e18395e18394e183a0e18393e18398, 'ka', 1017),
(1016, 0xe183a1e18390e183ace183a7e18398e183a1e18390e18393202d20e18395e18398e183a120e183a8e18394e18394e183abe1839ae1839de183a120e1839ce18390e183aee18395e1839020e18393e1839020e183a9e18390e183a1e183ace1839de183a0e18394e18391e18390, 'ka', 1018),
(1017, 0xe18392e18390e18396e18394e18397e18398, 'ka', 1019),
(1018, 0xe183a1e18390e183a4e18398e1839ce18390e1839ce183a1e1839d, 'ka', 1020),
(1019, 0xe1839be183aee18390e183a0e18393e18390e183ade18394e183a0e18390, 'ka', 1021),
(1020, 0xe183a2e18394e183a5e1839ce18398e18399e183a3e183a0e18398, 'ka', 1022),
(1021, 0xe18390e18393e1839be18398e1839ce18398e183a1e183a2e183a0e18390e183aae18398e183a3e1839ae18398, 'ka', 1023),
(1022, 0xe1839ee183a0e1839de18394e183a5e183a2e18394e18391e18398e183a120e183aee18394e1839ae1839be183abe183a6e18395e18390e1839ce18394e1839ae18398, 'ka', 1024),
(1023, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 1025),
(1024, 0xe18392e18398e1839ae1839de183aae18390e18395e183972120457a636f6478202de18398e183a120e18398e1839ce183a1e183a2e18390e1839ae18390e183aae18398e1839020e18393e18390e183a1e183a0e183a3e1839ae18394e18391e183a3e1839ae18398e183902e, 'ka', 1026),
(1025, 0xe18392e18398e1839ae1839de183aae18390e18395e183972120e1839be1839de1839ce18390e183aae18394e1839be18397e1839020e18393e18394e1839be1839d20e18395e18394e183a0e183a1e18398e1839020e183ace18390e183a0e1839be18390e183a2e18394e18391e18398e1839720e18390e18398e183a2e18395e18398e183a0e18397e183902e, 'ka', 1027),
(1026, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e18394e18391e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 1028),
(1027, 0x7b6e7d20e183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390207c7b6e7d20e183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e18394e18391e1839820e183a8e18394e183a5e1839be1839ce18398e1839ae18398e18390, 'ka', 1029),
(1028, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e18394e18391e18398, 'ka', 1030),
(1029, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e18394e18391e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 1031),
(1030, 0xe183a1e18390e1839be183a3e183a8e18390e1839d20e1839ee183a0e1839de183aae18394e183a1e18394e18391e18398e183a120e183a2e18390e18391e18396e1839420e183ace18395e18393e1839de1839be18390, 'ka', 1032),
(1031, 0xe1839de183a0e18398e18395e183942c20e18390e183aee18390e1839ae1839820e18393e1839020e18390e183a0e183a1e18394e18391e183a3e1839ae1839820e183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398, 'ka', 1033),
(1032, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398, 'ka', 1034),
(1033, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 1035),
(1034, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18398e183a120e18393e18390e1839be18390e183a2e18394e18391e18390, 'ka', 1036),
(1035, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 1037),
(1036, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18396e1839420e183ace18395e18393e1839de1839be18390, 'ka', 1038),
(1037, 0xe183a8e18394e183a1e18390e183abe1839ae18394e18391e1839ae1839de18391e18394e18391e1839820e183a9e18390e1839ce18390e183ace18394e183a0e18397e1839020e183ace183a7e18390e183a0e1839de18393e18390e1839c, 'ka', 1039),
(1038, 0xe1839de18393e18394e1839ce1839de18391e18390, 'ka', 1040),
(1039, 0xe18390e183a1e183a0e183a3e1839ae18394e18391e18398e183a120e18397e18390e183a0e18398e183a6e18398, 'ka', 1041),
(1040, 0xe18397e183a5e18395e18394e1839c20e18392e18390e18393e18390e183aee18395e18394e18393e18398e1839720207b6e6578744c6576656c7d2de1839420e18393e1839de1839ce18394e18396e18394, 'ka', 1042),
(1041, 0xe18390e183a3e183a2e18394e1839ce183a2e18398e183a4e18398e18399e18390e183aae18398e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 1043),
(1042, 0xe183a0e1839de1839ae18394e18391e18398, 'ka', 1044),
(1043, 0xe183afe18392e183a3e183a4e18398, 'ka', 1045),
(1044, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18394e18391e18398, 'ka', 1046),
(1045, 0xe183afe18392e183a3e183a4e18398e183a120e183a8e18394e183a0e183a9e18394e18395e18390, 'ka', 1047),
(1046, 0xe183afe18392e183a3e183a4e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 1048),
(1047, 0xe18393e18390e183a0e183ace1839be183a3e1839ce18394e18391e183a3e18398e1839ae1839820e183aee18390e183a0e1839720e183a0e1839de1839b2020e18392e183a1e183a3e183a0e1839720e18390e1839b20e183afe18392e183a3e183a4e18398e183a120e183ace18390e183a8e1839ae183903f, 'ka', 1049),
(1048, 0xe183a0e1839de1839ae18394e18391e18398e183a120e183ade18390e183a8e1839ae18390, 'ka', 1050),
(1049, 0xe183a0e1839de1839ae18394e18391e18398e183a120e183a2e18390e18391e18396e1839420e183ace18395e18393e1839de1839be18390, 'ka', 1051),
(1050, 0xe183a0e1839de1839ae18394e18391e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 1052),
(1051, 0xe183a2e18394e18392e18394e18391e18398e183a120e183a8e18394e18394e183a0e18397e18394e18391e1839020e18390e183a0e18398e183a120e18398e1839ce183a4e1839de183a0e1839be18390e183aae18398e18398e183a120e18392e18390e18393e18390e183aae18394e1839be18398e183a120e18390e18393e18395e18398e1839ae1839820e18392e18396e18390, 'ka', 1053),
(1052, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e183a8e18390e18391e1839ae1839de1839ce18394e18391e18398e183a120e183ace18390e183a8e1839ae18390, 'ka', 1054),
(1053, 0xe18394e1839a2e20e183a4e1839de183a1e183a2e18398e183a120e183a8e18390e18391e1839ae1839de1839ce18394e18391e18398e183a120e183a2e18390e18391e18396e1839420e183ace18395e18393e1839de1839be18390, 'ka', 1055),
(1054, 0xe183afe18392e183a3e183a4e18398e183a120e1839be1839de18393e183a3e1839ae18398e183a120e183a3e183a4e1839ae18394e18391e18394e18391e18398, 'ka', 1056),
(1055, 0xe183a8e18394e183a5e1839be18394e1839ce18398e1839720e183a8e18390e18391e1839ae1839de1839ce1839820e18398e1839be18394e18398e1839ae18398e183a1e18397e18395e18398e183a120e183a0e1839de1839be1839ae18398e183a120e18392e18390e18392e18396e18390e18395e1839ce18390e183a1e18390e183aa20e18390e1839ee18398e183a0e18394e18391e183972c20e18393e18390e18390e18398e1839be1839ee1839de183a0e183a2e18394e1839720e18393e1839020e18392e18390e1839be1839de18398e183a7e18394e1839ce18394e1839720e18390e1839c20e1839be18397e1839ae18398e18390e1839ce183982048544d4c2de1839820e18390e1839c20e18392e18390e18390e18392e18396e18390e18395e1839ce18394e1839720e183a3e18391e183a0e18390e1839ae1839de1839320e183a2e18394e183a5e183a1e183a2e18398, 'ka', 1057),
(1056, 0xe18392e18390e1839be1839de183a2e1839de18395e18394, 'ka', 1058),
(1057, 0xe18394e1839ce18390, 'ka', 1059),
(1058, 0xe183a8e18390e18391e1839ae1839de1839ce18394e18391e18398, 'ka', 1060),
(1059, 0xe18392e18398e1839ae1839de183aae18390e18395e1839721, 'ka', 1061),
(1060, 0xe18392e18390e1839be1839de183a2e1839de18395e18394e18391e18390, 'ka', 1062),
(1061, 0xe1839ee183a0e1839de183a4e18398e1839ae18396e1839420e18392e18390e1839ce18397e18390e18395e183a1e18394e18391e18390, 'ka', 1063),
(1062, 0xe18394e1839ce18390, 'ka', 1064),
(1063, 0xe1839ee183a0e1839de18393e183a3e183a5e183aae18398e18398e183a120e183a8e18394e183a0e183aae18394e18395e18390, 'ka', 1065),
(1064, 0xe18397e183a5e18395e18394e1839c20e1839be18398e18398e183a6e18394e18397207b6e7d20e1839be1839de1839ce18394e183a2e1839020207c20e18397e183a5e18395e18394e1839c20e1839be18398e18398e183a6e18394e18397207b6e7d20e1839be1839de1839ce18394e183a2e18390, 'ka', 1066),
(1065, 0xe18394e1839ce18394e18391e18398, 'ka', 1067),
(1066, 0xe183a9e18390e1839ce18390e183ace18394e183a0e18394e18391e18398, 'ka', 1068),
(1067, 0xe18393e18390e18395e18390e1839ae18394e18391e18398e183a120e1839be18394e1839ce18394e183afe18394e183a0e18398, 'ka', 1069),
(1068, 0xe18392e1839ae1839de18391e18390e1839ae183a3e183a0e1839820e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 1070),
(1069, 0xe18395e18390e1839ae183a3e183a2e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 1071),
(1070, 0xe18393e18394e18395e18394e1839ae1839de1839ee18394e183a0e18394e18391e18398e183a120e183aee18394e1839ae183a1e18390e183ace183a7e1839de18394e18391e18398, 'ka', 1072),
(1071, 0xe18392e18390e183a4e18390e183a0e18397e1839de18394e18391e18394e18391e18398, 'ka', 1073),
(1072, 0xe18397e18390e1839be18390e183a8e18398e183a120e183afe18398e1839ae18393e1839de18394e18391e18398, 'ka', 1074),
(1073, 0xe183a1e18390e1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae1839d20e18398e1839ce183a2e18394e183a0e183a4e18394e18398e183a1e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 1075),
(1074, 0xe1839be1839de18393e183a3e1839ae18398e183a120e18390e183a0e183a9e18394e18395e18390, 'ka', 1076),
(1075, 0xe18399e18390e183a2e18390e1839ae1839de18392e18398e183a120e1839ee183a3e1839ce183a5e183a2e18394e18391e18398, 'ka', 1077),
(1076, 0xe183a4e18390e18398e1839ae18398e183a120e18390e183a2e18395e18398e183a0e18397e18395e18390, 'ka', 1078),
(1077, 0xe18395e18394e1839ae18394e18391e18398e183a120e1839be18398e18397e18398e18397e18394e18391e18390, 'ka', 1079),
(1078, 0xe183a0e1839de18392e1839de183a020e1839be183a3e183a8e18390e1839de18391e183a120e183a3e183a1e18390e183a4e183a0e18397e183aee1839de18394e18391e1839020457a636f64782de183a8e183983f, 'ka', 1080),
(1079, 0xe18390e1839ce18392e18390e183a0e18398e183a8e18398e183a120e18390e1839ce18392e18390e183a0e18398e183a8e18396e1839420e1839be18398e18391e1839be18390, 'ka', 1081),
(1080, 0xe1839be18394e1839ce18394e183afe18394e183a0e18394e18391e18398e183a1e18397e18395e18398e183a120e183ace18390e18399e18398e18397e183aee18395e183902fe183a9e18390e183ace18394e183a0e1839020e18393e1839020e183ace18390e183a8e1839ae18398e183a120e1839ce18394e18391e18390e183a0e18397e18395e18398e183a120e1839be18398e183aae18394e1839be18390, 'ka', 1082),
(1081, 0xe18392e18390e18398e18392e1839420e1839be18394e183a2e1839820e183a3e183a1e18390e183a4e183a0e18397e183aee1839de18394e18391e18398e183a120e183a8e18394e183a1e18390e183aee18394e18391, 'ka', 1083),
(1082, 0xe18390e1839ce18392e18390e183a0e18398e183a8e18398e183a120e18390e1839ce18392e18390e183a0e18398e183a8e18396e1839420e1839be18398e18391e1839be18390, 'ka', 1084),
(1083, 0xe183a3e18390e183a0e183a7e1839de183a4e18390, 'ka', 1085),
(1084, 0xe1839be18398e183a6e18394e18391e18390, 'ka', 1086),
(1085, 0xe18399e1839de1839ce183a4e18398e18392e183a3e183a0e18398e183a0e18394e18391e18390, 'ka', 1087),
(1086, 0xe18392e18390e183a8e18395e18394e18391e18390, 'ka', 1088),
(1087, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e18394e1839ae18397e1839020e1839be18390e183a0e18397e18395e18390, 'ka', 1089),
(1088, 0xe1839be1839de1839ce18390e183aae18394e1839be18394e18391e18398e183a120e18398e1839be1839ee1839de183a0e183a2e18398, 'ka', 1090),
(1089, 0xe18393e18390e18395e18390e1839ae18394e18391e18394e18391e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 1091),
(1090, 0xe18392e1839ae1839de18391e18390e1839ae183a3e183a0e1839820e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 1092),
(1091, 0xe18395e18390e1839ae183a3e183a2e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 1093),
(1092, 0xe18393e18394e18395e18394e1839ae1839de1839ee18394e183a0e18397e1839020e183aee18394e1839ae183a1e18390e183ace183a7e1839de18394e18391e18398e183a120e18392e18390e1839be1839de183a7e18394e1839ce18394e18391e18390, 'ka', 1094),
(1093, 0xe1839be1839de1839be183aee1839be18390e183a0e18394e18391e1839ae18398e183a120e18398e1839ce183a2e18394e183a0e183a4e18394e18398e183a1e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 1095),
(1094, 0xe18390e183a3e183a2e18394e1839ce183a2e18398e183a4e18398e18399e18390e183aae18398e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 1096),
(1095, 0xe18398e1839ce183a2e18394e18392e183a0e18390e183aae18398e18394e18391e18398e183a1e1839020e18393e1839020e18392e18390e183a4e18390e183a0e18397e1839de18394e18391e18394e18391e18398e183a120e1839be18390e183a0e18397e18395e18390, 'ka', 1097),
(1096, 0xe18397e18390e1839be18390e183a8e18394e18391e18398e183a120e18393e18390e183afe18398e1839ae183a4e1839de18394e18391e18394e18391e18398e183a120e18399e1839de1839ce183a4e18398e18392e183a3e183a0e18390e183aae18398e18390, 'ka', 1098),
(1097, 0xe18393e183a0e1839de18396e1839420e18393e18390e183a7e183a0e18393e1839ce1839de18391e18398e18397, 'ka', 1099),
(1098, 0x43617074636861202d20e183a120e18392e18390e18390e183a5e183a2e18398e183a3e183a0e18394e18391e18390, 'ka', 1100);

-- --------------------------------------------------------

--
-- Table structure for table `mission`
--

CREATE TABLE IF NOT EXISTS `mission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `duedatetime` datetime DEFAULT NULL,
  `latestdatetime` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `reward` text COLLATE utf8_unicode_ci,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `takenbyuser__user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mission`
--

INSERT INTO `mission` (`id`, `description`, `duedatetime`, `latestdatetime`, `status`, `reward`, `ownedsecurableitem_id`, `takenbyuser__user_id`) VALUES
(1, 'sdadadad', '2014-04-25 20:00:00', '2014-04-22 14:08:59', 1, 'asdasdasdas', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mission_read`
--

CREATE TABLE IF NOT EXISTS `mission_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `mission_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `mission_read`
--

INSERT INTO `mission_read` (`id`, `securableitem_id`, `munge_id`, `count`) VALUES
(1, 2, 'G2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modelcreationapisync`
--

CREATE TABLE IF NOT EXISTS `modelcreationapisync` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `servicename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `modelid` int(11) unsigned NOT NULL,
  `modelclassname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `createddatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `multiplevaluescustomfield`
--

CREATE TABLE IF NOT EXISTS `multiplevaluescustomfield` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `basecustomfield_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `namedsecurableitem`
--

CREATE TABLE IF NOT EXISTS `namedsecurableitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `securableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_eman` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `occurredondatetime` datetime DEFAULT NULL,
  `activity_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `note_read`
--

CREATE TABLE IF NOT EXISTS `note_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `note_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownerhasreadlatest` tinyint(1) unsigned DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `notificationmessage_id` int(11) unsigned DEFAULT NULL,
  `owner__user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `type`, `ownerhasreadlatest`, `item_id`, `notificationmessage_id`, `owner__user_id`) VALUES
(1, 'RemoveApiTestEntryScriptFile', NULL, 5, 1, 2),
(3, 'ClearAssetsFolder', NULL, 11, 2, 2),
(4, 'ClearAssetsFolder', NULL, 83, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notificationmessage`
--

CREATE TABLE IF NOT EXISTS `notificationmessage` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `htmlcontent` text COLLATE utf8_unicode_ci,
  `textcontent` text COLLATE utf8_unicode_ci,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `notificationmessage`
--

INSERT INTO `notificationmessage` (`id`, `htmlcontent`, `textcontent`, `item_id`) VALUES
(1, NULL, 'If this website is in production mode, please remove the app/test.php file.', 6),
(2, NULL, 'Please delete all files from assets folder on server.', 12),
(3, NULL, 'Please delete all files from assets folder on server.', 84);

-- --------------------------------------------------------

--
-- Table structure for table `notificationsubscriber`
--

CREATE TABLE IF NOT EXISTS `notificationsubscriber` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hasreadlatest` tinyint(1) unsigned DEFAULT NULL,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  `task_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opportunity`
--

CREATE TABLE IF NOT EXISTS `opportunity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `closedate` date DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `probability` tinyint(11) DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `account_id` int(11) unsigned DEFAULT NULL,
  `amount_currencyvalue_id` int(11) unsigned DEFAULT NULL,
  `stage_customfield_id` int(11) unsigned DEFAULT NULL,
  `source_customfield_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opportunitystarred`
--

CREATE TABLE IF NOT EXISTS `opportunitystarred` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `basestarredmodel_id` int(11) unsigned DEFAULT NULL,
  `opportunity_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `basestarredmodel_id_opportunity_id` (`basestarredmodel_id`,`opportunity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opportunity_project`
--

CREATE TABLE IF NOT EXISTS `opportunity_project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `opportunity_id` int(11) unsigned DEFAULT NULL,
  `project_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_tcejorp_di_ytinutroppo` (`opportunity_id`,`project_id`),
  KEY `di_ytinutroppo` (`opportunity_id`),
  KEY `di_tcejorp` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opportunity_read`
--

CREATE TABLE IF NOT EXISTS `opportunity_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `opportunity_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ownedsecurableitem`
--

CREATE TABLE IF NOT EXISTS `ownedsecurableitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned DEFAULT NULL,
  `owner__user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ownedsecurableitem`
--

INSERT INTO `ownedsecurableitem` (`id`, `securableitem_id`, `owner__user_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE IF NOT EXISTS `permission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `permissions` tinyint(11) DEFAULT NULL,
  `type` tinyint(11) DEFAULT NULL,
  `permitable_id` int(11) unsigned DEFAULT NULL,
  `securableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `permissions`, `type`, `permitable_id`, `securableitem_id`) VALUES
(1, 27, 1, 3, 2),
(2, 27, 1, 3, 3),
(3, 27, 1, 3, 4),
(4, 27, 1, 3, 5),
(5, 27, 1, 3, 6);

-- --------------------------------------------------------

--
-- Table structure for table `permitable`
--

CREATE TABLE IF NOT EXISTS `permitable` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `permitable`
--

INSERT INTO `permitable` (`id`, `item_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `department` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jobtitle` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobilephone` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `officephone` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `officefax` varchar(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `primaryaddress_address_id` int(11) unsigned DEFAULT NULL,
  `primaryemail_email_id` int(11) unsigned DEFAULT NULL,
  `title_customfield_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `department`, `firstname`, `jobtitle`, `lastname`, `mobilephone`, `officephone`, `officefax`, `ownedsecurableitem_id`, `primaryaddress_address_id`, `primaryemail_email_id`, `title_customfield_id`) VALUES
(1, '', 'Super', '', 'User', '', '', NULL, NULL, 1, 1, 1),
(2, NULL, 'System', NULL, 'User', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '', '', '', 'cvxcvxcvxc', '', '', '', 3, 2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `personwhohavenotreadlatest`
--

CREATE TABLE IF NOT EXISTS `personwhohavenotreadlatest` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_item_id` int(11) unsigned DEFAULT NULL,
  `mission_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `personwhohavenotreadlatest`
--

INSERT INTO `personwhohavenotreadlatest` (`id`, `person_item_id`, `mission_id`) VALUES
(1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `perusermetadata`
--

CREATE TABLE IF NOT EXISTS `perusermetadata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `classname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serializedmetadata` text COLLATE utf8_unicode_ci,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `perusermetadata`
--

INSERT INTO `perusermetadata` (`id`, `classname`, `serializedmetadata`, `_user_id`) VALUES
(1, 'UsersModule', 'a:1:{s:17:"timeZoneConfirmed";b:1;}', 1),
(2, 'ActivitiesModule', 'a:5:{s:15:"2_ownedByFilter";s:4:"user";s:21:"2_filteredByModelName";s:12:"EmailMessage";s:9:"15_rollup";s:1:"1";s:16:"15_ownedByFilter";s:3:"all";s:22:"15_filteredByModelName";s:3:"all";}', 1),
(3, 'EzcodxModule', 'a:1:{s:14:"recentlyViewed";s:427:"a:6:{i:0;a:3:{i:0;s:11:"UsersModule";i:1;i:1;i:2;s:10:"Super User";}i:1;a:3:{i:0;s:14:"AccountsModule";i:1;i:3;i:2;s:19:"fdsfsfdsfsdghsdgdsg";}i:2;a:3:{i:0;s:14:"AccountsModule";i:1;i:2;i:2;s:12:"fdsfsfdsfsdf";}i:3;a:3:{i:0;s:14:"AccountsModule";i:1;i:1;i:2;s:33:"ეწფსდფსდფდს";}i:4;a:3:{i:0;s:11:"LeadsModule";i:1;i:1;i:2;s:10:"cvxcvxcvxc";}i:5;a:3:{i:0;s:14:"MissionsModule";i:1;i:1;i:2;s:8:"sdadadad";}}";}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `policy`
--

CREATE TABLE IF NOT EXISTS `policy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modulename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permitable_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `portlet`
--

CREATE TABLE IF NOT EXISTS `portlet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `column` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `layoutid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `viewtype` text COLLATE utf8_unicode_ci,
  `serializedviewdata` text COLLATE utf8_unicode_ci,
  `collapsed` tinyint(1) unsigned DEFAULT NULL,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `portlet`
--

INSERT INTO `portlet` (`id`, `column`, `position`, `layoutid`, `viewtype`, `serializedviewdata`, `collapsed`, `_user_id`) VALUES
(1, 2, 1, 'HomeDashboard1', 'MyUpcomingMeetingsCalendar', NULL, 0, 1),
(2, 1, 3, 'HomeDashboard1', 'AllLatestActivitiesForPortlet', NULL, 0, 1),
(3, 1, 4, 'HomeDashboard1', 'TasksMyList', NULL, 0, 1),
(4, 2, 3, 'HomeDashboard1', 'OpportunitiesBySourceChart', 'a:2:{s:5:"title";s:54:"&lt;style&gt;#corp-logo {display: none;}&lt;/style&gt;";s:4:"type";s:5:"Pie3D";}', 0, 1),
(5, 2, 2, 'HomeDashboard1', 'AllSocialItemsForPortlet', NULL, 0, 1),
(6, 1, 2, 'HomeDashboard1', 'MyMissionsForPortlet', NULL, 0, 1),
(7, 1, 1, 'HomeDashboard1', 'OpportunitiesByStageChart', 'a:2:{s:5:"title";s:45:"&lt;script&gt;alert(\\''hello\\'')&lt;/script&gt;";s:4:"type";s:8:"Column2D";}', 0, 1),
(8, 1, 5, 'HomeDashboard1', 'ContactsMyList', NULL, 0, 1),
(9, 1, 1, 'UserDetailsAndRelationsViewLeftBottomView', 'UserSocialItemsForPortlet', NULL, 0, 1),
(10, 1, 1, 'ProjectsDashboard', 'ActiveProjectsPortlet', NULL, 0, 1),
(11, 2, 1, 'ProjectsDashboard', 'ProjectsActivityFeedPortlet', NULL, 0, 1),
(12, 1, 1, 'MarketingDashboard', 'MarketingOverallMetrics', NULL, 0, 1),
(13, 1, 1, 'LeadDetailsAndRelationsView', 'LeadDetailsPortlet', NULL, 0, 1),
(14, 1, 2, 'LeadDetailsAndRelationsView', 'NoteInlineEditForPortlet', NULL, 0, 1),
(15, 1, 3, 'LeadDetailsAndRelationsView', 'ContactLatestActivitiesForPortlet', NULL, 0, 1),
(16, 2, 1, 'LeadDetailsAndRelationsView', 'UpcomingMeetingsForContactCalendar', NULL, 0, 1),
(17, 2, 2, 'LeadDetailsAndRelationsView', 'OpenTasksForContactRelatedList', NULL, 0, 1),
(18, 1, 1, 'AccountDetailsAndRelationsView', 'AccountDetailsPortlet', NULL, 0, 1),
(19, 1, 2, 'AccountDetailsAndRelationsView', 'NoteInlineEditForPortlet', NULL, 0, 1),
(20, 1, 3, 'AccountDetailsAndRelationsView', 'AccountLatestActivitiesForPortlet', NULL, 0, 1),
(21, 2, 1, 'AccountDetailsAndRelationsView', 'UpcomingMeetingsForAccountCalendar', NULL, 0, 1),
(22, 2, 2, 'AccountDetailsAndRelationsView', 'OpenTasksForAccountRelatedList', NULL, 0, 1),
(23, 2, 3, 'AccountDetailsAndRelationsView', 'ContactsForAccountRelatedList', NULL, 0, 1),
(24, 2, 4, 'AccountDetailsAndRelationsView', 'OpportunitiesForAccountRelatedList', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `quantity` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `pricefrequency` int(11) DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `account_id` int(11) unsigned DEFAULT NULL,
  `contact_id` int(11) unsigned DEFAULT NULL,
  `opportunity_id` int(11) unsigned DEFAULT NULL,
  `producttemplate_id` int(11) unsigned DEFAULT NULL,
  `stage_customfield_id` int(11) unsigned DEFAULT NULL,
  `sellprice_currencyvalue_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productcatalog`
--

CREATE TABLE IF NOT EXISTS `productcatalog` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productcatalog_productcategory`
--

CREATE TABLE IF NOT EXISTS `productcatalog_productcategory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `productcatalog_id` int(11) unsigned DEFAULT NULL,
  `productcategory_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_yrogetactcudorp_di_golatactcudorp` (`productcatalog_id`,`productcategory_id`),
  KEY `di_golatactcudorp` (`productcatalog_id`),
  KEY `di_yrogetactcudorp` (`productcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productcategory`
--

CREATE TABLE IF NOT EXISTS `productcategory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `productcategory_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productcategory_producttemplate`
--

CREATE TABLE IF NOT EXISTS `productcategory_producttemplate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `productcategory_id` int(11) unsigned DEFAULT NULL,
  `producttemplate_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_etalpmettcudorp_di_yrogetactcudorp` (`productcategory_id`,`producttemplate_id`),
  KEY `di_yrogetactcudorp` (`productcategory_id`),
  KEY `di_etalpmettcudorp` (`producttemplate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `producttemplate`
--

CREATE TABLE IF NOT EXISTS `producttemplate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `pricefrequency` int(11) DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `sellpriceformula_id` int(11) unsigned DEFAULT NULL,
  `cost_currencyvalue_id` int(11) unsigned DEFAULT NULL,
  `listprice_currencyvalue_id` int(11) unsigned DEFAULT NULL,
  `sellprice_currencyvalue_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_productcategory`
--

CREATE TABLE IF NOT EXISTS `product_productcategory` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned DEFAULT NULL,
  `productcategory_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_yrogetactcudorp_di_tcudorp` (`product_id`,`productcategory_id`),
  KEY `di_tcudorp` (`product_id`),
  KEY `di_yrogetactcudorp` (`productcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_read`
--

CREATE TABLE IF NOT EXISTS `product_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `product_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `projectauditevent`
--

CREATE TABLE IF NOT EXISTS `projectauditevent` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `eventname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `_user_id` int(11) unsigned DEFAULT NULL,
  `project_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_read`
--

CREATE TABLE IF NOT EXISTS `project_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `project_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_eman` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `savedreport`
--

CREATE TABLE IF NOT EXISTS `savedreport` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `moduleclassname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `type` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `savedreport_read`
--

CREATE TABLE IF NOT EXISTS `savedreport_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `savedreport_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `savedsearch`
--

CREATE TABLE IF NOT EXISTS `savedsearch` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `viewclassname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `savedworkflow`
--

CREATE TABLE IF NOT EXISTS `savedworkflow` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `isactive` tinyint(1) unsigned DEFAULT NULL,
  `moduleclassname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `type` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `triggeron` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `securableitem`
--

CREATE TABLE IF NOT EXISTS `securableitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `securableitem`
--

INSERT INTO `securableitem` (`id`, `item_id`) VALUES
(1, 14),
(2, 61),
(3, 65),
(4, 70),
(5, 75),
(6, 76),
(7, 79);

-- --------------------------------------------------------

--
-- Table structure for table `sellpriceformula`
--

CREATE TABLE IF NOT EXISTS `sellpriceformula` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `discountormarkuppercentage` double DEFAULT NULL,
  `producttemplate_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shorturl`
--

CREATE TABLE IF NOT EXISTS `shorturl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `createddatetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `socialitem`
--

CREATE TABLE IF NOT EXISTS `socialitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `latestdatetime` datetime DEFAULT NULL,
  `ownedsecurableitem_id` int(11) unsigned DEFAULT NULL,
  `note_id` int(11) unsigned DEFAULT NULL,
  `touser__user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `socialitem_read`
--

CREATE TABLE IF NOT EXISTS `socialitem_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `socialitem_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stuckjob`
--

CREATE TABLE IF NOT EXISTS `stuckjob` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `completeddatetime` datetime DEFAULT NULL,
  `completed` tinyint(1) unsigned DEFAULT NULL,
  `duedatetime` datetime DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `activity_id` int(11) unsigned DEFAULT NULL,
  `requestedbyuser__user_id` int(11) unsigned DEFAULT NULL,
  `project_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `taskchecklistitem`
--

CREATE TABLE IF NOT EXISTS `taskchecklistitem` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci,
  `completed` tinyint(1) unsigned DEFAULT NULL,
  `task_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_read`
--

CREATE TABLE IF NOT EXISTS `task_read` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `securableitem_id` int(11) unsigned NOT NULL,
  `munge_id` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `securableitem_id_munge_id` (`securableitem_id`,`munge_id`),
  KEY `task_read_securableitem_id` (`securableitem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `task_read_subscription`
--

CREATE TABLE IF NOT EXISTS `task_read_subscription` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `modelid` int(11) unsigned NOT NULL,
  `modifieddatetime` datetime DEFAULT NULL,
  `subscriptiontype` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userid_modelid` (`userid`,`modelid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `workflowmessageinqueue`
--

CREATE TABLE IF NOT EXISTS `workflowmessageinqueue` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modelclassname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `processdatetime` datetime DEFAULT NULL,
  `serializeddata` text COLLATE utf8_unicode_ci,
  `item_id` int(11) unsigned DEFAULT NULL,
  `modelitem_item_id` int(11) unsigned DEFAULT NULL,
  `savedworkflow_id` int(11) unsigned DEFAULT NULL,
  `triggeredbyuser__user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_group`
--

CREATE TABLE IF NOT EXISTS `_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permitable_id` int(11) unsigned DEFAULT NULL,
  `_group_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_eman` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_group`
--

INSERT INTO `_group` (`id`, `name`, `permitable_id`, `_group_id`) VALUES
(1, 'Super Administrators', 2, NULL),
(2, 'Everyone', 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `_group__user`
--

CREATE TABLE IF NOT EXISTS `_group__user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `_group_id` int(11) unsigned DEFAULT NULL,
  `_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_di_resu__di_puorg_` (`_group_id`,`_user_id`),
  KEY `di_puorg_` (`_group_id`),
  KEY `di_resu_` (`_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_group__user`
--

INSERT INTO `_group__user` (`id`, `_group_id`, `_user_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `_right`
--

CREATE TABLE IF NOT EXISTS `_right` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `modulename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(11) DEFAULT NULL,
  `permitable_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=65 ;

--
-- Dumping data for table `_right`
--

INSERT INTO `_right` (`id`, `modulename`, `name`, `type`, `permitable_id`) VALUES
(1, 'UsersModule', 'Login Via Web', 1, 3),
(2, 'UsersModule', 'Login Via Mobile', 1, 3),
(3, 'UsersModule', 'Login Via Web API', 1, 3),
(4, 'AccountsModule', 'Access Accounts Tab', 1, 3),
(5, 'AccountsModule', 'Create Accounts', 1, 3),
(6, 'AccountsModule', 'Delete Accounts', 1, 3),
(7, 'CampaignsModule', 'Access Campaigns Tab', 1, 3),
(8, 'CampaignsModule', 'Create Campaigns', 1, 3),
(9, 'CampaignsModule', 'Delete Campaigns', 1, 3),
(10, 'ContactsModule', 'Access Contacts Tab', 1, 3),
(11, 'ContactsModule', 'Create Contacts', 1, 3),
(12, 'ContactsModule', 'Delete Contacts', 1, 3),
(13, 'ConversationsModule', 'Access Conversations Tab', 1, 3),
(14, 'ConversationsModule', 'Create Conversations', 1, 3),
(15, 'ConversationsModule', 'Delete Conversations', 1, 3),
(16, 'EmailMessagesModule', 'Access Emails Tab', 1, 3),
(17, 'EmailMessagesModule', 'Create Emails', 1, 3),
(18, 'EmailMessagesModule', 'Delete Emails', 1, 3),
(19, 'EmailTemplatesModule', 'Access Email Templates', 1, 3),
(20, 'EmailTemplatesModule', 'Create Email Templates', 1, 3),
(21, 'EmailTemplatesModule', 'Delete Email Templates', 1, 3),
(22, 'LeadsModule', 'Access Leads Tab', 1, 3),
(23, 'LeadsModule', 'Create Leads', 1, 3),
(24, 'LeadsModule', 'Delete Leads', 1, 3),
(25, 'LeadsModule', 'Convert Leads', 1, 3),
(26, 'OpportunitiesModule', 'Access Opportunities Tab', 1, 3),
(27, 'OpportunitiesModule', 'Create Opportunities', 1, 3),
(28, 'OpportunitiesModule', 'Delete Opportunities', 1, 3),
(29, 'MarketingModule', 'Access Marketing Tab', 1, 3),
(30, 'MarketingListsModule', 'Access Marketing Lists Tab', 1, 3),
(31, 'MarketingListsModule', 'Create Marketing Lists', 1, 3),
(32, 'MarketingListsModule', 'Delete Marketing Lists', 1, 3),
(33, 'MeetingsModule', 'Access Meetings', 1, 3),
(34, 'MeetingsModule', 'Create Meetings', 1, 3),
(35, 'MeetingsModule', 'Delete Meetings', 1, 3),
(36, 'MissionsModule', 'Access Missions Tab', 1, 3),
(37, 'MissionsModule', 'Create Missions', 1, 3),
(38, 'MissionsModule', 'Delete Missions', 1, 3),
(39, 'NotesModule', 'Access Notes', 1, 3),
(40, 'NotesModule', 'Create Notes', 1, 3),
(41, 'NotesModule', 'Delete Notes', 1, 3),
(42, 'ReportsModule', 'Access Reports Tab', 1, 3),
(43, 'ReportsModule', 'Create Reports', 1, 3),
(44, 'ReportsModule', 'Delete Reports', 1, 3),
(45, 'TasksModule', 'Access Tasks', 1, 3),
(46, 'TasksModule', 'Create Tasks', 1, 3),
(47, 'TasksModule', 'Delete Tasks', 1, 3),
(48, 'HomeModule', 'Access Dashboards', 1, 3),
(49, 'HomeModule', 'Create Dashboards', 1, 3),
(50, 'HomeModule', 'Delete Dashboards', 1, 3),
(51, 'ExportModule', 'Access Export Tool', 1, 3),
(52, 'SocialItemsModule', 'Access Social Items', 1, 3),
(53, 'ProductsModule', 'Access Products Tab', 1, 3),
(54, 'ProductsModule', 'Create Products', 1, 3),
(55, 'ProductsModule', 'Delete Products', 1, 3),
(56, 'ProductTemplatesModule', 'Access Catalog Items Tab', 1, 3),
(57, 'ProductTemplatesModule', 'Create Catalog Items', 1, 3),
(58, 'ProductTemplatesModule', 'Delete Catalog Items', 1, 3),
(59, 'ProjectsModule', 'Access Projects Tab', 1, 3),
(60, 'ProjectsModule', 'Create Projects', 1, 3),
(61, 'ProjectsModule', 'Delete Projects', 1, 3),
(62, 'UsersModule', 'Login Via Mobile', 2, 4),
(63, 'UsersModule', 'Login Via Web', 2, 4),
(64, 'UsersModule', 'Login Via Web API', 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `_user`
--

CREATE TABLE IF NOT EXISTS `_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serializedavatardata` text COLLATE utf8_unicode_ci,
  `isactive` tinyint(1) unsigned DEFAULT NULL,
  `isrootuser` tinyint(1) unsigned DEFAULT NULL,
  `hidefromselecting` tinyint(1) unsigned DEFAULT NULL,
  `issystemuser` tinyint(1) unsigned DEFAULT NULL,
  `hidefromleaderboard` tinyint(1) unsigned DEFAULT NULL,
  `lastlogindatetime` datetime DEFAULT NULL,
  `permitable_id` int(11) unsigned DEFAULT NULL,
  `person_id` int(11) unsigned DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `manager__user_id` int(11) unsigned DEFAULT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_emanresu` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_user`
--

INSERT INTO `_user` (`id`, `hash`, `language`, `locale`, `timezone`, `username`, `serializedavatardata`, `isactive`, `isrootuser`, `hidefromselecting`, `issystemuser`, `hidefromleaderboard`, `lastlogindatetime`, `permitable_id`, `person_id`, `currency_id`, `manager__user_id`, `role_id`) VALUES
(1, '5f4dcc3b5aa765d61d8327deb882cf99', 'ka', '', 'Asia/Tbilisi', 'super', NULL, 1, NULL, NULL, NULL, NULL, '2014-04-25 15:13:08', 1, 1, NULL, NULL, NULL),
(2, '23ee5987dda16f2384d7b8502c5fcf80', NULL, NULL, 'America/Chicago', 'backendjoboractionuser', NULL, 0, NULL, 1, 1, 1, NULL, 4, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `__role_children_cache`
--

CREATE TABLE IF NOT EXISTS `__role_children_cache` (
  `permitable_id` int(11) NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permitable_id`,`role_id`),
  UNIQUE KEY `permitable_id` (`permitable_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
